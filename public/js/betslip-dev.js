function refreshSlip() {
    $.post("betslip", {}, function (data) {
        $("#betslip").animate({ opacity: '0.8' });
        $("#betslip").html(data);
        $("#betslip").animate({ opacity: '1' });
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function refreshJackSlip() {
    $.post("/lite/matches/jackpot", {}, function (data) {
        $("#betslipJ").animate({ opacity: '0.8' });
        $("#betslipJ").html(data);
        $("#betslipJ").animate({ opacity: '1' });
    }).done(function () {
        $(".loader").css("display", "none");
    });
}


function refreshBingwaFour() {
    $.post("/lite/betslip/bingwa", {}, function (data) {
        $("#betslipB").animate({ opacity: '0.8' });
        $("#betslipB").html(data);
        $("#betslipB").animate({ opacity: '1' });
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function addBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away,
    odd, oddtype, parentmatchid, oddsid) {
    var self = this;
    if ($('.' + custom + ' button').hasClass('picked')) {
        var counterHolder = $(".slip-counter"),
            count = counterHolder.html() * 1;
        counterHolder.html(--count);
        return removeMatch(value);
    }
    $(".loader").slideDown("slow");
    $.post("/lite/betslip/add", {
        match_id: value,
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        oddsid: oddsid
    }, function (data) {

        console.log(data)
        if (data.status == 410) {

            console.log(data.message);
            $("#betslip").html(data.message);
            return;
        }
        $("." + value + ' button').removeClass('picked');
        $("." + value + " .odd td").removeClass("odd-pd-picked");
        $(self).addClass('picked');
        $("." + custom + ' button').addClass('picked');
        $("." + custom + " .odd td").addClass("odd-pd-picked");
        refreshSlip();
        $(".slip-counter").html(data.total);
        if (data.total > 1) {
            $("#multibetBoost").show();
        }
    });
}

function addJPBetAjax(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away,
    odd, oddtype, parentmatchid, oddsid, jackpot_event_id) {
    var self = this;
    if ($('.' + custom + ' button').hasClass('picked')) {
        var counterHolder = $("#selections"),
            count = counterHolder.html() * 1;
        if (count > 0) {
            counterHolder.html(--count);
        }
        return removeJPMatch(value, jackpot_event_id);
    }
    $(".loader").slideDown("slow");
    $.post("/lite/betslip/addjp", {
        sub_type_id: sub_type_id,
        odd_key: odd_key,
        custom: custom,
        special_bet_value: special_bet_value,
        bet_type: bet_type,
        home: home,
        away: away,
        odd: odd,
        oddtype: oddtype,
        parentmatchid: parentmatchid,
        oddsid: oddsid,
        jackpot_event_id: jackpot_event_id
    }, function (data) {

        $("." + value + ' button').removeClass('picked');
        $("." + value + " .odd td").removeClass("odd-pd-picked");

        console.log("Status is " + data.status);

        $(self).addClass('picked');
        $("." + custom + ' button').addClass('picked');
        $("." + custom + " .odd td").addClass("odd-pd-picked");

        $("#selections").html(data.total);

        if (data.total == data.total_jp_games) {
            document.getElementById("jackpot_bet_bet").style.background = '#01B601';
            document.getElementById("jackpot_bet_bet").style.border = '1px solid #01B601';
            document.getElementById('jackpot_bet_bet').disabled = false;
            document.getElementById('jackpot_bet_bet').innerText = "Place Jackpot Bet";
            document.getElementById('pick_for_me').classList.add("picked");
        } else {
            document.getElementById("jackpot_bet_bet").style.background = '#616161';
            document.getElementById("jackpot_bet_bet").style.border = '1px solid #616161';
            document.getElementById('jackpot_bet_bet').disabled = true;
            document.getElementById('jackpot_bet_bet').innerText = "Pick " + data.total_jp_games + " selections to place bet";
            document.getElementById('pick_for_me').classList.remove('picked');
        }
    });
}

function addJPBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away,
    odd, oddtype, parentmatchid, oddsid, jackpot_event_id) {

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        console.log("State " + this.readyState + " Status " + this.status)
        console.log(this);
        if (this.readyState == 4 && this.status == 201) {

            $("." + value + ' button').removeClass('picked');
            $("." + value + " .odd td").removeClass("odd-pd-picked");

            let response = this.response;
            let jsonResponse = JSON.parse(response);
            console.log(response);

            let customs = jsonResponse.selectedCustoms;
            console.log(customs)

            if (customs != null) {
                customs = customs.split(",");
            }

            if (jsonResponse.selectionRemoved == 0) {
                customs.forEach(custom => {
                    $("." + custom + ' button').addClass('picked');
                    $("." + custom + " .odd td").addClass("odd-pd-picked");
                });
            }

            if (jsonResponse.errors == 1) {

                alert(jsonResponse.errorMessage)
            }

            document.getElementById("selections").innerHTML = jsonResponse.total + " of " + jsonResponse.total_jp_games;
            document.getElementById('bet_amount').innerHTML = "KSH " + jsonResponse.jpBetAmount;
            document.getElementById('total_price').innerHTML = "KSH " + jsonResponse.jpBetAmount;

            if (jsonResponse.total == jsonResponse.total_jp_games) {
                document.getElementById("jackpot_bet_bet").style.background = '#01B601';
                document.getElementById("jackpot_bet_bet").style.border = '1px solid #01B601';
                document.getElementById('jackpot_bet_bet').disabled = false;
                document.getElementById('jackpot_bet_bet').innerText = "Place Jackpot Bet";
                document.getElementById('pick_for_me').classList.add("picked");
            } else {
                document.getElementById("jackpot_bet_bet").style.background = '#616161';
                document.getElementById("jackpot_bet_bet").style.border = '1px solid #616161';
                document.getElementById('jackpot_bet_bet').disabled = true;
                document.getElementById('jackpot_bet_bet').innerText = "Pick " + jsonResponse.total_jp_games + " selections to place bet";
                document.getElementById('pick_for_me').classList.remove('picked');
            }
        }
    };
    xhttp.open("POST", "/lite/betslip/addjp", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("sub_type_id=" + sub_type_id + "&odd_key=" + odd_key + "&custom=" + custom + "&special_bet_value=" + special_bet_value + "&bet_type=" + bet_type + "&home=" + home + "&away=" + away + "&odd=" + odd + "&oddtype=" + oddtype + "&parentmatchid=" + parentmatchid + "&oddsid=" + oddsid + "&jackpot_event_id=" + jackpot_event_id);
}

function removeMatch(value) {
    $(".loader").slideDown("slow");
    $.post("/lite/betslip/remove", { match_id: value }, function (data) {
        refreshSlip();
        $("." + value + " .odd td").removeClass("odd-pd-picked");
        $("." + value + " button").removeClass('picked');
    });
}

function removeJPMatchAjax(value, jackpot_event_id) {
    $(".loader").slideDown("slow");
    $.post("/lite/betslip/removejp", { parent_match_id: value, jackpot_event_id: jackpot_event_id }, function (data) {
        $("." + value + " .odd td").removeClass("odd-pd-picked");
        $("." + value + " button").removeClass('picked');
        console.log(data)
        document.getElementById("jackpot_bet_bet").style.background = '#616161';
        document.getElementById("jackpot_bet_bet").style.border = '1px solid #616161';
        document.getElementById('jackpot_bet_bet').innerText = "Pick " + data.total_jp_games + " selections to place bet";
        document.getElementById('jackpot_bet_bet').disabled = true;
        document.getElementById('pick_for_me').classList.remove('picked');
    });
}

function removeJPMatch(value, jackpot_event_id) {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        console.log("State " + this.readyState + " Status " + this.status)
        console.log(this);
        if (this.readyState == 4 && this.status == 201) {

            var response = this.response;
            var jsonResponse = JSON.parse(response);

            $("." + value + " .odd td").removeClass("odd-pd-picked");
            $("." + value + " button").removeClass('picked');
            document.getElementById("jackpot_bet_bet").style.background = '#616161';
            document.getElementById("jackpot_bet_bet").style.border = '1px solid #616161';
            document.getElementById('jackpot_bet_bet').innerText = "Pick " + jsonResponse.total_jp_games + " selections to place bet";
            document.getElementById('jackpot_bet_bet').disabled = true;
            document.getElementById('pick_for_me').classList.remove('picked');
        }
    };

    xhttp.open("POST", "/lite/betslip/removejp", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("parent_match_id=" + value + "&jackpot_event_id=" + jackpot_event_id)
}

function clearSlip(value) {
    $(".loader").slideDown("slow");
    $.post("/lite/betslip/clearslip", {}, function (data) {
        refreshSlip();
        $(".picked").removeClass('picked');
        $(".odd-pd-picked").removeClass('picked');
    });
}