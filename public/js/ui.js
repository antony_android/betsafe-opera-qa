for (let el of document.getElementsByClassName('opened')) el.classList.add("hide-item");
for (let el of document.getElementsByClassName('leagues-menu')) el.classList.add("hide-item");

function showHideNavigation() {

    if (document.getElementById('navigation').style.display == 'none') {
        document.getElementById('navigation').style.display = '';
        document.getElementById('contents').style.display = 'none';
        document.getElementById('brand-row').style.display = 'none';
        document.getElementById('profile_menu').style.display = 'none';

    } else {
        document.getElementById('brand-row').style.display = '';
        document.getElementById('contents').style.display = '';
        document.getElementById('profile_menu').style.display = 'none';
        document.getElementById('navigation').style.display = 'none';
    }
}

function showHideProfileNavigation() {
    if (document.getElementById('profile_menu').style.display == 'none') {
        document.getElementById('brand-row').style.display = 'none';
        document.getElementById('profile_menu').style.display = '';
        document.getElementById('contents').style.display = 'none';
        document.getElementById('navigation').style.display = 'none';
    } else {
        document.getElementById('brand-row').style.display = '';
        document.getElementById('contents').style.display = '';
        document.getElementById('profile_menu').style.display = 'none';
        document.getElementById('navigation').style.display = 'none';
    }
}

function toggleSearch() {
    if (document.getElementById('search-row').style.display == 'none') {
        document.getElementById('search-row').style.display = '';
    } else {
        document.getElementById('search-row').style.display = 'none';
    }
}

function showLeagues(e) {

    var rel = e.getAttribute("rel");

    if (document.getElementById('country-' + rel).classList.contains("hide-item")) {

        document.getElementById('union-down-' + rel).classList.add("hide-item");
        document.getElementById('union-up-' + rel).classList.remove("hide-item");
        document.getElementById('country-' + rel).classList.remove("hide-item");
    } else {
        document.getElementById('union-up-' + rel).classList.add("hide-item");
        document.getElementById('union-down-' + rel).classList.remove("hide-item");
        document.getElementById('country-' + rel).classList.add("hide-item");
    }

}

function togglePwdVisibility(passwordFieldId) {
    if (!window.operamini) {
        var field = document.getElementById(passwordFieldId);
        if (field.type === "password") {
            field.type = "text";
        } else {
            field.type = "password";
        }
    } else {
        $.post('/lite/my-profile/showpass', function (data) {
            data = JSON.parse(data)
            console.log(data.showpass)
        });
    }
}

function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

// When the user scrolls the page, execute stickHeader
window.onscroll = function () { stickHeader() };

// Get the header
var header = document.getElementById("overall-header");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function stickHeader() {
    if (window.pageYOffset > sticky && document.getElementById('navigation').style.display == 'none' && document.getElementById('profile_menu').style.display == 'none') {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}