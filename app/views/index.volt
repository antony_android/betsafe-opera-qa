<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<!-- this file -->
{{ partial('partials/head') }}

<body class="main-body-container-">
  <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRG4V83" height="0" width="0"
      style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <style>
    /* The Marketing Modal */
    .modal {
      position: fixed;
      z-index: 1;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 80%;
      height: 202px;
      overflow: hidden;
      background-color: rgb(0, 0, 0);
      background-color: rgba(0, 0, 0, 0.4);
    }

    .modal-content {
      background-color: #00B700;
      margin: 0;
      border: none;
      width: 100%;
      height: 67%;
      padding: 0px;
    }

    .close {
      color: rgba(255, 255, 255, 0.87);
      float: right;
      font-size: 18px;
      height: 32px;
      padding: 5px 2px;
      box-sizing: border-box;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: black;
      text-decoration: none;
      cursor: pointer;
    }
  </style>

  <table class="full-width">
    <?php if(isset($selected) && $selected != 'betslip'): ?>
    <tr>
      <td class="brand">
        {{ partial('partials/mpesa-paybill') }}
      </td>
    </tr>
  </table>
  <div id="overall-header">
    <table class="full-width">
      <tr class="header-row">
        <td class="header-module std-pd">
          {{ partial('partials/nav-header') }}
        </td>
      </tr>
      <script>
        if (window.operamini) {
          $.post('/lite/my-profile/useragent', function (data) {
            console.log("user agent set")
          });
        } else {
          $.post('/lite/my-profile/unsetagent', function (data) {
            console.log("user agent unset")
          });
        }
      </script>
      {% if session.get('user-agent') != 'OperaMiniExtreme' %}
      <tr id='navigation' style="display: none;">
        <td> {{ partial('menu/index') }} </td>
      </tr>
      <tr id="profile_menu" style="display: none;">
        <td>
          {{ partial('profile/index') }}
        </td>
      </tr>
      {% endif %}
      <?php endif; ?>
      <tr>
        <td>
          <?php if(isset($selected) && $selected != 'betslip'): ?>
          {{ partial('partials/header') }}
          <?php endif; ?>
        </td>
      </tr>
    </table>
  </div>
  <style>
    .blocked {
      font-weight: 400;
      font-size: 12px;
      line-height: 18px;
      color: rgba(255, 255, 255, 0.87);
      padding-top: 8px !important;
    }
  </style>
  <table id="contents" class="full-width">
    <!-- <tr>
      <td class="std-pd blocked">
        Our Paybill numbers are currently not accepting deposits. We are working to have them restored as soon as
        possible and assure you that your balances are safe. We apologise for the inconvenience and thank you for your
        patience- Betsafe Customer Support
      </td>
    </tr> -->
    <tr>
      <td>
        {{ content() }}
      </td>
    </tr>
  </table>
  <table id="footer-table" class="full-width">
    <tr>
      <td>
        <?php if(isset($selected) && $selected != 'betslip'): ?>
        {{ partial('partials/footer') }}
        <?php endif; ?>
        {{ partial('partials/scripts') }}
      </td>
    </tr>
  </table>
  <script> window._peq = window._peq || []; window._peq.push(["init"]); </script>
  <script src="https://clientcdn.pushengage.com/core/fa3e71b8-59cd-4f0f-bced-11f7ddf306b5.js" async></script>
  <?php if(isset($registeredNumber) && !is_null($registeredNumber)): ?>
  <script>
    var phone = <?= $registeredNumber; ?>;
    const dataLayer = window['dataLayer'] || [];
    dataLayer.push({
      event: "registration",
      phone_number: phone,
      action: "registration"
    })
  </script>
  <?php elseif(isset($justLoggedInUserNumber) && !is_null($justLoggedInUserNumber)): ?>
  <script>
    var phone = <?= $justLoggedInUserNumber; ?>;
    const dataLayer = window['dataLayer'] || [];
    dataLayer.push({
      event: "login",
      phone_number: phone,
      action: "login"
    })
  </script>
  <?php elseif(isset($successfulBetPlacement) && !is_null($successfulBetPlacement)): ?>
  <script>
    var phone = <?= $successfulBetPlacement; ?>;
    const dataLayer = window['dataLayer'] || [];
    dataLayer.push({
      event: "betplaced",
      phone_number: phone,
      action: "betplaced"
    })
  </script>
  <?php elseif(isset($initiatedDeposit) && !is_null($initiatedDeposit)): ?>
  <script>
    var phone = <?= $initiatedDeposit; ?>;
    const dataLayer = window['dataLayer'] || [];
    dataLayer.push({
      event: "deposit",
      phone_number: phone,
      action: "deposit"
    })
  </script>
  <?php elseif(isset($initiatedWithdrawal) && !is_null($initiatedWithdrawal)): ?>
  <script>
    var phone = <?= $initiatedWithdrawal; ?>;
    const dataLayer = window['dataLayer'] || [];
    dataLayer.push({
      event: "withdrawal",
      phone_number: phone,
      action: "withdrawal"
    })
  </script>
  <?php endif; ?>

  <script
    src="https://my.rtmark.net/p.js?f=sync&lr=1&partner=cc4fda8f96c2d1d7871b05355cc6a9d26c68b163b27c5101cd86d6b98e83a3da"
    defer></script>
  <noscript><img
      src="https://my.rtmark.net/img.gif?f=sync&lr=1&partner=cc4fda8f96c2d1d7871b05355cc6a9d26c68b163b27c5101cd86d6b98e83a3da"
      width="1" height="1" /></noscript>

</body>

</html>