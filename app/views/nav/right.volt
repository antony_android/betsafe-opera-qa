<table id="main">
    <tr>
        <td>
            {% if session.get('auth') == null %}
            {{ partial('partials/right-menu') }}
            {% else %}
            {{ partial('partials/profile-menu') }}
            {% endif %}
        </td>
    </tr>
</table>