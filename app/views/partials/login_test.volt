<table class="login" cellpadding="0" cellspacing="0">
    <tr class="login-row">
        <td class="game-title">
            Log In
        </td>
    </tr>
    <th>{{ this.flashSession.output() }}</th>
    <tr>
        <td>
            <?php echo $this->tag->form("/login/authenticate"); ?>
            <label class="opera-label">
                Your Mobile Number
            </label>
            <input type="text" name="mobile" id="mobileNo" onkeyup="refreshLoginButt()" tabindex="10"
                class="opera-input" style="margin-bottom: 16px;" />
            <label for="password" class="opera-label">
                Password <span class="show-pwd" onclick="togglePwdVisibility('loginPassword')">Show</span>
            </label>
            <input type="password" name="password" id="loginPassword" onkeyup="refreshLoginButt()" class="opera-input">
            <button type="submit" id="loginButt" tabindex="12" class="opera-butt">
                Login
            </button>

            <a href="resetpassword" tabindex="13" style="margin-bottom: 16px;display: block;text-align: center;">
                <?php echo $t->_('Forgot Password'); ?>?
            </a>
            </form>
        </td>
    </tr>
</table>

<script>

    function refreshLoginButt() {
        let loginButt = document.getElementById('loginButt');
        let mobile = document.getElementById("mobileNo").value;
        let password = document.getElementById("loginPassword").value;

        if (mobile.length == 0 || password.length == 0) {

            loginButt.style.background = '#616161';
            loginButt.style.border = '1px solid #616161';
            loginButt.style.color = 'rgba(255, 255, 255, 0.57)';
        } else {
            loginButt.style.background = '#01B601';
            loginButt.style.border = '1px solid #01B601';
            loginButt.style.color = 'rgba(255, 255, 255, 0.87)';
        }
    }

</script>