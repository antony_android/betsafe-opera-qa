<?php 
$sub_type_id=''; 
  function clean($string) {
     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
     $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

     return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
  }
?>
<table class="highlights--item full-width market-item" cellpadding="0" cellspacing="0">

  <tr>
    <td colspan="10" class="std-pd pt-10">
      <table class="league">
        <?php if(isset($selected) && $selected == 'live') { ?>
        <tr>
          <td class="live-header">
            Live, 86min, 2nd Half
          </td>
        </tr>
        <?php } ?>
        <tr>
          <td class="time">
            <span class="play-time">
              <?php echo date('g:ia', strtotime($matchInfo['start_time'])); ?>
            </span>
            <?php echo date('D d/m', strtotime($matchInfo['start_time'])); ?>
          </td>
        </tr>
        <tr class="game">
          <td colspan="10">
            <table class="full-width">
              <tr>
                <td class="clubs" colspan="2">
                  <p class="pb-2">
                    <?php echo $matchInfo['home_team']; ?>
                    <?php if(isset($selected) && $selected == 'live') { ?> <span class="pull-right live-color">1</span>
                    <?php } ?>
                  </p>
                  <p>
                    <?php echo $matchInfo['away_team']; ?>
                    <?php if(isset($selected) && $selected == 'live') { ?> <span class="pull-right live-color">0</span>
                    <?php } ?>
                  </p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="meta">
            <?php echo $eventsTitle." / ". $matchInfo['category']." / ".$matchInfo['competition_name']; ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table class="odds markets-details full-width">

  <tr>
    <td class="markets-separator">
    </td>
  </tr>
  <tr>
    <td class="markets-title pt-8 std-pd">
      1x2
    </td>
  </tr>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="3">
      Full Time
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php for($counter=0; $counter < 3; $counter++) { ?>

          <?php $bt = $onex2SubTypes[$counter]; ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>

          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">
                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                  <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                    if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                        echo ' picked';
                     }
                  ?>" oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>"
                    bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>"
                    oddsid="<?php echo $bt['betradar_odd_id']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                echo ' odd-pd-picked';
                            }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php } ?>
        </tr>
        <?php if(count($onex2SubTypes) > 3) { ?>
        <tr>
          <td class="markets-subtitle pb-8 std-pd" colspan="3">
            First Half
          </td>
        </tr>
        <tr>
          <?php for($counter=6; $counter < 9; $counter++) { ?>
          <?php $bt = $onex2SubTypes[$counter]; ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td class="<?php echo $bt['match_id']; ?>
                  <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                    <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                            if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                echo ' picked';
                             }
                          ?>" oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>"
                    bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>"
                    oddsid="<?php echo $bt['betradar_odd_id']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value']; ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                        echo ' odd-pd-picked';
                                    }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php } ?>
        </tr>
        <?php } ?>
        <?php if(count($onex2SubTypes) > 3) { ?>
        <tr>
          <td class="markets-subtitle pb-8 std-pd" colspan="3">
            Second Half
          </td>
        </tr>
        <tr>
          <?php for($counter=3; $counter < 6; $counter++) { ?>
          <?php $bt = $onex2SubTypes[$counter]; ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">
                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                            <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                    if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                        echo ' picked';
                                     }
                                  ?>" oddtype="<?= $bt['name'] ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                echo ' odd-pd-picked';
                                            }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php } ?>
        </tr>
        <?php } ?>
      </table>
    </td>
  </tr>
</table>
<table class="odds markets-details full-width">
  <tr>
    <td class="markets-separator" colspan="2">
    </td>
  </tr>
  <tr>
    <td class="markets-title pt-8 std-pd" colspan="2">
      Over/Under
    </td>
  </tr>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      Full Time
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php $counter=0; ?>
          <?php foreach($overUnderSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <?php if($counter==2): ?>
        </tr>
        <tr>
          <?php $counter=0; ?>
          <?php endif; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                            <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                      if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                          echo ' picked';
                                       }
                                    ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                  echo ' odd-pd-picked';
                                              }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php $counter++; ?>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>

  <?php if(count($overUnderHomeSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      <?php echo $matchInfo['home_team'] ?> - Full Time
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php $counter=0; ?>
          <?php foreach($overUnderHomeSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <?php if($counter==2): ?>
        </tr>
        <tr>
          <?php $counter=0; ?>
          <?php endif; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                                <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                        if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                            echo ' picked';
                                         }
                                      ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                    echo ' odd-pd-picked';
                                                }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php $counter++; ?>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>

  <?php if(count($overUnderAwaySubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      <?php echo $matchInfo['away_team'] ?> - Full Time
    </td>
  </tr>
  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php $counter=0; ?>
          <?php foreach($overUnderAwaySubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <?php if($counter==2): ?>
        </tr>
        <tr>
          <?php $counter=0; ?>
          <?php endif; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                                <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                                  <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                          if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                              echo ' picked';
                                           }
                                        ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                      echo ' odd-pd-picked';
                                                  }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php $counter++; ?>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>
</table>

<table class="odds markets-details full-width">
  <tr>
    <td class="markets-separator">
    </td>
  </tr>
  <tr>
    <td class="markets-title pt-8 std-pd">
      Both Teams to Score
    </td>
  </tr>

  <?php if(count($bothTeamsToScoreSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd">
      Full Time
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php foreach($bothTeamsToScoreSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                            <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                      if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                          echo ' picked';
                                       }
                                    ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                  echo ' odd-pd-picked';
                                              }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>
  <?php if(count($bothTeamsToScoreFHSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      First Half
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php foreach($bothTeamsToScoreFHSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                                <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                        if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                            echo ' picked';
                                         }
                                      ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                    echo ' odd-pd-picked';
                                                }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>

  <?php if(count($bothTeamsToScoreSHSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      Second Half
    </td>
  </tr>
  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php foreach($bothTeamsToScoreSHSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                                <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                                  <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                          if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                              echo ' picked';
                                           }
                                        ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                      echo ' odd-pd-picked';
                                                  }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>
</table>

<table class="odds markets-details full-width">
  <tr>
    <td class="markets-separator">
    </td>
  </tr>
  <tr>
    <td class="markets-title pt-8 std-pd">
      Double Chance
    </td>
  </tr>

  <?php if(count($doubleChanceSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd">
      Full Time
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php foreach($doubleChanceSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                            <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                      if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                          echo ' picked';
                                       }
                                    ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                  echo ' odd-pd-picked';
                                              }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>
  <?php if(count($doubleChanceFHSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      First Half
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php foreach($doubleChanceFHSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                                <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                        if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                            echo ' picked';
                                         }
                                      ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                    echo ' odd-pd-picked';
                                                }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>
  <?php if(count($doubleChanceSHSubTypes) > 0): ?>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      Second Half
    </td>
  </tr>
  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php foreach($doubleChanceSHSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                                <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                                  <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                          if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                              echo ' picked';
                                           }
                                        ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                      echo ' odd-pd-picked';
                                                  }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
  <?php endif; ?>
</table>
<table class="odds markets-details full-width">
  <tr>
    <td class="markets-separator" colspan="2">
    </td>
  </tr>
  <tr>
    <td class="markets-title pt-8 std-pd" colspan="2">
      Total Goals Exact
    </td>
  </tr>
  <tr>
    <td class="markets-subtitle pb-8 std-pd" colspan="2">
      Full Time
    </td>
  </tr>

  <tr>
    <td class="std-4pd">
      <table class="full-width">
        <tr>
          <?php $counter=0; ?>
          <?php foreach($exactGoalsNumberSubTypes as $bt): ?>
          <?php $theMatch = @$betslip[$bt['match_id']]; ?>
          <?php if($counter==2): ?>
        </tr>
        <tr>
          <?php $counter=0; ?>
          <?php endif; ?>
          <td style="padding:0px 4px 0px 4px;">
            <table cellspacing="0" cellpadding="0" class="full-width">
              <tr>

                <td
                  class="<?php echo $bt['match_id']; ?>
                            <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">

                  <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt[' match_id']; ?>
                              <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                      if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                          echo ' picked';
                                       }
                                    ?>" oddtype="<?= $bt['name'] ?>" oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                    parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                    hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>"
                    oddvalue="<?php echo $bt['odd_value']; ?>"
                    custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                    target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>"
                    value="<?php echo $bt['sub_type_id']; ?>" special-bet-value="<?= $bt['special_bet_value'] ?>"
                    onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                    <table class="full-width">
                      <tr>
                        <td class="odd-pd">
                          <?php echo $bt['display']; ?>
                        </td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                  echo ' odd-pd-picked';
                                              }?>">
                          <?php echo $bt['odd_value']; ?>
                        </td>
                      </tr>
                    </table>

                  </button>
                </td>
              </tr>
            </table>
          </td>
          <?php $counter++; ?>
          <?php endforeach; ?>
        </tr>
      </table>
    </td>
  </tr>
</table>