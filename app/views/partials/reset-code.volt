<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="reqpw-row">
        <td class="game-title">
            Input PIN Code
        </td>
    </tr>
    <?php if(isset($message) && !is_null($message)): ?>
    <tr>
        <td class="reset-pwd-code">
            <?=$message ?>
        </td>
    </tr>
    <?php endif; ?>
    <tr>
        <td class="code-separator"></td>
    </tr>
    <tr>
        <td class="pt-8">
            <?php echo $this->tag->form("/resetpassword/new"); ?>
            <table class="form" cellpadding="0" cellspacing="0">
                <tr class="input">
                    <td>
                        <label>
                            Your PIN Code
                        </label>
                        <input type="text" id="reset_code" name="reset_code" {% if session.get('user-agent')
                            !='OperaMiniExtreme' %} onkeyup="refreshPINCode()" {% endif %} />
                    </td>
                </tr>

                <tr class="input">
                    <td>
                        <button type="submit" id="requestPWDButt">
                            Continue
                        </button>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr class="reset-password text-center">
        <td>
            <a href="{{ url('/change-password/inputcode') }}">
                Resend PIN Code
            </a>
        </td>
    </tr>
</table>

<script>

    function refreshPINCode() {
        var requestPwdButt = document.getElementById('requestPWDButt');
        var resetCode = document.getElementById("reset_code").value;

        if (resetCode.length == 0) {

            requestPwdButt.style.background = '#616161';
            requestPwdButt.style.border = '1px solid #616161';
        } else {
            requestPwdButt.style.background = '#01B601';
            requestPwdButt.style.border = '1px solid #01B601';
        }
    }

</script>