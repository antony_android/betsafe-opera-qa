<?php 
      function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
       $empty_row_text = '<td>
         <table cellspacing="0" cellpadding="0"> <tr> 
         <td class="">
         <button  class="odds-btn disabled-odds" style="text-align: center;">
          <span class="odd" style="opacity:0.2;">
            <img height="15" width="15" src="/img/padlock.svg" alt="-" />
          </span>
        </button></td> </tr> 
      </table>
    </td>';
?>
<table class="highlights full-width">
  <th>{{ this.flashSession.output() }}</th>
  <?php if(isset($timeOutPeriodRemaining) && !is_null($timeOutPeriodRemaining)) { ?>
  <tr class="page-header-row bet-history-header">
    <td class="game-title std-pd">
      Responsible Gaming
    </td>
  </tr>
  <tr>
    <td>
      <p class="depo-mpesa-details pt-16 pb-16 std-pd" style="text-align: left;">
        You have chosen to Time-Out for a period of
        <?=$setTimeout; ?>.
        Time remaining
        <?=$timeOutPeriodRemaining; ?>.
      </p>
    </td>
  </tr>
  <?php } ?>
  <tr class="page-header-row">
    <td class="game-title std-pd pt-8 pb-8">
      <?php if(isset($eventsTitle)) { echo $eventsTitle; } else { echo 'Football'; } ?>
    </td>
  </tr>
  <tr>
    <td>

      <!-- List matches -->
      <?php $counter=12; ?>
      <?php foreach($matches as $day): ?>

      <?php $theMatch = @$betslip[$day['match_id']]; 
            ?>

      <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
        <tr>
          <td class="matches-border" colspan="4"></td>
        </tr>
        <tr>
          <td colspan="4" class="std-pd pt-10">
            <?php if(isset($selected) && $selected == 'live') { ?>
            {{ partial('partials/live-header') }}
            <?php } else { ?>
            {{ partial('partials/match-header') }}
            <?php } ?>
          </td>
        </tr>
        <tr class="odds">
          <?php if($day['home_odd']) { ?>
          <td tabindex="-10">
            <table cellspacing="0" cellpadding="0" tabindex="-11">
              <tr>

                <td class="clubone <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                  ?>" tabindex="-12">
                  <button href="javascript:;" class="<?php if($theMatch && $theMatch['bet_pick']==1 && $theMatch['sub_type_id']==$day['sub_type_id']){
                        echo ' picked';
                     }?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1X2" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>" target="."
                    odd-key="1" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" oddsid="<?php echo $day['home_odd_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>"
                    sub-type-id="<?= $day['sub_type_id']; ?>" special-bet-value="0.0"
                    onClick="addBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('oddsid'))"
                    tabindex="<?=$counter+1;?>">
                    <table cellspacing="0" cellpadding="0" tabindex="-13">
                      <tr>
                        <td class="odd-pd" tabindex="-14">1</td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==1 && $theMatch['sub_type_id']==$day['sub_type_id']){
                                echo ' odd-pd-picked';
                            }?>" tabindex="-15">
                          <?php echo number_format($day['home_odd'], 2); ?>
                        </td>
                      </tr>
                    </table>
                  </button>
                </td>

              </tr>
            </table>
          </td>
          <?php } else { ?>
          <td tabindex="-10">
            <table cellspacing="0" cellpadding="0" tabindex="-11">
              <tr>

                <td class="clubone" tabindex="-12">
                  <button href="javascript:;" class="odds-btn disabled-odds" tabindex="<?=$counter+1;?>">
                    <table cellspacing="0" cellpadding="0" tabindex="-13">
                      <tr>
                        <td class="odd-pd" tabindex="-14">1</td>
                      </tr>
                      <tr>
                        <td tabindex="-15">
                          -
                        </td>
                      </tr>
                    </table>
                  </button>
                </td>

              </tr>
            </table>
          </td>
          <?php }  ?>
          <?php if($day['neutral_odd']) { ?>
          <td class="" tabindex="16">
            <table tabindex="17">
              <tr>
                <td class="draw <?php echo $day['match_id']; ?> <?php
                        echo clean($day['match_id'].$day['sub_type_id'].'Draw');?>" tabindex="-18">
                  <button href="javascript:;" class="<?php if($theMatch && $theMatch['bet_pick']=='X' && $theMatch['sub_type_id']==$day['sub_type_id']){
                        echo ' picked';
                     }?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1X2" bettype='prematch'
                    awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['neutral_odd']; ?>"
                    target="javascript:;" odd-key="X" parentmatchid="<?php echo $day['parent_match_id']; ?>"
                    id="<?php echo $day['match_id']; ?>" oddsid="<?php echo $day['neutral_odd_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'])?>Draw"
                    sub-type-id="<?= $day['sub_type_id']; ?>" special-bet-value="0.0"
                    onClick="addBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('oddsid'))"
                    tabindex="<?=$counter+2;?>">
                    <table cellspacing="0" cellpadding="0" tabindex="-18">
                      <tr>
                        <td class="odd-pd" tabindex="-19">X</td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']=='X' && $theMatch['sub_type_id']==$day['sub_type_id']){
                                echo ' odd-pd-picked';
                            }?>" tabindex="-20">
                          <?php echo number_format($day['neutral_odd'], 2); ?>
                        </td>
                      </tr>
                    </table>
                  </button>
                </td>

              </tr>
            </table>
          </td>
          <?php } else { ?>
          <td tabindex="17">
            <table cellspacing="0" cellpadding="0" tabindex="18">
              <tr>

                <td class="draw">
                  <button href="javascript:;" class="odds-btn disabled-odds" tabindex="<?=$counter+1;?>">
                    <table cellspacing="0" cellpadding="0" tabindex="18">
                      <tr>
                        <td class="odd-pd" tabindex="19">X</td>
                      </tr>
                      <tr>
                        <td tabindex="20">
                          -
                        </td>
                      </tr>
                    </table>
                  </button>
                </td>

              </tr>
            </table>
          </td>
          <?php }  ?>
          <?php if($day['away_odd']) { ?>
          <td tabindex="-21">
            <table tabindex="-22">
              <tr>
                <td class="clubtwo <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                  ?>" tabindex="23" {% if session.get('user-agent')=='OperaMiniExtreme' %} style="padding-right:8px;"
                  {% endif %}>
                  <button href="javascript:;" class="<?php if($theMatch && $theMatch['bet_pick']==2 && $theMatch['sub_type_id']==$day['sub_type_id']){
                        echo ' picked';
                     }?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1X2" bettype='prematch'
                    oddsid="<?php echo $day['away_odd_id']; ?>" awayteam="<?php echo $day['away_team']; ?>"
                    oddvalue="<?php echo $day['away_odd']; ?>" target="javascript:;" odd-key="2"
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>"
                    custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>"
                    sub-type-id="<?= $day['sub_type_id']; ?>" special-bet-value="0.0"
                    onClick="addBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('oddsid'))"
                    tabindex="<?=$counter+3;?>">
                    <table cellspacing="0" cellpadding="0" tabindex="-25">
                      <tr>
                        <td class="odd-pd" tabindex="26">2</td>
                      </tr>
                      <tr class="odd">
                        <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==2 && $theMatch['sub_type_id']==$day['sub_type_id']){
                                echo ' odd-pd-picked';
                            }?>" tabindex="27">
                          <?php echo number_format($day['away_odd'], 2); ?>
                        </td>
                      </tr>
                    </table>
                  </button>
                </td>

              </tr>
            </table>
          </td>
          <?php } else { ?>
          <td tabindex="-21">
            <table cellspacing="0" cellpadding="0" tabindex="-22">
              <tr>

                <td class="clubtwo" tabindex="23">
                  <button href="javascript:;" class="odds-btn disabled-odds" tabindex="<?=$counter+1;?>">
                    <table cellspacing="0" cellpadding="0" tabindex="24">
                      <tr>
                        <td class="odd-pd" tabindex="25">2</td>
                      </tr>
                      <tr>
                        <td tabindex="26">
                          -
                        </td>
                      </tr>
                    </table>
                  </button>
                </td>

              </tr>
            </table>
          </td>
          <?php }  ?>
          {% if session.get('user-agent') !='OperaMiniExtreme' %}
          <td class="sidebet" tabindex="-28">
            <?php if(isset($selected) && $selected == 'live') { ?>
            <a href="{{url('livematch/open/') }}{{day['match_id']}}" tabindex="<?=$counter+4;?>">
              <span>
                <?php echo "+".$day['side_bets']; ?>
              </span>
            </a>
            <?php } else { ?>
            <a href="{{url('match/open/') }}{{day['match_id']}}" tabindex="<?=$counter+4;?>">
              <span>
                <?php echo "+".$day['side_bets']; ?>
              </span>
            </a>
            <?php } ?>
          </td>
          {% endif %}
        </tr>
      </table>
      <?php $counter = $counter+4; ?>
      <?php endforeach; ?>

      <!-- List matches end -->

    </td>
  </tr>
</table>