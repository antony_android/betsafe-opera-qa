<table class="login" cellpadding="0" cellspacing="0">
    <tr class="login-row">
        <td class="game-title std-pd">
            Promotions
        </td>
    </tr>
    <tr>
        <td>
            <table class="form" cellpadding="0" cellspacing="0">

                <tr class="bonus-row">
                    <td class="std-pd">
                        <span class="bonus-title">40 for 40 Jackpot</span>
                        <p class="bonus-message">
                            Play for a chance to win Ksh 5,000 every week
                        </p>
                        <a href="{{ url('/bonuses/forty') }}" class="bonus-link">
                            Read more
                        </a>
                    </td>
                </tr>
                <tr class="bonus-row">
                    <td class="std-pd">
                        <span class="bonus-title">Welcome Bonus</span>
                        <p class="bonus-message">
                            Register and claim your Free Bet today!
                        </p>
                        <a href="{{ url('/bonuses/welcome') }}" class="bonus-link">
                            Read more
                        </a>
                    </td>
                </tr>
                <tr class="bonus-row">
                    <td class="std-pd">
                        <span class="bonus-title">Multibet Boost</span>
                        <p class="bonus-message">
                            Increase your winnings by up to 200%
                        </p>
                        <a href="{{ url('/bonuses/boost') }}" class="bonus-link">
                            Read more
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>