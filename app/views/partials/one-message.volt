<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <?php $message = $message[0]; ?>
    <tr class="login-row">
        <td class="game-title std-pd">
            <a href="/messages">
                <img src="/img/one-message.svg" width="10px" alt="" />
            </a>
            <span class="messages-day one-message-dt">
                <?php if(date('Ymd') == date('Ymd', strtotime($message['message_date']))){ ?>
                <?php echo 'Today'; ?>
                <?php } elseif(date('Ymd', strtotime("-1 days")) == date('Ymd', strtotime($message['message_date']))) {?>
                <?php echo 'Yesterday'; ?>
                <?php } else { ?>
                <?php echo date('l', strtotime($message['message_date'])); ?>
                <?php } ?>
                <form action="/messages/del" method="post" style="display: inline;">
                    <input type="hidden" name="messageId" value="<?php echo $message['isolutions_messageid']; ?>" />
                    <button type="submit" class="one-message-bt">
                        <img src="/img/message-del.svg" alt="" style="margin-left: 4px;" />
                    </button>
                </form>
            </span>
        </td>
    </tr>
    <tr>
        <td class="deposit-details std-pd one-message-title">
            <?=$message['message_title']; ?>
        </td>
    </tr>
    <tr>
        <td class="std-pd one-message-fm">
            <?=$message['message']; ?>
        </td>
    </tr>
</table>