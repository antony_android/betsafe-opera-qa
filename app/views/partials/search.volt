<?php if(strpos($_SERVER['REQUEST_URI'], 'live') !== false) { ?>
<?php echo $this->tag->form("/live"); ?>
<?php } else { ?>
<?php echo $this->tag->form("/"); ?>
<?php } ?>
<table class="search-table">
  <tr>
    <td>
      <input type="text" name="keyword" placeholder="<?php echo $t->_('Team or Competition'); ?>"
        class="top--search--input" tabindex="5">
    </td>

    <td>
      <button type="submit" class="top--search--button" tabindex="6">
        <?php echo $t->_('Search'); ?>
      </button>
    </td>
  </tr>
</table>
</form>