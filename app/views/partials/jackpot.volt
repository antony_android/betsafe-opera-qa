<?php 
  function clean($string) {
     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
     $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

     return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
  }
?>

<table class="full-width" cellpadding="0" cellspacing="0">

  <tr class="page-header-row bet-history-header">
    <td class="game-title std-pd">
      Jackpots
    </td>
    <td class="jp-hist-td">
      <a href="{{ url('/jackpot/history') }}" class="jp-history">
        JP History
      </a>
    </td>
  </tr>
</table>
<?php foreach($jackpots as $jackpot): ?>
<table class="jackpots--item full-width" cellpadding="0" cellspacing="0">
  <tr>
    <td class="matches-border" colspan="4"></td>
  </tr>
  <tr>
    <td colspan="4" class="std-pd pt-8">
      <table class="league">
        <tr>
          <td class="pb-8 bold">
            <?php echo $jackpot['jackpot_name']; ?>
          </td>
        </tr>
        <tr>
          <td class="pb-8 bold">
            Win KSH
            <?php echo number_format($jackpot['jackpot_amount']); ?>
          </td>
        </tr>
        <tr class="game">
          <td class="pb-8 wins">
            For only
            <?php echo number_format($jackpot['bet_amount']); ?> bob.
          </td>
        </tr>
        <tr>
          <td class="pb-8 wins">
            Ends:
            <?=$jackpot['kick_off']; ?>
          </td>
        </tr>
        <tr>
          <td>
            <?php if($jackpot['status'] == 'ACTIVE'): ?>
            <a href="{{ url('/jackpot/open/')}}<?=$jackpot['jackpot_event_id']; ?>">
              <?php endif; ?>
              <button type="button" class="place mb-0
              <?php if($jackpot['status'] == 'ACTIVE'){ echo 'green-theme'; } ?>"
                style="font-size: 12px;margin-top: 0px;margin-bottom: 12px !important;">
                <?php if($jackpot['status'] == 'ACTIVE'): ?>
                Play for KSH
                <?=$jackpot['bet_amount'];?>
                <?php else: ?>
                Starting Soon...
                <?php endif; ?>
              </button>
              <?php if($jackpot['status'] == 'ACTIVE'): ?>
            </a>
            <?php endif; ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<?php endforeach; ?>