<table class="landing">
<tr>
<td>
<table class="top--nav" width="100%">
    <tr>
        <td class="<?= ($men == 'sports') ? 'selected': ''; ?>">
            <a href="{{ url('sports') }}?id={{current_sport['sport_id']}}">Sports</a>
        </td>
        <td class="<?= ($men == 'top-leagues') ? 'selected': ''; ?>">
            <a href="{{ url('top-leagues') }}?id={{current_sport['sport_id']}}">Leagues</a>
        </td>
        <td class="<?= ($menu == 'countries') ? 'selected': ''; ?>">
            <a href="{{ url('countries') }}?id={{current_sport['sport_id']}}">Countries</a>
        </td>
    </tr>
</table>
<table class="football">
    <th class="title" colspan="2">{{sport_name}} - {{category}} Leagues</th>
    <?php foreach($top_competitions as $competition): ?>
    <tr class="menu">
        <td class="text">
          <a href="{{url('competition?id=')}}{{ competition['competition_id'] }}">
          {{competition['country']}} - {{ competition['competition_name'] }} <span style="float:right; margin-right:5px">{{competition['games_count']}}</span></a></td>
    </tr>
    <?php endforeach; ?>
</table>

</td>
</tr>
</table> <!-- end table .landing -->

