<table class="login" cellpadding="0" cellspacing="0">
  <tr class="reqpw-row">
    <td class="game-title">
      Request New Password
    </td>
  </tr>
  <th>{{ this.flashSession.output() }}</th>
  <tr>
    <td>
      <?php echo $this->tag->form("/resetpassword/code"); ?>
      <table class="form" cellpadding="0" cellspacing="0">
        <tr class="input">
          <td>
            <label>
              Your Mobile Number
            </label>
            <div class="flag-div">
              <img src="{{url('/img/kenya-flag.png')}}" width=24 alt="" class="flag-img" />
              <span style="vertical-align: middle;"> +254</span>
              <input type="text" name="mobile" id="phone_no" {% if session.get('user-agent') !='OperaMiniExtreme' %}
                onkeyup="refreshResetPWd()" {% endif %} class="flag-input" />
            </div>
          </td>
        </tr>

        <tr class="input">
          <td>
            <button type="submit" id="requestPWDButt">
              Request New Password
            </button>
          </td>
        </tr>
        <tr class="reset-password text-center">
          <td style="padding-bottom: 24px;">
            <a href="{{url('/resetpassword/inputcode')}}">
              Input PIN Code
            </a>
          </td>
        </tr>
      </table>
      </form>
    </td>
  </tr>
</table>

<script>

  function refreshResetPWd() {
    var requestPwdButt = document.getElementById('requestPWDButt');
    var phone = document.getElementById("phone_no").value;

    if (phone.length == 0) {

      requestPwdButt.style.background = '#616161';
      requestPwdButt.style.border = '1px solid #616161';
    } else {
      requestPwdButt.style.background = '#01B601';
      requestPwdButt.style.border = '1px solid #01B601';
    }
  }

</script>