<table class="full-width" cellpadding="0" cellspacing="0">

    <tr class="page-header-row bet-history-header">
        <td class="game-title">
            Responsible Gaming
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td>
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="respGamingControl" id="respGamingControl"
                class="form-control slip-filter-select" value="General Information"
                onclick="toggleRespGamingFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="general-list-item">
                    <a href="{{ url('/responsible') }}">
                        General Information
                    </a>
                </li>
                <li id="limits-list-item">
                    <a href="{{ url('/responsible/limits') }}">
                        Budget Limits
                    </a>
                </li>
                <li id="timeout-list-item">
                    <a href="{{ url('/responsible/timeout') }}">
                        Time Out
                    </a>
                </li>
                <li id="exclusion-list-item">
                    <a href="{{ url('/responsible/exclusion') }}">
                        Self Exclusion
                    </a>
                </li>
            </ul>
            {% else %}
            <form method="post" action="{{ url('/responsible/filter')}}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
                    <option value="general" selected>General Information</option>
                    <option value="limits">Budget Limits</option>
                    <option value="timeout">Time Out</option>
                    <option value="exclusion">Self Exclusion</option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-16">
            General Information
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                Gambling can be addictive and may result in difficulties to stop even when you know it is causing
                issues. Gambling is
                not a solution for financial problems – only play for what you can afford to lose! Keep track of how
                much time and money
                you are spending online and take action if needed. Play responsibly and get in touch with us or use the
                online tools
                here should you have any concerns.
            </p>
            <p class="pb-16 depo-mpesa-header">
                One bet too many
            </p>
            <p class="pb-16">
                We want our players to have fun while gaming at Betsafe, so we encourage you to gamble responsibly at
                all times.
                Gambling should be fun. Borrowing money to play, spending more than you can afford or using money set
                aside for other
                purposes is unwise and can lead to significant problems for yourself and others around you. The Betsafe
                team wants
                players to be safe with their gaming and wise with the way they play.
            </p>
            <p class="pb-16 depo-mpesa-header">
                GamHelp Kenya
            </p>
            <p class="pb-16">
                That's why we work with Gamhelp Kenya, an independent center for Responsible Gambling oversight,
                education and research.
                They are a tech-driven call center equipped with e-therapy web-based and mobile platforms for digital
                gambling diagnosis
                and treatment.
            </p>
            <p class="pb-16">
                GamHelp Kenya offers free 24/7 telephone and online counselling to people with gambling addiction. You
                can also contact
                them for help if you have a loved one who cannot control their gambling behaviour but are still in
                denial.
            </p>
            <p class="pb-16">
                Call them toll-free 24/7 at 0800 000 023 for free gambling addiction treatment. You can also take their
                Gambling Addiction Test to determine if you are a problem gambler? (*All calls & messages are free and
                totally confidential).
            </p>
            <p class="pb-16">
                At GamHelp Kenya they also work with operators towards ensuring that their marketing campaigns do not
                appeal to those who are vulnerable to problem gambling.
            </p>
            <p class="pb-16">
                At Betsafe we also work with the Global Gambling Guidance G4 which certifies Internet gaming sites for
                this purpose. Together we provide you with tools to prevent unhealthy gaming behaviour and enable you to
                play responsibly.
            </p>
            <p class="pb-16 depo-mpesa-header">
                18+
            </p>
            <p class="pb-16">
                Young people under 18 are especially vulnerable for gambling. And for good reasons it is illegal for
                anyone under the age of 18 to open an account and/or to gamble. Betsafe reserves the right to request
                proof of age from
                any customer and may suspend an account until adequate verification is received. Equally it is highly
                irresponsible and
                illegal to act as a envoy or agent for a person under 18 years.
            </p>
            <p class="pb-16 depo-mpesa-header">
                Take a Time-Out
            </p>
            <p class="pb-16">
                Time-Outs allow you to take a short break from gaming at Betsafe. When you activate a Time-Out on your
                account, you will
                not be able to place any bets or transfer any money to your Betsafe account during the time-out period.
                You may activate
                a Time-Out by contacting Customer Care or online by following these steps:
            </p>
            <ol class="pb-16">
                <li>Simply log in to your account and navigate to the logged-in right-hand 'Account Menu'.</li>
                <li>Click on 'Responsible Gaming', dropdown 'Time-Out' label.</li>
                <li>Here, simply select a time period for the Time-Out. You have the following options: 24 hours, 48
                    hours, 1 week.</li>
                <li>Once you have selected the time period, click 'Set' to complete the process.</li>
            </ol>
            <p class="pb-16 depo-mpesa-header">
                Self-Exclusion
            </p>
            <p class="pb-16">
                Self-Exclusion allows you to stop yourself from gaming at Betsafe for a longer period of time. If you
                feel that you need
                a longer period or a permanent exclusion, please contact our Customer Care.
            </p>
            <p class="pb-16">
                Please note that once your account has been self-excluded, it cannot be re-opened before the
                self-exclusion period is
                over. If you feel you have made an error, please contact our Customer Care department.
            </p>
            <p class="pb-16 depo-mpesa-header">
                Budget Limits
            </p>
            <p class="pb-16">
                Giving you the ability to control how much you are wanting to spend is
                highly important, which is why we offer a 'Budget Limit' tool.
            </p>
            <p class="pb-16">
                The Budget Limit is purely based on how much of your own money you can deposit onto your Betsafe account
                in a single
                day. For example, if you were to set a Budget Limit of KSH 500, you won't be able to deposit more once
                this has been
                reached until the next day. Please note that the budget limit takes withdrawals into consideration, so
                if you were to
                deposit KSH 500 in the first part of the day and withdraw KSH 400 later on you may deposit another KSH
                400 within the same day.
            </p>
            <p class="pb-16 bold">
                Setting your Budget Limit:
            </p>
            <p class="pb-16">
                You can set a budget limit by logging in to your account and navigating to the logged-in right-hand
                'user menu'. Click on 'Responsible Gaming', then select the 'Budget Limits' tab where you can enter the
                amount.
            </p>
            <p class="pb-16 bold">
                Changing your Budget Limit:
            </p>
            <p class="pb-16">
                Budget limits can be changed at any time. When increasing the amount or removing the limit completely, a
                waiting period of 24 hours will apply. Making any decreases will take effect immediately.
            </p>
        </td>
    </tr>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    function toggleRespGamingFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }

</script>
<?php if($selectedRespControl == 'general'): ?>
<script>
    document.getElementById('general-list-item').classList.add("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "General Information";
</script>
<?php elseif($selectedRespControl == 'limits'): ?>
<script>
    document.getElementById('limits-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Budget Limits";
</script>
<?php elseif($selectedRespControl == 'timeout'): ?>
<script>
    document.getElementById('timeout-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Time-Out";
</script>
<?php elseif($selectedRespControl == 'exclusion'): ?>
<script>
    document.getElementById('exclusion-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Self-Exclusion";
</script>
<?php endif; ?>