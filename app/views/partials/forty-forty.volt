<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="depo-mpesa-header">
            Ekelea 40 for 40 Jackpot to win Ksh 5,000
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p>
                Place a bet on the 40 for 40 Jackpot for a chance to win Ksh 5,000 every week. Get 8 or more correct
                predictions & enter a draw to be one of the 50 weekly winners.
            </p>
            <p class="bonus-message-sep">
                Play the 40 for 40 Jackpot every week for a chance to be one of the 50 weekly winners of Ksh 5,000.
                Players who get 8 or more correct predictions will be randomly selected.
            </p>
        </td>
    </tr>
    <tr class="login-row">
        <td class="depo-mpesa-header pt-4">
            Win Ksh 5000 in 3 easy steps:
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <ol>
                <li>Play the 40 for 40 Jackpot at 40bob</li>
                <li>Get 8 or more correct predictions</li>
                <li>Enter draw</li>
            </ol>
            <p class="pt-16 pb-4">
                All 40 for 40 Jackpot players who get 8 or more will be entered into a random draw for a chance to win
                Ksh 5,000 every week. Each bet placed on the Jackpot counts as an entry.
            </p>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-16">
            Terms & Conditions
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                Qualification
            </p>
            <p class="pb-16">
                The qualification period for this promotion runs from Wednesday 3rd August until kick off of the first
                game of the 40 For 40 Jackpot played on the weekend of 23th-25th September.
            </p>
            <p class="pb-16">
                This promotion entitles fifty (50) players to win Ksh 5,000 each week during the promotional period.
            </p>
            <p class="pb-16">
                To qualify for the Draw, players need eight (8) or more correct predictions in one of the 40 For 40
                Jackpots played during the week.
            </p>
            <p class="pb-16">
                There will be two (2) 40 For 40 Jackpot games each week that players can enter to qualify for the draw.
            </p>
            <p class="pb-16">
                Players who have made eight (8) or more correct predictions on one of the 40 For 40 Jackpots played that
                week, will be entered into a random draw, which will take place within 48 hours of the weekend Jackpot
                being settled.
            </p>
            <p class="pb-16">
                Fifty (50) players who get eight (8) or more correct predictions will receive Ksh 5,000 each week.
            </p>
            <p class="pb-16">
                Draw winners will be contacted by after the draw has concluded.
            </p>
            <p class="pb-16">
                The results of this draw will be shared through Betsafe Social Media Platforms.
            </p>
            <p class="pb-16">
                Players are eligible to win one prize during this promotion. If a person is drawn twice, they will not
                receive the prize a second time and another player will be drawn instead.
            </p>
            <p class="pb-16">
                Verification and Prizes
            </p>
            <p class="pb-16">
                Betsafe reserves the right to verify the identity of draw winners with relevant authorities and verify
                identity documents before any prize payment is made.
            </p>
            <p class="pb-16">
                Betsafe reserves the right to use the names, video, audio, radio or other recordings, motion and still
                images of the entrants, for purposes of publicity and marketing campaigns
            </p>
            <p class="pb-16">
                Betsafe reserves the right to withhold a part, or all of the prize money won, until the draw winner is
                verified.
            </p>
            <p class="pb-16">
                The winnings will be credited to the players Betsafe account within 48 hours of the conclusion of the
                draw.
            </p>
            <p class="pb-16">
                All prize winnings are subject to a withholding tax of 20% in accordance with the Income Tax Act under
                section 35(1)(i), (3)(h). The tax shall be deducted from your winnings and collected by us for
                remittance to the Kenya Revenue Authority (KRA). Therefore the actual pay-out will be after tax (i.e
                winnings minus tax).
            </p>
            <p class="pb-16">
                All Standard Jackpot Rules apply.
            </p>
            <p class="pb-16">
                General
            </p>
            <p class="pb-16">
                Betsafe reserves the right to modify or change the Terms and Conditions for this promotion at any time
            </p>
            <p class="pb-16">
                Betsafe reserves the right to modify, change or cancel this promotion at any time.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to disqualify any player from this promotion.
            </p>
            <p class="pb-16">
                Betsafe staff, former staff members, third party suppliers and their family members are not eligible to
                take part in this promotion.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to close an account and confiscate existing funds if there is
                evidence of abuse/fraud found.
            </p>
            <p class="pb-16">
                In the event of any dispute,Betsafe management's decision will be considered full and final.
            </p>
            <p class="pb-16">
                This promotion is run in accordance with our general site Terms & Conditions and Privacy Policy
            </p>
        </td>
    </tr>
</table>