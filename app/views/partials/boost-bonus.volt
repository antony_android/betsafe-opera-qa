<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title">
            Multibet Boost
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p>
                Get up to 200% boost on your Multibet winnings every time you play!
            </p>
            <p class="bonus-message-sep">
                Get your Multibet Boost in three easy steps:
            </p>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <ol>
                <li>Add four or more selections to your betslip</li>
                <li>Place your Multibet</li>
                <li>If your bet wins, we will add up to 200% extra to your winnings!</li>
            </ol>
            <p class="bonus-message-sep">
                To boost your winnings, place a multibet with at least four selections with
                minimum odds of 1.20 per selection. See the table below for the bonus breakdown.

            <table class="full-width bonus-table">
                <tr>
                    <td>
                        <div class="bonus-td-title">
                            Selections
                        </div>
                    </td>
                    <td>
                        <div class="bonus-td-title bonus-tb-det">
                            Multibet Boost
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">
                            4
                        </div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">
                            3%
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">5</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">5%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">6</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">10%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">7</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">15%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">8</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">20%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">9</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">25%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">10</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">30%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">11</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">35%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">12</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">40%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">13</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">45%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">14</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">50%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">15</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">55%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">16</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">60%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">17</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">70%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">18</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">80%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">19</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">90%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">20</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">100%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">21</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">110%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">22</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">120%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">23</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">130%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">24</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">140%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">25</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">150%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">26</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">160%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">27</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">170%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">28</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">180%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">29</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">190%</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="bonus-td-content">30 or more</div>
                    </td>
                    <td>
                        <div class="bonus-td-content bonus-tb-det">200%</div>
                    </td>
                </tr>
            </table>
            </p>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-16">
            Terms & Conditions
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                Offer
            </p>
            <p class="pb-16">
                The promotion period runs from 12:00 21st April 2021 until further notice.
            </p>
            <p class="pb-16">
                The promotion entitles you to a boost on your winning Multibets.
            </p>
            <p class="pb-16">
                The amount of the boost is calculated by the amount of qualifying selections in
                your Multibet. The amount of additional winnings is shown in the table at the
                bottom of the Terms and Conditions.
            </p>
            <p class="pb-16">
                Boosted winnings apply to real money bets only.
            </p>
            <p class="pb-16">
                To qualify for the Multibet Boosts you need to place a bet with a minimum
                of four (4) selections. The more selections you add to your Multibet, the
                bigger the boost you will receive.
            </p>
            <p class="pb-16">
                The minimum odds for each leg must be minimum 1.2 to qualify for this promotion.
            </p>
            <p class="pb-16">
                Only legs with qualifying minimum odds will be counted towards this offer.
                Non-qualifying legs will not be counted.
            </p>
            <p class="pb-16">
                Upon settlement, boosted winnings will be credited to your account as cash.
            </p>
            <p class="pb-16">
                The maximum additional winnings that can be won with this offer is KSH 1,000,000.
            </p>
            <p class="pb-16">
                Single bets, Jackpot bets or bets on virtual sports will not qualify for this promotion.
            </p>
            <p class="pb-16">
                If any selections are postponed/void then the bonus will still apply, but the percentage bonus added
                will reflect the actual number of winning selections. For example, a nine (9) leg multibet with eight
                (8) winners and one (1) postponed match will receive a 20% bonus. Therefore, the boost awarded can be
                different to the boost
                amount stated in the original betslip.
            </p>
            <p class="pb-16">
                Multibet Boosts are automatically shown in your betslip. If you do not see a
                Multibet Boost, then your selections do not meet the qualifying criteria for
                the promotion.
            </p>
            <p class="pb-16">
                This offer cannot be used in conjunction with any other offer. Multibets placed using Free Bets or Bonus
                Money will not qualify for the Multibet Boost Offer.
            </p>
            <p class="pb-16">
                If players take an early cash out option on your Multibet, then your Multibet Boost will be forfeited.
            </p>
            <p class="pb-16">
                General
            </p>
            <p class="pb-16">
                Betsafe reserve the right to modify or change the
                Terms and Conditions for this promotion at any time.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to modify, change or cancel this promotion at any time.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to disqualify any player from this promotion.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to close an account and confiscate existing funds if there is
                evidence of abuse/fraud found.
            </p>
            <p class="pb-16">
                In the event of any dispute,Betsafe management's decision will be considered full and final.
            </p>
        </td>
    </tr>
</table>