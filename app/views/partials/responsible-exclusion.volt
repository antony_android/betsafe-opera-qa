<table class="full-width" cellpadding="0" cellspacing="0">

    <tr class="page-header-row bet-history-header">
        <td class="game-title">
            Responsible Gaming
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td>
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="respGamingControl" id="respGamingControl"
                class="form-control slip-filter-select" value="General Information"
                onclick="toggleRespGamingFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="general-list-item">
                    <a href="{{ url('/responsible') }}">
                        General Information
                    </a>
                </li>
                <li id="limits-list-item">
                    <a href="{{ url('/responsible/limits') }}">
                        Budget Limits
                    </a>
                </li>
                <li id="timeout-list-item">
                    <a href="{{ url('/responsible/timeout') }}">
                        Time Out
                    </a>
                </li>
                <li id="exclusion-list-item">
                    <a href="{{ url('/responsible/exclusion') }}">
                        Self Exclusion
                    </a>
                </li>
            </ul>
            {% else %}
            <form method="post" action="{{ url('/responsible/filter') }}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
                    <option value="general">General Information</option>
                    <option value="limits">Budget Limits</option>
                    <option value="timeout">Time Out</option>
                    <option value="exclusion" selected>Self Exclusion</option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-10">
            Self-Exclusion
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                If you believe you have a gambling problem you may wish to take a break from betting and depositing. If
                you would like to self-exclude yourself from our site:
            </p>
            <ol class="pb-16">
                <li>For a period of up until 1 week, you may either use the Time-Out section.</li>
                <li>For a longer period please contact our Customer Care department.</li>
            </ol>
            <p class="pb-16">
                If you need further information or help please contact our Customer Care department.
            </p>
        </td>
    </tr>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    function toggleRespGamingFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }
</script>
<?php if($selectedRespControl == 'general'): ?>
<script>
    document.getElementById('general-list-item').classList.add("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "General Information";
</script>
<?php elseif($selectedRespControl == 'limits'): ?>
<script>
    document.getElementById('limits-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Budget Limits";
</script>
<?php elseif($selectedRespControl == 'timeout'): ?>
<script>
    document.getElementById('timeout-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Time-Out";
</script>
<?php elseif($selectedRespControl == 'exclusion'): ?>
<script>
    document.getElementById('exclusion-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Self-Exclusion";
</script>
<?php endif; ?>