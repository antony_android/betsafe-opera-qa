<table class="landing">
    <tr>
        <td>
            <table class="football">
                <tr class="menu" style="border: 0;">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'home') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40 full-width">
                                    <img class="sports-icon" src="{{ url('/img/sports/Icon-Football.svg') }}" alt="" />
                                    <a href="{{ url('/') }}">Home / Sports</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'soon') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <img class="sports-icon" src="{{ url('/img/sports/Icon-Starting_Soon.svg') }}"
                                        alt="" />
                                    <a href="{{ url('/upcoming') }}">Starting Soon</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'live') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <img class="sports-icon" src="{{ url('/img/sports/Icon-Live_Betting.svg') }}"
                                        alt="" />
                                    <a href="{{ url('/live') }}">Live Betting</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'jackpots') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <img class="sports-icon" src="{{ url('/img/sports/jackpots.svg') }}" alt="" />
                                    <a href="{{ url('/jackpot') }}">Jackpots</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'promotions') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <img class="sports-icon" src="{{ url('/img/sports/Icon-Promotions.svg') }}"
                                        alt="" />
                                    <a href="{{ url('/promotions') }}">Promotions</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'sms') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <img class="sports-icon" src="{{ url('/img/sports/Icon-Text_Betting.svg') }}"
                                        alt="" />
                                    <a href="{{ url('/textbetting') }}">Text Betting</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="football full-width">
                <th class="title" colspan="2">Sports</th>
                <?php foreach($sports as $ss): ?>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                {% if session.get('selectedSportId') == ss['sport_id'] %}
                                <td class="selected-menu-item">
                                </td>
                                {% endif %}
                                <td class="std-pd vh-40">
                                    <?php $icon = $ss['sport_name'].".svg"; ?>
                                    <?php $icon = str_replace(" ", "_", $icon); ?>
                                    <?php if($ss['sport_name'] == "Boxing"): ?>
                                    <?php $icon = str_replace(".svg", ".png", $icon); ?>
                                    <?php endif; ?>
                                    <img class="sports-icon" src="{{ url('/img/sports/Icon') }}-{{ icon }}" alt="" />
                                    <a href="{{url('/sport/open/')}}{{ss['sport_id']}}">{{
                                        ss['sport_name'] }} </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>

        </td>

    </tr>
    <tr>
        <td>
            <table class="football full-width">
                <th class="title" colspan="2">Popular Leagues</th>
                <?php foreach($topLeagues as $c): ?>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == $c['competition_id']) { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a href="{{ url('/competition/open/')}}{{ c['competition_id'] }}"
                                        style="padding-left: 0px">{{
                                        c['competition_name'] }} </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table class="football highlights full-width">
                <th class="title" colspan="2">Countries</th>
                <?php $count = 0; ?>
                <?php foreach($countries as $cc): ?>
                <?php $count++; ?>
                <tr class="menu">
                    <?php $keys = array_keys($cc); ?>
                    <?php $leagues = $cc[$keys[0]]; ?>
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == $keys[0]) { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40" rel="<?php echo $count; ?>" onclick="showLeagues(this)">
                                    <span style="padding-left: 0px">
                                        <?php echo $keys[0]; ?>
                                    </span>
                                    <!-- <span class="pull-right closed" id="union-down-<?php echo $count; ?>">
                                        <img src="{{ url('/img/union-down.svg') }}" alt="More" />
                                    </span>
                                    <span class="pull-right opened" id="union-up-<?php echo $count; ?>">
                                        <img src="{{ url('/img/union.svg') }}" alt="Less" />
                                    </span> -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="leagues-menu" id="country-<?php echo $count; ?>">
                    <td>
                        <table class="full-width">
                            <?php foreach($leagues as $league): ?>
                            <tr class="menu">
                                <td>
                                    <table class="full-width">
                                        <tr>
                                            <?php if(isset($selected) && $selected == $league['competition_id']) { ?>
                                            <td class="selected-menu-item">
                                            </td>
                                            <?php } ?>
                                            <td class="std-pd vh-40">
                                                <a class="country-league"
                                                    href="{{ url('/competition/open/')}}<?php echo $league['competition_id'] ?>">
                                                    <?php echo $league['competition_name']; ?>
                                                    </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>

        </td>
    </tr>
</table> <!-- end table .landing -->