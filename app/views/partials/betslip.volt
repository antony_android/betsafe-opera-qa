<script>
  if (window.operamini) {
    $.post('/lite/my-profile/useragent', function (data) {
      console.log("user agent set")
    });
  } else {
    $.post('/lite/my-profile/unsetagent', function (data) {
      console.log("user agent unset")
    });
  }
</script>
<?php $totalOdds=1; ?>
<table class="betslip full-width">
  <th style="padding: 0px 8px;" colspan="3">{{ this.flashSession.output() }}</th>
  <tr {% if session.get('user-agent') !='OperaMiniExtreme' %} class="title" {% else %} class="title-opera" {% endif %}>
    <td class="std-pd">
      Betslip
    </td>
    <td class="std-pd">
      {% if session.get('auth') == null %}
      <span class="pull-right">
        <a href="{{ url('/login') }}">Login</a> or <a href="{{ url('/signup') }}">Join Now</a>
      </span>
      {% else %}
      <span class="slip-loggedin" id="betslipUserBalance">
        KSH {{ balance }}
      </span>
      {% endif %}
    </td>
    <td style="padding-right: 8px;text-align: center;">
      <span>
        <a href="{{ url('/') }}">
          <img src="{{ url('/img/betslip-close.svg') }}" alt="Close" />
        </a>
      </span>
    </td>
  </tr>
  <tr class="slip-subtitle">
    <td colspan="5" class="std-pd">
      <table class="full-width">
        <tr>
          <td class="bold">
            Sport
          </td>
          <td>
            <table class="pull-right">
              <tr>
                <td class="betslip-hd">
                  <a href="{{ url('/betslip') }}">
                    <button class="bs-butt">
                      <table>
                        <tr>
                          <td>
                            <img src="{{ url('/img/betslip.svg') }}" alt="Betslip" />
                          </td>
                          <td style="padding-left: 4px;">
                            <span class="slip-counter">
                              {% if slipCount > 0 %}
                              {{ slipCount }}
                              {% else %}
                              0
                              {% endif %}
                            </span>
                          </td>
                        </tr>
                      </table>
                    </button>
                  </a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>

  <?php if(count($betslip) > 0): ?>
  <tr>
    <td colspan="5">
      {{ this.flashSession.output() }}
      <?php
        $matchCount = 0;
        $bonus = 0;
        $bonusOdds = 1;
      ?>
      <?php foreach($betslip as $bet): ?>
      <?php
        if (!$bet){continue;}
        $odd = $bet['odd_value'];

        if($bet['bet_pick']=='Home'){
            $pick = '1';
        }else if($bet['bet_pick']=='Draw'){
            $pick = 'X';
        }else if($bet['bet_pick']=='Away'){
            $pick = '2';
        }
        else{
            $pick = $bet['bet_pick'];
        }

        if($bet['odd_type']=='3 Way'){
            $oddType = '1X2 - FT';
        }else {
          $oddType = $bet['odd_type'];
        }

        $totalOdds = round($totalOdds*$odd,2);

      ?>
      <table class="bet">
        <tr>
          <td colspan="3">
            <table class="full-width slip-match">
              <?php if($matchCount == 0){ ?>
              <tr>
                <td class="slip-match-border" colspan="2"></td>
              </tr>
              <?php } ?>
              <tr>
                <td class="slip-td">
                  <?php if($bet['bet_type']=='live'){ ?>
                  <a href="{{url('/livematch/open/')}}<?php echo $bet['match_id'] ?>"
                    style="text-align: left;width: 100%;">
                    <?php } else { ?>
                    <a href="{{url('/match/open/')}}<?php echo $bet['match_id'] ?>"
                      style="text-align: left;width: 100%;">
                      <?php } ?>
                      <table class="full-width">
                        <tr class="game">
                          <td class="std-pd pb-5">
                            <?php
                        if($bet['away_team']=='na'){
                          echo $bet['home_team']; 
                          }
                          else{
                            echo $bet['home_team']." v ".$bet['away_team'];
                          }

                        ?>
                            {% if bet['bet_type'] == "live" %}
                            <span class="live-highlight">LIVE</span>
                            {% endif %}
                          </td>
                        </tr>
                        <tr class="pick">
                          <td class="std-pd" style="padding-top: 4px !important;">
                            <?php echo $oddType; ?>
                            <?php echo "(".$pick.")"; ?>
                            <?php if($bet['odd_value'] == 1){ ?>
                            <span style="color: #A20000;font-weight: normal;">
                              <?=$bet['market_status']; ?>
                            </span>
                            <?php } ?>
                          </td>
                          <td class="std-pd">
                            <table class="full-width">
                              <tr>
                                <td class="delete">
                                  <span>
                                    <?php echo number_format($bet['odd_value'], 2); ?>
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </a>
                </td>
                <td class="remove-match-slip">
                  <?php echo $this->tag->form("betslip/remove"); ?>
                  <input type="hidden" class="selected_stake" name="selected_stake" value="">
                  <input type="hidden" name="match_id" value="{{bet['match_id']}}">
                  <button class="remove-match" type="submit" value="submit">
                    <img src="{{ url('/img/bs-closer.svg') }}" alt="" />
                  </button>
                  </form>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="slip-match-border" colspan="2"></td>
        </tr>
      </table>
      <?php $matchCount++; ?>
      <?php endforeach; ?>

    </td>
  </tr>

  <tr class="details">
    <td colspan="3">
      <table class="full-width">
        <tr>
          <td colspan="2">
            <input type="checkbox" id="oddschange" name="oddschange" value="Yes" style="vertical-align: bottom;" {% if
              session.get('user-agent') !='OperaMiniExtreme' %} onchange="submitOddsChange()" {% endif %} {%if
              session.get('oddschange')=="Yes" %} checked {% endif %} checked>
            <label for="oddschange"> Accept Odds Change</label>
          </td>
        </tr>
        <tr>
          <td class="slip-stake std-pd" style="padding-top: 12px !important;">Stake</td>
        </tr>
        <tr>
          <td colspan="2" class="std-pd">
            {% if session.get('user-agent') !='OperaMiniExtreme' %}
            <div class="flag-div">
              <span style="color: #000;">KSH</span>
              <input type="number" id="stake_amount" class="stake" name="stake" onkeyup="updateWinning()"
                value="<?=($stake==0)?'':$stake; ?>" placeholder="Your stake amount">
            </div>
            {% else %}
            <form method="post" action="{{ url('/betslip/updatebutt') }}" style="display: inline;width:100%">
              <table class="full-width">
                <tr>
                  <td style="width: 30px;">
                    <span
                      style="color: #000;display: block;background: #FFF;height: 32px;padding: 8px 4px;box-sizing: border-box;">KSH</span>
                  </td>
                  <td>
                    <input type="number" id="stake_amount" class="stake" name="stake" value="{{ session.get('stake') }}"
                      placeholder="Stake Amount" style="border-radius:0px;width: 98%;padding: 8px 4px;">
                  </td>
                  <td>
                    <button type="submit" class="update-butt" style="width: 100%;">
                      Update
                    </button>
                  </td>
                </tr>
              </table>
            </form>
            {% endif %}
          </td>
        </tr>
        <tr>
          <td colspan="2" class="min-stake std-pd">Min Stake is 1</td>
        </tr>
        <?php foreach($freebets as $freebet): ?>
        {% if session.get('user-agent') !='OperaMiniExtreme' %}
        <tr>
          <td colspan="2">
            <div>
              <input class="freebet-radio" type="radio" id="{{ freebet['freebetId'] }}" name="freebets"
                value="{{ freebet['amount'] }}" onchange="loadFreebet(this.value, this.id)">
              <label for="{{ freebet['freebetId'] }}" class="freebet-label"> Use KSH {{ freebet['amount'] }} Free
                Bet</label><br>
            </div>
          </td>
        </tr>
        {% else %}
        <tr>
          <td colspan="2">
            <form method="POST" action="{{ url('/betslip/freebet') }}" id="freebetForm-{{freebet['freebetId']}}">
              <div>
                <input class="freebet-radio" type="radio" id="{{ freebet['freebetId'] }}" name="freebets"
                  value="{{ freebet['amount'] }}" onchange="loadFreebet(this.value, this.id)">
                <label for="{{ freebet['freebetId'] }}" class="freebet-label"> Use KSH {{ freebet['amount'] }} Free
                  Bet</label><br>
              </div>
            </form>
          </td>
        </tr>
        {% endif %}
        <?php endforeach; ?>
        <tr>
          <td colspan="2" class="slip-stake-sel std-4pd">
            <table class="full-width">
              <tr>
                <td style="width: 25%;">
                  {% if session.get('user-agent') !='OperaMiniExtreme' %}
                  <button class="but-height" onclick="fiftyStake()" id="fiftyBobButt">
                    +50
                  </button>
                  {% else %}
                  <form method="post" action="{{ url('/betslip/fiftystake') }}" style="display: inline;">
                    <button type="submit" id="fiftyBobButt" class="but-height">
                      +50
                    </button>
                  </form>
                  {% endif %}
                </td>
                <td style="width: 25%;">
                  {% if session.get('user-agent') !='OperaMiniExtreme' %}
                  <button onclick="hundredStake()" id="hundredBobButt" class="but-height">
                    +100
                  </button>
                  {% else %}
                  <form method="post" action="{{ url('/betslip/hundredstake') }}" style="display: inline;">
                    <button type="submit" id="hundredBobButt" class="but-height">
                      +100
                    </button>
                  </form>
                  {% endif %}
                </td>
                <td style="width: 25%;">
                  {% if session.get('user-agent') !='OperaMiniExtreme' %}
                  <button onclick="twoHundredStake()" id="twoHundredBobButt" class="but-height">
                    +200
                  </button>
                  {% else %}
                  <form method="post" action="{{ url('/betslip/twohundredstake') }}" style="display: inline;">
                    <button type="submit" id="twoHundredBobButt" class="but-height">
                      +200
                    </button>
                  </form>
                  {% endif %}
                </td>
                <td style="width: 25%;">
                  {% if session.get('user-agent') !='OperaMiniExtreme' %}
                  <button onclick="fiveHundredStake()" id="fiveHundredBobButt" class="but-height">
                    +500
                  </button>
                  {% else %}
                  <form method="post" action="{{ url('/betslip/fivehundredstake') }}" style="display: inline;">
                    <button type="submit" id="fiveHundredBobButt" class="but-height">
                      +500
                    </button>
                  </form>
                  {% endif %}
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="std-pd">Odds</td>
          <td class="text-right std-pd" id="total_odds">
            <?= number_format(round($totalOdds, 2), 2); ?>
          </td>
        </tr>
        <tr>
          <td class="std-pd">Total Stake</td>
          <?php $netStake = $stake/1.075; ?>
          <td class="text-right std-pd" id="total_stake">
            <?= "KSH ".number_format(round($stake), 2); ?>
          </td>
        </tr>
        <?php $boostValue = 0; ?>
        {% if slipCount > 1 %}
        <tr id="multibetBoost" style="color:#f5a623;">
          <td class="std-pd">Multibet Boost {% if boost > 0 %} (<span id="boostPercent">{{ boost }}</span>%) {%endif%}
          </td>
          <td class="text-right std-pd">
            {% if boost > 0 %} <img src="{{ url('/img/icon_arrow_up_bonus.png') }}" alt="" style="width: 15px;" />
            {%endif%}
            KSH <span id="boostValue">
              <?php $boostValue = $netStake * $totalOdds * ($boost/100); ?>
              <?= number_format(round($boostValue, 2),2); ?>
            </span>
          </td>
        </tr>
        {% endif %}
        <tr class="payout">
          <td class="std-pd">Payout <span id="expandSlip" onclick="toggleSlip()">
              <img style="margin-left: 4px;" src="{{ url('/img/slip-arrow-down.svg') }}" alt="More" />
            </span>
          </td>
          <td class="text-right std-pd" id="net_win_id">
            <?= "KSH ".number_format(round((($totalOdds*$netStake) - $totalOdds*$netStake*0.2) ,2));  ?>
          </td>
        </tr>
        <tr id="excise_row">
          <td class="std-pd">Excise Tax (7.5%)</td>
          <td class="text-right std-pd" id="excise-tax">
            <?= "KSH ".number_format(round(($stake - $netStake), 2),2); ?>
          </td>
        </tr>
        <tr id="grosswin_row">
          <td class="std-pd">Full Stake Winnings</td>
          <td class="text-right std-pd" id="possible_win_id">
            <?= "KSH ".number_format(round($totalOdds*$netStake,2) + $boostValue); ?>
          </td>
        </tr>
        <tr id="tax_row">
          <td class="std-pd">Withholding Tax 20%</td>
          <td class="text-right std-pd" id="tax_id">
            <?= "KSH ".number_format(round((($totalOdds*$netStake) + $boostValue -$stake)*0.2, 2));  ?>
          </td>
        </tr>
        <tr>
          <td class="std-pd" colspan="5">
            {% if session.get('auth') != null %}
            <?php echo $this->tag->form("/betslip/placebet"); ?>
            <input type="hidden" id="stakeAmt" name="stakeAmt" {% if session.get('user-agent')=='OperaMiniExtreme' %}
              value="{{ session.get('stake') }}" {% endif %}>
            <input type="hidden" name="src" value="mobile">
            <input type="hidden" id="freebetField" name="freebet" <?php if(isset($_GET['freebet'])): ?>
            value="
            <?php echo $_GET['freebet']; ?>"
            <?php else: ?>
            value="0"
            <?php endif; ?>>
            <input type="hidden" id="oddschangeaccept" name="oddschangeaccept"
              value="{{ session.get('oddschange') }}" />
            <input type="hidden" id="total_odd_m" name="total_odd" value="<?php echo $totalOdds; ?>">
            <button type="submit" class="place mb-0" id="place_bet_butt" {% if
              session.get('user-agent')=='OperaMiniExtreme' %} style="background: #01B601;border-color: #01B601;" {%
              endif %}>Place Bet</button>
            </form>
            {% else %}
            <form method="post" action="{{url('login')}}" style="width: 100%;">
              <input type="hidden" name="ref" value="betslip" />
              <button class="login-join mb-0" type="submit">
                Login to Place Bet
              </button>
            </form>
            {% endif %}
          </td>
        </tr>
        <tr class="clear-slip">
          <td class="" colspan="5">
            <?php echo $this->tag->form("betslip/clearslip"); ?>
            <input type="hidden" name="src" value="mobile">
            <button type="submit" class="delete-all">Clear Betslip</button>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <?php else: ?>
  <tr class="empty-slip-row">
    <td class="" colspan="5">
      <img src="{{ url('img/logo.svg') }}" width="90px" alt="BetSafe" />
      <span>Betslip is empty. Tap on the odds to add them to your Betslip</span>
    </td>
  </tr>
  <?php endif; ?>
</table>

<script type="text/javascript">

  document.getElementById('excise_row').style.display = 'none';
  document.getElementById('grosswin_row').style.display = 'none';
  document.getElementById('tax_row').style.display = 'none';

  function updateWinning() {

    let nf = new Intl.NumberFormat('en-US', { minimumFractionDigits: 2 });
    var bamount = document.getElementById("stake_amount").value;
    for (let el of document.getElementsByClassName('selected_stake')) el.value = bamount;

    var placeBetButt = document.getElementById('place_bet_butt');
    if (placeBetButt) {
      if (bamount.length == 0 || bamount == 0) {
        bamount = 0;
        placeBetButt.style.background = '#616161';
        placeBetButt.style.border = '1px solid #616161';
      } else {
        placeBetButt.style.background = '#01B601';
        placeBetButt.style.border = '1px solid #01B601';
      }
    }
    bamount = parseInt(bamount);
    var boostValue = 0.00;
    var boostPercent = 0;
    var total_odd = document.getElementById("total_odd_m").value;
    var exciseTax = bamount - ((bamount / 107.5) * 100);
    var stakeAfterTax = parseInt(bamount) - exciseTax.toFixed(2);
    var raw_win = bamount * total_odd;
    var gross_win = stakeAfterTax * total_odd;
    var boostPercentControl = document.getElementById("boostPercent");

    if (boostPercentControl != null) {
      boostPercent = boostPercentControl.innerText;
    }

    if (boostPercent > 0) {
      boostValue = (boostPercent / 100) * gross_win;
      document.getElementById("boostValue").innerHTML = nf.format(boostValue.toFixed(2));
      gross_win = gross_win + boostValue;
    }

    var js_tax = (gross_win - stakeAfterTax) * 0.2;
    var net_wi = gross_win - js_tax;
    var poss_id = document.getElementById("possible_win_id");
    poss_id.innerHTML = "KSH " + nf.format(gross_win.toFixed(2));

    var tax_id = document.getElementById("tax_id")
    tax_id.innerHTML = "KSH " + nf.format(js_tax.toFixed(2))

    var excise_tax = document.getElementById("excise-tax")
    excise_tax.innerHTML = "KSH " + nf.format(exciseTax.toFixed(2))

    var total_stake = document.getElementById("total_stake");
    total_stake.innerHTML = "KSH " + nf.format(bamount.toFixed(2))
    document.getElementById("stakeAmt").value = "KSH " + nf.format(bamount.toFixed(2))

    var new_win_id = document.getElementById("net_win_id")
    net_win_id.innerHTML = "KSH " + nf.format(net_wi.toFixed(2));
  }

  function toggleSlip() {
    if (document.getElementById('excise_row').style.display == 'none') {

      document.getElementById('excise_row').style.display = '';
      document.getElementById('grosswin_row').style.display = '';
      document.getElementById('tax_row').style.display = '';
      document.getElementById('expandSlip').innerHTML = '<img style="margin-left: 4px;vertical-align: text-bottom;" src="/lite/img/slip-arrow-up.svg" alt="Less" />';
    } else {
      document.getElementById('excise_row').style.display = 'none';
      document.getElementById('grosswin_row').style.display = 'none';
      document.getElementById('tax_row').style.display = 'none';
      document.getElementById('expandSlip').innerHTML = '<img style="margin-left: 4px;" src="/lite/img/slip-arrow-down.svg" alt="More" />';
    }
  }

  function showMpesaForm() {
    var tg = document.getElementById("tigopesa");
    tg.style.display = 'none';
    var am = document.getElementById("airtelmoney")
    am.style.display = 'none';
    var mp = document.getElementById("mpesa");
    mp.style.display = 'block';
    return false;
  }

  function fiftyStake() {


    var existing = document.getElementById("stake_amount").value;
    if (existing.length == 0) {
      existing = 0;
    }
    var newValue = parseInt(existing) + 50;
    document.getElementById("stake_amount").value = newValue;
    updateWinning();
    document.getElementById("fiftyBobButt").style.background = '#A20000';
    document.getElementById("fiftyBobButt").style.border = '1px solid #A20000';
    document.getElementById("hundredBobButt").style.background = '#2E2F30';
    document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
    document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function hundredStake() {
    var existing = document.getElementById("stake_amount").value;
    if (existing.length == 0) {
      existing = 0;
    }
    var newValue = parseInt(existing) + 100;
    document.getElementById("stake_amount").value = newValue;
    updateWinning();
    document.getElementById("hundredBobButt").style.background = '#A20000';
    document.getElementById("hundredBobButt").style.border = '1px solid #A20000';
    document.getElementById("fiftyBobButt").style.background = '#2E2F30';
    document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
    document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function twoHundredStake() {
    var existing = document.getElementById("stake_amount").value;
    if (existing.length == 0) {
      existing = 0;
    }
    var newValue = parseInt(existing) + 200;
    document.getElementById("stake_amount").value = newValue;
    updateWinning();
    document.getElementById("twoHundredBobButt").style.background = '#A20000';
    document.getElementById("twoHundredBobButt").style.border = '1px solid #A20000';
    document.getElementById("fiftyBobButt").style.background = '#2E2F30';
    document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("hundredBobButt").style.background = '#2E2F30';
    document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function fiveHundredStake() {
    var existing = document.getElementById("stake_amount").value;
    if (existing.length == 0) {
      existing = 0;
    }
    var newValue = parseInt(existing) + 500;
    document.getElementById("stake_amount").value = newValue;
    updateWinning();
    document.getElementById("fiveHundredBobButt").style.background = '#A20000';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid #A20000';
    document.getElementById("fiftyBobButt").style.background = '#2E2F30';
    document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("hundredBobButt").style.background = '#2E2F30';
    document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
    document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function pollBalance() {

    $.post('my-profile/balance', function (data) {

      try {
        data = JSON.parse(data);
      } catch (e) {
        return false;
      }
      if (data.ResultCode == 1) {

        if (typeof data.Balance.Amount === "number") {
          $('#betslipUserBalance').html("KSH " + data.Balance.Amount.toFixed(2));
        } else {
          $('#betslipUserBalance').html("KSH 0.00");
        }
        return true;
      }
    });

    setTimeout(pollBalance, 10000);
  }

  $(document).ready(function () {
    pollBalance();
  });

  function loadFreebet(freebet, freebetID) {

    if (window.operamini) {

      document.getElementById("freebetsForm-" + freebetID).submit();
    } else {

      document.getElementById("stake_amount").value = freebet;
      $("#stake_amount").prop('readonly', true);
      $("#freebetField").val(freebetID);
      updateWinning();
    }
  }

  function submitOddsChange() {

    let valueSelected = "No";
    if ($('#oddschange').is(":checked")) {
      valueSelected = "Yes";
    }
    $("#oddschangeaccept").val(valueSelected);
  }

</script>

<style>
  input[type='radio']:after {

    background-color: #262626;
  }
</style>