<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title">
            How to play
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                Placing a bet is simple. Once you are logged in to your account and have browsed through the various
                betting options (either the Sports or the Live from the Left Menu):
            </p>
            <ol class="pb-16">
                <li>Click on the odds according to your selection.</li>
                <li>In your bet slip, enter the amount of money you would like to use on this bet.</li>
                <li>Click on Place bet/Send.</li>
            </ol>
        </td>
    </tr>
</table>