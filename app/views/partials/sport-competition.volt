<table class="football">
    <th class="title" colspan="2">{{sport_name}} - Top Competitions</th>
    <?php foreach($top_competitions as $competition): ?>
    <tr class="menu">
        <td class="text"><a href="{{url('competition?id=')}}{{ competition['competition_id'] }}">{{ competition['competition_name'] }} <span style="float:right; margin-right:5px">{{competition['games_count']}}</span></a></td>
    </tr>
    <?php endforeach; ?>
</table>
<table class="football highlights">
    <th class="title" colspan="2">{{sport_name}} Competitions (A-Z)</th>
    <?php foreach($competitions as $comp): ?>
    <tr class="menu">
        <td class="text"><a href="{{url('competition?id=')}}{{comp['competition_id']}}">{{comp['category']}}, {{comp['competition_name']}} <span style="float:right; margin-right:5px">{{comp['games_count']}}</span></a></a></td>
    </tr>
<?php endforeach; ?>
</table>
