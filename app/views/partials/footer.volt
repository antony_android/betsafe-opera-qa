<table id="footer">

    <tr>
        <td class="matches-border"></td>
    </tr>
    {% if session.get('auth') == null %}
    <?php if($_SERVER['REQUEST_URI'] != '/lite/signup'): ?>
    <tr>
        <td class="std-pd pt-8">
            <a href="{{ url('/signup') }}">
                <button class="login-join join-now-footer">
                    Join Now
                </button>
            </a>
        </td>
    </tr>
    <?php endif; ?>
    {% endif %}
    <tr class="top">
        <td colspan="2" class="std-pd pt-10 pb-12">
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <button class="bck-to-top" onclick="topFunction()">
                Back to Top
            </button>
            {% else %}
            <?php if(strpos($_SERVER['REQUEST_URI'], '?') !== false): ?>
            <a href="/lite/<?php echo $_SERVER['REQUEST_URI'].'/1';?>">
                <button class="bck-to-top" onclick="topFunction()">
                    Back to Top
                </button>
            </a>
            <?php else: ?>
            <form method="get" action="/lite/<?php echo $_SERVER['REQUEST_URI']; ?>">
                <button type="submit" class="bck-to-top">
                    Back to Top
                </button>
            </form>
            <?php endif; ?>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td class="std-pd" colspan="2">
            <table class="full-width">
                <tr>
                    <td class="footer-logo">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- <tr>
        <td class="std-pd">
            <table class="full-width">
                <tr>
                    <td class="sponsor-gor">
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td class="sponsor-bd">
                                    <p class="sponsor-title">OFFICIAL BRAND SPONSOR</p>
                                    <p class="sponsor-txt">Gor Mahia<br />
                                        Football Club</p>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td class="sponsor-afc">
                    </td>
                    <td class="pt-12">
                        <table>
                            <tr>
                                <td class="sponsor-bd">
                                    <p class="sponsor-title">OFFICIAL BRAND SPONSOR</p>
                                    <p class="sponsor-txt">AFC Leopards<br />
                                        Football Club</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr> -->
    <tr>
        <td colspan="2" class="std-pd pv-8">
            <p class="about-footer">
                Betsafe is a leading international betting brand with 14 years of experience and a strong passion for
                sports. <span id="readMore" onclick="readMore()" class="about-rdmore"><u>Read more</u></span>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="ft-separator std-pd pv-8">
            <table class="full-width">
                <tr>
                    <td class="sms-cell std-pd">
                        <p class="sms-content">
                            SMS "Join" To
                        </p>
                        <p class="sms-box">
                            23333
                        </p>
                    </td>
                    <td>
                        <p class="sms-content">
                            MPESA Paybill
                        </p>
                        <p class="sms-box">
                            7290099
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="std-pd pv-8">
            <table class="full-width">
                <tr>
                    <td class="sms-cell">
                        <p class="sms-box">
                            Customer Service
                        </p>
                        <p class="cs-content">
                            Need to speak to us? Reach out day or night! Our 24/7 customer care agents are here to help
                            in any way possible to ensure your happiness at all times.
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="std-pd">
            <a href="tel:0111040000">
                <button class="cs-tel">
                    0111040000
                </button>
            </a>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="sp-menu-ft pt-8">
            <p class="ft-details" onclick="toggleSportDetails()">
                Sports <span id="sportDetailsSpan">
                    <img src="{{ url('/img/plus.svg') }}" alt="+" />
                </span>
            </p>
            <ul id="sportDetails" class="footer-menu">
                <li>
                    <a href="{{ url('/') }}">Bet Now</a>
                </li>
                <li>
                    <a href="{{ url('/live') }}">Live Now</a>
                </li>
                <li>
                    <a href="{{ url('/bettingrules') }}">Sports Betting Rules</a>
                </li>
                <li>
                    <a href="{{ url('/promotions') }}">Promotions</a>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="sp-menu-ft">
            <p class="ft-details" onclick="toggleHelpDetails()">
                Help <span id="helpDetailsSpan">
                    <img src="{{ url('/img/plus.svg') }}" alt="+" />
                </span>
            </p>
            <ul id="helpDetails" class="footer-menu">
                <li>
                    <a href="https://support.betsafe.co.ke/">Customer Care</a>
                </li>
                <li>
                    <a href="{{ url('/responsible') }}">Responsible Gaming</a>
                </li>
                <li>
                    <a href="{{ url('/howtoplay') }}">How to Play</a>
                </li>
                <li>
                    <a href="{{ url('/faq') }}">FAQ</a>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="sp-menu-ft">
            <p class="ft-details" onclick="togglePaymentDetails()">
                Company <span id="paymentDetailsSpan">
                    <img src="{{ url('/img/plus.svg') }}" alt="+" />
                </span>
            </p>
            <ul id="paymentDetails" class="footer-menu">
                <li>
                    <a href="{{ url('/deposit') }}">Payment Options</a>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="std-pd">
            <table class="mt-10">
                <tr>
                    {% if session.get('user-agent') != 'OperaMiniExtreme' %}
                    <td>
                        <a href="https://www.instagram.com/betsafe_kenya/" target="_blank" class="social-footer va-b">
                            <img src="/lite/img/social/Icon-Instagram.svg" alt="Instagram" class="mt-10">
                        </a>
                        <a href="https://twitter.com/Betsafe_KE" target="_blank" class="social-footer">
                            <svg viewBox="0 0 512 512">
                                <path
                                    d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z" />
                            </svg>
                        </a>
                        <a href="https://www.facebook.com/BetsafeKe/" target="_blank" class="social-footer">
                            <svg viewBox="0 0 512 512">
                                <path
                                    d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z" />
                            </svg>
                        </a>
                    </td>
                    {% else %}
                    <td>
                        <a href="https://www.instagram.com/betsafe_kenya/" target="_blank" class="social-footer va-b">
                            <img src="/lite/img/social/Icon-Instagram.svg" alt="Instagram" class="mt-10">
                        </a>
                    </td>
                    <td>
                        <a href="https://twitter.com/Betsafe_KE" target="_blank" class="social-footer">
                            <svg viewBox="0 0 512 512">
                                <path
                                    d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z" />
                            </svg>
                        </a>
                    </td>
                    <td>
                        <a href="https://www.facebook.com/BetsafeKe/" target="_blank" class="social-footer">
                            <svg viewBox="0 0 512 512">
                                <path
                                    d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z" />
                            </svg>
                        </a>
                    </td>
                    {% endif %}
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="std-pd">
            <table>
                <tr>
                    <td class="mpesa-ft">
                    </td>
                    <td class="safaricom-ft">
                    </td>
                    <td class="footer-18">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="std-pd">
            <p class="cs-content">
                Gambling is addictive. Please play responsibly.
            </p>
            <p class="cs-content cs-middle-pad">
                Bet High Ltd. the proprietor and operator of this website is licensed and regulated by the Betting
                Control and Licensing Board of Kenya (BCLB). Public Gaming License #0000119 and Bookmakers License
                #0000170.
            </p>
            <p class="cs-content">
                All rights reserved. &copy; Betsafe 2017-2021
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="std-pd pv-8">
            <p class="footer-terms">
                <a href="{{ url('/terms') }}" class="tcc" {% if session.get('user-agent')=='OperaMiniExtreme' %}
                    style="font-size: smaller;" {% endif %}>
                    Terms & Conditions
                </a>
                <a href="{{ url('/privacy') }}" {% if session.get('user-agent')!='OperaMiniExtreme' %}
                    style="margin-left: 10px;" {% else %} style="margin-left: 10px;font-size: smaller;" {% endif %}>
                    Privacy Policy
                </a>
            </p>

        </td>
    </tr>

</table>
<script>
    document.getElementById('sportDetails').style.display = 'none';
    document.getElementById('helpDetails').style.display = 'none';
    document.getElementById('paymentDetails').style.display = 'none';
    document.getElementById('sportDetailsSpan').style.lineHeight = '0.6em';
    document.getElementById('helpDetailsSpan').style.lineHeight = '0.6em';
    document.getElementById('paymentDetailsSpan').style.lineHeight = '0.6em';

    function toggleSportDetails() {
        if (document.getElementById('sportDetails').style.display == 'none') {
            document.getElementById('sportDetails').style.display = '';
            document.getElementById('sportDetailsSpan').innerHTML = '<img src="/lite/img/cancel-x.svg" alt="X" />';
            document.getElementById('sportDetailsSpan').style.fontSize = '14px';
            document.getElementById('sportDetailsSpan').style.lineHeight = '';
        } else {
            document.getElementById('sportDetails').style.display = 'none';
            document.getElementById('sportDetailsSpan').innerHTML = '<img src="/lite/img/plus.svg" alt="+" />';
            document.getElementById('sportDetailsSpan').style.fontSize = '23px';
            document.getElementById('sportDetailsSpan').style.lineHeight = '0.6em';
        }
    }

    function toggleHelpDetails() {
        if (document.getElementById('helpDetails').style.display == 'none') {
            document.getElementById('helpDetails').style.display = '';
            document.getElementById('helpDetailsSpan').innerHTML = '<img src="/lite/img/cancel-x.svg" alt="X" />';
            document.getElementById('helpDetailsSpan').style.fontSize = '14px';
            document.getElementById('helpDetailsSpan').style.lineHeight = '';
        } else {
            document.getElementById('helpDetails').style.display = 'none';
            document.getElementById('helpDetailsSpan').innerHTML = '<img src="/lite/img/plus.svg" alt="+" />';
            document.getElementById('helpDetailsSpan').style.fontSize = '23px';
            document.getElementById('helpDetailsSpan').style.lineHeight = '0.6em';
        }
    }

    function togglePaymentDetails() {
        if (document.getElementById('paymentDetails').style.display == 'none') {
            document.getElementById('paymentDetails').style.display = '';
            document.getElementById('paymentDetailsSpan').innerHTML = '<img src="/lite/img/cancel-x.svg" alt="X" />';
            document.getElementById('paymentDetailsSpan').style.fontSize = '14px';
            document.getElementById('paymentDetailsSpan').style.lineHeight = '';
        } else {
            document.getElementById('paymentDetails').style.display = 'none';
            document.getElementById('paymentDetailsSpan').innerHTML = '<img src="/lite/img/plus.svg" alt="+" />';
            document.getElementById('paymentDetailsSpan').style.fontSize = '23px';
            document.getElementById('paymentDetailsSpan').style.lineHeight = '0.6em';
        }
    }

</script>