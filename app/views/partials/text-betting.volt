<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title">
            Text Betting
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16 pt-8 bold">
                Ekelea na Game ID
            </p>
            <p class="pb-16">
                Place your bets easily via SMS through game IDs.
            </p>
            <p class="pb-16 bold">
                Game ID Description
            </p>
            <p class="pb-16">
                Get the best betting experience on SMS via Game IDs in three easy steps:
            </p>
            <ol class="pb-16">
                <li>Select the game(s) you want through the Game ID e.g 1426 followed by # and your Stake</li>
                <li>Send to 23333</li>
                <li>Your bet will be automatically placed</li>
            </ol>
            <p class="pb-16 bold">
                Register & Deposit via SMS
            </p>
            <p class="pb-16">
                To register via SMS, simply send BETSAFE or JOIN to 23333<br />
                To deposit, just send ‘Deposit+Amount’ to 23333. You will receive a prompt to enter your M-Pesa PIN
            </p>
            <p class="pb-16 bold">
                SMS Betting & Markets
            </p>
            <p class="pb-16 bold">
                Jackpots
            </p>
            <p class="pb-16 bold">
                Play the jackpot via SMS
            </p>
            <p class="pb-16">
                <span class="bold">Daily Jackpot:</span> Send djp#games selection to 23333. e.g djp#12121212x Or
                djp#auto
                <br />
                <span class="bold">Middle Jackpot:</span> Send mjp#games selection to 23333. e.g mjp#12121212x1x1212 Or
                mjp#auto<br />
                <span class="bold">Super Jackpot:</span> Send sjp#games selection to 23333. e.g sjp#12121212x1x1212xx Or
                sjp#auto
            </p>
            <p class="pb-16 bold">
                To get Jackpot Games, send:
            </p>
            <p class="pb-16">
                <span class="bold">DJP GAMES</span> to 23333<br />
                <span class="bold">MJP GAMES</span> to 23333<br />
                <span class="bold">SJP GAMES</span> to 23333
            </p>
            <p class="pb-16 bold">
                How to play sportsbook via SMS
            </p>
            <p class="pb-16 bold">
                Markets
            </p>
            <p class="pb-16">
                Place a bet on your favourite teams via SMS through game IDs. You can now play on Full time markets;
                1X2, Double Chance
                (DC1X, DCX2, DC12), Correct Score (CS) & Both Teams To Score (GG or NG).<br /><br />
                You can also place bets on half-time markets; Home win (HT1 or FH1), Draw (XH1 or FH1), Away win (HT2 or
                FH2), Over
                (HT#O25 or FH#O25), Under (HT#U25 or FH#U25), Correct score (CS#Scoreline or FH#Scoreline) & Both Teams
                to Score (HT#GG
                or FH#GG & HT#NG or FH#NG)
            </p>
            <p class="pb-16 bold">
                How to place a bet
            </p>
            <p class="pb-16 bold">
                Single Bet
            </p>
            <p class="pb-16">
                Send the Game ID#Selection#Amount to 23333 e.g 2345#1#50
            </p>
            <p class="pb-16 bold">
                Multi-bet
            </p>
            <p class="pb-16">
                Send the Game ID#Selection#Game ID#Selection#Amount to 23333 e.g 2345#1#3321#X#1123#GG#50
            </p>
            <p class="pb-16">
                If you have any further queries please check out our FAQS or contact our Customer Care team.
            </p>
        </td>
    </tr>
</table>