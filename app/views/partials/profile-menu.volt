<table class="landing full-width">
    <tr>
        <td>
            <table class="football full-width">
                <tr class="menu" style="border: 0;">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'deposit') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40 full-width">
                                    <a class="pl-0 full-width" href="{{ url('/deposit') }}">Deposit</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'withdraw') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/withdraw') }}">Withdraw</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'history') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/bet-history') }}">Bet History</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'jackpot-history') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/jackpot/history') }}">Jackpot History</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'transactions') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/transactions') }}">Transactions</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'messages') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/messages') }}">Messages {% if
                                        notifications > 0 %} ({{
                                        notifications }}) {% endif %}</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'bonuses') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/bonuses') }}">Bonuses {% if freebetscount
                                        > 0 %} ({{
                                        freebetscount }}) {% endif %}</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'change-password') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/change-password') }}">Change Password</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'my-profile') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/my-profile') }}">My Profile</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'resp-gaming') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/responsible') }}">Responsible Gaming</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'help') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" target="_blank"
                                        href="https://support.betsafe.co.ke/">Help</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'logout') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/logout') }}">Logout</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>