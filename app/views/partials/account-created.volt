<table class="login" cellpadding="0" cellspacing="0">
    <tr class="acc-created-row">
        <td class="game-title std-pd">
            Account Created
        </td>
    </tr>
    <tr>
        <td class="acc-created-details std-pd">
            Welcome to Betsafe, home of great Sportsbook & Live Betting. Make a deposit to start betting.
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 6px;" class="std-pd">
            <a href="/deposit">
                <button class="login-join join-now-footer">
                    Make a deposit
                </button>
            </a>
        </td>
    </tr>
    <tr>
        <td class="acc-created-border"></td>
    </tr>
</table>