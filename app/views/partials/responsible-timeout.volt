<table class="full-width" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="page-header-row bet-history-header">
        <td class="game-title">
            Responsible Gaming
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td>
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="respGamingControl" id="respGamingControl"
                class="form-control slip-filter-select" value="General Information"
                onclick="toggleRespGamingFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img"
                onclick="toggleRespGamingFilter()" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="general-list-item">
                    <a href="{{ url('/responsible') }}">
                        General Information
                    </a>
                </li>
                <li id="limits-list-item">
                    <a href="{{ url('/responsible/limits') }}">
                        Budget Limits
                    </a>
                </li>
                <li id="timeout-list-item">
                    <a href="{{ url('/responsible/timeout') }}">
                        Time Out
                    </a>
                </li>
                <li id="exclusion-list-item">
                    <a href="{{ url('/responsible/exclusion') }}">
                        Self Exclusion
                    </a>
                </li>
            </ul>
            {% else %}
            <form method="post" action="{{ url('/responsible/filter') }}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
                    <option value="general" {% if session.get('selected-responsible')=='' %} selected {% endif %}>
                        General Information</option>
                    <option value="limits" {% if session.get('selected-responsible')=='limits' %} selected {% endif %}>
                        Budget Limits</option>
                    <option value="timeout" {% if session.get('selected-responsible')=='timeout' %} selected {% endif
                        %}>Time Out</option>
                    <option value="exclusion" {% if session.get('selected-responsible')=='exclusion' %} selected {%
                        endif %}>Self Exclusion</option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-10">
            Time-Out
        </td>
    </tr>
    <tr class="input">
        <td>
            <?php echo $this->tag->form("/responsible/addtimeout"); ?>
            <table class="form" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <label for="timeoutPeriod">
                            Time-Out Period
                        </label>
                        {% if session.get('user-agent') != 'OperaMiniExtreme' %}
                        <img src="{{ url('/img/down-timeout.svg') }}" alt="" class="timeout-filter-img"
                            onclick="toggleTimeOutFilter()" />
                        <input type="text" readonly name="timeoutPeriod" id="timeoutPeriod"
                            class="form-control timeout-filter-select" value="None" onclick="toggleTimeOutFilter()" />
                        <ul class="slip-filter-ul timeout-ul-top" id="timeout-filter-ul">
                            <li id="none-list-item" onclick="noneClick()">
                                None
                            </li>
                            <li id="twentyfour-list-item" onclick="twentyFourClick()">
                                24 Hours
                            </li>
                            <li id="fortyeight-list-item" onclick="fortyEightClick()">
                                48 Hours
                            </li>
                            <li id="oneweek-list-item" onclick="oneWeekClick()">
                                1 Week
                            </li>
                        </ul>
                        {% else %}
                        <select name="timeoutPeriod" id="timeoutPeriod" class="form-control">
                            <option value="None">None</option>
                            <option value="24 Hours">24 Hours</option>
                            <option value="48 Hours">48 Hours</option>
                            <option value="1 Week">1 Week</option>
                        </select>
                        {% endif %}
                        <?php if(count($setTimeout) == 0) { ?>
                        <span class="min-chars">No Time-Out Period Set</span>
                        <p class="depo-mpesa-details pt-16">
                            By opting to Time-Out this will disable the Betslip and you will not be able to Deposit into
                            your account for the chosen
                            time period. You will not be able to remove it once it is set.
                        </p>
                        <?php } else { ?>
                        <p class="depo-mpesa-details pt-16 pb-16">
                            You have chosen to Time-Out for a period of
                            <?=$setTimeout; ?>.
                            Time remaining
                            <?=$timeRemaining; ?>.
                        </p>
                        <p class="depo-mpesa-details">
                            Betslip and Deposit has been disabled.
                        </p>
                        <?php } ?>
                    </td>
                </tr>
                <tr class="input">
                    <td>
                        <button type="submit" class="set-resp-limits" id="setButton">
                            Set
                        </button>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                If you need further information or help please contact our Customer Care department.
            </p>
        </td>
    </tr>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    document.getElementById('timeout-filter-ul').style.display = 'none';

    function toggleTimeOutFilter() {

        if (document.getElementById('timeout-filter-ul').style.display == 'none') {
            document.getElementById('timeout-filter-ul').style.display = '';
        } else {
            document.getElementById('timeout-filter-ul').style.display = 'none';
        }
    }

    function toggleRespGamingFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }
</script>
<?php if($selectedRespControl == 'general'): ?>
<script>
    document.getElementById('general-list-item').classList.add("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "General Information";
</script>
<?php elseif($selectedRespControl == 'limits'): ?>
<script>
    document.getElementById('limits-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Budget Limits";
</script>
<?php elseif($selectedRespControl == 'timeout'): ?>
<script>
    document.getElementById('timeout-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Time-Out";
</script>
<?php elseif($selectedRespControl == 'exclusion'): ?>
<script>
    document.getElementById('exclusion-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Self-Exclusion";
</script>
<?php endif; ?>
<?php if($selectedTimeoutPeriod == 'None'): ?>
<script>
    document.getElementById('none-list-item').classList.add("slip-filter-selected");
    document.getElementById('twentyfour-list-item').classList.remove("slip-filter-selected");
    document.getElementById('fortyeight-list-item').classList.remove("slip-filter-selected");
    document.getElementById('oneweek-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeoutPeriod').value = "None";
</script>
<?php elseif($selectedTimeoutPeriod == '24 Hours'): ?>
<script>
    document.getElementById('twentyfour-list-item').classList.add("slip-filter-selected");
    document.getElementById('none-list-item').classList.remove("slip-filter-selected");
    document.getElementById('fortyeight-list-item').classList.remove("slip-filter-selected");
    document.getElementById('oneweek-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeoutPeriod').value = "24 Hours";
</script>
<?php elseif($selectedTimeoutPeriod == '48 Hours'): ?>
<script>
    document.getElementById('twentyfour-list-item').classList.remove("slip-filter-selected");
    document.getElementById('none-list-item').classList.remove("slip-filter-selected");
    document.getElementById('fortyeight-list-item').classList.add("slip-filter-selected");
    document.getElementById('oneweek-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeoutPeriod').value = "48 Hours";
</script>
<?php elseif($selectedTimeoutPeriod == '1 Week'): ?>
<script>
    document.getElementById('twentyfour-list-item').classList.remove("slip-filter-selected");
    document.getElementById('none-list-item').classList.remove("slip-filter-selected");
    document.getElementById('fortyeight-list-item').classList.remove("slip-filter-selected");
    document.getElementById('oneweek-list-item').classList.add("slip-filter-selected");
    document.getElementById('timeoutPeriod').value = "1 Week";
</script>
<?php endif; ?>

<script>
    function noneClick() {
        document.getElementById('none-list-item').classList.add("slip-filter-selected");
        document.getElementById('twentyfour-list-item').classList.remove("slip-filter-selected");
        document.getElementById('fortyeight-list-item').classList.remove("slip-filter-selected");
        document.getElementById('oneweek-list-item').classList.remove("slip-filter-selected");
        document.getElementById('timeoutPeriod').value = "None";
        document.getElementById('timeout-filter-ul').style.display = 'none';
        document.getElementById("setButton").focus();
    }

    function twentyFourClick() {
        document.getElementById('none-list-item').classList.remove("slip-filter-selected");
        document.getElementById('twentyfour-list-item').classList.add("slip-filter-selected");
        document.getElementById('fortyeight-list-item').classList.remove("slip-filter-selected");
        document.getElementById('oneweek-list-item').classList.remove("slip-filter-selected");
        document.getElementById('timeoutPeriod').value = "24 Hours";
        document.getElementById('timeout-filter-ul').style.display = 'none';
        document.getElementById("setButton").focus();
    }

    function fortyEightClick() {
        document.getElementById('none-list-item').classList.remove("slip-filter-selected");
        document.getElementById('twentyfour-list-item').classList.remove("slip-filter-selected");
        document.getElementById('fortyeight-list-item').classList.add("slip-filter-selected");
        document.getElementById('oneweek-list-item').classList.remove("slip-filter-selected");
        document.getElementById('timeoutPeriod').value = "48 Hours";
        document.getElementById('timeout-filter-ul').style.display = 'none';
        document.getElementById("setButton").focus();
    }

    function oneWeekClick() {
        document.getElementById('none-list-item').classList.remove("slip-filter-selected");
        document.getElementById('twentyfour-list-item').classList.remove("slip-filter-selected");
        document.getElementById('fortyeight-list-item').classList.remove("slip-filter-selected");
        document.getElementById('oneweek-list-item').classList.add("slip-filter-selected");
        document.getElementById('timeoutPeriod').value = "1 Week";
        document.getElementById('timeout-filter-ul').style.display = 'none';
        document.getElementById("setButton").focus();
    }
</script>