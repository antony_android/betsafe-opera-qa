<style>
    a {
        text-decoration: underline !important;
    }
</style>
<table class="full-width" cellpadding="0" cellspacing="0">

    <tr class="page-header-row bet-history-header">
        <td class="game-title">
            Sports Betting Rules
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16 depo-mpesa-header">
                Last Updated 15th November 2021
            </p>
            <p class="pb-16">
                The Sports Betting Rules are divided into General Sports Betting Rules (Section 1) and Individual Sports
                Rules (Section 2) for ease of understanding, collectively referred to as “the Rules”. It is important
                that you make yourself aware of these Rules. In addition to these Rules, Event/Market Specific Rules
                that may be found under events and markets on the Website shall be viewed as part of these Rules.
            </p>
            <p class="pb-16">
                The Rules are part of the agreement between you and us. The Terms and Conditions shall apply and all
                definitions from those Terms and Conditions shall apply to these Rules.
            </p>
            <p class="pb-16 depo-mpesa-header">
                Section 1: General Sports Betting Rules
            </p>
            <p class="pb-16">
                1. Introduction and Interpretation.
            </p>
            <p class="pb-16">
                1.1 Where there is a conflict between these Rules and any other terms and conditions, these Rules shall
                apply.
            </p>
            <p class="pb-16">
                1.2 In the event there is a conflict between these General Sports Betting Rules and a particular
                Individual Sports Rule, the Individual Sports Rule prevails.
            </p>
            <p class="pb-16">
                1.3 If there are differences between the English original version and a translated version of this text,
                the English version shall prevail.
            </p>
            <p class="pb-16">
                1.4 We reserve the right to revise and update the Rules at any time. You are responsible to keep updated
                with the current valid Rules. With the placement of a bet, you accept the validity and applicability of
                the Rules in the respective valid version.
            </p>
            <p class="pb-16 bold">
                2. Definitions
            </p>
            <p class="pb-16">
                “Extra Time” or “Overtime” refers to a period of time in a sports game in which the play continues if
                neither team has won in the Regulation Time allowed for the game where a winning result is required.
            </p>
            <p class="pb-16">
                “Multibet” means a bet that is wagered against multiple selections, where all selections must be correct
                for the bet to be won.
            </p>
            <p class="pb-16">
                “Regulation Time” or “Normal Time” refers to the period of time over which a sports game is played that
                is set out as the normal duration of play within the sport’s rules.
            </p>
            <p class="pb-16">
                “Stake” means the amount wagered on a bet.
            </p>
            <p class="pb-16">
                “Single” or “Single Bet” means a bet that is wagered against a single selection.
            </p>
            <p class="pb-16 bold">
                3. Limits
            </p>
            <p class="pb-16">
                3.1 The minimum stake allowed to be wagered is: (a) KSH 1 for multibets and (b) KSH 1 for single bets.
            </p>
            <p class="pb-16">
                3.2. The maximum winnings allowed per bet are: (a) KSH 5,000,000 for multibets and (b) KSH 400,000 for
                single bets.
            </p>
            <p class="pb-16">
                3.3 The maximum winnings allowed for a customer is KSH 10,000,000 per day.
            </p>
            <p class="pb-16">
                3.4 In the case that you place several identical bets (also combinations of single bets and multibets)
                for which the
                total winnings exceed the limit for the maximum winnings allowed per bet as defined in term 3.2, then we
                reserve the
                right to void or reduce the bet(s) to comply with the winnings limits, and furthermore to suspend or
                close your account.
            </p>
            <p class="pb-16">
                3.5 We may, at our own absolute discretion, impose a lower limit on the maximum winnings allowed than as
                defined in term
                3.2 on specific bets, depending on the sport, competition, and market.
            </p>
            <p class="pb-16">
                3.6 We may, at our own absolute discretion, apply different limits to your account than those specified
                in these Rules.
            </p>
            <p class="pb-16">
                3.7 All amounts specified for these limits are gross figures and therefore inclusive of applicable
                taxes.
            </p>

            <p class="pb-16 bold">
                4. Bet Placement & Acceptance
            </p>
            <p class="pb-16">
                4.1 We only accept bets that are placed through the following channels: (a) our Website; (b) by SMS sent
                to our short code (23333); (c) through Telegram sent to our handle “Betsafe Kenya”.
            </p>
            <p class="pb-16">
                4.2 It is your responsibility to ensure the details of any bets that you submit to us are correct and
                that you have understood the mechanics of the bets before placing them.
            </p>
            <p class="pb-16">
                4.3 The stakes wagered for your bets will be deducted from your account balance in the order in which
                they are received. We will not accept any bets for which the stake shall exceed your account balance.
            </p>
            <p class="pb-16">
                4.4 Multibets are not accepted where the outcome of one of the selections contributes to or correlates
                with the outcome of any other selection in the bet.
            </p>
            <p class="pb-16">
                4.5 We reserve the right to suspend a market or selection at any time, either on a temporary or
                permanent basis. We will not accept any bets on markets / selections while they are suspended.
            </p>
            <p class="pb-16">
                4.6 We reserve the right to decline all or part of any bet you place with us at our sole and absolute
                discretion.
            </p>
            <p class="pb-16">
                4.7 A bet that you have placed will only be considered as valid once it has been accepted by us and it
                has been assigned
                a unique bet identifier, which shall be communicated to you in the bet receipt. We shall not be liable
                for the
                settlement of any bets which have not been accepted by us and that have not been issued with a unique
                bet identifier. If
                you are unsure about the validity of a bet, please check your bet history or contact our customer
                service team.
            </p>
            <p class="pb-16">
                4.8 In the event that you do not receive confirmation of the bet placement due to issues outside of our
                control, the bet
                shall still be considered as valid.
            </p>
            <p class="pb-16">
                4.9 All odds in our sportsbook are subject to change at any time. All bets shall be placed against the
                latest odds.
                Should the odds change from the time a bet is placed until it is accepted, the latest odds will be
                applied.
            </p>
            <p class="pb-16">
                4.10 For pre-match markets, we will usually only accept bets before or up to the advertised start time.
                Any bets taken
                after the event has started shall not be considered as valid and will be voided, unless there has been
                no action that
                would in our reasonable opinion significantly affect the outcome of the bet.
            </p>
            <p class="pb-16">
                4.11 A bet is invalid if the actual date or time of the bet or your personal data are missing due to
                transmission errors.
            </p>
            <p class="pb-16">
                4.12 We do not tolerate deliberate and repeated incidents of late betting. This will be considered to be
                fraud and we will deal with it accordingly.
            </p>

            <p class="pb-16 bold">
                5. Bet Cancellation
            </p>
            <p class="pb-16">
                5.1 You may request the cancellation of a bet placed through SMS or Telegram within ten (10) minutes
                from the time you placed the bet. Any requests made to cancel your bet after this time has elapsed will
                be automatically rejected.
            </p>
            <p class="pb-16">
                5.2 Bets placed from the Website cannot be cancelled.
            </p>
            <p class="pb-16">
                5.3 Bets can only be cancelled up to 10 minutes before the start time of the game(s) being bet on.
            </p>
            <p class="pb-16">
                5.4 Bets on live events cannot be cancelled.
            </p>
            <p class="pb-16">
                5.5 You may request the cancellation of no more than three (3) bets per day.
            </p>
            <p class="pb-16">
                5.6 We reserve the right to reject any request to cancel a bet at our sole and absolute discretion.
            </p>

            <p class="pb-16 bold">
                6. Bet Settlement & Payout
            </p>
            <p class="pb-16">
                6.1 The official result is final for settlement purposes except where specific rules state the contrary.
                The official result will be determined according to our reasonable opinion, such opinion being formed by
                reference to all publicly available evidence. Any subsequent corrections or amendments to this result
                will not apply for settlement purposes. The podium position in Motor Racing and the medal ceremony in
                Athletics and any similar official ceremony or presentations in other sports are to be treated as the
                official result. Where in our reasonable judgement no official result is available, the result will be:
            </p>
            <p>
                (a) determined by an independent authority whose verdict shall be final for all purposes, or
            </p>
            <p class="pb-16">
                (b) otherwise determined in accordance with our reasonable opinion, such opinion to be formed by
                reference to all available evidence.
            </p>
            <p class="pb-16">
                6.2 Bets will provisionally be settled by reference to all available evidence, but if the result which
                is determined as
                set out above differs from the result used for provisional settlement, settlement may be retrospectively
                adjusted so as
                to reflect the result so determined.
            </p>
            <p class="pb-16">
                6.3 If, for some reason, a result occurs that is unclear or not covered by the Rules, we reserve the
                right to decide the
                outcome of each such case at our own discretion for settlement purposes.
            </p>
            <p class="pb-16">
                6.4 All bets will be settled once the last remaining selection in the bet has been settled.
            </p>
            <p class="pb-16">
                6.5 Where a multibet includes any void selections, the bet will be settled on the remaining selections.
                If all
                selections in a bet are void, then the bet will be settled as void and your stake will be returned.
            </p>
            <p class="pb-16">
                6.6 Winnings from settled bets will be added to the balance of your betting account. Winnings from real
                money will be added as real money, and winnings from bonus money will be added according to the terms of
                the bonus.
            </p>
            <p class="pb-16">
                6.7 All winnings from settled bets are subject to a withholding tax of 20% in accordance with the Income
                Tax Act under
                section 35 (1) (i), (3) (h). The tax shall be deducted from your winnings and collected by us for
                remittance to the
                Kenya Revenue Authority (KRA). Therefore, the actual payout that you receive will be after tax (i.e.
                winnings - tax).
            </p>
            <p class="pb-16">
                6.8 A placement tax of 7.5% must be applied to all bet stakes, in line with the Kenyan Governments
                re-introduction of an
                Excise Tax on gaming.
            </p>
            <p class="pb-16">
                6.9 When prices for both match odds and handicap betting are advertised, the bet will be settled on the
                match odds
                result unless the handicap price is specifically requested at the time the bet is placed. Where only
                handicap betting is
                available the bet will be settled as a handicap bet whether requested or not.
            </p>
            <p class="pb-16 bold">
                7. Dead Heats
            </p>
            <p class="pb-16">
                7.1 If a dead heat between two selections is declared on any event as the official result, unless stated
                otherwise
                elsewhere in these Rules, half the stake is applied to the selection at full price and the other half is
                lost.
            </p>
            <p class="pb-16">
                7.2 If a dead heat is declared between more than two selections, the stake is proportioned accordingly.
            </p>

            <p class="pb-16 bold">
                8. Venue Changes
            </p>
            <p class="pb-16">
                If your selection is no longer playing at the venue advertised your bet will still stand unless the
                venue has been
                changed to the opponent's venue, in which case all bets are void. In the case of international matches
                the venue must
                remain in the same country.
            </p>
            <p class="pb-16 bold">
                9. Abandoned Events
            </p>
            <p class="pb-16">
                If an event is abandoned before it can be concluded, all markets related to the match outcome will,
                unless specifically
                stated otherwise, be voided unless an official result is declared. Bets on all markets which have been
                unconditionally
                determined will stand, for example in football matches, first goalscorer bets will stand if a goal has
                been scored.
            </p>

            <p class="pb-16 bold">
                10. Cancelled or Postponed Events
            </p>
            <p class="pb-16">
                All markets related to cancelled or postponed events will be voided.
            </p>

            <p class="pb-16 bold">
                11. No Show
            </p>
            <p class="pb-16">
                In case of a participant not attending a particular event as scheduled, all related bets are still valid
                unless the
                event is cancelled. For example, if Usain Bolt fails to show up at the World Championships, bets
                relating to Usain Bolt
                are still valid.
            </p>

            <p class="pb-16 bold">
                12. Errors
            </p>
            <p class="pb-16">
                12.1 We do not assume liability for transmitted errors of betting data and results. No claim for damages
                can in any
                instance be made because of incorrect, delayed, manipulated or abusive transfer of data on the internet
                or because of
                other transmission errors of data and results.
            </p>
            <p class="pb-16">
                12.2 We assume no liability for the accuracy of live scores, statistics or other information/data that
                we provide on the
                Website. Any such information/data displayed on the Website is for reference only and we do not
                guarantee the accuracy
                of this information.
            </p>
            <p class="pb-16">
                12.3 We reserve the right to correct any palpable errors and to take the necessary steps to provide a
                fair service for
                our customers at our sole and absolute discretion. A palpable error could be any incorrect information
                published in
                regard to an event. For example, incorrectly (reversed) setup handicap, wrong team/teams/participants,
                incorrect
                deadlines or technical errors etc.
            </p>
            <p class="pb-16">
                12.4 We also reserve the right to correct obvious errors with the input of betting odds and/or the
                evaluation of betting
                results even after the event or to declare affected bets void.
            </p>
            <p class="pb-16">
                12.5 It is your responsibility to instantly report each amount mistakenly credited to Your Account. All
                winnings occurring as a result of such error are invalid under whatever circumstances they occurred.
            </p>

            <p class="pb-16 bold">
                13. Cashout
            </p>
            <p class="pb-16">
                13.1 We may, at our absolute discretion, allow you to take a return on a bet you have made before
                settlement of the
                market on which the wager was placed (“Cashout”).
            </p>
            <p class="pb-16">
                13.2 Cashout may be available on selected sports markets via the website only. Cashout may not be
                available at all
                times.
            </p>
            <p class="pb-16">
                13.3 Cashout is available for fixed-odds wagers only. Cashout is not available for: (a) Bets placed with
                free bets or
                risk free bets; (b) Multibets, unless each individual leg or event within that multibet is eligible for
                Cashout; (c)
                System bets; (d) Split column bets.
            </p>
            <p class="pb-16">
                13.4 A predetermined settlement value (“Cashout Price”) may be offered based upon the selections, prizes
                taken and the
                current status of the wagering transaction.
            </p>
            <p class="pb-16">
                13.5 Cashout may not be available for certain bets placed on pre-match markets, where the event or match
                has already
                commenced. For multibets, Cashout may not be available if any leg of the bet has been placed on a
                pre-match market and
                the event or match relating to such leg has already commenced (is in-play).
            </p>
            <p class="pb-16">
                13.6 To Cashout a bet you must select “Confirm Cashout” or “Confirm” (a “Cashout Request”). You cannot
                cancel a Cashout
                Request once it has been confirmed.
            </p>
            <p class="pb-16">
                13.7 We may, at our absolute discretion, choose to accept or decline a Cashout Request. A Cashout
                Request may not be
                accepted for reasons including, but not limited to, a change in odds or the suspension of a market
                (including any leg of
                a multi wager).
            </p>
            <p class="pb-16">
                13.8 If a Cashout Request is not accepted for some reason, a message will be shown advising you that the
                Cashout Request
                was declined and a new Cashout Price may be offered to you. If you did not make a Cashout Request in
                relation to the new
                Cashout Price offered, settlement of the original wager will take place, based on the original
                instructions and odds
                when the wager was accepted.
            </p>
            <p class="pb-16">
                13.9 Where a Cashout Request has been accepted: (a) Your original bet will come to an end (be “Cashed
                Out”); (b) We will
                pay you the Cashout Price; (c) The original bet will be deemed to have been settled and we will have no
                further
                obligation to you in respect of the original bet. You will not receive any payout, dividend or refund in
                relation to the
                Cashed Out wager; and (d) The occurrence or non-occurrence of the event(s) on which the original wager
                was placed will
                not create any additional rights or obligations to or for you.
            </p>
            <p class="pb-16">
                13.10 The acceptance of a Cashout Request does not represent a new bet, it is an agreement to end the
                original bet early.
            </p>
            <p class="pb-16">
                13.11 The Cashout Price is inclusive of your original stake. The Cashout Price is non-negotiable and
                will not exceed the
                value of the original estimated return.
            </p>
            <p class="pb-16">
                13.12 You will not be eligible for any Bonuses or any other promotions in relation to bets that have
                been successfully Cashed Out.
            </p>
            <p class="pb-16">
                13.13 Any bet which is Cashed Out will not be considered for the purposes of meeting any turnover or
                wagering requirements for Bonuses, rewards, prizes, or any tasks necessary to fulfil a campaign or
                promotional offering.
            </p>
            <p class="pb-16">
                13.14 In the event that a Cashout Request has been accepted in error (including where a Cashout Price
                has been
                incorrectly offered) or an amount has been incorrectly credited to Your account in connection with a bet
                that has been
                Cashed Out, We reserve the right, at our absolute discretion, to adjust the account to the extent
                necessary to rectify
                the error or the incorrect credit.
            </p>
            <p class="pb-16">
                Such adjustment may include:
            </p>
            <p>
                (a) settling the Cashout Request for an amount equal to the Cashout Price which would have been
                available in the absence
                of such error; or
            </p>
            <p class="pb-16">
                (b) reversing the Cashout Request and settling the wager on the original bet instructions.
            </p>
            <p class="pb-16">
                If, as a result of any such adjustment, the account goes into debit, we may recover from your account,
                as a debt due,
                the amount of the deficiency in the account.
            </p>

            <p class="pb-16">
                13.15 We reserve the right to suspend, amend, restrict or cease to offer Cashout at any time, on any
                market or to any
                customer, without providing a reason or advance notification, even where Cashout has been advertised to
                a customer as
                being available. We are not liable for any losses which may arise in connection with Cashout not being
                available to a
                Customer, even in circumstances where Cashout has been advertised as being available for a particular
                event or at a
                particular time.
            </p>
            <p class="pb-16">
                13.16 We reserve the right, at our absolute discretion, to void the original bet which was Cashed Out,
                refuse to Cashout
                a bet, or require repayment of any amounts paid by Us in respect of the original bet (including the
                Cashout Price)
                where:
            </p>
            <p>
                (a) We reasonably suspect a bet or Cashout Request has been made by any individual or group of people
                (including, but
                not limited to relatives, organisations, bookmakers and their employees) acting together either in an
                attempt to defraud
                Us; or
            </p>
            <p>
                (b) The original bet or the Cashout Request was made after the relevant event has finished; or
            </p>
            <p class="pb-16">
                (c) Where we are otherwise entitled to void the original bet or require repayment under these Rules.
            </p>
            <p class="pb-16">
                13.17 You acknowledge and accept that circumstances within or beyond Our control may prevent, limit or
                delay the
                operation of Cashout, and that We will not be liable to any person for any loss incurred or sustained in
                connection with
                or as a result of:
            </p>
            <p>
                (a) Cashout being unavailable or delayed; and/or
            </p>
            <p class="pb-16">
                (b) Any act or omission, deliberate or negligent, by you, any customer or other third party, in
                connection with Cashout.
            </p>
            <p class="pb-16">
                13.18 We are not liable for any losses which may arise in connection with a Customer’s use of the
                Cashout feature or a
                Cashout Request being accepted or declined by us.
            </p>
            <p class="pb-16">
                13.19 Where a Customer has placed a bet on a market that was not available for Cashout at the time the
                bet was placed by
                the customer, the customer will not be able to make a Cashout Request in relation to that bet, even if
                we subsequently
                make Cashout available on that market.
            </p>
            <p class="pb-16">
                13.20 Scoreboard services and other third-party information streams or provisions are provided for
                guidance only. We do
                not accept any liability for any bets cashed out based on information provided by these services.
            </p>
            <p class="pb-16 depo-mpesa-header">
                Section 2: Individual Sports Rules
            </p>
            <ol class="pb-16">
                <li>
                    <a href="#football">
                        Football Rules
                    </a>
                </li>
                <li>
                    <a href="#americanFootball">
                        American Football Rules
                    </a>
                </li>
                <li>
                    <a href="#australian">
                        Australian Rules
                    </a>
                </li>
                <li>
                    <a href="#badminton">
                        Badminton Rules
                    </a>
                </li>
                <li>
                    <a href="#baseball">
                        Baseball Rules
                    </a>
                </li>
                <li>
                    <a href="#football">
                        Football Rules
                    </a>
                </li>
                <li>
                    <a href="#basketball">
                        Basketball Rules
                    </a>
                </li>
                <li>
                    <a href="#boxing">
                        Boxing Rules
                    </a>
                </li>
                <li>
                    <a href="#cricket">
                        Cricket Rules
                    </a>
                </li>
                <li>
                    <a href="#darts">
                        Darts Rules
                    </a>
                </li>
                <li>
                    <a href="#gaa">
                        GAA Rules
                    </a>
                </li>
                <li>
                    <a href="#football">
                        Football Rules
                    </a>
                </li>
                <li>
                    <a href="#handball">
                        Handball Rules
                    </a>
                </li>
                <li>
                    <a href="#iceHockeyNHL">
                        Ice Hockey Rules (NHL)
                    </a>
                </li>
                <li>
                    <a href="#iceHockey">
                        Ice Hockey Rules
                    </a>
                </li>
                <li>
                    <a href="#motorRacing">
                        Motor Racing & Formula 1 Rules
                    </a>
                </li>
                <li>
                    <a href="#rugby">
                        Rugby League and Rugby Union Rules
                    </a>
                </li>
                <li>
                    <a href="#snooker">
                        Snooker Rules
                    </a>
                </li>
                <li>
                    <a href="#tennis">
                        Tennis Rules
                    </a>
                </li>
                <li>
                    <a href="#volleyball">
                        Volleyball Rules
                    </a>
                </li>
            </ol>
            <p class="pb-16 bold" id="football">
                1. Football Rules
            </p>
            <p class="pb-16">
                1.1 Acceptance of bets:
            </p>
            <p>
                (a) All football bets will be settled on the score at the final whistle at the end of full time. This
                denotes the period
                of play after 90 minutes plus any time added by the referee for injuries and other stoppages but does
                not include
                scheduled extra time, or penalty shoot-outs, if played. Extra time denotes the period of play which
                takes place after
                full time but which does not include any penalty shoot outs. In matches where penalty shoot outs or
                extra time are due
                to take place, all bets are settled on the full-time score unless specifically stated otherwise.
            </p>
            <p>
                (b) All bets are normal time only unless otherwise stated. Normal time is defined by the official rules
                published by the
                respective governing association. At least 80 minutes of the match must be played for bets to be
                considered as valid.
                Any bets where the result is conclusively known before normal time is completed or before a match is
                abandoned should be
                settled regardless if 80 minutes are completed or not.
            </p>
            <p class="pb-16">
                (c) For matches played at neutral venues, the team listed on the left in the match fixture emanating
                from the relevant
                governing body of the football competition in question is classed as the 'home' team for settlement
                purposes.
            </p>
            <p class="pb-16">
                1.2 Postponed/re-arranged matches:
            </p>
            <p>
                (a) If a match is postponed, all single bets will stand if the match takes place within, but not
                including, 24 hours of
                the scheduled kick off time. This will be the case unless the venue of the match is altered.
            </p>
            <p>
                (b) If a match is abandoned, any bets where the outcome has been decided will be settled (First half
                winner/first
                goalscorer). All other bets will be void regardless of the score at the time.
            </p>
            <p class="pb-16">
                1.3 Match result (1X2): Bets will be settled according to which team wins the match e.g. Home team (1),
                Draw (X) or Away
                team (2).
            </p>
            <p class="pb-16">
                1.4 Half Time Result: Bets will be settled according to which team is winning at the end of the first
                half of the match
                e.g. Home team, Draw or Away team.
            </p>
            <p class="pb-16">
                1.5 Second Half Result: Bets will be settled according to which team will win the second half of the
                match e.g. Home
                team, Draw or Away team.
            </p>
            <p class="pb-16">
                1.6 Half with most goals: Bets will be settled according to which half will have the most goals. Equal
                number of goals
                will be an available selection in this market.
            </p>
            <p class="pb-16">
                1.7 Handicap: Bets will be settled according to which team wins the match after the handicap line has
                been applied.
            </p>
            <p class="pb-16">
                1.8 Draw no Bet: Bets will be settled according to which selection is the final result of the match.
                Stake will be
                returned if match ends in a draw.
            </p>
            <p class="pb-16">
                1.9 Score in Both Halves: Bets will be settled according to whether there will be a goal scored in both
                the 1st Half and
                the 2nd Half of the match.
            </p>
            <p class="pb-16">
                1.10 Both Teams to Score in 1st Half: Bets will be settled according to whether both teams will score a
                goal in the 1st
                Half of the match
            </p>
            <p class="pb-16">
                1.11 Both Teams to Score in 2nd Half: Bets will be settled according to whether both teams will score a
                goal in the 2nd
                Half of the match
            </p>
            <p class="pb-16">
                1.12 Number of goals: Bets will be settled according to whether the total number of goals scored in the
                match is over or
                under the stated line.
            </p>
            <p class="pb-16">
                1.13 Home team - Number of goals: Bets will be settled according to whether the total number of goals
                scored by the
                nominated team is over or under the stated line.
            </p>
            <p class="pb-16">
                1.14 Away team - Number of goals: Bets will be settled according to whether the total number of goals
                scored by the
                nominated team is over or under the stated line.
            </p>
            <p class="pb-16">
                1.15 1st Half Goals U/O: Bets will be settled according to whether the total number of goals scored
                during the first
                half is over or under the stated line.
            </p>
            <p class="pb-16">
                1.16 Double chance: Bets will be settled according to whether the final match result ends in one of the
                two displayed
                selections.
            </p>
            <p class="pb-16">
                1.17 Both teams to score: Bets will be settled according to whether the match will end with both teams
                scoring at least
                one goal.
            </p>
            <p class="pb-16">
                1.18 Correct score: Bets will be settled according to the exact score line of the match.
            </p>
            <p class="pb-16">
                1.19 Team to score: Bets will be settled according to which Team scores the next goal. Own goals count
                towards this bet.
            </p>
            <p class="pb-16">
                1.20 Score in Both Halves: Bets will be settled according to whether both teams will score in both the
                1st Half and the
                2nd Half of the match.
            </p>
            <p class="pb-16">
                1.21 Match Winner & Both Teams to Score: Bets will be settled according to which team wins the match and
                to whether the
                match will end with both teams scoring at least one goal.
            </p>
            <p class="pb-16">
                1.22 Team to Score Last Goal: Bets will be settled according to which Team scores the last goal. Own
                goals count towards
                this bet.
            </p>
            <p class="pb-16">
                1.23 Double Result (HT/FT): Bets will be settled according to the result of the match at half-time and
                full-time.
            </p>
            <p class="pb-16">
                1.24 Number of Goals – Odd/Even: Bets will be settled according to whether the total number of goals
                will be an odd or
                an even number. No goals is considered as Even for betting purposes.
            </p>
            <p class="pb-16">
                1.25 Goalscorer Bets:
            </p>
            <p>
                (a) Own goals are ignored for the settlement of all goalscorer bets.
            </p>
            <p>
                (b) Bets on a player who does not take to the pitch will be void.
            </p>
            <p>
                (c) For first goalscorer bets, all bets on a player who takes no part in the match or who comes on as a
                substitute after
                the first goal has been scored will be void. If a match is abandoned before the first goal is scored,
                all bets are void.
                This includes any 'no goalscorer’ bet. If the match is abandoned after the first goal is scored, all
                bets will stand. If
                the only goal scored before the end of full time is an own goal, then 'no goalscorer’ will be deemed the
                outcome.
            </p>
            <p>
                (d) For last goalscorer best, all bets on a player stand as long as they take to the pitch, regardless
                of whether they
                were on the pitch when the last goal was scored. If the only goal scored within 90 minutes is an own
                goal, then 'no
                goalscorer' will be deemed to have been the outcome.
            </p>
            <p>
                (e) For anytime, brace or hat-trick goalscorer bets, all bets on a player stand as long as they take to
                the pitch. In
                the event of an abandoned match all bets will be void, unless bets on a player to score a brace,
                hat-trick or at anytime
                have already been determined.
            </p>
            <p>
                (f) For last team to score, settlement is based upon the team scoring the last goal before the end of
                full time. Own
                goals DO count for this market. For example, if Chelsea are playing Tottenham and the last goal is an
                own goal by a
                Tottenham player, Chelsea would be the winner in the 'last team to score' market.
            </p>
            <p class="pb-16">
                (g) In the event of uncertainty about who scored a goal, where possible, the official league source is
                used. In the
                event that a goalscorer is changed during the game, all bets will be resettled and accounts adjusted
                accordingly. Bets
                such as brace, hat trick or scorecast will be subject to being rebooked at the appropriate price if
                resettlement occurs.
            </p>
            <p class="pb-16">
                1.26 Goal Minute Markets:
            </p>
            <p class="pb-16">
                For settlement purposes the first minute of the game will be from 1 second to 59 seconds, the 2nd minute
                from 1 minute
                to 1 minute 59 seconds and so on. For example, if a bet is placed on the first goal to be scored between
                11 and 20
                minutes and the first goal is scored at 20 minutes 03 seconds; the bet will be a loser as this falls
                within the 21 to 30
                minutes category.
            </p>
            <p class="pb-16">
                1.27 Scorecasts (first player to score and correct score):
            </p>
            <p>
                (a) Winning bets will have successfully predicted both the player who will score the first goal in a
                selected match and
                also the correct score at the end of full time.
            </p>
            <p>
                (b) In the event that a selected player takes part in the match after the first goal has been scored or
                does not take
                part in the game at all, a scorecast bet will revert to a single bet on the correct score at the price
                advertised for
                that single bet at the time the bet was placed.
            </p>
            <p>
                (c) As own goals do not count for first goalscorer purposes, in the event that the first goal is an own
                goal the
                scorecast will be settled on the next goalscorer and correct score of the game. If the final score is
                1-0 and the goal
                is an own goal, scorecast bets are settled as a correct score single as above.
            </p>
            <p class="pb-16">
                (d) In the event that a match is abandoned prior to the completion of full time and a goal has been
                scored, or the first
                goal was an own goal, scorecast bets will be settled as first goalscorer single bets. If no goal has
                been scored, all
                bets will be void.
            </p>
            <p class="pb-16">
                1.28 Bookings Markets:
            </p>
            <p>
                (a) The settlement of markets involving the incidence of cards shown to players during an individual
                match will be
                determined in accordance with General Sports Rules in paragraph 2. Any bet taken on the basis of a wrong
                so far will be
                rebooked at the correct price.
            </p>
            <p>
                (b) Bookings markets are settled by reference to cards shown during the first or second half of the
                match including
                injury time but excluding extra time. Cards will only count which are shown to players after they take
                the pitch for the
                first time and before they leave the pitch for the final time. Cards shown to non-players (e.g. managers
                or subs) do not
                count. Any card shown after the full-time whistle has been blown shall be disregarded. If after the
                final whistle a card
                shown during a game is rescinded, or reduced to a yellow card from a red card, that will not affect the
                settlement of
                any markets involving bookings on the game in question.
            </p>
            <p>
                (c) A player receiving a red card as a consequence of receiving a second yellow card will in all cases
                be deemed to have
                been shown one yellow card and one red card.
            </p>
            <p>
                (d) For markets involving the time of yellow and red cards, these markets will be settled by reference
                to the time at
                which the relevant card is shown.
            </p>
            <p>
                (e) For booking index bets a yellow card is 10 points and a red card is 25 points. Settlement is
                determined by adding
                the points per card issued before the end of full time. A maximum of 35 points can be awarded to any one
                player.
            </p>
            <p>
                (f) For 1st team carded, when two cards are shown in the same incident, the market will settle to the
                player that is
                shown the card first.
            </p>
            <p>
                (g) For 1st player carded, if a player does not play or comes on after the first card, bets will be
                void. Only cards to
                players on the pitch count, not subs or managers (unless specified in a special bet). When two cards are
                shown in the
                same incident, the market will settle to the player that is shown the card first.
            </p>
            <p>
                (h) For 1st player carded double chance, both players need to start for bets to stand.
            </p>
            <p class="pb-16">
                (i) For player to be carded, if the player does not start, bets will be void.
            </p>
            <p class="pb-16">
                1.29 Number Of Corners:
            </p>
            <p>
                (a) The total number of corners taken before the end of full time is used for settlement of such bets.
                Corners awarded
                but not taken will not count. If a corner has to be re-taken, only one corner will be counted.
            </p>
            <p class="pb-16">
                (b) Bets on all corner markets that are placed when the 'so far' is incorrect will be rebooked at the
                correct price.
            </p>
            <p class="pb-16">
                1.30 First corner:
            </p>
            <p class="pb-16">
                Bets will be settled according to which team will be awarded the first corner of the match.
            </p>
            <p class="pb-16">
                1.31 Corner handicap:
            </p>
            <p class="pb-16">
                Bets will be settled according to which Team will take the most number of corner kicks in the match
                after the handicap
                line has been applied.
            </p>
            <p class="pb-16">
                1.32 Penalty Shoot Outs:
            </p>
            <p class="pb-16">
                All penalty shootout markets apply only to the result of a penalty shootout. Penalties scored before the
                completion of
                full time and/or extra time will not count for the purposes of this market. In the event that a match
                does not go to a
                penalty shootout, all bets on these markets will be void. This does not include 'To Qualify' or 'Method
                of Victory'
                which will settle on the match or extra time result.
            </p>
            <p class="pb-16">
                1.33 Own Goal markets:
            </p>
            <p class="pb-16">
                The official result passed by the Official League source will be deemed final at midnight local time the
                day the match
                is played.
            </p>
            <p class="pb-16">
                1.34 Extra Time Betting:
            </p>
            <p>
                (a) Extra time betting will be based on the scheduled period of play during extra time.
            </p>
            <p class="pb-16">
                (b) All extra time betting markets will start from the beginning of extra time and not include normal
                time (i.e. the
                time before the end of full time).
            </p>
            <p class="pb-16">
                1.35 Minute markets:
            </p>
            <p class="pb-16">
                Bets will be settled as per what minute an event happens in. 02.00 would be the 3rd minute or the game,
                with 02.59 being
                the last second of that minute. Corners will be either settled on when they are awarded or taken subject
                to the
                particular market rule. Bookings will be settled as when the referee issues the card to the player in
                question.
            </p>

            <p class="pb-16 bold" id="americanFootball">
                2. American Football Rules
            </p>
            <p class="pb-16">
                2.1 Acceptance of Bets & Settlement:
            </p>
            <p>
                (a) We accept single and multiple bets on all National Football League games and selected NCAA games,
                except doubles
                consisting of the ‘Handicap’ and ‘Totals’ markets in the same game.
            </p>
            <p>
                (b) Bets will be settled on the official result, including overtime, as declared by the National
                Football League or NCAA
                governing body.
            </p>
            <p>
                (c) If a match is abandoned before the end of a game (including during overtime played) all outright
                bets will be void,
                save where a game has been traded in play and a market has been unconditionally determined (for example
                first touchdown
                scorer bets will stand if a touchdown has been scored).
            </p>
            <p class="pb-16">
                (d) Player match bets: When a market is offered on one player over another player (i.e Wide Receiver
                match bet) both
                players must be active for bets to stand.
            </p>
            <p class="pb-16">
                2.2 Match Betting & Winning Margins:
            </p>
            <p class="pb-16">
                2.2 Match Betting & Winning Margins:
            </p>
            <p>
                (a) Where odds for both outright and handicap betting are available, all bets are settled on the
                outright price, unless
                the handicap price has been specifically selected. Overtime counts except for bets specifically on the
                results at the
                end of the second quarter and at the end of the fourth quarter.
            </p>
            <p class="pb-16">
                (b) A winning margin bet is based on predicting the margin of victory by one team in a selected match.
                Winning margin
                betting is from scratch. 2.3 First Touchdown Scorer: Bets are accepted on an 'all in, play or not'
                basis, the exception
                to this being any bets placed on "inactive/not active" players will be deemed void. A player is
                considered "inactive/not
                active" if he is listed as such on the official match gamebook. However bets on players coming under the
                heading 'did
                not play' are losing bets.
            </p>
            <p class="pb-16 bold" id="australian">
                Australian Rules
            </p>
            <p class="pb-16">
                3.1 Bets Settlement:
            </p>
            <p>
                (a) Regulation time must be completed for bets to stand unless otherwise stated.
            </p>
            <p>
                (b) Bets will be settled on statistics provided by the official AFL governing body at
                http://www.afl.com.au.
            </p>
            <p class="pb-16">
                (c) Where a match is abandoned or postponed and played within 24 hours of the original scheduled date
                all bets will
                stand, otherwise bets will be void after a 24 hour period has expired.
            </p>
            <p class="pb-16">
                3.2 Match/Handicap Betting:
            </p>
            <p>
                (a) Any match ending regulation time in a draw/tie will have the stake refunded unless the tie/draw is a
                selection in the market.
            </p>
            <p class="pb-16">
                (b) For Handicap betting purposes, all bets are settled on 80 minutes’ play and exclude overtime.
            </p>
            <p class="pb-16">
                3.3 Quarter/Half Betting:
            </p>
            <p>
                (a) All bets on Quarter/Half markets will be settled on the regulation time result (including injury
                time) unless
                otherwise stated.
            </p>
            <p class="pb-16">
                (b) In the event of any quarter/half markets not being completed bets will be void unless the specific
                market outcome
                has already been determined.
            </p>
            <p class="pb-16">
                3.4 Total Match Points: All bets on total score markets will be settled on the regulation time result
                (including injury
                time) unless otherwise stated. The market result is determined by the total score of the game when 6
                points is awarded
                for a Goal and 1 point is awarded for a Behind.
            </p>
            <p class="pb-16">
                For example, Essendon 10.8 (68) Melbourne 11.4 (70) has a total score of 138.
            </p>
            <p class="pb-16">
                ('10.8' 10 Goals = 60pts + 8 Behinds = 68 (+) '11.4' 11 Goals = 66pts + 4 Behinds = 70).
            </p>
            <p class="pb-16">
                3.5 Total Match Goals:
            </p>
            <p class="pb-16">
                All bets on total goals markets will be settled on the regulation time result (including injury time)
                unless otherwise
                stated. The market result is determined by the total goals scored in the game.
            </p>
            <p class="pb-16">
                For example, Essendon 10.8 Melbourne 11.4 the total goals equal 21.
            </p>
            <p class="pb-16">
                3.6 Total Match Behinds:
            </p>
            <p class="pb-16">
                All bets on total points markets will be settled on the regulation time result (including injury time)
                unless otherwise
                stated. The market result is determined by the total behinds scored in a game.
            </p>
            <p class="pb-16">
                For example, Essendon 10.8 Melbourne 11.4 the total goals equal 12.
            </p>
            <p class="pb-16 bold">
                4. Badminton Rules
            </p>
            <p class="pb-16">
                4.1 Bets Settlement:
            </p>
            <p>
                (a) For match betting purposes, if a player withdraws or is disqualified before the start of a match,
                then all bets will
                be void.
            </p>
            <p class="pb-16">
                (b) In the event of a match starting but not being completed the player progressing to the next round
                will be deemed as
                the winner for settlement purposes for the match betting market. One full set needs to be completed for
                match winner
                bets to stand. If a retirement occurs before the conclusion of the first set, bets are void on this
                market.
            </p>
            <p class="pb-16 bold" id="baseball">
                5. Baseball Rules
            </p>
            <!-- <p class="pb-16">
                5. Baseball Rules
            </p> -->
        </td>
    </tr>
</table>