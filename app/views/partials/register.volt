<table class="login" cellpadding="0" cellspacing="0">
  <tr class="login-row">
    <td class="game-title">
      Join Now
    </td>
  </tr>
  <th>{{ this.flashSession.output() }}</th>
  {% if session.get('user-agent') !='OperaMiniExtreme' %}
  <tr>
    <td>
      <?php echo $this->tag->form("/signup/join"); ?>
      <table class="form">
        <tr class="input">
          <td>
            <label>
              Your Mobile Number
            </label>
            <div class="flag-div">
              <img src="{{ url('/img/kenya-flag.png') }}" width=24 alt="" class="flag-img" />
              <span style="vertical-align: middle;"> +254</span>
              <input type="text" name="mobile" id="mobile" onkeyup="refreshJoinNowButt()" class="flag-input"
                autocomplete="off" tabindex="10" />
            </div>
          </td>
        </tr>
        <tr class="input">
          <td class="pt-16">
            <label for="password">
              Password <span class="show-pwd" onclick="togglePwdVisibility('password')">Show</span>
            </label>
            <input type="password" name="password" id="password" onkeyup="refreshJoinNowButt()">
          </td>
        </tr>
        <tr class="input">
          <td class="pt-16">
            <label for="confpassword">
              Confirm Password <span class="show-pwd" onclick="togglePwdVisibility('confpassword')">Show</span>
            </label>
            <input type="password" name="confpassword" id="confpassword" onkeyup="refreshJoinNowButt()">
          </td>
        </tr>

        <tr class="input">
          <td>
            <button type="submit" class="login-join" id="joinNowButt" tabindex="13">
              Join Now
            </button>
          </td>
        </tr>
        <tr class="input">
          <td class="checkbox">
            <table>
              <tr>
                <td style="vertical-align: top;">
                  <input type="checkbox" class="terms-checkbox" name="terms" value="1" checked tabindex="14">
                </td>
                <td>
                  By creating an account you accept our
                  <span id="signupTermsSpan" class="join-terms" onclick="showHideTerms()" tabindex="15">Terms &
                    Conditions.</span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      </form>
    </td>
  </tr>
  {% else %}
  <tr>
    <td style="color: rgba(255,255,255,0.87);padding-bottom: 18px;">
      <?php echo $this->tag->form("/signup/join"); ?>
      <label class="opera-label">
        Your Mobile Number
      </label>
      <input type="text" name="mobile" id="mobile" tabindex="10" class="opera-input" style="margin-bottom: 16px;" />
      <label for="password" class="opera-label">
        Password
      </label>
      <input type="password" name="password" id="password" class="opera-input" style="margin-bottom: 16px;">
      <label for="confpassword" class="opera-label">
        Confirm Password
      </label>
      <input type="password" name="confpassword" id="confpassword" class="opera-input">

      <button type="submit" class="opera-butt" id="joinNowButt" tabindex="13">
        Join Now
      </button>

      <table>
        <tr>
          <td style="vertical-align: top;">
            <input type="checkbox" class="terms-checkbox" name="terms" value="1" checked tabindex="14">
          </td>
          <td style="line-height: 18px;">
            By creating an account you accept our
            <a href="{{ url('/terms') }}">
              <span class="join-terms" tabindex="15">Terms &
                Conditions.</span>
            </a>
          </td>
        </tr>
      </table>

      </form>
    </td>
  </tr>
  {% endif %}
</table>

<table id="signupTerms" style="display: none;">
  <tr>
    <td class="std-pd">
      {{ partial('partials/terms') }}
    </td>
  </tr>
</table>

<script>

  function showHideTerms() {
    if (document.getElementById('signupTerms').style.display == 'none') {
      document.getElementById('signupTerms').style.display = '';
    } else {
      document.getElementById('signupTerms').style.display = 'none';
    }
  }

  function refreshJoinNowButt() {
    let joinNowButt = document.getElementById('joinNowButt');
    let mobile = document.getElementById("mobile").value;
    let password = document.getElementById("password").value;
    let confpassword = document.getElementById("confpassword").value;

    if (mobile.length == 0 || password.length == 0 || confpassword.length == 0) {

      joinNowButt.style.background = '#616161';
      joinNowButt.style.border = '1px solid #616161';
      joinNowButt.style.color = 'rgba(255, 255, 255, 0.57)';
    } else {
      joinNowButt.style.background = '#01B601';
      joinNowButt.style.border = '1px solid #01B601';
      joinNowButt.style.color = 'rgba(255, 255, 255, 0.87)';
    }
  }

</script>