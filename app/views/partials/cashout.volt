<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="page-header-row bet-history-header">
        <td class="game-title std-pd">
            Cashout
        </td>
    </tr>
    <tr>
        <td class="std-pd">
            <table class="form deposit-input full-width" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="deposit-details" colspan="2">
                        You are Cashing Out KSH
                        <?=$amount; ?>. Are you sure?
                    </td>
                </tr>
                <tr class="input">
                    <td style="width: 50%;padding:0px 8px 0px 0px;">
                        <?php echo $this->tag->form("/bet-history/confirmcashout"); ?>
                        <input type="hidden" name="coupon_id" value="<?=$couponId; ?>" />
                        <input type="hidden" name="cashout_amount" value="<?=$amount; ?>" />
                        <button type="submit" class="green-theme" style="margin:8px;">
                            Yes
                        </button>
                        </form>
                    </td>
                    <td style="width: 50%;padding:0px 16px 0px 8px;">
                        <a href="/bet-history/show/<?=$couponId;?>/<?=$couponCode;?>">
                            <button type="button" class="red-theme" style="margin:8px;">
                                No
                            </button>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>