<table class="league">
    <tr>
        <td class="time">
            <span class="play-time">
                <?php echo date('g:ia', strtotime($day['start_time'])); ?>
            </span>
            <?php echo date('D d/m', strtotime($day['start_time'])); ?>
        </td>
    </tr>
    <?php $scores = explode(":", $day['score']); ?>
    <tr class="game">
        <td>
            <table width="100%">
                <tr>
                    <td class="clubs" colspan="3">
                        <p class="pb-2">
                            <?php echo $day['home_team']; ?>
                        </p>
                    </td>
                    <td>
                        <span class="pull-right live-color">
                            <?=$scores[0]; ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="clubs" colspan="3">
                        <p class="pb-2">
                            <?php echo $day['away_team']; ?>
                        </p>
                    </td>
                    <td>
                        <span class="pull-right live-color">
                            <?=$scores[1]; ?>
                        </span>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td class="meta" colspan="4">
            <?php echo $day['sport_name']." / ".$day['competition_name']; ?>
        </td>
    </tr>
    {% if session.get('user-agent') =='OperaMiniExtreme' %}
    <tr>
        <td>
            <?php if(isset($selected) && $selected == 'live') { ?>
            <a href="{{url('livematch/open/') }}{{day['match_id']}}" tabindex="<?=$counter+4;?>"
                style="display: block;margin-bottom: 8px;font-size: medium;">
                <span>
                    <?php echo "+".$day['side_bets']; ?> Markets
                </span>
            </a>
            <?php } else { ?>
            <a href="{{url('match/open/') }}{{day['match_id']}}" tabindex="<?=$counter+4;?>"
                style="display: block;margin-bottom: 8px;font-size: medium;">
                <span>
                    <?php echo "+".$day['side_bets']; ?> Markets
                </span>
            </a>
            <?php } ?>
        </td>
    </tr>
    {% endif %}
</table>