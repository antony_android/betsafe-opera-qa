<table class="login" cellpadding="0" cellspacing="0">
    <tr class="login-row">
        <td class="game-title">
            Log In
        </td>
    </tr>
    <th>{{ this.flashSession.output() }}</th>
    {% if session.get('user-agent') !='OperaMiniExtreme' %}
    <tr>
        <td>
            <?php echo $this->tag->form("/login/authenticate"); ?>
            <table class="form" cellpadding="0" cellspacing="0">
                <tr class="input">
                    <td>
                        <label>
                            Your Mobile Number
                        </label>
                        <div class="flag-div">
                            <img src="{{ url('/img/kenya-flag.png') }}" width=24 alt="" class="flag-img"
                                tabindex="-1" />
                            <span style="vertical-align: middle;"> +254</span>
                            <input type="text" name="mobile" id="mobileNo" onkeyup="refreshLoginButt()"
                                class="flag-input" tabindex="10" />
                        </div>
                    </td>
                </tr>
                <tr class="input">
                    <td class="pt-16">
                        <label for="password">
                            Password <span class="show-pwd" onclick="togglePwdVisibility('loginPassword')">Show</span>
                        </label>
                        <input type="password" name="password" id="loginPassword" onkeyup="refreshLoginButt()">
                        <span class="min-chars">Min. 4 Characters</span>
                        <input type="hidden" name="ref" value="{{ ref }}">
                    </td>
                </tr>

                <tr class="input">
                    <td>
                        <button type="submit" id="loginButt" tabindex="12">
                            Login
                        </button>
                    </td>
                </tr>
                <tr class="reset-password text-center">
                    <td style="padding-bottom: 24px;">
                        <a href="{{ url('/resetpassword') }}" tabindex="13">
                            <?php echo $t->_('Forgot Password'); ?>?
                        </a>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    {% else %}
    <tr>
        <td>
            <?php echo $this->tag->form("/login/authenticate"); ?>
            <label class="opera-label">
                Your Mobile Number
            </label>
            <input type="text" name="mobile" id="mobileNo" tabindex="10" class="opera-input"
                style="margin-bottom: 16px;" />
            <label for="password" class="opera-label">
                Password
            </label>
            <input type="password" name="password" id="loginPassword" class="opera-input">
            <input type="hidden" name="ref" value="{{ ref }}">
            <button type="submit" id="loginButt" tabindex="12" class="opera-butt">
                Login
            </button>

            <a href="{{ url('/resetpassword') }}" tabindex="13"
                style="margin-bottom: 16px;display: block;text-align: center;">
                <?php echo $t->_('Forgot Password'); ?>?
            </a>
            </form>
        </td>
    </tr>
    {% endif %}
</table>

<script>

    function refreshLoginButt() {
        let loginButt = document.getElementById('loginButt');
        let mobile = document.getElementById("mobileNo").value;
        let password = document.getElementById("loginPassword").value;

        if (mobile.length == 0 || password.length == 0) {

            loginButt.style.background = '#616161';
            loginButt.style.border = '1px solid #616161';
            loginButt.style.color = 'rgba(255, 255, 255, 0.57)';
        } else {
            loginButt.style.background = '#01B601';
            loginButt.style.border = '1px solid #01B601';
            loginButt.style.color = 'rgba(255, 255, 255, 0.87)';
        }
    }

</script>