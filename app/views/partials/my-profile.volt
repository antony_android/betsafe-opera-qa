<table class="login" cellpadding="0" cellspacing="0">
    <tr class="login-row">
        <td class="game-title">
            My Profile
        </td>
    </tr>
    <th>{{ this.flashSession.output() }}</th>
    <tr>
        <td>
            <?php echo $this->tag->form("/my-profile/update"); ?>
            <table class="form">
                <tr class="input">
                    <td>
                        <label>
                            Your Mobile Number
                        </label>
                        {% if session.get('user-agent') !='OperaMiniExtreme' %}
                        <div class="flag-div">
                            <img src="{{ url('/img/kenya-flag.png') }}" width=24 alt="" class="flag-img" />
                            <span style="vertical-align: middle;"> +254</span>
                            <input type="text" name="mobile" id="mobile" readonly
                                value="<?=!is_null($this->session->get('auth'))?$this->session->get('auth')['phone']:''; ?>"
                                class="flag-input" {% if profile !=null %} value="
                            <?=$profile[0]['msisdn'];?>" {% endif %} />
                        </div>
                        {% else %}
                        <input type="text" name="mobile" id="mobile"
                            value="<?=!is_null($this->session->get('auth'))?$this->session->get('auth')['phone']:''; ?>"
                            class="opera-input" readonly />
                        {% endif %}
                    </td>
                </tr>
                <tr class="input">
                    <td class="pt-16">
                        <label for="first_name">
                            Name
                        </label>
                        <input type="text" name="first_name" id="first_name" placeholder="Your First Name" {% if profile
                            !=null %} value="<?=$profile[0]['first_name'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="surname">
                            Surname
                        </label>
                        <input type="text" name="surname" id="surname" placeholder="Your Surname" {% if profile !=null
                            %} value="<?=$profile[0]['surname'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="last_name">
                            Last Name
                        </label>
                        <input type="text" name="last_name" id="last_name" placeholder="Your Last Name" {% if profile
                            !=null %} value="<?=$profile[0]['last_name'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="email">
                            Email
                        </label>
                        <input type="text" name="email" id="email" placeholder="Your Email Address" {% if profile !=null
                            %} value="<?=$profile[0]['email'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="hse_street">
                            House Number / Street
                        </label>
                        <input type="text" name="hse_street" id="hse_street" placeholder="Your House Number / Street" {%
                            if profile !=null %} value="<?=$profile[0]['hse_street'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="city">
                            City
                        </label>
                        {% if session.get('user-agent') != 'OperaMiniExtreme' %}
                        <img src="{{ url('/img/down-timeout.svg') }}" alt="" class="timeout-filter-img"
                            onclick="toggleProfileFilter()" style="margin-top: 30px;" />
                        <input type="text" readonly name="city" id="city" class="form-control timeout-filter-select"
                            {%if profile !=null %} value="<?=$profile[0]['city'];?>" {% endif %}
                            onclick="toggleProfileFilter()" placeholder="Choose a City" />
                        <ul class="slip-filter-ul profile-ul-top" id="profile-filter-ul">
                            <?php foreach($cities as $city): ?>
                            <li id="city-<?=$city['city_id']; ?>" class="city" onclick="selectCity(this)">
                                <?=$city['city_name']; ?>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        {% else %}
                        <select name="city" id="city" class="form-control">
                            <option value=""></option>
                            <?php foreach($cities as $city): ?>
                            <option value="<?=$city['city_name']; ?>" <?php if($city['city_name']==$profile[0]['city']):
                                ?>
                                selected
                                <?php endif; ?>>
                                <?=$city['city_name']; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                        {% endif %}
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="address">
                            PO Box
                        </label>
                        <input type="text" name="address" id="address" placeholder="Your PO Box" {% if profile !=null %}
                            value="<?=$profile[0]['address'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label for="country">
                            Country
                        </label>
                        <input type="text" name="country" id="country" value="Kenya" placeholder="Country" {% if profile
                            !=null %} value="<?=$profile[0]['country'];?>" {% endif %}>
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16 profile-receive-comm-header">
                        Contact Preferences
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16 profile-receive-comm">
                        Please let us know if you would like to receive communication from
                        Betsafe about new and ongoing promotions by selecting one of the
                        options below.
                    </td>
                </tr>

                <tr class="input">
                    <td class="checkbox pt-16">
                        <table>
                            <tr>
                                <td style="vertical-align: top;">
                                    <input type="checkbox" class="terms-checkbox" name="receive_communication" value="1"
                                        id="yes-receive-comm" {% if profile !=null %} {% if
                                        profile[0]['receive_promo_comm']==1 %} checked {% endif %} {% else %} checked {%
                                        endif %} onchange="toggleYesReceiveCommPromo()">
                                </td>
                                <td style="font-size: 14px;">
                                    Yes please, I'd like to hear about new offers and products.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="input">
                    <td class="checkbox" style="padding-bottom: 0px;">
                        <table>
                            <tr>
                                <td style="vertical-align: top;">
                                    <input type="checkbox" class="terms-checkbox" name="dont_receive_communication"
                                        id="no-receive-comm" value="0" {% if profile !=null %} {% if
                                        profile[0]['receive_promo_comm']==0 %} checked {% endif %} {% endif %}
                                        onchange="toggleNoReceiveCommPromo()">
                                </td>
                                <td style="font-size: 14px;">
                                    No thanks, I don't want to hear about new offers and products.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="input">
                    <td>
                        <button type="submit" class="login-join" id="joinNowButt"
                            style="background: #01B601;border:1px solid #01B601;">
                            Update My Profile
                        </button>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>

<script>

    document.getElementById('profile-filter-ul').style.display = 'none';

    function toggleProfileFilter() {

        if (document.getElementById('profile-filter-ul').style.display == 'none') {
            document.getElementById('profile-filter-ul').style.display = '';
        } else {
            document.getElementById('profile-filter-ul').style.display = 'none';
        }
    }

    function selectCity(elem) {
        for (let el of document.getElementsByClassName('city')) el.classList.remove("slip-filter-selected");
        elem.classList.add("slip-filter-selected");
        document.getElementById('city').value = elem.innerText;
        document.getElementById('profile-filter-ul').style.display = 'none';
        document.getElementById("address").focus();
    }

    function toggleYesReceiveCommPromo() {

        if (document.getElementById('yes-receive-comm').checked) {
            document.getElementById('no-receive-comm').checked = false;
        } else {
            document.getElementById('no-receive-comm').checked = true;
        }
    }

    function toggleNoReceiveCommPromo() {

        if (document.getElementById('no-receive-comm').checked) {
            document.getElementById('yes-receive-comm').checked = false;
        } else {
            document.getElementById('yes-receive-comm').checked = true;
        }
    }

</script>