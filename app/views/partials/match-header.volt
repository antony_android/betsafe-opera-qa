<table class="league">
    <tr>
        <td class="time">
            <span class="play-time">
                <?php if(!is_null($day['game_id']) && $day['game_id'] != 100): ?>
                ID: {{ day['game_id'] }} -
                <?php endif; ?>
                <?php echo date('g:ia', strtotime($day['start_time'])); ?>
            </span>
            <?php echo date('D d/m', strtotime($day['start_time'])); ?>
        </td>
    </tr>
    <tr class="game">
        <td colspan="10">
            <table width="100%">
                <tr>
                    <td class="clubs" colspan="2">
                        <p class="pb-2">
                            <a href="{{url('match/open/') }}{{day['match_id']}}">
                                <?php echo $day['home_team']; ?>
                            </a>
                        </p>
                        <p>
                            <a href="{{url('match/open/') }}{{day['match_id']}}">
                                <?php echo $day['away_team']; ?>
                            </a>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="meta">
            <a href="{{url('match/open/') }}{{day['match_id']}}" style="color: rgba(255, 255, 255, 0.57) !important;"
                tabindex="12">
                <?php echo $eventsTitle." / ".$day['competition_name']; ?>
            </a>
        </td>
    </tr>
    {% if session.get('user-agent') =='OperaMiniExtreme' %}
    <tr>
        <td>
            <?php if(isset($selected) && $selected == 'live') { ?>
            <a href="{{url('livematch/open/') }}{{day['match_id']}}"
                style="display: block;margin-bottom: 8px;font-size: medium;">
                <span>
                    <?php echo "+".$day['side_bets']; ?> Markets
                </span>
            </a>
            <?php } else { ?>
            <a href="{{url('match/open/') }}{{day['match_id']}}"
                style="display: block;margin-bottom: 8px;font-size: medium;">
                <span>
                    <?php echo "+".$day['side_bets']; ?> Markets
                </span>
            </a>
            <?php } ?>
        </td>
    </tr>
    {% endif %}
</table>