<table class="full-width" cellpadding="0" cellspacing="0">

  <tr class="page-header-row bet-history-header">
    <td class="slip-one-game-title std-pd" colspan="2">
      <a href="{{ url('/bet-history') }}" style="font-size: 12px;">
        <img src="{{ url('/img/icon-arrow-right.svg')}}" alt="<" style="vertical-align: text-top;" /> ID:
        <?=$myBet['coupon_code'] ?>
      </a>
    </td>
  </tr>
  <tr class="filter-bets-row bet-history-header">
    <td class="std-pd slip-one-game-header" colspan="2">
      <img src="{{ url('/img/betsafe-logo-slip.svg')}}" alt="" width="130px" />
      <p>
        Coupon ID:
        <?=$myBet['coupon_code'] ?>
      </p>
      <p>
        Bet placed on
        <?= date('D d/m/Y', strtotime($myBet['created'])) ?> at
        <?php echo date('g:ia', strtotime($myBet['created'])); ?>
      </p>
      <p>
        Status:
        <?php
            if($myBet['cashout']){
              echo '<span class="won">Cashout</span>';
            }elseif($myBet['status']==10){
              echo '<span class="open">Open</span>';
            }elseif($myBet['win']==1){
              echo '<span class="won">Won</span>';
            }elseif($myBet['win']==2){
              echo '<span class="lost">Lost</span>';
            }else{
              echo '<span class="open">Open</span>';
            }
          ?>
      </p>
    </td>
  </tr>
  <tr class="bet-history-header">
    <td class="std-pd slip-hist-details pv-2">
      Odds:
    </td>
    <td class="std-pd slip-hist-details text-right pv-2">
      <?=number_format($myBet['total_odd'], 2) ?>
      <?php $totalOdds = $myBet['total_odd']; ?>
    </td>
  </tr>
  <tr class="bet-history-header">
    <td class="std-pd slip-hist-details pv-2">
      Total Stake:
    </td>
    <?php $stake = $myBet['bet_amount']; ?>
    <?php $netStake = $stake/1.075; ?>
    <td class="std-pd slip-hist-details text-right pv-2">
      <?=number_format($myBet['bet_amount'], 2) ?>
    </td>
  </tr>
  <?php $exciseTax = $stake - $netStake; ?>
  <?php $grossWin = $netStake * $totalOdds; ?>
  <?php if($myBet['cashout']): ?>
  <?php $grossWin = $myBet['user_winnings']*1.25; ?>
  <?php endif; ?>
  <?php $netWin = $grossWin - $stake; ?>
  <?php $withHoldingTax = $netWin * 0.2; ?>
  <?php if($myBet['cashout']): ?>
  <?php $withHoldingTax = $grossWin - $myBet['user_winnings']; ?>
  <?php endif; ?>
  <?php $payout = $grossWin - $withHoldingTax; ?>
  <tr class="bet-history-header">
    <td class="std-pd slip-hist-details payout pv-2">
      <?php if($myBet['status']==50): ?>
      Payout
      <?php else: ?>
      Potential Payout
      <?php endif; ?>
      <span id="expandSlip" onclick="toggleSlip()">
        <img style="margin-left: 4px;" src="{{ url('/img/slip-arrow-down.svg')}}" alt="More" />
      </span>
    </td>
    <td class="std-pd slip-hist-details text-right pv-2">
      <?php if($myBet['cashout']): ?>
      <span class="won">
        CASHOUT &nbsp;
        <?="KSH ".number_format($myBet['user_winnings'],2); ?>
      </span>
      <?php elseif($myBet['status'] == 10): ?>
      <span class="won">
        <?="KSH ".number_format($payout, 2); ?>
      </span>
      <?php elseif($myBet['win'] == 1): ?>
      <span class="won">
        WON
        <?="KSH ".number_format($payout, 2); ?>
      </span>
      <?php elseif($myBet['win'] == 2): ?>
      <span class="lost">
        LOST
        <?="KSH ".number_format($payout, 2); ?>
      </span>
      <?php endif; ?>
    </td>
  </tr>
  <tr class="bet-history-header" id="excise_row">
    <td class="std-pd slip-hist-details pv-2">Excise Tax (7.5%)</td>
    <td class="std-pd slip-hist-details pv-2 text-right" id="excise-tax">
      <?= "KSH ".number_format($exciseTax, 2); ?>
    </td>
  </tr>
  <tr class="bet-history-header" id="grosswin_row">
    <td class="std-pd slip-hist-details pv-2">Gross Win</td>
    <td class="std-pd slip-hist-details pv-2 text-right" id="possible_win_id">
      <?= "KSH ".number_format($grossWin, 2); ?>
    </td>
  </tr>
  <tr class="bet-history-header" id="tax_row">
    <td class="std-pd slip-hist-details pv-2">Withholding Tax 20%</td>
    <td class="std-pd slip-hist-details pv-2 text-right" id="tax_id">
      <?= "KSH ".number_format($withHoldingTax,2);  ?>
    </td>
  </tr>
  <?php if($myBet['cashout_enabled'] && $myBet['cashout_amount'] > 0): ?>
  <tr class="bet-history-header">
    <td class="game-title" colspan="2">
      {% if pageToLoad=="CASHOUT" %}
      <div class="page-header-row slip-one-game-cashout std-pd" style="padding-top: 4px !important">
        <?php echo $this->tag->form("/bet-history/confirmcashout/".$myBet['isolutions_couponid']."/".$myBet['cashout_amount']); ?>
        <input type="hidden" name="coupon_id" value="<?=$myBet['isolutions_couponid']; ?>" />
        <input type="hidden" name="cashout_amount" value="<?=$myBet['cashout_amount']; ?>" />
        <table class="full-width">
          <tr>
            <td>
              Cashout Offer
            </td>
            <td rowspan="2">
              <button type="submit" class="cashout-confirm">
                Confirm
              </button>
            </td>
          </tr>
          <tr>
            <td style="font-weight: 700;font-size: 12px;">
              KSH
              <?=$myBet['cashout_amount']; ?>
            </td>
          </tr>
        </table>
        </form>
      </div>
      {% else %}
      <a
        href="{{ url('/bet-history/cashout/')}}<?=$myBet['isolutions_couponid'];?>/<?=$myBet['coupon_code'];?>/<?=$myBet['cashout_amount'];?>">
        <div class="page-header-row slip-one-game-cashout std-pd">
          Cashout Available
          <img src="{{ url('/img/arrow-right.svg') }}" alt="" />
        </div>
      </a>
      {% endif %}
    </td>
  </tr>
  <?php endif; ?>
  <?php if(!empty($betDetails)): ?>
  <?php foreach($betDetails as $bet): ?>
  <tr class="bet">
    <td colspan="2">
      <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
        <tr>
          <td class="matches-border" colspan="4"></td>
        </tr>
        <tr>
          <td class="std-pd pt-10">
            <table class="league">
              <tr>
                <td class="time" colspan="2">
                  <span class="play-time">
                    <?php echo date('g:ia', strtotime($bet['start_time'])); ?>
                  </span>
                  <?php echo date('D d/m', strtotime($bet['start_time'])); ?>
                </td>
                <td class="text-right slip-outcome">
                  <?php
                    if($bet['outcome_id']==0){
                      echo '<span class="open">Open</span>';
                    }elseif($bet['outcome_id']==3){
                      echo '<span class="won">Won</span>';
                    }elseif($bet['outcome_id']==1){
                      echo '<span class="lost">Lost</span>';
                    }else{
                      echo '<span class="open">Open</span>';
                    }
                  ?>
                </td>
              </tr>
              <tr class="game">
                <td colspan="3">
                  <table width="100%">
                    <tr>
                      <td class="clubs" colspan="3">
                        <p class="pb-2">
                          <?php echo $bet['home_team']; ?>
                        </p>
                        <p>
                          <?php echo $bet['away_team']; ?>
                        </p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="meta" colspan="3">
                  <?php echo $bet['sport']." / ".$bet['competition_name']; ?>
                </td>
              </tr>
              <tr>
                <td class="time" colspan="2">
                  <span class="play-time">
                    <?= $bet['bet_type']." (".$bet['bet_pick'].")" ?>
                  </span>
                </td>
                <td class="time text-right">
                  <?= number_format($bet['odd_value'], 2) ?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <?php endforeach; ?>
  <?php endif; ?>
</table>
<script>
  document.getElementById('excise_row').style.display = 'none';
  document.getElementById('grosswin_row').style.display = 'none';
  document.getElementById('tax_row').style.display = 'none';

  function toggleSlip() {
    if (document.getElementById('excise_row').style.display == 'none') {

      document.getElementById('excise_row').style.display = '';
      document.getElementById('grosswin_row').style.display = '';
      document.getElementById('tax_row').style.display = '';
      document.getElementById('expandSlip').innerHTML = '<img style="margin-left: 4px;" src="/lite/img/slip-arrow-up.svg" alt="Less" />';
    } else {
      document.getElementById('excise_row').style.display = 'none';
      document.getElementById('grosswin_row').style.display = 'none';
      document.getElementById('tax_row').style.display = 'none';
      document.getElementById('expandSlip').innerHTML = '<img style="margin-left: 4px;" src="/lite/img/slip-arrow-down.svg" alt="More" />';
    }
  }
</script>