<table class="full-width" cellpadding="0" cellspacing="0">

    <tr class="page-header-row bet-history-header">
        <td class="slip-one-game-title std-pd" colspan="2">
            <?php if($referrer == "tran"): ?>
            <a href="{{ url('/transactions') }}">
                <img src="{{ url('/img/icon-arrow-right.svg') }}" alt="<" /> ID:
                <?=$myBet['jackpot_bet_id'] ?>
            </a>
            <?php else: ?>
            <a href="{{ url('/jackpot/history') }}">
                <img src="{{ url('/img/icon-arrow-right.svg') }}" alt="<" /> ID:
                <?=$myBet['jackpot_bet_id'] ?>
            </a>
            <?php endif; ?>
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td class="std-pd slip-one-game-header" colspan="2">
            <img src="{{ url('/img/betsafe-logo-slip.svg') }}" alt="" width="130px" />
            <p>
                <?=$myBet['jackpot_name'] ?>
            </p>
            <p>
                Coupon ID:
                <?=$myBet['jackpot_bet_id'] ?>
            </p>
            <p>
                Bet placed
                <?= date('D d/m/Y', strtotime($myBet['created'])) ?> at
                <?php echo date('g:ia', strtotime($myBet['created'])); ?>
            </p>
            <p>
                Status:
                <?php
                    if($myBet['status']==0){
                    echo '<span class="open">Open</span>';
                    }elseif($myBet['win']==5){
                    echo '<span class="won">Winner!</span>';
                    }elseif($myBet['win']==3){
                    echo '<span class="lost">Lost</span>';
                    }elseif($myBet['win']==-1){
                    echo '<span class="lost">Cancelled</span>';
                    }else{
                    echo '<span class="open">Open</span>';
                    }
                ?>
            </p>
        </td>
    </tr>
    <?php if(!empty($betDetails)): ?>
    <?php foreach($betDetails as $bet): ?>
    <tr class="bet">
        <td colspan="2">
            <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="matches-border" colspan="4"></td>
                </tr>
                <tr>
                    <td class="std-4pd pt-10">
                        <table class="league">
                            <tr>
                                <td class="std-4pd time" colspan="2">
                                    <span class="play-time">
                                        <?php echo date('g:ia', strtotime($bet['created'])); ?>
                                    </span>
                                    <?php echo date('D d/m', strtotime($bet['created'])); ?>
                                </td>
                                <td class="std-4pd text-right slip-outcome">
                                    <?php
                                        if($bet['status']==1){
                                            echo '<span class="open">-</span>';
                                        }elseif($bet['status']==5){
                                            echo '<span class="won">
                                                <img src="/lite/img/icon-winner.svg" width="10px" alt=""/>
                                            </span>';
                                        }elseif($bet['status']==3){
                                            echo '<span class="lost">
                                                <img src="/lite/img/no_win.svg" width="10px" alt="" />
                                            </span>';
                                        }else{
                                            echo '<span class="open">-</span>';
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr class="game">
                                <td class="std-4pd" colspan="3">
                                    <table width="100%">
                                        <tr>
                                            <td class="clubs" colspan="3">
                                                <p class="pb-2">
                                                    <?php echo $bet['home_team']; ?>
                                                </p>
                                                <p>
                                                    <?php echo $bet['away_team']; ?>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="std-4pd meta" colspan="3">
                                    <?php echo "Football / ".$bet['competition_name']; ?>
                                </td>
                            </tr>
                            <tr class="odds">
                                <td class="std-4pd">
                                    <table cellspacing="0" cellpadding="0" class="full-width">
                                        <tr>

                                            <td>
                                                <button class="jb-butt <?php if($bet['bet_pick']=='1'){
                                                            echo ' picked';
                                                         }?>">
                                                    1
                                                </button>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                                <td class="std-4pd">
                                    <table cellspacing="0" cellpadding="0" class="full-width">
                                        <tr>

                                            <td>
                                                <button class="jb-butt <?php if($bet['bet_pick']=='X'){ 
                                                        echo ' picked'; 
                                                    }?>">
                                                    X
                                                </button>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                                <td class="std-4pd">
                                    <table cellspacing="0" cellpadding="0" class="full-width">
                                        <tr>

                                            <td>
                                                <button class="jb-butt <?php if($bet['bet_pick']=='2'){ 
                                                        echo ' picked'; 
                                                    }?>">
                                                    2
                                                </button>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
</table>