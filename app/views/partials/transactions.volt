<table class="full-width" cellpadding="0" cellspacing="0">

    <tr class="page-header-row bet-history-header">
        <td class="game-title std-pd">
            Transactions
        </td>
    </tr>
    <tr class="page-header-row bet-history-header">
        <td class="balance-title std-pd">
            Your Balance: <span class="bold" id="tranbalance">KSH
                {{ session.get('auth')['balance'] }}
            </span>
        </td>
    </tr>

    <?php if(!empty($transactions)): ?>
    <?php foreach($transactions as $transaction): ?>
    <tr class="bet">
        <td>
            <?php if($transaction['coupon_id'] > 0 && !is_null($transaction['coupon'])): ?>
            <a href="{{ url('transactions/show/') }}{{transaction['coupon_id']}}/{{transaction['coupon']}}"
                class="undecorate">
                <?php elseif($transaction['coupon_id'] > 0 && is_null($transaction['coupon']) && strpos($transaction['account'], 'Jackpot') !== false): ?>
                <a href="{{ url('jackpot/show/') }}{{transaction['coupon_id']}}/tran" class="undecorate">
                    <?php endif; ?>
                    <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="matches-border" colspan="4"></td>
                        </tr>
                        <tr>
                            <td class="std-pd pt-10">
                                <table class="league">
                                    <tr>
                                        <td class="time" colspan="3">
                                            <span class="play-time">
                                                <?php echo date('g:ia', strtotime($transaction['transaction_time'])); ?>
                                            </span>
                                            <?php echo date('D d/m', strtotime($transaction['transaction_time'])); ?>
                                        </td>
                                    </tr>
                                    <tr class="game">
                                        <td colspan="3">
                                            <table width="100%">
                                                <tr>
                                                    <td class="clubs" colspan="3" style="style=" font-size: 12px;"">
                                                        ID:
                                                        <?php echo $transaction['isolutions_transactionid']; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="slip-hist pt-10 pb-0" colspan="2">
                                            TYPE:
                                        </td>
                                        <td class="slip-hist pt-10 pb-0 text-right" colspan="2">
                                            <span class="account-hist-sp">CHANGE:</span>
                                            <span class="tran-balance">BALANCE:</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="slip-hist-details" colspan="2">
                                            <?php 
                                        $account = $transaction['account'];
                                        if($account == 'Bet'){
                                            $account = 'Bet Placed';
                                        }else if($account == 'M-Pesa Withdrawal'){
                                            $account = 'Withdrawal';
                                        }else if($account == 'M-Pesa Deposit'){
                                            $account = 'Deposit';
                                        }else if($account == 'Bet Win'){
                                            $account = 'Winnings';
                                        }
                                        ?>
                                            <span class="account-type-tran"> {{ account }} </span>
                                        </td>
                                        <td class="slip-hist-details text-right pb-8 pt-4">
                                            <span
                                                class="account-hist-sp <?php echo ($transaction['iscredit'] == 1)?' tran-up':' tran-down'; ?>">
                                                <?php echo ($transaction['iscredit'] == 1)?"+":"-"; ?>{{
                                                transaction['amount'] }}
                                            </span>
                                            <span class="tran-balance">{{ transaction['running_balance'] }}</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <?php if($transaction['coupon_id'] > 0): ?>
                </a>
                <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php else: ?>

    <tr class="empty-slip-row">
        <td class="" colspan="4">
            <img src="{{ url('/img/logo.svg')}}" width="90px" alt="BetSafe" />
            <span>There are no transactions yet in your account</span>
        </td>
    </tr>
    <?php endif; ?>
</table>