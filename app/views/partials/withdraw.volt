<table class="login" cellpadding="0" cellspacing="0">
	<tr class="login-row">
		<td class="game-title">
			Withdraw
		</td>
	</tr>
	<th>{{ this.flashSession.output() }}</th>
	<tr>
		<td class="deposit-details">
			Enter your desired withdrawal amount in KSH
		</td>
	</tr>
	<tr>
		<td>
			<?php echo $this->tag->form("withdraw/withdrawal"); ?>
			<table class="form deposit-input" cellpadding="0" cellspacing="0">
				<tr class="input">
					<td colspan="4">
						<label>
							Enter Amount
						</label>
						<input type="text" name="amount" id="withdrawal_amount" placeholder="KSH" value="" />
						<span class="min-chars">Min. 20</span>
					</td>
				</tr>

				{% if session.get('user-agent') !='OperaMiniExtreme' %}
				<tr>
					<td class="std-4pd" style="width: 25%;padding: 0px 4px 0px 8px;">
						<button class="deposit-sel" onclick="fiftyWithdrawal()" type="button" id="fiftyBobButt">
							49
						</button>
					</td>
					<td class="std-4pd" style="width: 25%;padding: 0px 4px">
						<button class="deposit-sel" onclick="hundredWithdrawal()" type="button" id="hundredBobButt">
							99
						</button>
					</td>
					<td class="std-4pd" style="width: 25%;padding: 0px 4px">
						<button class="deposit-sel" onclick="twoHundredWithdrawal()" type="button"
							id="twoHundredBobButt">
							199
						</button>
					</td>
					<td style="width: 25%;padding: 0px 8px 0px 4px">
						<button class="deposit-sel" onclick=" fiveHundredWithdrawal()" type="button"
							id="fiveHundredBobButt">
							499
						</button>
					</td>
				</tr>
				{% else %}
				<tr>
					<td class="std-pd">
						<table class="highlights full-width">
							<tr class="odds">
								<td style="padding: 0px 4px 0px 0px;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<button type="button" onclick="fiftyWithdrawal()" id="fiftyBobButt"
												style="background: #2E2F30 !important;width:100%">
												49
											</button>
										</tr>
									</table>
								</td>
								<td style="padding: 0px 4px 0px 4px;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<button type="button" onclick="hundredWithdrawal()" id="hundredBobButt"
												style="background: #2E2F30 !important;width:100%">
												99
											</button>
										</tr>
									</table>
								</td>
								<td style="padding: 0px 4px 0px 4px;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<button type="button" onclick="twoHundredWithdrawal()"
												id="twoHundredBobButt"
												style="background: #2E2F30 !important;width:100%">
												199
											</button>
										</tr>
									</table>
								</td>
								<td style="padding: 0px 0px 0px 4px;">
									<table cellspacing="0" cellpadding="0">
										<tr>
											<button type="button" onclick=" fiveHundredWithdrawal()"
												id="fiveHundredBobButt"
												style="background: #2E2F30 !important;width:100%">
												499
											</button>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				{% endif %}

				<tr class="input">
					<td colspan="4">
						<button type="submit" onclick="fbWithdaw()" class="green-theme m-0">
							Withdraw
						</button>
					</td>
				</tr>

				<tr>
					<td class="deposit-terms pb-12" colspan="4">
						Free Withdrawals ends 1st January 2022
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
	<tr>
		<td class="depo-mpesa-header">
			When will I recieve my money?
		</td>
	</tr>
	<tr>
		<td class="depo-mpesa-details">
			<p class="pb-16">
				We process withdrawals within a matter of minutes. Should your request take longer than 30 minutes,
				please contact our Customer Care team.
			</p>
		</td>
	</tr>
	<tr>
		<td class="depo-mpesa-header">
			When can I withdraw my bonus balance?
		</td>
	</tr>
	<tr>
		<td class="depo-mpesa-details">
			<p>
				Your bonus balance will automatically be credited to your withdrawable balance once the wagering/betting
				requirements
				for that bonus have been completed.
			</p>
			<p style="padding: 20px 0px;">
				Once completed, the bonus funds will be available for withdrawing.
			</p>
		</td>
	</tr>
	<tr>
		<td class="depo-mpesa-header">
			Minimum & Maximum Withdrawals
		</td>
	</tr>
	<tr>
		<td class="depo-mpesa-details">
			<p>
				The minimum withdrawal amount is kshs 20.
			</p>
			<p style="padding: 20px 0px;">
				The maximum withdrawal amount is kshs 70,000 per transaction and kshs 300,000
				per day.
			</p>
		</td>
	</tr>
</table>
<script>
	function fiftyWithdrawal() {
		document.getElementById("withdrawal_amount").value = 49;
		document.getElementById("fiftyBobButt").style.background = '#01B601';
		document.getElementById("fiftyBobButt").style.border = '1px solid #01B601';
		document.getElementById("hundredBobButt").style.background = '#2E2F30';
		document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
		document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
		document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
	}

	function hundredWithdrawal() {
		document.getElementById("withdrawal_amount").value = 99;
		document.getElementById("hundredBobButt").style.background = '#01B601';
		document.getElementById("hundredBobButt").style.border = '1px solid #01B601';
		document.getElementById("fiftyBobButt").style.background = '#2E2F30';
		document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
		document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
		document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
	}

	function twoHundredWithdrawal() {
		document.getElementById("withdrawal_amount").value = 199;
		document.getElementById("twoHundredBobButt").style.background = '#01B601';
		document.getElementById("twoHundredBobButt").style.border = '1px solid #01B601';
		document.getElementById("fiftyBobButt").style.background = '#2E2F30';
		document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("hundredBobButt").style.background = '#2E2F30';
		document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
		document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
	}

	function fiveHundredWithdrawal() {
		document.getElementById("withdrawal_amount").value = 499;
		document.getElementById("fiveHundredBobButt").style.background = '#01B601';
		document.getElementById("fiveHundredBobButt").style.border = '1px solid #01B601';
		document.getElementById("fiftyBobButt").style.background = '#2E2F30';
		document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("hundredBobButt").style.background = '#2E2F30';
		document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
		document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
		document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
	}

	function updateAmt(value) {
		document.getElementById("amountput").value = value;
	}

</script>