<?php 
      function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
       $empty_row_text = '<table cellspacing="0" cellpadding="0"> <tr> <td class="">
         <button  class="odds-btn disabled-odds" style="text-align: center;">
          <span class="odd" style="opacity:0.2;">
            <img height="15" width="15" src="/img/padlock.svg" alt="-" />
          </span>
        </button></td> </tr> </table>';
?>
<table class="jp-opened full-width ">
    <th>{{ this.flashSession.output() }}</th>
    <?php if(isset($timeOutPeriodRemaining)) { ?>
    <tr class="page-header-row bet-history-header">
        <td class="game-title std-pd">
            Responsible Gaming
        </td>
    </tr>
    <tr>
        <td>
            <p class="depo-mpesa-details pt-16 pb-16 std-pd" style="text-align: left;">
                You have chosen to Time-Out for a period of
                <?=$setTimeout; ?>.
                Time remaining
                <?=$timeOutPeriodRemaining; ?>.
            </p>
        </td>
    </tr>
    <?php } ?>
    <tr class="page-header-row">
        <td class="game-title std-pd pt-8 pb-12">
            <?php echo $jEvent['jackpot_name']; ?>
        </td>
    </tr>
    <tr>
        <td class="std-pd pb-8 bold">
            Win KSH
            <?php echo number_format($jEvent['jackpot_amount']); ?>
        </td>
    </tr>
    <tr class="game">
        <td class="std-pd pb-12 wins">
            Pick
            <?php echo $jEvent['total_games']; ?> predictions, prizes for
            <?php echo $jEvent['requisite_wins']; ?> correct.
        </td>
    </tr>
    <tr>
        <td class="std-pd pb-12 wins">
            <?php echo $this->tag->form("/jackpot/auto"); ?>
            <input type="hidden" name="jackpot_event_id" value="<?=$jEvent['jackpot_event_id']; ?>" />
            <button type="submit" id="pick_for_me"
                class="pick-for-me {% if jPCount == jEvent['total_games'] %} picked {% endif %} ?>">
                Pick for me
            </button>
            </form>
        </td>
    </tr>
    <tr>
        <td class="std-pd pb-8 wins">
            Ends:
            <?=$jEvent['kick_off']; ?>
        </td>
    </tr>
    <tr>
        <td>
            <!-- List matches -->

            <?php foreach($matches as $day): ?>
            <?php $theMatch = @$theBetslip[$day['parent_match_id']]; ?>
            <?php if(!is_null($theMatch)): ?>
            <?php $selections = explode(",",$theMatch['bet_pick']); ?>
            <?php endif; ?>

            <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="matches-border" colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4" class="std-pd pt-10">
                        {{ partial('partials/jackpot-header') }}
                    </td>
                </tr>
                <tr class="odds">
                    <?php if($day['home_odd']) { ?>
                    <td style="padding-left: 8px;">
                        <table cellspacing="0" cellpadding="0">
                            <tr>

                                <td class="<?php echo $day['parent_match_id']; ?> <?php
                                    echo clean($day['parent_match_id'].$day['sub_type_id'].$day['home_team']);
                                    ?>" style="text-align: left;padding-right:4px;">
                                    <button href="javascript:;" class="<?php if($theMatch && in_array('1', $selections) && $theMatch['sub_type_id']==$day['sub_type_id']){
                                            echo ' picked';
                                        }?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1X2"
                                        bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>"
                                        oddvalue="<?php echo $day['home_odd']; ?>" target="." odd-key="1"
                                        parentmatchid="<?php echo $day['parent_match_id']; ?>"
                                        id="<?php echo $day['parent_match_id']; ?>"
                                        jackpot_event_id="<?php echo $jEvent['jackpot_event_id']; ?>"
                                        oddsid="<?php echo $day['betradar_odd_id']; ?>"
                                        custom="<?php echo clean($day['parent_match_id'].$day['sub_type_id'].$day['home_team']); ?>"
                                        sub-type-id="<?= $day['sub_type_id']; ?>" special-bet-value="0.0"
                                        onClick="addJPBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('oddsid'), this.getAttribute('jackpot_event_id'))">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="odd-pd">1</td>
                                            </tr>
                                            <tr class="odd">
                                                <td class="odd-pd <?php if($theMatch && in_array('1', $selections) && $theMatch['sub_type_id']==$day['sub_type_id']){
                                                        echo ' odd-pd-picked';
                                                    }?>">
                                                    <?php echo number_format($day['home_odd'], 2); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </button>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <?php } ?>
                    <?php if($day['neutral_odd']) { ?>
                    <td class="">
                        <table>
                            <tr>
                                <td class="<?php echo $day['parent_match_id']; ?> <?php
                                                echo clean($day['parent_match_id'].$day['sub_type_id'].'Draw');?>"
                                    style="padding-left: 4px;padding-right: 4px;">
                                    <button href="javascript:;" class="<?php if($theMatch && in_array('X', $selections) && $theMatch['sub_type_id']==$day['sub_type_id']){
                                                echo ' picked';
                                            }?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1X2"
                                        bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>"
                                        oddvalue="<?php echo $day['neutral_odd']; ?>" target="javascript:;" odd-key="X"
                                        parentmatchid="<?php echo $day['parent_match_id']; ?>"
                                        id="<?php echo $day['parent_match_id']; ?>"
                                        oddsid="<?php echo $day['betradar_odd_id']; ?>"
                                        custom="<?php echo clean($day['parent_match_id'].$day['sub_type_id'])?>Draw"
                                        sub-type-id="<?= $day['sub_type_id']; ?>" special-bet-value="0.0"
                                        jackpot_event_id="<?php echo $jEvent['jackpot_event_id']; ?>"
                                        onClick="addJPBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('oddsid'), this.getAttribute('jackpot_event_id'))">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="odd-pd">X</td>
                                            </tr>
                                            <tr class="odd">
                                                <td class="odd-pd <?php if($theMatch && in_array('X', $selections) && $theMatch['sub_type_id']==$day['sub_type_id']){
                                                        echo ' odd-pd-picked';
                                                    }?>">
                                                    <?php echo number_format($day['neutral_odd'], 2); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </button>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <?php } ?>
                    <?php if($day['away_odd']) { ?>
                    <td style="padding-right: 8px;">
                        <table>
                            <tr>
                                <td class="<?php echo $day['parent_match_id']; ?> <?php
                                    echo clean($day['parent_match_id'].$day['sub_type_id'].$day['away_team']);
                                    ?>" style="padding-left: 4px;text-align: left;">
                                    <button href="javascript:;" class="<?php if($theMatch && in_array('2', $selections) && $theMatch['sub_type_id']==$day['sub_type_id']){
                                            echo ' picked';
                                        }?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="1X2"
                                        bettype='jackpot' oddsid="<?php echo $day['betradar_odd_id']; ?>"
                                        awayteam="<?php echo $day['away_team']; ?>"
                                        oddvalue="<?php echo $day['away_odd']; ?>" target="javascript:;" odd-key="2"
                                        parentmatchid="<?php echo $day['parent_match_id']; ?>"
                                        id="<?php echo $day['parent_match_id']; ?>"
                                        custom="<?php echo clean($day['parent_match_id'].$day['sub_type_id'].$day['away_team']); ?>"
                                        sub-type-id="<?= $day['sub_type_id']; ?>" special-bet-value="0.0"
                                        jackpot_event_id="<?php echo $jEvent['jackpot_event_id']; ?>"
                                        onClick="addJPBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'), this.getAttribute('oddsid'), this.getAttribute('jackpot_event_id'))">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="odd-pd">2</td>
                                            </tr>
                                            <tr class="odd">
                                                <td class="odd-pd <?php if($theMatch && in_array('2', $selections) && $theMatch['sub_type_id']==$day['sub_type_id']){
                                                        echo ' odd-pd-picked';
                                                    }?>">
                                                    <?php echo number_format($day['away_odd'], 2); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </button>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <?php } ?>
                </tr>
            </table>

            <?php endforeach; ?>

        </td>
    </tr>
</table>

<table class="betslip full-width">
    <tr class="details">
        <td colspan="3">
            <table class="full-width">
                <tr>
                    <td class="slip-stake std-pd" style="padding-top: 12px !important;">Jackpot Betslip</td>
                </tr>
                <tr>
                    <td class="std-pd">Price</td>
                    <td class="text-right std-pd" id="bet_amount">
                        <?= "KSH ".number_format($jpBetAmount, 2); ?>
                    </td>
                </tr>
                <tr>
                    <td class="std-pd">Number of Selections</td>
                    <td class="text-right std-pd" id="total_stake">
                        <span id="selections">
                            {{ jPCount }}
                        </span>
                        of {{ jEvent['total_games'] }}
                    </td>
                </tr>
                <tr id="grosswin_row">
                    <td class="std-pd">Total Tickets</td>
                    <td class="text-right std-pd" id="total_tickets">
                        1
                    </td>
                </tr>
                <tr id="tax_row">
                    <td class="std-pd">Total Price</td>
                    <td class="text-right std-pd" id="total_price">
                        <?= "KSH ".number_format(round($jpBetAmount, 2));  ?>
                    </td>
                </tr>
                <tr>
                    <td class="std-pd" colspan="5">
                        {% if session.get('auth') != null %}
                        <?php echo $this->tag->form("/jackpot/placebet"); ?>
                        <input type="hidden" name="jackpot_event_id" value="<?=$jEvent['jackpot_event_id']; ?>" />
                        <input type="hidden" name="jackpotAmount" value="<?=$jpBetAmount; ?>" />
                        {% if session.get('user-agent') !='OperaMiniExtreme' %}
                        <button type="submit" <?php if(!is_null($jEvent)){ if($jEvent['total_games'] !=$jPCount){
                            echo "readonly" ; }} ?>
                            class="place mb-0
                            <?php if(!is_null($jEvent)){ if($jEvent['total_games'] == $jPCount){ echo 'green-theme'; }} ?>"
                            id="jackpot_bet_bet" style="font-size: 12px;">
                            <?php if(!is_null($jEvent)): ?>
                            <?php if($jEvent['total_games'] == $jPCount): ?>
                            Place Jackpot Bet
                            <?php else: ?>
                            Pick
                            <?=$jEvent['total_games']; ?> selections to place bet
                            <?php endif; ?>
                            <?php else: ?>
                            Pick
                            <?=$jEvent['total_games']; ?> selections to place bet
                            <?php endif; ?>
                        </button>
                        {% else %}
                        <button type="submit" class="place mb-0 green-theme">
                            Place Jackpot Bet
                        </button>
                        {% endif %}
                        </form>
                        {% else %}
                        <form method="post" action="{{url('login')}}" style="width: 100%;">
                            <input type="hidden" name="ref" value="jackpot/open/<?=$jEvent['jackpot_event_id']; ?>" />
                            <button class="login-join mb-0" type="submit" id="jackpot_bet_bet">
                                Login to Place Bet
                            </button>
                        </form>
                        {% endif %}
                    </td>
                </tr>
                <tr style="height: 70px;">
                    <td class="" colspan="5">
                        <?php echo $this->tag->form("/betslip/clearjpslip"); ?>
                        <input type="hidden" name="jackpot_event_id" value="<?=$jEvent['jackpot_event_id']; ?>" />
                        <button type="submit" class="delete-all">Clear all</button>
                        </form>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>