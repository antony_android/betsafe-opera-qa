<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title">
            Welcome Bonus
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p>
                Get a 100% Welcome Free Bet equal to your first deposit up to KSH 2,000 when you register with Betsafe!
            </p>
            <p class="bonus-message-sep">
                3 easy steps to claim your Welcome Free Bet:
            </p>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <ol>
                <li>Register with Betsafe</li>
                <li>Make a deposit of at least KSH 49</li>
                <li>Place bets matching your deposut amount with minimum odds of 2.00</li>
            </ol>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-16">
            How to activate Free Bet?
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p>
                To activate your Free Bet, place bets matching your first deposit with odds of 2.00 and you will be
                credited with your
                Free Bet within 24 hours of the bets being settled.
            </p>
            <p class="bonus-message-sep">
                The Welcome Free Bet offer is valid for 30 days after registration.
            </p>
            <p>
                Your Free Bet must be placed on a multi-bet with at least 3 selections, minimum odds of 1.30 per
                selection and total combined odds of at least 3.50
            </p>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-16">
            Terms & Conditions
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                The promotion period runs from 12th April 2021 until further notice.
            </p>
            <p class="pb-16">
                The Welcome Offer is valid for 30 days after it has been claimed.
            </p>
            <p class="pb-16">
                The Welcome Offer applies to new and existing customers first deposit since the start of the promotion.
            </p>
            <p class="pb-16">
                The promotion entitles you to a Free Bet that matches your first deposit since the start of the
                promotion.
            </p>
            <p class="pb-16">
                The minimum deposit is KSH 49.
            </p>
            <p class="pb-16">
                The maximum Free Bet that will be awarded is KSH 2,000.
            </p>
            <p class="pb-16">
                Your deposit amount needs to be
                bet one (1) time and settled on Sportsbook at odds of 2.00 or greater before your Free Bet is credited
                to your account.
                This amount can be reached in a single bet or in multiple bets.
            </p>
            <p class="pb-16">
                Your Free Bet will be credited to your account within 24
                hours of settlement.
            </p>
            <p class="pb-16">
                You have thirty (30) days to deposit and meet the wagering requirements to unlock your
                Free Bet.
            </p>
            <p class="pb-16">
                The Free Bet is not withdrawable.
            </p>
            <p class="pb-16">
                The Free Bet can only be used on-site and cannot be used through the Text
                Betting service.
            </p>
            <p class="pb-16">
                Cashed out bets, Split bets, Jackpot Bets or bets on virtual sports will not be counted towards
                unlocking your Free Bet.
            </p>
            <p class="pb-16">
                Voided bets will not count towards unlocking your Free Bet. Only settled bets will count towards
                this.
            </p>
            <p class="pb-16">
                Only your first deposit will be counted towards this promotion.
            </p>
            <p class="pb-16">
                Free Bet
            </p>
            <p class="pb-16">
                The Free Bet will be credited to your account once the deposit amount is played one time.
            </p>
            <p class="pb-16">
                Your Free Bet will be credited to your account within 24 hours of settlement.
            </p>
            <p class="pb-16">
                The Free Bet must be used once, in it’s entirety, in one bet.
            </p>
            <p class="pb-16">
                The Free Bet must be placed on a minimum of three (3) legs,
                with the minimum odds of each leg being 1.30.
            </p>
            <p class="pb-16">
                The total combination odds must be minimum 3.50.
            </p>
            <p class="pb-16">
                All winnings derived from the Free Bet will automatically be transferred to
                your main wallet (minus the initial Free Bet stake).
            </p>
            <p class="pb-16">
                You cannot make a bet combining real money and the Free Bet.
            </p>
            <p class="pb-16">
                You have fourteen (14) days to use your Free Bet once it is credited.
            </p>
            <p class="pb-16">
                Only bets placed on-site will be valid.
            </p>
            <p class="pb-16">
                The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or
                Virtual Sports and the Free Bet cannot be cashed out.
            </p>
            <p class="pb-16">
                Should any of the selections in your bet be voided, the Free Bet will be settled according
                to the non-voided selections and any payout adjusted accordingly. If the bet is voided in
                full, you may contact support to request a replacement Free Bet to be assigned to your
                account.
            </p>
            <p class="pb-16">
                Any rules applicable for real money bets are also valid for Free Bets.
            </p>
            <p class="pb-16">
                Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same
                event/market without risk) or any other irregular betting patterns are not allowed.
                For example, betting on all three outcomes in a 1X2 market in a football game.
                All players suspected of collaborating will have their accounts suspended
                indefinitely, and all money will be permanently frozen and we reserve the right to
                withhold any withdrawals and/or confiscate all winnings.
            </p>
            <p class="pb-16">
                If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts,
                we will declare a bet or stake partially or fully voided and/or your account may be
                closed.
            </p>
            <p class="pb-16">
                General
            </p>
            <p class="pb-16">
                This offer cannot be used in conjunction with any other offer.
            </p>
            <p class="pb-16">
                The offer can only be received once per person/account, family, household, address,
                e-mail address, credit card number, IP addresses and environments where computers
                are shared (university, school, public library, workplace, etc.).
            </p>
            <p class="pb-16">
                Betsafe reserve the right to modify or change the Terms and
                Conditions for this promotion at any time.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to modify, change or cancel this promotion at any time.
            </p>
            <p class="pb-16">
                Betsafe reserve the right to disqualify any player from this promotion. Betsafe reserve the
                right to close an account and confiscate existing funds if there is evidence of
                abuse/fraud found.
            </p>
            <p class="pb-16">
                In the event of any dispute,Betsafe management's decision will be considered full and final.
            </p>
        </td>
    </tr>
</table>