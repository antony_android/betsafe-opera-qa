<table class="full-width" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="page-header-row bet-history-header">
        <td class="game-title">
            Responsible Gaming
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td>
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="respGamingControl" id="respGamingControl"
                class="form-control slip-filter-select" value="General Information"
                onclick="toggleRespGamingFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img"
                onclick="toggleRespGamingFilter()" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="general-list-item">
                    <a href="{{ url('/responsible') }}">
                        General Information
                    </a>
                </li>
                <li id="limits-list-item">
                    <a href="{{ url('/responsible/limits') }}">
                        Budget Limits
                    </a>
                </li>
                <li id="timeout-list-item">
                    <a href="{{ url('/responsible/timeout') }}">
                        Time Out
                    </a>
                </li>
                <li id="exclusion-list-item">
                    <a href="{{ url('/responsible/exclusion') }}">
                        Self Exclusion
                    </a>
                </li>
            </ul>
            {% else %}
            <form method="post" action="{{ url('/responsible/filter') }}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
                    <option value="general">General Information</option>
                    <option value="limits" selected>Budget Limits</option>
                    <option value="timeout">Time Out</option>
                    <option value="exclusion">Self Exclusion</option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-header pt-16">
            Budget Limits
        </td>
    </tr>
    <tr class="input">
        <td>
            <?php echo $this->tag->form("/responsible/addlimit"); ?>
            <table class="form" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <label for="dailyLimit">
                            Daily Limit
                        </label>
                        <input type="text" name="dailyLimit" id="dailyLimit">
                        <?php if(count($setLimit) == 0) { ?>
                        <span class="min-chars">No Daily Limit set</span>
                        <?php } else { ?>
                        <span class="min-chars">Daily Limit Set: KSH
                            <?=$setLimit; ?>
                        </span>
                        <a href="{{ url('/responsible/removelimit') }}">
                            <span class="show-pwd limit-remove">Remove</span>
                        </a>
                        <?php } ?>
                    </td>
                </tr>
                <tr class="input">
                    <td>
                        <button type="submit" class="set-resp-limits">
                            Set
                        </button>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16">
                If you need further information or help please contact our Customer Care department.
            </p>
        </td>
    </tr>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    function toggleRespGamingFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }
</script>
<?php if($selectedRespControl == 'general'): ?>
<script>
    document.getElementById('general-list-item').classList.add("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "General Information";
</script>
<?php elseif($selectedRespControl == 'limits'): ?>
<script>
    document.getElementById('limits-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Budget Limits";
</script>
<?php elseif($selectedRespControl == 'timeout'): ?>
<script>
    document.getElementById('timeout-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('exclusion-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Time-Out";
</script>
<?php elseif($selectedRespControl == 'exclusion'): ?>
<script>
    document.getElementById('exclusion-list-item').classList.add("slip-filter-selected");
    document.getElementById('general-list-item').classList.remove("slip-filter-selected");
    document.getElementById('limits-list-item').classList.remove("slip-filter-selected");
    document.getElementById('timeout-list-item').classList.remove("slip-filter-selected");
    document.getElementById('respGamingControl').value = "Self-Exclusion";
</script>
<?php endif; ?>