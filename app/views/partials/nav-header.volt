<table class="full-width">
    <tr>
        <td class="nav-hd">
            <script>
                if (window.operamini) {
                    document.write('<a href="/lite/nav" tabindex="2">');
                }
            </script>
            <button class="cursor-hand clear-button" onclick="showHideNavigation()">
                <img src="{{ url('/img/nav.png') }}" alt="Menu" />
            </button>
            <script>
                if (window.operamini) {
                    document.write('</a>');
                }
            </script>
        </td>
        <td>
            <table style="float: right;">
                <tr>
                    {% if session.get('user-agent') !='OperaMiniExtreme' %}
                    <td class="search-hd" onclick="toggleSearch()"></td>
                    {% endif %}
                    {% if session.get('user-agent') != 'OperaMiniExtreme' %}
                    <td class="profile-hd" onclick="showHideProfileNavigation()">
                    </td>
                    {% else %}
                    <td>
                        <a href="{{ url('/nav/right') }}" style="width: 100%;height:100%;display: inline-block;"
                            tabindex="3">
                            <img src="{{ url('/img/profile.svg') }}" alt="" />
                        </a>
                    </td>
                    {% endif %}
                    {% if session.get('auth') != null %}
                    {% if notifications > 0 %}
                    {% if session.get('user-agent') !='OperaMiniExtreme' %}
                    <span class="notification">
                        {{ notifications }}
                    </span>
                    {% else %}
                    <span class="notification-opera">
                        {{ notifications }}
                    </span>
                    {% endif %}
                    {% endif %}
                    {% endif %}
                    <td class="betslip-hd">
                        <a href="{{ url('/betslip')}}" tabindex="4">
                            <button class="cursor-hand bs-butt">
                                <table>
                                    <tr>
                                        <td>
                                            <img src="{{ url('/img/betslip.svg')}}" alt="Betslip" />
                                        </td>
                                        <td style="padding-left: 4px;">
                                            <span class="slip-counter">
                                                {% if slipCount > 0 %}
                                                {{ slipCount }}
                                                {% else %}
                                                0
                                                {% endif %}
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </button>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>