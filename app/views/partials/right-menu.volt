<table class="landing full-width">
    <tr>
        <td>
            <table class="football">
                <tr class="menu" style="border: 0;">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'signup') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40 full-width">
                                    <a class="pl-0 full-width" href="{{ url('/signup')}}">Join Now</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'login') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0  full-width" href="{{ url('/login') }}">Login</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'resp-gaming') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/responsible')}}">Responsible Gaming</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'help') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" target="_blank"
                                        href="https://support.betsafe.co.ke/">Help</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="menu">
                    <td>
                        <table class="full-width">
                            <tr>
                                <?php if(isset($selected) && $selected == 'sms') { ?>
                                <td class="selected-menu-item">
                                </td>
                                <?php } ?>
                                <td class="std-pd vh-40">
                                    <a class="pl-0 full-width" href="{{ url('/textbetting')}}">Text Betting</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>