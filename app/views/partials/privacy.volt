<table class="full-width" cellpadding="0" cellspacing="0">
    <tr class="terms-header">
        <td class="game-title">
            Privacy Policy
        </td>
    </tr>
    <tr>
        <td class="terms-details">
            <p class="terms-subtitle">
                Last updated 19th October 2020
            </p>
            <p>
                Betsafe.co.ke (“We, “Us, or “Our”) respects your right to privacy. We have in place appropriate
                procedures and security
                measures to protect and manage your personal data in accordance with applicable data privacy regulations
                and principles. This Privacy Policy sets out how We manage your personal information in the course of
                Our Website and SMS Betting
                services (“Services”). Please read this Privacy Policy (“Policy”) carefully.
            </p>
        </td>
    </tr>
    <tr>
        <td class="terms-details" id="terms-details">
            <h2>Who We are:</h2>
            <p>
                Please note that the data controller of your personal data is BetHigh K Limited, of PO Box 41840-00100,
                Nairobi, Kenya
                (reg. no. PVT-DUAMBE C81741). The principles set out in this Privacy Policy apply to all instances in
                which BetHigh K
                Limited processes your personal data as a data controller of Betsafe.co.ke for the purposes described in
                this Policy.
                Betsafe.co.ke forms part of the Betsson Group.
            </p>
            <h2>What information do We collect from you, for which purposes and what is Our legal ground to do so?</h2>
            <p style="padding-top: 16px;">
                In the first place, We need to collect and process information that you provided to Us during the
                registration process,
                when you deposit funds, or throughout your use of the Service. This is necessary in order to open/set up
                your account
                and enable you to use the requested Services. This information is collected and processed based on your
                acceptance of
                Our Terms and Conditions (based on the Agreement with Us).
            </p>
            <p style="padding-top: 16px;">
                The following is the minimum necessary information that We process based on the specific function of the
                Services that
                you use:
            </p>
            <p style="padding-top: 16px;">
                Contact information: your name, surname, address, age, birth date, gender, email address, phone number,
                zip code;
            </p>
            <p style="padding-top: 16px;">
                Financial information: in order to operate online payments also based on the Agreement with Us.
                Depending on the payment
                method you use, We collect your payment account details such as your account number, registered name of
                account owner,
                and billing address;
            </p>
            <p style="padding-top: 16px;">
                Furthermore, due to Our obligations under applicable gaming and Anti Money Laundering (AML) legislation,
                We are required
                to ask you for supplementary documents such as identification documents including your identity card or
                relevant
                document, your photo, copies of the relevant credit card/s, documents regarding your source of wealth,
                utility bills and
                other documents depending on the relevant jurisdiction as well as the transactions executed. Please note
                that as per our
                data sharing agreements, your Personal Data is shared between various brands which Betsson Group
                operates to meet Our
                AML obligations.
            </p>
            <p style="padding-top: 16px;">
                Finally, We need to keep track of self-excluded customers in order to fulfil Our legal obligations
                related to
                responsible gambling (RG). The way of verification of self-excluded customers depends on the specific
                jurisdiction.
                Please note that for the purpose of fulfilling Our obligations coming from relevant gaming laws, We need
                to share and
                check data about self-excluded customers and customers with gambling issues among Betsson Group
                companies and local
                authorities in order to enable cross-network exclusion and similar measures where necessary.
            </p>
            <p style="padding-top: 16px;">
                The above-mentioned points are the minimum necessary information that we collect in order to register
                you as Our
                customer and set up your account. Please be aware that if you do not want to provide this information to
                Us, We will not
                be able to provide you with the requested Service.
            </p>
            <p style="padding-top: 16px;">
                Moreover, We also collect and process data that We produced based on your activity while you are using
                Our Services.
                Such data are processed for the following purposes:
            </p>
            <p style="padding-top: 16px;">
                On the first place We need to collect these data in order to provide you with requested Services;
            </p>
            <p style="padding-top: 16px;">
                Our customer support is working 24/7 to resolve any potential technical or other issues while using Our
                Services. In
                order to help you with your requests they will need to use data you provided for registration as well as
                data about your
                activity to enable you to use Our Services without any interruptions, in accordance with our Terms and
                Conditions.
                Please note that calls with our customer support may be recorded for the purpose of potential legal
                disputes;
            </p>
            <p style="padding-top: 16px;">
                We need to analyse complete customer activity, gambling patterns, including bets placed in order to
                provide you with
                requested Service as per Agreement with Us (data necessary for provisioning of the Service).
            </p>
            <p style="padding-top: 16px;">
                This data is processed for the following purposes:
            </p>
            <p style="padding-top: 16px;">
                We are required to implement measures to identify and investigate any suspected unlawful or fraudulent
                activity
                connected with the Services, including possible money laundering, the use of proceeds of crime,
                terrorist financing and
                fraud. Hence, in order to comply with Our obligations arising from the relevant AML and gambling license
                regulations We
                need to analyse your transactions/bets/behaviours and create a risk profile. This includes identity
                verification, as
                well as other measures depending on jurisdiction and facts of the case (for suspicious/risky activities
                we need to apply
                additional measures of verification);
            </p>
            <p style="padding-top: 16px;">
                We need to analyse your activity and gambling patterns in order to prevent, detect and manage
                responsible gambling
                matters (RG) and to ensure that your use of Our Services is responsible and in accordance with relevant
                gambling
                regulations. In line with Our RG procedures, We carry out risk assessments on players by analysing
                behaviour,
                transaction, usage, and other data such as communications;
            </p>
            <p style="padding-top: 16px;">
                To protect Our business, ensure fairness based on Our Terms and Conditions and prevent and detect fraud,
                We need to
                oversee compliance with Our Terms and Conditions and Our internal policies e.g. to refuse, terminate or
                limit any bet,
                wager or account, to manage Our risks and odds, etc. In order to do so We need to process data about
                your activity and
                betting history;
            </p>
            <p style="padding-top: 16px;">
                In some cases, We may use automated or semi decision-making processes to help Us to execute certain
                actions. For
                example, Our systems will automatically prevent you from depositing funds beyond limits set by you; or
                you will not be
                able to register from countries that are on the sanction list (or with IP address that is on blacklist
                due to fraud) or
                if you are on the list of Politically Exposed Persons (PEPs); or self-excluded players will not be able
                to access Our
                Services (e.g. games). Please note that any decisions which produce legal or significant effects are not
                taken without
                human intervention unless this is allowed by law, or this is necessary to enter into an Agreement with
                you;
            </p>
            <p style="padding-top: 16px;">
                Also, all the above processing operations that include an analysis of betting patterns/behaviour and
                other customer
                related information rely on profiling. Tools and procedures followed are backed up by rigorous research
                and are
                regularly audited and fine-tuned to ensure that they are fair, effective and unbiased. Please be aware
                that information
                about these profiling activities, especially the logic behind profiling activities for these purposes
                (based on AML, RG
                law, for fraud detection, risk analysis of risky bets) cannot be revealed since that would allow
                customers to bypass
                these mechanisms aimed at the protection of Our business and compliance with legal obligations.
            </p>
            <p style="padding-top: 16px;">
                We will also analyse your betting patterns and activity in order to investigate and identify any
                possible issues related
                to integrity, malpractice and other seriously improper conducts in sport, such as match-fixing based on
                Our legitimate
                interest and/or legal obligation. Where We have a suspicion about such activities or We receive a
                well-grounded
                legitimate request to share information about such activities from a relevant body (including sports
                bodies and
                associations), We are required to disclose this information to them;
            </p>
            <p style="padding-top: 16px;">
                Based on Our legitimate interest to carry out surveys and market research in order to: evaluate customer
                experience and
                identify how We can improve Our Services and products, prioritise Our product features and improve
                product designs or to
                evaluate creative campaigns to ensure that communication is relevant and appropriate. In order to do so,
                We make use of
                the data you provided at registration and also the data that We collect from surveys. You can always
                object to this kind
                of processing. Please note that most of Our surveys as well as our market analysis will be anonymous
                (i.e. this will not
                involve any processing of personal data), hence such will be out of scope of this Policy;
            </p>
            <p style="padding-top: 16px;">
                We will use details of the products you have previously used, information about your preferences and use
                of the
                Services, based on Our legitimate interest to make suggestions to you for other products/promotional
                offers which We
                believe you will also be interested in (you can be subject to targeted advertisements). Please be aware
                that based on
                this logic for specific brands We might use different types of profiling, in order to offer you with the
                relevant
                information and promotions. If specific types of profiling might produce legal or similarly significant
                effect on you,
                We will ask for your consent before taking such action. Please note that you can always object to this
                kind of
                processing and we will stop profiling for marketing purposes where we use it;
            </p>
            <p style="padding-top: 16px;">
                We collect information from the devices you use for Our products and services in order to ensure that we
                provide Our
                Services in a secure manner and that We can solve any technical issue. This includes, but is not limited
                to your IP
                address (a number that identifies a specific device on the internet and is required for your device to
                communicate with
                websites), hardware model, operating system and version, software, preferred language, serial numbers,
                device motion
                information, mobile network information and location data. We also collect server logs, which include
                information such
                as your logins (first log in, last login, last failed login), duration of log ins, the app features or
                pages you view,
                app crashes and other system activity.
            </p>
            <p style="padding-top: 16px;">
                All your data may be analysed for business insight and intelligence purposes, where mostly anonymised
                data will be used,
                so this will not affect your privacy and personal data;
            </p>
            <p style="padding-top: 16px;">
                All your data will be retained and may be used for potential legal claims, in order to defend Our
                company business based
                on Our legitimate interest.
            </p>
            <p style="padding-top: 16px;">
                In addition to the above, your contact details or profile ID will be used:
            </p>
            <p style="padding-top: 16px;">
                To communicate offers and other promotional materials to your registered phone number, email address or
                other direct
                contact based on your consent to send direct marketing messages. Kindly note that you can, at any time
                and free of
                charge, change your direct marketing communication settings from your account, or by sending Us an
                email, or via the
                unsubscribe button in order to opt out from receiving any direct marketing communication to your phone
                number and email
                address;
            </p>
            <p style="padding-top: 16px;">
                To send you on-site/in-app marketing messages to your profile, based on Our legitimate interest to
                inform you about
                important updates/promotions related to all Our brands. You will only be able to see these messages when
                you log-in to
                your profile (app). Whilst static in-game advertising forms part of Our Services (i.e. it is not direct
                marketing as
                adverts are not based on any user characteristics but are common for all users), dynamic in-game
                advertising is tailored
                for the user therefore you can always object to this specific kind of processing by sending an email to
                Us;
            </p>
            <p style="padding-top: 16px;">
                To send you push notifications in order to keep you updated about important events/promotions related to
                all Our brands
                while you are browsing Our or other websites, only upon your consent that you can revoke at anytime;
            </p>
            <p style="padding-top: 16px;">
                To pass on Service communications regarding your account and the Services through different
                communications channels.
                This is based on Our legal obligations, Our agreement with you and/or in Our legitimate interest to keep
                you informed as
                well as your legitimate interest to be informed about important updates that affect how you will use Our
                Services. In
                order to inform you about these We will use your contact details (email, SMS, or phone where feasible)
                or We will send
                you these messages to your profile. Please be aware that these are not marketing messages, but Service
                information that
                you need to be aware of as Our customer. Please note that where Service communications are sent in Our
                or your
                legitimate interests, you can always object to this kind of processing.
            </p>
            <p style="padding-top: 16px;">
                Finally, We may combine the information We collect from you with the information that We create or
                receive from other
                sources, such as public databases, providers of demographic information and other third parties (such
                as, data brokers).
            </p>
            <p style="padding-top: 16px;">
                These include the following:
            </p>
            <p style="padding-top: 16px;">
                For the processing purposes of the prevention or detection of crime, fraud as well as responsible
                gambling as described
                above based on Our legal obligations and legitimate interest, We supplement the information that you
                provide to Us with
                information that We receive from third parties or collate by accessing third party sources, including
                (without
                limitation) information published on the internet about or by you, information provided by third party
                providers such as
                KYC agencies, providers of verification services/tools, etc.;
            </p>
            <p style="padding-top: 16px;">
                Data brokers to whom you provided your data in order to share with their business partners, or any other
                company that
                has permission to share your data. We will use these data brokers/companies when we want to enrich Our
                customer
                information with additional information that they have about your other activities. Also, We can use
                these brokers as
                well as other companies (social media platforms, etc.) for prospective marketing – creating prospective
                audience
                segments that may be interested in Our products, and exposing them to marketing campaigns via the
                technologies available
                (DPSs, real time biding, etc.). Please be aware that when We use your data for these purposes, We will
                only cooperate
                with partners that can assure Us they collected data from you in accordance with the GDPR (i.e. based on
                your consent).
            </p>
            <p style="padding-top: 16px;">
                Moreover, We will make sure that We process your personal data in accordance with GDPR principles and We
                do not obtain
                information about your behaviour that would reveal special categories of your personal data.
            </p>
            <p style="padding-top: 16px;">
                We also collect certain information about Our customers as well visitors of Our websites via cookies.
                For more
                information about cookies that We use, please read Our Cookies Policy.
            </p>
            <h2>With whom might We share your information?</h2>
            <p>
                Your Personal Information will be transferred or disclosed (for the purposes described in this Policy)
                to any EEA
                company within the Betsson Group for business and operational purposes based on our intra-group data
                processing
                agreements.
            </p>
            <p style="padding-top: 16px;">
                Apart from that, We share your data with Our business partners (recipients) in order to provide you with
                the Services
                requested. In general, these partners are Our data processors that act only based upon Our instructions.
                However, in
                some cases, they can act as joint/separate controllers (like payment providers or social media
                platforms). We always
                share your data with them in accordance with the GDPR and subject to appropriate agreements.
            </p>
            <p style="padding-top: 16px;">
                Categories of recipients with which We share data are:
            </p>
            <p style="padding-top: 16px;">
                Third-party suppliers that provide technical support, and maintain your account with Us;
            </p>
            <p style="padding-top: 16px;">
                Analytics and search engine providers that assist Us in the improvement and optimisation of Our site;
            </p>
            <p style="padding-top: 16px;">
                Vendors for communication purposes such as Our marketing platforms or voice call and SMS mobile
                communication service
                providers;
            </p>
            <p style="padding-top: 16px;">
                Partners carrying out surveys and market research on Our behalf; Interactive media platforms (social
                media platforms and
                similar);
            </p>
            <p style="padding-top: 16px;">
                Organisations that enable Us to place interesting and relevant promotions on third party websites and
                platforms you
                visit;
            </p>
            <p style="padding-top: 16px;">
                Payment service providers and payment facilitators;
            </p>
            <p style="padding-top: 16px;">
                Cloud service providers;
            </p>
            <p style="padding-top: 16px;">
                Analytics tools;
            </p>
            <p style="padding-top: 16px;">
                Anti-fraud, risk and compliance service providers (KYC verification providers and similar);
            </p>
            <p style="padding-top: 16px;">
                Anti-fraud, risk and compliance service providers (KYC verification providers and similar);
            </p>
            <p style="padding-top: 16px;">
                Suppliers of information verification services for the purposes of validating the information you
                provide to Us in the
                course of interacting with Us or entering into a contract with Us;
            </p>
            <p style="padding-top: 16px;">
                Professional advisers such as lawyers, auditors, consultants and insurers, providing Us with legal,
                advisory and
                insurance services;
            </p>
            <p style="padding-top: 16px;">
                Other partners that are helping Us to create a better experience for you.
            </p>
            <p style="padding-top: 16px;">
                While using the Services you may find links to third-party websites/applications (for example identity
                masking tools or
                social media platforms). Please note that this Privacy Policy does not apply to such third-party
                websites/applications.
                In order to find out more about processing of your personal data by these third-party
                websites/applications, We instruct
                you to carefully read their privacy policies and the terms of use before starting to use their services.
            </p>
            <p style="padding-top: 16px;">
                Please note that Our internal processing operations are conducted within countries in the European
                Economic Area (EEA)
                which are bound by the General Data Protection Regulation (GDPR) and in Kenya where the Data Protection
                Act of 25th
                November, 2019 applies. With regards to service providers or partners located outside the European
                Economic Area (EEA)
                in a third country, territory or sector that has not been found to provide for an adequate level of data
                protection by
                the European Commission (see here the list of third countries offering an adequate level of data
                protection as per the
                European Commission), We ensured that the data transfers are subject to appropriate safeguards, such as
                the Standard
                Contractual Clauses.
            </p>
            <p style="padding-top: 16px;">
                Please be aware that the list of providers (recipients) may differ depending on the brand and country
                where you make use
                of Our Service. Hence, on your request, We can provide you with the categories of recipients whom we
                share data with,
                and also specific information about data transfers to third countries.
            </p>
            <p style="padding-top: 16px;">
                Where required by law, your Personal Information may be disclosed to an applicable governmental,
                regulatory or
                enforcement authority for the purpose of prevention and detection of different type of crimes or in
                response to any
                court subpoena, order or similar official request. In certain cases, relevant laws may oblige Us to
                disclose your
                Personal Information to financial institutions such as banks and insolvency services. Your Personal
                Information may also
                be disclosed to any regulatory or sporting body in connection with policing the integrity or enforcing
                the rules of a
                sport or game and/or prevention and detection of crime and where there are reasonable grounds to suspect
                that you may be
                involved in a breach of such rules or the law, have knowledge of a breach of such rules or the law or
                otherwise pose a threat to the integrity of the relevant sport or
                game. Those bodies may then use your Personal Information to investigate and act on any such breaches in
                accordance with
                their procedures.
            </p>
            <p style="padding-top: 16px;">
                We may share some or all of your Personal Information with any subsequent owner, co-owner or operator of
                the Service and
                their advisors in connection with a corporate merger, consolidation, restructuring, the sale of
                substantially all Our
                stock and/or assets, or in connection with bankruptcy proceedings, or other corporate reorganization.
                Alternatively, We
                may seek to acquire other businesses or merge with them. In the event of a change to Our business, We
                will inform you in
                accordance with applicable law.
            </p>
            <h2>How long do We retain your information?</h2>
            <p>
                We will retain your information only for as long as is necessary for the purposes set out in this
                Policy. Where Your
                information is no longer required by Us, We will either securely delete or anonymise the Personal Data
                in question.
            </p>
            <p style="padding-top: 16px;">
                The criteria we follow in determining what is 'necessary' depends on the nature of data; the purpose of
                processing; the
                legal basis in place including but not limited to consent; whether there are any applicable statutory
                obligations or
                industry codes of practice; and other relevant circumstances.
            </p>
            <p style="padding-top: 16px;">
                After closing your account, We will retain your information to the extent necessary to comply with Our
                legal obligations
                such as applicable tax/revenue laws, AML and gambling laws and other applicable regulatory requirements
                as well as to
                resolve any potential legal disputes. Please be aware that due to applicable AML and gambling
                regulations as well as for
                the purpose of any potential legal claims, We need to store all your personal data for specific
                retention periods
                defined in these laws. Since retention periods are jurisdiction specific, for further information about
                data retention
                terms relevant to your jurisdiction, please contact Our data privacy team at:
            </p>
            <p style="padding-top: 16px;">
                dataprivacy@betssongroup.com.
            </p>
            <h2>Your rights:</h2>
            <p style="padding-top: 16px;">
                You as the data subject have certain rights with respect to the Personal Data, We hold on you. These
                include the
                following:
            </p>
            <p style="padding-top: 16px;">
                Right to obtain confirmation that We are processing your data and have access to or obtain a copy of the
                same. Kindly
                observe that as a general rule, subject to exemptions provided by applicable laws, We reserve the right
                not to disclose:
            </p>
            <p style="padding-top: 16px;">
                Information which is likely to prejudice either an internal ongoing investigation (for example in
                relation to fraudulent
                behaviour, bonus abuse etc.) or one conducted by the relevant authorities in relation to other offences;
            </p>
            <p style="padding-top: 16px;">
                Our AML/TF and RG risk assessments and monitoring information pursuant to applicable laws, since this
                disclosure is
                likely to prejudice the operation of the business by enabling customers to bypass mechanisms
                specifically set for the
                prevention and detection of such activities;
            </p>
            <p style="padding-top: 16px;">
                Information relating to ongoing negotiations with the data subject if such disclosure is likely to
                prejudice any
                negotiations to settle disputes/issues;
            </p>
            <p style="padding-top: 16px;">
                Information relating to Our internal processes including customer and risk management procedures which
                are strictly
                confidential, and disclosure would disrupt internal business operations;
            </p>
            <p style="padding-top: 16px;">
                Information including third party personal data since such disclosure may adversely affect the rights
                and freedoms of
                the third parties in question; and Information subject to Legal Professional Privilege (LPP). Right to
                request
                correction or completion of your incorrect or incomplete personal data to the extent allowed by law;
            </p>
            <p style="padding-top: 16px;">
                Right to erasure where:
            </p>
            <p style="padding-top: 16px;">
                Your personal data are no longer necessary for the purposes which they were collected;
            </p>
            <p style="padding-top: 16px;">
                The lawful basis for processing is consent and you withdraw your consent;
            </p>
            <p style="padding-top: 16px;">
                You object to processing based on legitimate interests and there is no overriding interest to continue
                processing;
            </p>
            <p style="padding-top: 16px;">
                Data processing is done for direct marketing purposes and you object to such processing;
            </p>
            <p style="padding-top: 16px;">
                Data processing is unlawful; or Personal data must be erased for compliance with a legal obligation.
            </p>
            <p style="padding-top: 16px;">
                Kindly note that right to deletion is not absolute and may be limited e.g. due to Our legal obligations,
                such as AML
                laws, or for the establishment, exercise or defence of legal claims (meaning We will not be able to
                exercise this right
                in relation to the data that We need to keep based on these retention periods for their duration);
            </p>
            <p style="padding-top: 16px;">
                Right to restrict processing where:
            </p>
            <p style="padding-top: 16px;">
                You have asked Us to rectify your data and We are in the process of verifying the accuracy of such data;
            </p>
            <p style="padding-top: 16px;">
                Personal data processing has been unlawful and ask for restriction instead of deletion;
            </p>
            <p style="padding-top: 16px;">
                Your personal data is no longer needed for the Services but you request Us to retain it in order to
                establish, exercise
                or defend a legal claim; or
            </p>
            <p style="padding-top: 16px;">
                You have objected to Our processing of your data based on legitimate interest and We are evaluating
                whether Our
                legitimate interests override yours.
            </p>
            <p style="padding-top: 16px;">
                Kindly observe that your data will be stored for the time of restriction and shall be processed only if
                you gave your
                consent or in order to establish, exercise or defend legal claims, protect the rights of another natural
                or legal person
                and for the reasons of important public interest;
            </p>
            <p style="padding-top: 16px;">
                Right to data portability i.e. right to transmit personal data you have provided Us with in a
                structured, commonly used
                and machine-readable format to another controller where the processing is based on consent or contract
                and carried out
                by automated means;
            </p>
            <p style="padding-top: 16px;">
                Right to object where processing is based on legitimate interests, tasks carried in public interest
                and/or where your
                personal data are processed for direct marketing purposes, including profiling for these purposes. The
                latter is an
                absolute right while the first one will require a balance assessment of your interests, rights and
                freedoms against Our
                legitimate interests;
            </p>
            <p style="padding-top: 16px;">
                Right to obtain human intervention, express your point of view and contest the decision where it is
                based on solely
                automated decision making, including profiling, which produces legal effects concerning you or similar
                significant
                effects. Please note that We will respond to any of your requests about your rights within 30 days at
                the latest (this
                period may be further extended in line with applicable law).
            </p>
            <p style="padding-top: 16px;">
                Finally, please note that you have a right to lodge a complaint with the Data Commissioner, as per the
                applicable data
                privacy regulations.
            </p>
            <h2>How to contact Us:</h2>
            <p style="padding-top: 16px;">
                We have appointed a Data Protection Officer (DPO). If you have any questions about this Privacy Policy
                or Our data
                collection practices, please contact Us at dataprivacy@betssongroup.com and specify the nature of your
                question.
            </p>
            <h2>Changes to the terms of this Privacy Policy</h2>
            <p style="padding-top: 16px;">
                We will occasionally make changes and corrections to this Privacy Policy and We will inform you by
                posting the changes
                on this site and sending you an email or message about the changes. Any change will be effective
                immediately.
            </p>
        </td>
    </tr>
</table>