<table cellpadding="0" cellspacing="0" class="full-width">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title std-pd">
            Bonuses
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td class="std-pd">
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="bonusesControl" id="bonusesControl"
                class="form-control slip-filter-select" style="margin-top:0 !important;"
                value="Available Bonuses (<?=$freebetsCount + 2; ?>)" onclick="toggleBonusesFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img" style="margin-top: 9px;"
                onclick="toggleBonusesFilter()" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="all-list-item">
                    <a href="{{ url('/bonuses') }}">
                        Available Bonuses
                        <?= "(".$freebetsCount.")"; ?>
                    </a>
                </li>
                <li id="active-list-item">
                    <a href="{{ url('/bonuses/active') }}">
                        Active Bonuses
                        <?= "(".$freebetsCount.")"; ?>
                    </a>
                </li>
                <li id="expired-list-item">
                    <a href="{{ url('/bonuses/expired') }}">
                        Expired Bonuses
                    </a>
                </li>
            </ul>
            {% else %}
            <form method="post" action="{{ url('/bonuses/filter') }}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()"
                    style="margin: 0;">
                    <option value="all" {% if selectedBonusesControl=='all' %} selected {% endif %}>
                        Available
                        Bonuses (
                        <?=$freebetsCount; ?>
                        )
                    </option>
                    <option value="active" {% if selectedBonusesControl=='active' %} selected {% endif %}>Active Bonuses
                        (
                        <?=$freebetsCount; ?>)
                    </option>
                    <option value="expired" {% if selectedBonusesControl=='expired' %} selected {% endif %}>Expired
                        Bonuses</option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <tr>
        <td>
            <table class="form messages-input full-width" cellpadding="0" cellspacing="0">

                <?php foreach($freebets as $freebet): ?>
                <tr class="bonus-row">
                    <td class="std-pd">
                        <form action="{{ url('/bonuses/freebet') }}" method="post">
                            <button type="submit" class="bonus-butt-more">
                                <span class="bonus-title">
                                    <?=$freebet["name"]; ?>
                                </span>
                                <p class="bonus-message">
                                    You've been awarded a Free Bet!
                                </p>

                                <input type="hidden" name="freebetId" value="{{ freebet['freebetId'] }}" />
                                <input type="hidden" name="name" value="{{ freebet['name'] }}" />
                                <input type="hidden" name="amount" value="{{ freebet['amount'] }}" />
                                <input type="hidden" name="maxPayout" value="{{ freebet['maxPayout'] }}" />
                                <input type="hidden" name="minSelectionOdds"
                                    value="{{ freebet['minSelectionOdds'] }}" />
                                <input type="hidden" name="minNumSelections"
                                    value="{{ freebet['minNumSelections'] }}" />
                                <input type="hidden" name="minTotalOdds" value="{{ freebet['minTotalOdds'] }}" />

                                <span class="bonus-link">
                                    Read more
                                </span>
                            </button>
                        </form>
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr class="bonus-row">
                    <td class="std-pd">
                        <a href="{{ url('/bonuses/welcome') }}">
                            <span class="bonus-title">Welcome Bonus</span>
                            <p class="bonus-message">
                                Register and claim your Free Bet today!
                            </p>
                            <span class="bonus-link">
                                Read more
                            </span>
                        </a>
                    </td>
                </tr>
                <tr class="bonus-row">
                    <td class="std-pd">
                        <a href="{{ url('/bonuses/boost') }}">
                            <span class="bonus-title">Multibet Boost</span>
                            <p class="bonus-message">
                                Increase your winnings by up to 200%
                            </p>
                            <span class="bonus-link">
                                Read more
                            </span>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    function toggleBonusesFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }
</script>
<?php if($selectedBonusesControl == 'all'): ?>
<script>
    document.getElementById('all-list-item').classList.add("slip-filter-selected");
    document.getElementById('active-list-item').classList.remove("slip-filter-selected");
    document.getElementById('expired-list-item').classList.remove("slip-filter-selected");
    document.getElementById('bonusesControl').value = "Available Bonuses";
</script>
<?php elseif($selectedBonusesControl == 'active'): ?>
<script>
    document.getElementById('all-list-item').classList.remove("slip-filter-selected");
    document.getElementById('active-list-item').classList.add("slip-filter-selected");
    document.getElementById('expired-list-item').classList.remove("slip-filter-selected");
    document.getElementById('bonusesControl').value = "Active Bonuses";
</script>
<?php elseif($selectedBonusesControl == 'expired'): ?>
<script>
    document.getElementById('all-list-item').classList.remove("slip-filter-selected");
    document.getElementById('active-list-item').classList.remove("slip-filter-selected");
    document.getElementById('expired-list-item').classList.add("slip-filter-selected");
    document.getElementById('bonusesControl').value = "Expired Bonuses";
</script>
<?php endif; ?>