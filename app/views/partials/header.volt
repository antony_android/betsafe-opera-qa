<table class="full-width">
  <tr class="search-row" id="search-row" {% if session.get('user-agent') !='OperaMiniExtreme' %} style="display: none;"
    {% endif %}>
    <td>
      <table class="top full-width">
        <tr class="top--search">
          <td class="std-pd">
            {{ partial("partials/search") }}
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td class="header-lb"></td>
  </tr>
  <tr id="brand-row" class="brand-row">
    <td>
      <table class="full-width">
        <tr class="logo-row">
          <td class="std-pd">
            {{ partial('partials/brand') }}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>