<style>
    a {
        text-decoration: underline !important;
    }
</style>
<table class="full-width" cellpadding="0" cellspacing="0">

    <tr class="page-header-row bet-history-header">
        <td class="game-title">
            FAQ
        </td>
    </tr>
    <tr>
        <td class="depo-mpesa-details">
            <p class="pb-16 depo-mpesa-header">
                Most Popular FAQs
            </p>
            <ol class="pb-16">
                <li>
                    <a href="#makedeposit">
                        How do I make a deposit?
                    </a>
                </li>
                <li>
                    <a href="#placebet">
                        How do I place a bet?
                    </a>
                </li>
                <li>
                    <a href="#forgotpassword">
                        I've forgotten my password, how do I get a new one?
                    </a>
                </li>
                <li>
                    <a href="#activatebonus">
                        How do I activate a bonus?
                    </a>
                </li>
                <li>
                    <a href="#joinbetsafe">
                        How do I join Betsafe?
                    </a>
                </li>
                <li>
                    <a href="#requestwithdrawal">
                        How do I request a withdrawal?
                    </a>
                </li>
                <li>
                    <a href="#budgetlimits">
                        Budget Limits
                    </a>
                </li>
                <li>
                    <a href="#bethistory">
                        Where can I view my bet history?
                    </a>
                </li>
                <li>
                    <a href="#wagering">
                        What is Wagering?
                    </a>
                </li>
                <li>
                    <a href="#tools">
                        What types of tools are available?
                    </a>
                </li>
            </ol>
            <p class="pb-16 bold" id="makedeposit">
                How do I make a deposit?
            </p>
            <p class="pb-16">
                Direct Deposit from M-Pesa Menu:
            </p>
            <ol class="pb-16">
                <li>Go to the M-Pesa menu located in your SIM Toolkit.</li>
                <li>Select the 'Lipa Na M-Pesa' option.</li>
                <li>Head on to pay bill.</li>
                <li>Enter business number '7290099'</li>
                <li>Key in account no. BETSAFE.</li>
                <li>Enter the amount you wish to deposit.</li>
                <li>Finally, insert your PIN and click 'Okay'.</li>
                <li>Click 'Deposit'. The minimum deposit amount is Ksh 49.</li>
            </ol>
            <p class="pb-16">
                Deposit via Website:
            </p>
            <ol class="pb-16">
                <li>Visit www.betsafe.co.ke and log in to your account.</li>
                <li>Click on the 'deposit' icon/button.</li>
                <li>Enter in your preferred amount you wish to deposit.</li>
                <li>Click 'Deposit'.</li>
            </ol>
            <p class="pb-16">
                The minimum deposit amount is Ksh 49.
            </p>
            <p class="pb-16 bold" id="placebet">
                How do I place a bet?
            </p>
            <p class="pb-16">
                Placing a bet is simple. Once you are logged in to your account and have browsed through the various
                betting options
                (either the Sports or the In-Play tab at the top menu bar of the home page):
            </p>
            <p class="pb-16">
                1. Click on the odds according to your selection.
            </p>
            <p class="pb-16">
                2. In your bet slip, enter the amount of money you would like to use on this bet.
            </p>
            <p class="pb-16">
                3. Click on Place bet/Send.
            </p>
            <p class="pb-16 bold" id="forgotpassword">
                I've forgotten my password, how do I get a new one?
            </p>
            <p class="pb-16">
                In order to reset your password, kindly follow the below steps:
            </p>
            <ol class="pb-16">
                <li>Visit our website: www.betsafe.co.ke, click on "Login".</li>
                <li>Select the ‘Reset Password’.</li>
                <li>Enter your phone number and click ‘Send SMS and Continue’ option and a reset code will be sent to
                    your phone via SMS.</li>
                <li>Key in the 5-digit code in the ‘Code’ box.</li>
                <li>Insert your preferred password.</li>
                <li>Ensure all required fields are filled in and then click ‘Change Password’. You will receive a
                    "Password Successfully
                    Reset" confirmation message on screen.</li>
                <li>
                    Log in with your new password to access your Betsafe account.
                </li>
            </ol>
            <p class="pb-16 bold" id="activatebonus">
                How do I activate a bonus?
            </p>
            <p class="pb-16">
                If you are interested in taking advantage of a promotional offer, simply follow the instructions on the
                promotion to
                qualify and receive the reward.
            </p>
            <p class="pb-16">
                You can check out our available promotions <a href="/promotions">here</a>.
            </p>
            <p class="pb-16 bold" id="joinbetsafe">
                How do I join Betsafe?
            </p>
            <p class="pb-16">
                In order to register an account at Betsafe, simply click 'Register Now' from the homepage. Once you have
                entered in your
                correct details click 'Join Betsafe'.
            </p>
            <p class="pb-16">
                If you wish to register an account via SMS, kindly send ' Betsafe ' to 23333.
            </p>
            <p class="pb-16">
                By creating an account, you confirm that you have read and accept our Terms & Conditions and that you
                are at least 18
                years of age.
            </p>
            <p class="pb-16 bold" id="requestwithdrawal">
                How do I request a withdrawal?
            </p>
            <p class="pb-16">
                Withdrawal via website
            </p>
            <p class="pb-16">
                To make a withdrawal via the Betsafe website:
            </p>
            <ol class="pb-16">
                <li>Login to your Betsafe Account on www.betsafe.co.ke</li>
                <li>Select the ‘My Profile’ icon.</li>
                <li>Select ‘Wallet’.</li>
                <li>Select ‘Withdrawal’.</li>
                <li>Select ‘Withdraw Now’.</li>
            </ol>
            <p class="pb-16">
                Withdrawal via SMS
            </p>
            <p class="pb-16">
                To withdraw via SMS, Send the amount you want to withdraw to 23333 (SMS charges apply).
            </p>
            <p class="pb-16">
                For example, if you want to withdraw Ksh 100 simply send an SMS to 23333 with the message ' Withdraw 100
                '.
            </p>
            <p class="pb-16">
                The minimum withdrawal amount is Ksh 100.
            </p>
            <p class="pb-16 bold" id="receivemoney">
                When will I receive my money?
            </p>
            <p class="pb-16">
                We process withdrawals within a matter of minutes. Should your request take longer than 30 minutes,
                please contact our
                Customer Care team.
            </p>
            <p class="pb-16 bold" id="withdrawbonus">
                When can I withdraw my bonus balance?
            </p>
            <p class="pb-16">
                Your bonus balance will automatically be credited to your withdrawable balance once the wagering/betting
                requirements
                for that bonus have been completed.
            </p>
            <p class="pb-16">
                Once completed, the bonus funds will be available for withdrawing.
            </p>
            <p class="pb-16 bold" id="budgetlimits">
                Budget Limits
            </p>
            <p class="pb-16">
                Giving you the ability to control how much you are wanting to spend is highly important, which is why we
                offer a 'Budget
                Limit' tool.
            </p>
            <p class="pb-16">
                The Budget Limit is purely based on how much of your own money you can deposit onto your Betsafe account
                in a single
                day. For example, if you were to set a Budget Limit of KSH 500, you won't be able to deposit more once
                this has been
                reached until the next day. Please note that the budget limit takes withdrawals into consideration, so
                if you were to
                deposit KSH 500 in the first part of the day and withdraw KSH 400 later on you may deposit another KSH
                400 within the
                same day.
            </p>
            <p class="pb-16">
                Setting your budget limit:
            </p>
            <p class="pb-16">
                You can set a budget limit by logging in to your account and navigating to the logged-in right-hand
                'user menu'. Click
                on 'Responsible Gaming', then select the Budget Limits' tab where you can enter the amount.
            </p>
            <p class="pb-16">
                Changing your budget limit:
            </p>
            <p class="pb-16">
                Budget limits can be changed at any time. When increasing the amount or removing the limit completely, a
                waiting period
                of 24 hours will apply. Making any decreases will take effect immediately.
            </p>
            <p class="pb-16 bold" id="bethistory">
                Where can I view my Bet History?
            </p>
            <p class="pb-16">
                To check your bet history, simply login to your Betsafe account and click on the Right Account Menu
                Icon. Then choose
                Bet History.
            </p>
            <p class="pb-16">
                Here, you will be able to see your placed and settled bets.
            </p>
            <p class="pb-16 bold" id="wagering">
                What is wagering?
            </p>
            <p class="pb-16">
                A bonus 'wagering requirement' means that you must play the amount offered and/or deposited a certain
                number of times,
                in order to complete the bonus and unlock your funds.
            </p>
            <p class="pb-16">
                For example, if you receive a Ksh 20 bonus with a X10 wagering requirement, this means that you will
                have to have
                settled bets that total Ksh 200 (Ksh 20 x 10) to complete the bonus.
            </p>
            <p class="pb-16">
                As soon as the wagering requirements are met, you can then withdraw your funds.
            </p>
            <p class="pb-16">
                Please keep in mind that not all games contribute the same amount towards wagering. You can check the
                different wagering
                contribution percentages from the bonus offer, or from the General Terms & Conditions.
            </p>
            <p class="pb-16 bold" id="responsiblegaming">
                Responsible Gaming
            </p>
            <p class="pb-16">
                What types of tools are available?
            </p>
            <p class="pb-16">
                We want to give you the opportunity to set your own gaming limits, budgets, and boundaries. That is why
                we offer the
                below tools to help you:
            </p>
            <ol class="pb-16">
                <li>Budget Limit</li>
                <li>Time-Out</li>
                <li>Self Exclusion</li>
            </ol>
            <p class="pb-16">
                If you are concerned about your gambling, you can try our Self Assessment Test.
            </p>
            <p class="pb-16">
                If you are concerned about your test result, consider activating a time out on your account or contact
                our Customer Care
                Team to receive further help and advice.
            </p>
        </td>
    </tr>
</table>