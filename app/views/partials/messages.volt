<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title std-pd">
            Messages
        </td>
    </tr>
    <tr>
        <td class="deposit-details std-pd">
            You have {% if notifications > 0 %} {{ notifications }} {% else %} no {% endif %} new messages
        </td>
    </tr>
    <tr>
        <td>
            <table class="form messages-input" cellpadding="0" cellspacing="0">
                <?php foreach($messages as $message): ?>
                <tr class="message-title">
                    <td class="new-message">
                        <a href="{{ url('/messages/message/')}}<?php echo $message['isolutions_messageid']; ?>">
                            <?php if($message['read'] == 0) { ?>
                            <img src="/img/new-mail.svg" alt="" />
                            <?php } ?>
                        </a>
                    </td>
                    <td>
                        <a href="{{ url('/messages/message/')}}<?php echo $message['isolutions_messageid']; ?>">
                            <div>
                                <?=(strlen($message['message_title']) > 31)?substr($message['message_title'], 0, 30)."...":$message['message_title']; ?>
                            </div>
                        </a>
                    </td>
                    <td class="messages-day std-pd">
                        <a href="{{ url('/messages/message/')}}<?php echo $message['isolutions_messageid']; ?>">
                            <span class="messages-day">
                                <?php if(date('Ymd') == date('Ymd', strtotime($message['message_date']))){ ?>
                                <?php echo 'Today'; ?>
                                <?php } elseif(date('Ymd', strtotime("-1 days")) == date('Ymd', strtotime($message['message_date']))) {?>
                                <?php echo 'Yesterday'; ?>
                                <?php } else { ?>
                                <?php echo date('l', strtotime($message['message_date'])); ?>
                                <?php } ?>
                                <img src="{{ url('/img/message-arrow.svg') }}" alt="" style="margin-bottom: 3px;" />
                            </span>
                        </a>
                    </td>
                </tr>
                <tr class="message-row">
                    <td></td>
                    <td style="padding-right: 8px;" colspan="2">
                        <a href="{{ url('/messages/message/')}}<?php echo $message['isolutions_messageid']; ?>">
                            <table class="full-width">
                                <td>
                                    <div>
                                        <?=(strlen($message['message']) > 80)?substr($message['message'], 0, 79)."...":$message['message']; ?>
                                    </div>
                                </td>
                                <td style="vertical-align: bottom;text-align: right;">
                                    <form action="{{ url('/messages/del')}}" method="post" style="display: inline;">
                                        <input type="hidden" name="messageId"
                                            value="<?php echo $message['isolutions_messageid']; ?>" />
                                        <button type="submit" class="one-message-bt">
                                            <img src="{{ url('/img/message-del.svg')}}" alt=""
                                                style="margin-left: 2px;" />
                                        </button>
                                    </form>
                                </td>
                            </table>
                        </a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table>