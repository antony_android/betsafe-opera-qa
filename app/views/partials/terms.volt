<table class="full-width" cellpadding="0" cellspacing="0">
	<tr class="terms-header">
		<td class="game-title">
			Terms & Conditions
		</td>
	</tr>
	<tr>
		<td class="terms-row">
			<p class="terms-subtitle">
				V1.12 Dated 18th August 2021
			</p>
			<ol style="margin: 0;">
				<li>Introduction</li>
				<li>The Basics</li>
				<li>Definitions</li>
				<li>Licenses</li>
				<li>Who can play</li>
				<li>How you can play</li>
				<li>Generals rules for you</li>
				<li>What we can do</li>
				<li>Payment rules</li>
				<li>Closing your account</li>
				<li>Inactive accounts</li>
				<li>Bonuses</li>
				<li>Responsible Gaming</li>
				<li>Miscarried & Aborted Games</li>
				<li>How to complain</li>
				<li>When talking to us</li>
				<li>When talking to each other</li>
				<li>Privacy Policy</li>
				<li>Your liability to us</li>
				<li>Breaches, Penalties, and Terminations</li>
				<li>Anti-Money Laundering reporting</li>
				<li>Intellectual Property</li>
				<li>Severability</li>
				<li>Entire Agreement & Admissibility</li>
				<li>Assignment</li>
				<li>Applicable Law, Juristriction, and Language</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix I: Bet Bila Bundles & Bet Bila Charges
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Introduction</h2>
			<p>
				Thank you for taking the time to learn about the Betsafe Terms & Conditions (the “Terms”) "). This is
				how we let you know what you can and can’t do on Betsafe, how we will use your personal information and
				how we manage the Games. We have tried to keep the boring legal stuff to a minimum.
			</p>
			<p style="padding-top: 16px;">
				If You still have any questions after reading them, please get in touch
			</p>
			<h2>Basics</h2>
			<p>
				The Terms are a binding agreement between You and Betsafe. By registering Your Account and/or using the
				Service and/or
				accepting any Bonus or prize, you agree that you have read and accepted the Terms and any amendments
				thereto. If you do
				not agree with the Terms, You should not register or continue to use the Service.
			</p>
			<p style="padding-top: 16px;">
				Everything changes and we may change these Terms at any time. Any changes to the Terms will be published
				to the Website
				at the time they come into effect. If you continue using the Service after the date on which these Terms
				come into
				effect, you will be deemed to have accepted the changes.
			</p>
			<h2>Definitions</h2>
			<p>
				You will see the following terms used throughout the Terms:<br />
				"Bonus” shall cover all promotional offers that gift a tangible reward, including but not limited to:
				welcome offers, reload bonuses, deposit bonuses, free/bonus spins, free bets, and risk free bets.
			</p>
			<p style="padding-top: 16px;">
				"Games” are Sportsbook, Casino, and any other game we may offer and "Game" can mean any one of them. We
				can remove or add Games or parts thereof to the Service at any time.
			</p>
			<p style="padding-top: 16px;">
				“Service” includes the Website and the Text Betting service available through SMS and Telegram.
			</p>
			<p style="padding-top: 16px;">
				"Terms" are these terms and conditions and any other rules referenced within, which you agree to when
				using the Service.
			</p>
			<p style="padding-top: 16px;">
				"Website" www.betsafe.co.ke, and including, if applicable, any mobile and mobile application version
				thereof or any
				other subdomains of ‘betsafe.co.ke'.
			</p>
			<p style="padding-top: 16px;">
				"We" The Service is run by Bet High (K) Limited, a private limited company registered in Kenya, with
				company
				registration number PVT-DLUAMBE and having its registered office at L.R No. 209/1/399, Ground Floor,
				Palacina Court,
				Kitale Lane, Kilimani, P.O. Box Number 41840-00100 Nairobi, Kenya.
			</p>
			<p style="padding-top: 16px;">
				"we", "us", "our", “Betsafe” means Bet High (K) Limited.
			</p>
			<p style="padding-top: 16px;">
				"You" is you using the Service.
			</p>
			<p style="padding-top: 16px;">
				"Your" has its usual meaning in relation to you.
			</p>
			<p style="padding-top: 16px;">
				"Your Account" is your registered account with Betsafe through which you use the Service.
			</p>
			<h2>Licences</h2>
			<p style="padding-top: 16px;">
				We are licensed and regulated by the Betting Control and Licensing Board of Kenya (BCLB): Public Gaming
				Licence
				#0000119, Bookmakers Licence #0000301.
			</p>
			<h2>Who can play</h2>
			<p style="padding-top: 16px;">
				To play the Games, you must be:
			</p>
			<ol style="margin: 0;">
				<li>over 18 years of age;</li>
				<li>an actual person. You cannot be a company or other legal entity;</li>
				<li>playing the Games for yourself and in a non-professional capacity, and not on behalf of someone
					else;</li>
				<li>legally allowed to play the Games;</li>
				<li>a resident of Kenya.</li>
			</ol>
			<h2>How can you play</h2>
			<p style="padding-top: 16px;">
				6.1 Register
			</p>
			<p style="padding-top: 16px;">
				You can register Your Account either on the Website or by Text.
			</p>
			<p style="padding-top: 16px;">
				(a) To register from the Website:
			</p>
			<ol style="margin: 0;">
				<li>
					Click on the "Register Now" button or any other link to the online registration form.
				</li>
				<li>
					Accurately complete the registration form including your mobile number and password.
				</li>
				<li>
					Click on the "Join Betsafe" button to confirm that the registration information in the form is the
					correct information
					and that you agree to the Terms.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 2: Bet Ksh 50 get Ksh 50 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 50 Free Bet when you place a bet of Ksh 50 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 50 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management's decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 3: Bet Ksh 50 get Ksh 150 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 150 Free Bet when you place a bet of Ksh 50 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 50 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any other irregular betting patterns are not allowed. For example, betting on all
					three outcomes in a 1X2 market in a football game. All players suspected of collaborating will have
					their accounts suspended indefinitely, and all money will be permanently frozen. We reserve the
					right withhold any withdrawals and/or confiscate any winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud found.</li>
				<li>In the event of any dispute, Betsafe management's decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 4: Bet Ksh 100 get Ksh 300 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 300 Free Bet when you place a bet of Ksh 100 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 100 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management's decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 5: Bet Ksh 300 get Ksh 300 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 300 Free Bet when you place a bet of Ksh 300 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 300 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud found.</li>
				<li>In the event of any dispute, Betsafe management's decision will be considered full and final.</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 6: Bet Ksh 300 get Ksh 900 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 900 Free Bet when you place a bet of Ksh 300 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 300 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>

			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management&#39;s decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 7: Bet Ksh 700 get Ksh 700 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 700 Free Bet when you place a bet of Ksh 700 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 700 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on minimum odds of 2.00</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management&#39;s decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 8: Bet Ksh 700 get Ksh 1500 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 1500 Free Bet when you place a bet of Ksh 700 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 700 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on minimum odds of 2.00</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management&#39;s decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 9: Ksh 20 Free Bet
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>Once the offer has been received, players have three (3) days to use their Free Bet</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>The promotion entitles you to a Ksh 20 Free Bet</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management&#39;s decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
	<tr class="appendix-row">
		<td>
			Appendix 10: Bet Ksh 20 get Ksh 20 Cashback
		</td>
	</tr>
	<tr>
		<td class="terms-details" id="terms-details">
			<h2>Offer</h2>
			<ol style="margin: 0;">
				<li>The qualification bet can be placed within 72 hours of receiving this offer.</li>
				<li>Only customers who have received this offer directly from Betsafe are eligible for this promotion.
				</li>
				<li>Any bets placed or settled outside the promotional period above will not qualify for this offer</li>
				<li>The promotion entitles you to a Ksh 20 Free Bet when you place a bet of Ksh 20 or more at odds of
					over 1.5.</li>
				<li>This offer applies to a players first qualifying bet settled within the promotion period.</li>
			</ol>
			<h2>Qualifying Bet</h2>
			<ol style="margin: 0;">
				<li>Players must place a bet of Ksh 20 or more to receive the Free Bet.</li>
				<li>The qualifying bet must have minimum odds of 1.5. If placing a combination/multi-bet, the total odds
					must be equal
					to or greater than 1.5.</li>
				<li>The qualifying bet must be settled before the Free Bet will be paid.</li>
				<li>Qualifying bets must be placed with real cash. Bets placed with bonus funds or free bets will not
					count towards
					qualifying for this promotion.</li>
				<li>Cashed out or voided bets will not count towards this promotion.</li>
				<li>System or split bets will not count towards this promotion.</li>
				<li>The qualifying bet can only be placed on Sportsbook.</li>
				<li>Jackpot Entries are not included in this promotion.</li>
			</ol>
			<h2>Free Bet</h2>
			<ol style="margin: 0;">
				<li>The Free Bet is not withdrawable.</li>
				<li>The Free Bet will be credited to your account within 24 hours of the settlement of your qualifying
					bet.</li>
				<li>The Free Bet must be used once, in its entirety in one bet.</li>
				<li>The Free Bet must be placed on a minimum of three (3) selections with the minimum odds per selection
					1.30</li>
				<li>The total combination odds must be minimum 3.50</li>
				<li>All winnings derived from the Free Bet will automatically be transferred to your main wallet (minus
					the initial Free
					Bet stake).</li>
				<li>You cannot make a bet combining real money and the Free Bet.</li>
				<li>Players have three (3) days to use their Free Bet once it is credited.</li>
				<li>The Free Bet can only be used on-site and cannot be used through the Text Betting service.</li>
				<li>The Free Bet cannot be placed on System bets, Jackpot Bets or used on Casino or Virtual Sports and
					the Free Bet
					cannot be cashed out.</li>
				<li>If a Free Bet is placed on an event that is then voided, the Free Bet will be refunded back to the
					players account.
				</li>
				<li>Any thresholds and limits set for real money are also valid for Free Bets.</li>
			</ol>
			<h2>General</h2>
			<ol style="margin: 0;">
				<li>This offer cannot be used in conjunction with any other offer.</li>
				<li>The bonus can only be received once per person/account, family, household, address, e-mail address,
					credit card
					number, IP addresses and environments where computers are shared (university, school, public
					library, workplace,
					etc.).</li>
				<li>Risk-Free, Arbitrage, Hedge Bets, Matched Bets (where bets are placed on the same event/market
					without risk) or any
					other irregular betting patterns are not allowed. For example, betting on all three outcomes in a
					1X2 market in a
					football game. All players suspected of collaborating will have their accounts suspended
					indefinitely, and all money
					will be permanently frozen. We reserve the right withhold any withdrawals and/or confiscate any
					winnings.</li>
				<li>If Risk-Free, Arbitrage or Matched Bets have been made on one or multiple accounts, we will declare
					a bet or stake
					partially or fully voided and/or your account may be closed.</li>
				<li>Betsafe reserve the right to modify or change the Terms and Conditions for this promotion at any
					time</li>
				<li>Betsafe reserve the right to modify, change or cancel this promotion at any time.</li>
				<li>Betsafe reserve the right to disqualify any player from this promotion.</li>
				<li>Betsafe reserve the right to close an account and confiscate existing funds if there is evidence of
					abuse/fraud
					found.</li>
				<li>In the event of any dispute, Betsafe management&#39;s decision will be considered full and final.
				</li>
			</ol>
		</td>
	</tr>
</table>