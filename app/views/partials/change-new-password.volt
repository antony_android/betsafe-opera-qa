<table class="login" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="reqpw-row">
        <td class="game-title">
            Set New Password
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $this->tag->form("/change-password/password"); ?>
            <table class="form" cellpadding="0" cellspacing="0">
                <tr class="input">
                    <td>
                        <label>
                            New Password {% if session.get('user-agent') !='OperaMiniExtreme' %} <span class="show-pwd"
                                onclick="togglePwdVisibility('password')">Show</span> {% endif %}
                        </label>
                        <input type="password" name="password" id="password" onkeyup="refreshResetPWd()">
                        <span class="min-chars">Min. 4 Characters</span>
                        <input type="hidden" name="token" value="<?=$token; ?>" />
                    </td>
                </tr>

                <tr class="input">
                    <td class="pt-16">
                        <label>
                            Confirm Password {% if session.get('user-agent') !='OperaMiniExtreme' %} <span
                                class="show-pwd" onclick="togglePwdVisibility('confirm_password')">Show</span> {% endif
                            %}
                        </label>
                        <input type="password" name="repeatPassword" id="confirm_password" onkeyup="refreshResetPWd()">
                        <span class="min-chars">Min. 4 Characters</span>
                    </td>
                </tr>

                <tr class="input">
                    <td>
                        <button type="submit" id="resetPWDButt">
                            Update Password
                        </button>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>

<script>

    function refreshResetPWd() {
        let resetPWDButt = document.getElementById('resetPWDButt');
        let password = document.getElementById("password").value;
        let confPassword = document.getElementById("confirm_password").value;

        if (password.length == 0 || confPassword.length == 0) {

            resetPWDButt.style.background = '#616161';
            resetPWDButt.style.border = '1px solid #616161';
        } else {
            resetPWDButt.style.background = '#01B601';
            resetPWDButt.style.border = '1px solid #01B601';
        }
    }

</script>