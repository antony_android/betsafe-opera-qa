<table class="login" cellpadding="0" cellspacing="0">
    <tr class="acc-created-row">
        <td class="game-title std-pd withdraw-pd">
            Withdrawal Request Sent
        </td>
    </tr>
    <tr>
        <td class="acc-created-details std-pd">
            Your withdrawal of KSH
            <?=$withdrawalAmount; ?> has been sent to our Customer Care team for approval.
        </td>
    </tr>
    <tr>
        <td class="acc-created-border"></td>
    </tr>
</table>