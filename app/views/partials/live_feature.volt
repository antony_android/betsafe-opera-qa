<?php 
$sub_type_id=''; 
  function clean($string) {
     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
     $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

     return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
  }

  $empty_row_text = '<td>
         <table cellspacing="0" cellpadding="0"> <tr> 
         <td class="">
         <button  class="odds-btn disabled-odds" style="text-align: center;">
          <span class="odd" style="opacity:0.2;">
            <img height="15" width="15" src="/img/padlock.svg" alt="-" />
          </span>
        </button></td> </tr> 
      </table>
    </td>';
?>
<table class="highlights--item full-width market-item" cellpadding="0" cellspacing="0">
    <?php $scores = explode(":", $matchInfo['score']); ?>
    <tr>
        <td colspan="10" class="std-pd pt-10">
            <table class="league">
                <?php if(isset($selected) && $selected == 'live') { ?>
                <tr>
                    <td class="live-header">
                        Live, {{ matchInfo['match_time']}}min, {{ matchInfo['phase'] }}
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td class="time">
                        <span class="play-time">
                            <?php echo date('g:ia', strtotime($matchInfo['start_time'])); ?>
                        </span>
                        <?php echo date('D d/m', strtotime($matchInfo['start_time'])); ?>
                    </td>
                </tr>
                <tr class="game">
                    <td colspan="10">
                        <table class="full-width">
                            <tr>
                                <td class="clubs" colspan="2">
                                    <p class="pb-2">
                                        <?php echo $matchInfo['home_team']; ?>
                                        <?php if(isset($selected) && $selected == 'live') { ?> <span
                                            class="pull-right live-color">
                                            <?=$scores[0]; ?>
                                        </span>
                                        <?php } ?>
                                    </p>
                                    <p>
                                        <?php echo $matchInfo['away_team']; ?>
                                        <?php if(isset($selected) && $selected == 'live') { ?> <span
                                            class="pull-right live-color">
                                            <?=$scores[1]; ?>
                                        </span>
                                        <?php } ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="meta">
                        <?php echo $matchInfo['sport_name']." / ".$matchInfo['competition_name']; ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?php if(count($subTypes) > 0) { ?>
<?php $oddsCounter = 0; ?>
<?php for($counter=0; $counter < count($subTypes); $counter++) { ?>
<?php $bt = $subTypes[$counter]; ?>
<?php if($counter > 0): ?>
<?php $prevBt = $subTypes[$counter - 1]; ?>
<?php if($counter + 1 < count($subTypes)): ?>
<?php $nextBt = $subTypes[$counter + 1]; ?>
<?php endif; ?>
<?php endif; ?>
<?php $theMatch = @$betslip[$bt['match_id']]; ?>
<?php if($counter == 0 || ($counter > 0 && ($bt['sub_type_id'] != $prevBt['sub_type_id']))): ?>
<table class="odds markets-details full-width">
    <tr>
        <td class="markets-separator" colspan="3"></td>
    </tr>
    <?php endif; ?>
    <?php if($counter == 0 || ($counter > 0 && ($bt['sub_type_id'] != $prevBt['sub_type_id']))): ?>
    <tr>
        <td class="markets-title pt-8 std-pd" colspan="4">
            <?=$bt['name'] ?>
        </td>
    </tr>
    <?php endif; ?>

    <?php if($counter == 0 || $oddsCounter == 3): ?>
    <tr>
        <?php endif; ?>
        <?php if($bt['odd_active'] == 1) { ?>
        <td>
            <table class="full-width">
                <tr>
                    <?php if($oddsCounter == 0): ?>
                    <td style="padding:0px 4px 0px 8px;">
                        <?php elseif($oddsCounter == 2): ?>
                    <td style="padding:0px 8px 0px 4px;">
                        <?php elseif($counter > 0 && ($bt['sub_type_id'] != $nextBt['sub_type_id'])): ?>
                    <td style="padding:0px 8px 0px 4px;">
                        <?php else: ?>
                    <td style="padding:0px 4px 0px 4px;">
                        <?php endif; ?>
                        <table cellspacing="0" cellpadding="0" class="full-width">
                            <tr>

                                <td
                                    class="<?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>">
                                    <button class="mb-12 sidebet-odd <?= @$style[$sub_type_id]; ?> <?php echo $bt['match_id']; ?>
                                        <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); 
                                            if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                echo ' picked';
                                            }
                                        ?>" oddtype="<?= $bt['name'] ?>"
                                        parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch'
                                        hometeam="<?php echo $matchInfo['home_team']; ?>"
                                        oddsid="<?php echo $bt['betradar_odd_id']; ?>"
                                        awayteam="<?php echo $matchInfo['away_team']; ?>"
                                        oddvalue="<?php echo $bt['odd_value']; ?>"
                                        custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$bt['special_bet_value']); ?>"
                                        target="javascript:;" id="<?php echo $bt['match_id']; ?>"
                                        odd-key="<?php echo $bt['odd_key']; ?>"
                                        value="<?php echo $bt['sub_type_id']; ?>"
                                        special-bet-value="<?= $bt['special_bet_value'] ?>"
                                        onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-bet-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('oddsid'))">

                                        <table class="full-width">
                                            <tr>
                                                <td class="odd-pd" <?php if(strpos($bt['name'], 'Winning Margin' )
                                                    !==false): ?> style="height:32px;"
                                                    <?php endif; ?>>
                                                    <?php echo $bt['display']; ?>
                                                </td>
                                            </tr>
                                            <tr class="odd">
                                                <td class="odd-pd <?php if($theMatch && $theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                                                        echo ' odd-pd-picked';
                                                    }?>">
                                                    <?php echo number_format($bt['odd_value'], 2); ?>
                                                </td>
                                            </tr>
                                        </table>

                                    </button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <?php } else { ?>
        <td>
            <table cellspacing="0" cellpadding="0" class="full-width">
                <tr>

                    <?php if($oddsCounter == 0): ?>
                    <td style="padding:0px 4px 0px 8px;">
                        <?php elseif($oddsCounter == 2): ?>
                    <td style="padding:0px 8px 0px 4px;">
                        <?php elseif($counter > 0 && ($bt['sub_type_id'] != $nextBt['sub_type_id'])): ?>
                    <td style="padding:0px 8px 0px 4px;">
                        <?php else: ?>
                    <td style="padding:0px 4px 0px 4px;">
                        <?php endif; ?>
                        <button href="javascript:;" class="mb-12 sidebet-odd disabled-odds" <?php
                            if(strpos($bt['name'], 'Winning Margin' ) !==false): ?>
                            style="height:67px;"
                            <?php endif; ?>>
                            <table cellspacing="0" cellpadding="0" class="full-width">
                                <tr>
                                    <td class="odd-pd">
                                        <?php echo $bt['display']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        -
                                    </td>
                                </tr>
                            </table>
                        </button>
                    </td>

                </tr>
            </table>
        </td>
        <?php }  ?>
        <?php $oddsCounter++; ?>
        <?php if($oddsCounter == 3): ?>
    </tr>
    <?php $oddsCounter = 0; ?>
    <?php endif; ?>
    <?php if($counter > 0 && ($bt['sub_type_id'] != $nextBt['sub_type_id'])): ?>
</table>
<?php $oddsCounter = 0; ?>
<?php endif; ?>
<?php } ?>
<?php } ?>