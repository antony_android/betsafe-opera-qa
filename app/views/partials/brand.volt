<table class="full-width">
    <tr>
        <td class="logo">
            <?php $hash = bin2hex(openssl_random_pseudo_bytes(32)); ?>
            <a href="{{ url('/') }}?v={{ hash }}" tabindex="7">
                <img src="{{ url('/img/logo.svg') }}" {% if session.get('user-agent')=='OperaMiniExtreme' %}
                    style="width: 70px;" {% endif %} alt="logo">
            </a>
        </td>
        <td>
            {% if session.get('auth') != null %}
            <a href="{{ url('/deposit') }}" tabindex="8">
                <button class="deposit-header">
                    <img src="{{ url('/img/deposit.svg') }}" alt="Deposit" />
                </button>
            </a>
            <span class="hd-loggedin" id="userbalance">
                KSH {{ session.get('auth')['balance'] }}
            </span>
            {% else %}
            <a href="{{ url('/signup') }}" tabindex="8">
                <button class="join-now">{% if session.get('user-agent')=='OperaMiniExtreme' %} Join {% else %} Join Now
                    {% endif %}</button>
            </a>
            <a href="{{ url('/login') }}" class="hd-login" {% if session.get('user-agent')=='OperaMiniExtreme' %}
                style="padding-top: 4px;" {% endif %} tabindex="9">
                Login
            </a>
            {% endif %}
        </td>
    </tr>
</table>

{% if session.get('auth') != null %}
<script>

    function pollBalance() {

        $.post('/lite/my-profile/balance', function (data) {
            try {
                data = JSON.parse(data);
            } catch (e) {
                return false;
            }
            if (data.ResultCode == 1) {
                if (typeof data.Balance.Amount === "number") {
                    $('#userbalance').html("KSH " + data.Balance.Amount.toFixed(2));
                    if ($('#tranbalance').length) {
                        $('#tranbalance').html("KSH " + data.Balance.Amount.toFixed(2));
                    }
                } else {
                    $('#userbalance').html("KSH 0.00");
                    if ($('#tranbalance').length) {
                        $('#tranbalance').html("KSH 0.00");
                    }
                }
                return true;
            }
        });

        setTimeout(pollBalance, 10000);
    }

    $(document).ready(function () {
        pollBalance();
    });
</script>
{% endif %}