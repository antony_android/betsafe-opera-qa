<table class="login" cellpadding="0" cellspacing="0">
    <tr class="login-row">
        <td class="game-title std-pd" style="padding-bottom: 5px !important;">
            Super Jackpot Ksh. 40M Guaranteed Bonus
        </td>
    </tr>
    <tr>
        <td>
            <table class="form" cellpadding="0" cellspacing="0">
                <tr class="bonus-row">
                    <td style="padding-top: 0px !important;">
                        <a href="{{ url('/jackpot') }}">
                            <img src="{{ url('/img/promotions/super-jackpot.jpg') }}" alt="Super Jackpot"
                                style="width: 100%;" />
                        </a>
                        <p class="bonus-message">
                            Win Ksh.40,000,000 Super Jackpot bonus. <br />
                            The biggest guaranteed bonus. Play the Super Jackpot, get at least 10 correct predictions &
                            enter the live draw for a
                            chance to win.
                        </p>
                        <p style="padding-top: 8px;">
                            Play the Super Jackpot on 30th April, 7th May and 14th May for a chance to enter a live draw
                            and win a Ksh.40,000,000 guaranteed bonus. 10 Super Jackpot players will be selected for the
                            live draw with one
                            player winning the biggest ever bonus.
                            You can be a winner in 3 easy steps:
                        <ol style="margin: 4px;padding-left: 25px;">
                            <li>Play the Super Jackpot</li>
                            <li>Get 10 or more correct predictions</li>
                            <li>Enter live draw</li>
                        </ol>
                        All Super Jackpot players who get 10 or more predictions between the 30th of April and 14th of
                        May will be entered into a live draw for a chance to win the biggest guaranteed bonus in Kenya.
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>