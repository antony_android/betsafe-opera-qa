<table cellpadding="0" cellspacing="0" style="margin-bottom: 10px;" class="full-width">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="login-row">
        <td class="game-title">
            Bonuses
        </td>
    </tr>

    <tr class="filter-bets-row bet-history-header">
        <td>
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="bonusesControl" id="bonusesControl"
                class="form-control slip-filter-select" style="margin-top:0 !important;" value="Expired Bonuses"
                onclick="toggleBonusesFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img" style="margin-top: 9px;"
                onclick="toggleBonusesFilter()" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="all-list-item">
                    <a href="{{ url('/bonuses') }}">
                        Available Bonuses
                        <?= "(".($freebetsCount + 2).")"; ?>
                    </a>
                </li>
                <li id="active-list-item">
                    <a href="{{ url('/bonuses/active') }}">
                        Active Bonuses
                        <?= "(".($freebetsCount + 2).")"; ?>
                    </a>
                </li>
                <li id="expired-list-item">
                    <a href="{{ url('/bonuses/expired') }}">
                        Expired Bonuses
                    </a>
                </li>
            </ul>
            {% else %}
            <form method="post" action="{{ url('/bonuses/filter') }}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
                    <option value="all" {% if selectedBonusesControl=='all' %} selected {% endif %}>Available Bonuses (
                        <?=$freebetsCount + 2; ?>
                        )
                    </option>
                    <option value="active" {% if selectedBonusesControl=='active' %} selected {% endif %}>Active Bonuses
                        (
                        <?=$freebetsCount + 2; ?>)
                    </option>
                    <option value="expired" {% if selectedBonusesControl=='expired' %} selected {% endif %}>Expired
                        Bonuses</option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <tr class="empty-slip-row" style="height: 136px;">
        <td class="" colspan="5">
            <img src="{{ url('/img/logo.svg') }}" width="90px" alt="BetSafe" />
            <span>You currently have no Expired bonuses.</span>
        </td>
    </tr>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    function toggleBonusesFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }
</script>
<?php if($selectedBonusesControl == 'all'): ?>
<script>
    document.getElementById('all-list-item').classList.add("slip-filter-selected");
    document.getElementById('active-list-item').classList.remove("slip-filter-selected");
    document.getElementById('expired-list-item').classList.remove("slip-filter-selected");
    document.getElementById('bonusesControl').value = "Available Bonuses";
</script>
<?php elseif($selectedBonusesControl == 'active'): ?>
<script>
    document.getElementById('all-list-item').classList.remove("slip-filter-selected");
    document.getElementById('active-list-item').classList.add("slip-filter-selected");
    document.getElementById('expired-list-item').classList.remove("slip-filter-selected");
    document.getElementById('bonusesControl').value = "Active Bonuses";
</script>
<?php elseif($selectedBonusesControl == 'expired'): ?>
<script>
    document.getElementById('all-list-item').classList.remove("slip-filter-selected");
    document.getElementById('active-list-item').classList.remove("slip-filter-selected");
    document.getElementById('expired-list-item').classList.add("slip-filter-selected");
    document.getElementById('bonusesControl').value = "Expired Bonuses";
</script>
<?php endif; ?>