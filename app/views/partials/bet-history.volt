<table class="full-width" cellpadding="0" cellspacing="0">
  <th>{{ this.flashSession.output() }}</th>
  <tr class="page-header-row bet-history-header">
    <td class="game-title std-pd">
      Bet History
    </td>
  </tr>
  <tr class="filter-bets-row bet-history-header">
    <td class="std-pd">
      {% if session.get('user-agent') != 'OperaMiniExtreme' %}
      <input type="text" readonly name="betType" id="betType" class="form-control slip-filter-select" value="All Bets"
        onclick="toggleBetsFilter()" />
      <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img" />
      <ul class="slip-filter-ul" id="slip-filter-ul">
        <li id="all-list-item">
          <a href="{{ url('/bet-history') }}">
            All Bets
          </a>
        </li>
        <li id="open-list-item">
          <a href="{{ url('/bet-history/open') }}">
            Open Bets
          </a>
        </li>
        <li id="settled-list-item">
          <a href="{{ url('/bet-history/settled') }}">
            Settled Bets
          </a>
        </li>
        <li id="settled-list-item">
          <a href="{{ url('/jackpot/history') }}">
            Jackpot Bets
          </a>
        </li>
      </ul>
      {% else %}
      <form method="post" action="{{ url('/bet-history/filter') }}">
        <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
          <option value="all" {% if session.get('selected-bet-history')=='' %} selected {% endif %}>All Bets</option>
          <option value="open" {% if session.get('selected-bet-history')=='open' %} selected {% endif %}>
            Open Bets
          </option>
          <option value="settled" {% if session.get('selected-bet-history')=='settled' %} selected {% endif %}>Settled
            Bets
          </option>
          <option value="settled" {% if selected=='jackpot-history' %} selected {% endif %}>Jackpot
            Bets
          </option>
        </select>
      </form>
      {% endif %}
    </td>
  </tr>
  <?php if(!empty($myBets)): ?>
  <?php foreach($myBets as $bet): ?>
  <tr class="bet">
    <td>
      <a href="{{ url('/bet-history/show/')}}<?=$bet['isolutions_couponid']; ?>/<?=$bet['coupon_code']; ?>"
        class="undecorate">

        <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
          <tr>
            <td class="matches-border" colspan="4"></td>
          </tr>
          <tr>
            <td class="std-pd pt-10">
              <table class="league">
                <tr>
                  <td class="time" colspan="2">
                    <span class="play-time">
                      <?php echo date('g:ia', strtotime($bet['created'])); ?>
                    </span>
                    <?php echo date('D d/m', strtotime($bet['created'])); ?>
                  </td>
                  <td class="text-right slip-outcome time">
                    <?php
                    if($bet['cashout']){
                      echo '<span class="won">Cashout</span>';
                    }elseif($bet['status']==10){
                      echo '<span class="open">Open</span>';
                    }elseif($bet['status']==50 && $bet['win'] == 1 && !$bet['cashout']){
                      echo '<span class="won">Won</span>';
                    }elseif($bet['win']==2){
                      echo '<span class="lost">Lost</span>';
                    }else{
                      echo '<span class="open">Open</span>';
                    }
                  ?>
                  </td>
                </tr>
                <tr class="game">
                  <td>
                    <table width="100%">
                      <tr>
                        <td class="clubs" colspan="3" style="font-size: 12px;">
                          ID:
                          <?php echo $bet['coupon_code']; ?>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="slip-hist pt-10 pb-0">
                    <span class="slip-hist-sp">STAKE:</span>
                    ODDS:
                  </td>
                  <td class="slip-hist pt-10 pb-0 text-right" colspan="2">
                    PAYOUT:
                  </td>
                </tr>
                <tr>
                  <td class="slip-hist-details">
                    <span class="slip-hist-sp">
                      <?= number_format($bet['bet_amount'], 2); ?>
                    </span>
                    {{ bet['total_odd'] }}
                  </td>
                  <td class="slip-hist-details text-right pb-8 pt-4" colspan="2">
                    {{ bet['user_winnings'] }}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </a>
    </td>
  </tr>
  <?php endforeach; ?>
  <?php else: ?>

  <tr class="empty-slip-row">
    <td class="" colspan="4">
      <img src="{{ url('/img/logo.svg') }}" width="90px" alt="BetSafe" />
      <span>You have not placed any bets yet</span>
    </td>
  </tr>
  <?php endif; ?>
</table>
<script>
  document.getElementById('slip-filter-ul').style.display = 'none';
  function toggleBetsFilter() {
    if (document.getElementById('slip-filter-ul').style.display == 'none') {
      document.getElementById('slip-filter-ul').style.display = '';
    } else {
      document.getElementById('slip-filter-ul').style.display = 'none';
    }
  }
</script>
<?php if($selectedBets == 'all'): ?>
<script>
  document.getElementById('all-list-item').classList.add("slip-filter-selected");
  document.getElementById('open-list-item').classList.remove("slip-filter-selected");
  document.getElementById('settled-list-item').classList.remove("slip-filter-selected");
  document.getElementById('betType').value = "All Bets";
</script>
<?php elseif($selectedBets == 'open'): ?>
<script>
  document.getElementById('open-list-item').classList.add("slip-filter-selected");
  document.getElementById('all-list-item').classList.remove("slip-filter-selected");
  document.getElementById('settled-list-item').classList.remove("slip-filter-selected");
  document.getElementById('betType').value = "Open Bets";
</script>
<?php elseif($selectedBets == 'settled'): ?>
<script>
  document.getElementById('settled-list-item').classList.add("slip-filter-selected");
  document.getElementById('open-list-item').classList.remove("slip-filter-selected");
  document.getElementById('all-list-item').classList.remove("slip-filter-selected");
  document.getElementById('betType').value = "Settled Bets";
</script>
<?php endif; ?>