<table class="full-width" cellpadding="0" cellspacing="0">
    <th>{{ this.flashSession.output() }}</th>
    <tr class="page-header-row bet-history-header">
        <td class="game-title std-pd">
            Jackpot History
        </td>
    </tr>
    <tr class="filter-bets-row bet-history-header">
        <td class="std-pd">
            {% if session.get('user-agent') != 'OperaMiniExtreme' %}
            <input type="text" readonly name="betType" id="betType" class="form-control slip-filter-select"
                value="Open Bets" onclick="toggleBetsFilter()" />
            <img src="{{ url('/img/slip-filter.png') }}" alt="" class="slip-filter-img" />
            <ul class="slip-filter-ul" id="slip-filter-ul">
                <li id="open-list-item">
                    <a href="{{ url('/jackpot/history') }}">
                        Open Bets
                    </a>
                </li>
                <li id="settled-list-item">
                    <a href="{{ url('/jackpot/closedbets') }}">
                        Closed Bets
                    </a>
                </li>
            </ul>
            {% else %}

            <form method="post" action="{{ url('/jackpot/filter') }}">
                <select name="selection" id="selection" class="form-control" onchange="this.form.submit()">
                    <option value="open" {% if session.get('selected-jp-history')=='open' %} selected {% endif %}>
                        Open Bets
                    </option>
                    <option value="closedbets" {% if session.get('selected-jp-history')=='closedbets' %} selected {%
                        endif %}>
                        Closed Bets
                    </option>
                </select>
            </form>
            {% endif %}
        </td>
    </tr>
    <?php if(!empty($jackpotBets)): ?>
    <?php foreach($jackpotBets as $bet): ?>
    <tr class="bet">
        <td>
            <a href="{{ url('jackpot/show/') }}{{bet['jackpot_bet_id']}}/jp" class="undecorate">

                <table class="highlights--item full-width" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="matches-border" colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="std-pd pt-10">
                            <table class="league">
                                <tr>
                                    <td class="time" colspan="2">
                                        <span class="play-time">
                                            <?php echo $bet['jackpot_name']; ?>
                                        </span>
                                    </td>
                                    <td class="text-right slip-outcome time">
                                        <?php
                                            if($bet['status']==0){
                                            echo '<span class="open">Open</span>';
                                            }elseif($bet['status']==5){
                                            echo '<span class="won">Winner!</span>';
                                            }elseif($bet['status']==3){
                                            echo '<span class="lost">No Win</span>';
                                            }elseif($bet['status']==-1){
                                            echo '<span class="lost">Cancelled</span>';
                                            }else{
                                                echo '<span class="open">Open</span>';
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr class="game">
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td class="clubs" colspan="3">
                                                    ID:
                                                    <?php echo $bet['jackpot_bet_id']; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="slip-hist pt-8 pb-0">
                                        <span class="slip-hist-sp">STAKE:</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="slip-hist-details">
                                        <span class="slip-hist-sp" style="padding-bottom: 8px;padding-top: 2px;">{{
                                            bet['bet_amount'] }}</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </a>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php else: ?>

    <tr class="empty-slip-row">
        <td class="" colspan="4">
            <img src="{{ url('/img/logo.svg') }}" width="90px" alt="BetSafe" />
            <span>You currently have no {{ type_of_bets }} Jackpot Bets</span>
        </td>
    </tr>
    <?php endif; ?>
</table>
<script>
    document.getElementById('slip-filter-ul').style.display = 'none';
    function toggleBetsFilter() {
        if (document.getElementById('slip-filter-ul').style.display == 'none') {
            document.getElementById('slip-filter-ul').style.display = '';
        } else {
            document.getElementById('slip-filter-ul').style.display = 'none';
        }
    }
</script>
<?php if($selectedBets == 'open'): ?>
<script>
    document.getElementById('open-list-item').classList.add("slip-filter-selected");
    document.getElementById('settled-list-item').classList.remove("slip-filter-selected");
    document.getElementById('betType').value = "Open Bets";
</script>
<?php elseif($selectedBets == 'closed'): ?>
<script>
    document.getElementById('settled-list-item').classList.add("slip-filter-selected");
    document.getElementById('open-list-item').classList.remove("slip-filter-selected");
    document.getElementById('betType').value = "Closed Bets";
</script>
<?php endif; ?>