<table class="login" cellpadding="0" cellspacing="0">
  <th>{{ this.flashSession.output() }}</th>
  <tr class="login-row">
    <td class="game-title">
      Deposit
    </td>
  </tr>
  <tr>
    <td class="deposit-details">
      Enter your desired deposit amount in KSH, then authorise the transaction using your service PIN
    </td>
  </tr>
  <tr>
    <td>
      <form action="{{ url('/deposit/topup') }}" method="post" id="topupForm">
        <table class="form deposit-input" cellpadding="0" cellspacing="0">
          <tr class="input">
            <td colspan="4">
              <label>
                Enter Amount
              </label>
              <div class="flag-div">
                <span style="vertical-align: middle;">KSH</span>
                <input type="text" name="amount" id="deposit_amount" class="flag-input" value="" />
              </div>
              <span class="min-chars">Min. 10</span>
            </td>
          </tr>
          {% if session.get('user-agent') !='OperaMiniExtreme' %}
          <tr>
            <td class="std-4pd" style="width: 25%;padding: 0px 4px 0px 8px;">
              <button class="deposit-sel" onclick="fiftyDeposit()" type="button" id="fiftyBobButt">
                49
              </button>
            </td>
            <td class="std-4pd" style="width: 25%;padding: 0px 4px">
              <button class="deposit-sel" onclick="hundredDeposit()" type="button" id="hundredBobButt">
                99
              </button>
            </td>
            <td class="std-4pd" style="width: 25%;padding: 0px 4px">
              <button class="deposit-sel" onclick="twoHundredDeposit()" type="button" id="twoHundredBobButt">
                199
              </button>
            </td>
            <td style="width: 25%;padding: 0px 8px 0px 4px">
              <button class="deposit-sel" onclick=" fiveHundredDeposit()" type="button" id="fiveHundredBobButt">
                499
              </button>
            </td>
          </tr>
          {% else %}
          <tr>
            <td class="std-pd">
              <table class="highlights full-width">
                <tr class="odds">
                  <td style="padding: 0px 4px 0px 0px;">
                    <table cellspacing="0" cellpadding="0">
                      <tr>
                        <button type="button" onclick="fiftyDeposit()" id="fiftyBobButt"
                          style="background: #2E2F30 !important;width:100%">
                          49
                        </button>
                      </tr>
                    </table>
                  </td>
                  <td style="padding: 0px 4px 0px 4px;">
                    <table cellspacing="0" cellpadding="0">
                      <tr>
                        <button type="button" onclick="hundredDeposit()" id="hundredBobButt"
                          style="background: #2E2F30 !important;width:100%">
                          99
                        </button>
                      </tr>
                    </table>
                  </td>
                  <td style="padding: 0px 4px 0px 4px;">
                    <table cellspacing="0" cellpadding="0">
                      <tr>
                        <button type="button" onclick="twoHundredDeposit()" id="twoHundredBobButt"
                          style="background: #2E2F30 !important;width:100%">
                          199
                        </button>
                      </tr>
                    </table>
                  </td>
                  <td style="padding: 0px 0px 0px 4px;">
                    <table cellspacing="0" cellpadding="0">
                      <tr>
                        <button type="button" onclick=" fiveHundredDeposit()" id="fiveHundredBobButt"
                          style="background: #2E2F30 !important;width:100%">
                          499
                        </button>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          {% endif %}

          <tr class="input">
            <td colspan="4">
              <button type="submit" class="green-theme m-0" id="depositButt">
                Deposit
              </button>
            </td>
          </tr>

          <tr>
            <td class="deposit-terms pb-12" colspan="4">
              Deposit subject to M-Pesa charges
            </td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
  <tr>
    <td class="depo-mpesa-header">
      Deposit using M-Pesa
    </td>
  </tr>
  <tr>
    <td class="depo-mpesa-details">
      <p>
        Add funds to your Bestafe account in seconds using online deposit.
      </p>
      <p style="padding: 20px 0px;">
        <span class="bold">Deposit onilne</span><br />
        Minimum Deposit: KSH 10<br />
        Maximum Deposit: KSH 10,000
      </p>
    </td>
  </tr>
  <tr>
    <td class="depo-mpesa-header">
      Deposit using M-Pesa short code *334#
    </td>
  </tr>
  <tr>
    <td class="depo-mpesa-details">
      <ol>
        <li>Dial *334#</li>
        <li>Select Lipa na M-Pesa</li>
        <li>Select Pay Bill</li>
        <li>Enter 7290099 as the Business Number</li>
        <li>Enter BETSAFE as the Account Number</li>
        <li>Enter an amount (no commas) e.g 2000</li>
        <li>Enter your M-Pesa PIN and send</li>
        <li>Accept the payment and you will receive an SMS confirming the transaction</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td class="depo-mpesa-header pt-16">
      Deposit using M-Pesa Menu
    </td>
  </tr>
  <tr>
    <td class="depo-mpesa-details pb-16">
      <ol>
        <li>Go to M-PESA Menu on your mobile phone</li>
        <li>Select Lipa na M-Pesa</li>
        <li>Select Pay Bill</li>
        <li>Enter 7290099 as the Business Number</li>
        <li>Enter BETSAFE as the Account Number</li>
        <li>Enter an amount (no commas) e.g 2000</li>
        <li>Enter your M-Pesa PIN and send</li>
        <li>You will receive an SMS confirming the transactions</li>
      </ol>
    </td>
  </tr>
</table>
<script>
  function fiftyDeposit() {
    document.getElementById("deposit_amount").value = 49;
    document.getElementById("fiftyBobButt").style.background = '#01B601';
    document.getElementById("fiftyBobButt").style.border = '1px solid #01B601';
    document.getElementById("hundredBobButt").style.background = '#2E2F30';
    document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
    document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function hundredDeposit() {
    document.getElementById("deposit_amount").value = 99;
    document.getElementById("hundredBobButt").style.background = '#01B601';
    document.getElementById("hundredBobButt").style.border = '1px solid #01B601';
    document.getElementById("fiftyBobButt").style.background = '#2E2F30';
    document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
    document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function twoHundredDeposit() {
    document.getElementById("deposit_amount").value = 199;
    document.getElementById("twoHundredBobButt").style.background = '#01B601';
    document.getElementById("twoHundredBobButt").style.border = '1px solid #01B601';
    document.getElementById("fiftyBobButt").style.background = '#2E2F30';
    document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("hundredBobButt").style.background = '#2E2F30';
    document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("fiveHundredBobButt").style.background = '#2E2F30';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  function fiveHundredDeposit() {
    document.getElementById("deposit_amount").value = 499;
    document.getElementById("fiveHundredBobButt").style.background = '#01B601';
    document.getElementById("fiveHundredBobButt").style.border = '1px solid #01B601';
    document.getElementById("fiftyBobButt").style.background = '#2E2F30';
    document.getElementById("fiftyBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("hundredBobButt").style.background = '#2E2F30';
    document.getElementById("hundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
    document.getElementById("twoHundredBobButt").style.background = '#2E2F30';
    document.getElementById("twoHundredBobButt").style.border = '1px solid rgba(255, 255, 255, 0.57)';
  }

  $(document).ready(function () {
    $('#topupForm').submit(function (e) {
      e.preventDefault()
      $('#depositButt').prop('disabled', true);
      document.getElementById("topupForm").submit();
    });
  });
</script>