<table class="full-width">
    <tr>
        <td class="header-paybill main-padding">
            MPESA Paybill: 7290099
        </td>
        <td class="header-mpesa main-padding">
            <a href="{{ url('/deposit') }}" tabindex="1">
                <img src="{{ url('/img/banner_mpesa_logo.png') }}" alt="MPESA" />
            </a>
        </td>
    </tr>
</table>