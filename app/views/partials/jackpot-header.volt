<table class="league">
    <tr>
        <td class="time">
            <span class="play-time">
                <?php echo date('g:ia', strtotime($day['start_time'])); ?>
            </span>
            <?php echo date('D d/m', strtotime($day['start_time'])); ?>
        </td>
    </tr>
    <tr class="game">
        <td colspan="10">
            <table width="100%">
                <tr>
                    <td class="clubs" colspan="2">
                        <p class="pb-2">
                            <?php echo $day['home_team']; ?>
                        </p>
                        <p>
                            <?php echo $day['away_team']; ?>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="meta">
            <?php echo "Football / ".$day['competition_name']; ?>
        </td>
    </tr>
</table>