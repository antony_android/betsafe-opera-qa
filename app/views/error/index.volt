<table id="main">
    <tr>
        <td class="bonus-bg">
            <table class="full-width" cellpadding="0" cellspacing="0">

                <tr class="page-header-row bet-history-header">
                    <td class="game-title std-pd">
                        Page Not Found
                    </td>
                </tr>
                <tr>
                    <td class="depo-mpesa-details std-pd">
                        <p class="pb-16">
                            We could not find the page that you are looking for. Please navigate to Home
                            Page
                        </p>
                    </td>
                </tr>
                <tr class="empty-slip-row" style="height: 90px !important;">
                    <td class="" colspan="5">
                        <a href="/lite/">
                            <img src="/lite/img/logo.svg" width="90px" alt="BetSafe" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>