<table id="main">
    <?php if(isset($referrer) && strpos($referrer, 'signup') !== false && !is_null($this->session->get('auth'))) { ?>
    <tr>
        <td>
            {{ partial('partials/account-created') }}
        </td>
    </tr>
    <?php } else if(isset($referrer) && strpos($referrer, 'resetpassword/new') !== false && !is_null($this->session->get('auth'))) { ?>
    <tr>
        <td>
            {{ partial('partials/password-reset-note') }}
        </td>
    </tr>
    <?php } else if(strpos($referrer, 'withdraw') !== false && $this->session->get('withdrawalAmount') > 0) {?>
    <tr>
        <td>
            {{ partial('partials/withdrawal-sent') }}
        </td>
    </tr>
    <?php } else if(isset($referrer) && strpos($referrer, 'login') !== false && !is_null($this->session->get('auth'))) { ?>
    <tr>
        <td>
            {{ partial('partials/welcome') }}
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td>
            {{ partial('partials/matches') }}
        </td>
    </tr>
</table>