<?php

class LivematchController extends ControllerBase
{
    public function openAction($id)
    {

        $style = [
            '208' => 'one-row',
            '55' => 'one-row',
            '201' => 'one-row',
            '235' => 'one-row',
            '46' => 'three-rows',
            '270' => 'three-rows',
            '323' => 'three-rows',
            '246' => 'one-row',
            '10' => 'one-row',
            '390' => 'one-row',
            '41' => 'one-row',
            '42' => 'one-row',
        ];

        $matchInfo = $this->rawSelect("SELECT m.home_team, m.game_id, m.away_team,m.start_time,
            m.sport_name, m.score,m.competition_name,m.parent_match_id, m.phase, m.match_time FROM
            `live_match` m WHERE m.match_id=? LIMIT 1", [$id]);

        $sportId = $matchInfo[0]['sport_id'];
        $sportDetails = $this->rawQueries("SELECT * FROM sport WHERE
            sport_id='$sportId' LIMIT 1");

        $eventsTitle = $sportDetails[0]['sport_name'];

        $matchInfo = array_shift($matchInfo);

        $subTypes = $this->rawSelect("SELECT m.match_id, e.odd_active,
                e.odd_key as display,e.market_name as `name`, e.betradar_odd_id,
                e.odd_key, e.odd_value,e.sub_type_id, e.special_bet_value, e.market_code
                FROM live_odds e INNER JOIN `live_match` m  ON
                m.parent_match_id = e.parent_match_id
               WHERE match_id = '$id' AND e.odd_key NOT LIKE '%Match HCP%'
               AND e.odd_key <> '-1' AND CHAR_LENGTH(e.odd_key) < 16
               ORDER BY e.sequence_no ASC, e.market_name");

        $theBetslip = $this->session->get("betslip");
        $title = $matchInfo['home_team'] . " vs " . $matchInfo['away_team'];

        $this->tag->setTitle($title);

        $navigation = $this->getNavigation();
        $selected = 'live';

        $this->view->setVars([
            'subTypes' => $subTypes,
            'eventsTitle' => $eventsTitle,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'selected' => $selected,
            'matchInfo' => $matchInfo,
            'betslip' => $theBetslip,
            'slipCount' => sizeof($theBetslip),
            'style' => $style,
        ]);

        $this->view->pick('livematch/open');
    }
}