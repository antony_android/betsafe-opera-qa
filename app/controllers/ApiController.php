<?php

class ApiController extends ControllerBase
{

    public function jackpotsAction()
    {

        $postData = file_get_contents('php://input');

        // $file = fopen("C:/tmp/jackpots.log", "a");
        $file = fopen("/var/log/jackpot/jackpot.log", "a");

        if (fwrite($file, $postData) === false) {
            fwrite($file, "Error: no data written");
        }

        fwrite($file, "\r\n");
        fclose($file);

        if ($postData != null) {

            $decoded = json_decode($postData);
            $userId = $decoded->UserId;
            $apiPassword = $decoded->APIPassword;

            if ($userId == "LiteSMS" && $apiPassword == "gffewhf2792!!!") {

                $jackpots = $this->rawQueries("SELECT DISTINCT je.jackpot_event_id, je.jackpot_name,
                    je.jackpot_amount,je.status, je.total_games, je.requisite_wins,
                    je.bet_amount FROM jackpot_event je INNER JOIN jackpot_match jm ON
                    je.jackpot_event_id = jm.jackpot_event_id WHERE je.status = 'ACTIVE' AND
                    je.jackpot_event_id NOT IN (SELECT DISTINCT mj.jackpot_event_id FROM
                    jackpot_match mj WHERE mj.start_time < NOW()) ORDER BY je.jackpot_name DESC;");

                if (is_null($jackpots) || empty($jackpots)) {

                    $payload = [
                        "Status" => 503,
                        "Message" => "No jackpot events at the moment. Please check later.",
                    ];

                } else {

                    $payload = [

                        "Status" => 200,
                    ];

                    $jackpotsArr = array();

                    foreach ($jackpots as $jackpot) {

                        $jackpotEventId = $jackpot['jackpot_event_id'];
                        $earliestKickOffRS = $this->rawQueries("SELECT jm.start_time FROM
                        jackpot_match jm WHERE jm.jackpot_event_id = '$jackpotEventId'
                        ORDER BY jm.start_time ASC LIMIT 1");

                        $earliestKickOff = $earliestKickOffRS[0]['start_time'];

                        $remainingTimeInSeconds = strtotime($earliestKickOff) - strtotime(date('Y-m-d H:i:s'));
                        $timeRemaining = JackpotController::fromSeconds($remainingTimeInSeconds);

                        $oneJackpot = [
                            "JackpotID" => $jackpotEventId,
                            "JackpotName" => $jackpot['jackpot_name'],
                            "JackpotAmount" => $jackpot['jackpot_amount'],
                            "TotalGames" => $jackpot['total_games'],
                            "Stake" => $jackpot['bet_amount'],
                            "TimeRemaining" => $timeRemaining,
                        ];

                        array_push($jackpotsArr, $oneJackpot);
                    }

                    $payload["Jackpots"] = $jackpotsArr;

                }

            } else {

                $payload = [
                    "Status" => 403,
                    "Message" => "You are not authorized to use this resource",
                ];
            }
        } else {

            $payload = [
                "Status" => 400,
                "Message" => "Bad Request",
            ];
        }

        return json_encode($payload);
    }

    public function gamesAction()
    {

        $postData = file_get_contents('php://input');

        $file = fopen("/var/log/jackpot/jackpot.log", "a");

        if (fwrite($file, $postData) === false) {
            fwrite($file, "Error: no data written");
        }

        fwrite($file, "\r\n");
        fclose($file);

        if ($postData != null) {

            $decoded = json_decode($postData);
            $userId = $decoded->UserId;
            $apiPassword = $decoded->APIPassword;
            $jackpotId = $decoded->JackpotID;

            if ($userId == "LiteSMS" && $apiPassword == "gffewhf2792!!!") {

                $jEvent = $this->rawQueries("SELECT je.jackpot_event_id, je.jackpot_name, je.jackpot_amount,
                    je.total_games, je.requisite_wins, m.start_time AS kick_off, je.bet_amount FROM
                    jackpot_event je INNER JOIN jackpot_match jm ON je.jackpot_event_id=jm.jackpot_event_id
                    INNER JOIN `match` m ON jm.parent_match_id=m.parent_match_id WHERE
                    je.jackpot_event_id='$jackpotId' AND m.start_time > NOW() ORDER BY m.start_time
                     ASC LIMIT 1");

                if (is_null($jEvent) || empty($jEvent)) {

                    $payload = [
                        "Status" => 503,
                        "Message" => "Jackpot with that ID doesn't exist or is inactive",
                    ];

                } else {

                    $jEvent = $jEvent[0];
                    $kickOffTime = $jEvent["kick_off"];

                    $matches = $this->rawQueries("SELECT j.game_order AS pos, jackpot_match_id,
                        e.sub_type_id,j.start_time, e.betradar_odd_id,  j.game_id,
                        MAX(CASE WHEN e.odd_key = '1' THEN odd_value END) AS home_odd,
                        MAX(CASE WHEN e.odd_key = 'X' THEN odd_value END) AS neutral_odd,
                        MAX(CASE WHEN e.odd_key = '2' THEN odd_value END) AS away_odd,
                        j.parent_match_id, j.away_team, j.home_team, c.competition_name, c.category FROM
                        jackpot_match j INNER JOIN competition c ON j.competition_id = c.competition_id
                        INNER JOIN event_odd e ON e.parent_match_id = j.parent_match_id WHERE
                        j.jackpot_event_id='$jackpotId' AND j.status='ACTIVE' AND
                        e.sub_type_id = 1 GROUP BY j.parent_match_id ORDER BY j.game_order ASC");

                    $payload = [

                        "Status" => 200,
                        "JackpotID" => $jackpotId,
                        "JackpotDate" => $kickOffTime,
                    ];

                    $gamesArr = array();

                    foreach ($matches as $match) {

                        $oneGame = [
                            "GameID" => $match['game_id'],
                            "EventID" => $match['parent_match_id'],
                            "Event" => $match['home_team'] . " vs " . $match['away_team'],
                            "StartTime" => $match['start_time'],
                            "Home" => $match['home_odd'],
                            "Draw" => $match['neutral_odd'],
                            "Away" => $match['away_odd'],
                            "Order" => $match['pos'],
                        ];

                        array_push($gamesArr, $oneGame);
                    }

                    $payload["JPGames"] = $gamesArr;

                }

            } else {

                $payload = [
                    "Status" => 403,
                    "Message" => "You are not authorized to use this resource",
                ];
            }

        } else {

            $payload = [
                "Status" => 400,
                "Message" => "Bad Request",
            ];
        }

        return json_encode($payload);
    }

    public function placejackpotAction()
    {

        $postData = file_get_contents('php://input');

        $file = fopen("/var/log/jackpot/jackpot.log", "a");

        if (fwrite($file, $postData) === false) {
            fwrite($file, "Error: no data written");
        }

        fwrite($file, "\r\n");
        fclose($file);

        if ($postData != null) {

            $decoded = json_decode($postData);
            $userId = $decoded->UserId;
            $apiPassword = $decoded->APIPassword;
            $jackpotId = $decoded->JackpotID;
            $userMsisdn = $decoded->MSISDN;

            if ($userId == "LiteSMS" && $apiPassword == "gffewhf2792!!!") {

                $mobile = $this->formatMobileNumber($userMsisdn);

                $payload = [
                    "MobilePhone" => "254" . $mobile,
                    "VerifyReceivedFields" => true,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID,
                ];

                $url = $this->qaBaseURL . "/api/users/v2/GetUserInfoFromMobilePhoneV3";
                $userInfoResponse = $this->postToUrl($payload, $url);

                if ($userInfoResponse->ResultCode == 1) {

                    $balance = 0;
                    $userId = $userInfoResponse->UserInfo->UserID;

                    $balancePayload = [
                        "UserID" => $userId,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID,
                    ];

                    $balanceUrl = $this->qaBaseURL . "/api/users/GetBalanceFull";
                    $balanceResponse = $this->postToUrl($balancePayload, $balanceUrl);

                    if ($balanceResponse->ResultCode == 1) {

                        $balance = $balanceResponse->Balance->Amount;

                        $jEvent = $this->rawQueries("SELECT je.jackpot_event_id, je.jackpot_name, je.jackpot_amount,
                            je.total_games, je.requisite_wins, m.start_time AS kick_off, je.bet_amount FROM
                            jackpot_event je INNER JOIN jackpot_match jm ON je.jackpot_event_id=jm.jackpot_event_id
                            INNER JOIN `match` m ON jm.parent_match_id=m.parent_match_id WHERE
                            je.jackpot_event_id='$jackpotId' AND m.start_time > NOW()
                            ORDER BY m.start_time ASC LIMIT 1");

                        if (is_null($jEvent) || empty($jEvent)) {

                            $responsePayload = [
                                "Status" => 4,
                                "Message" => "Jackpot with that ID doesn't exist or is inactive",
                            ];

                        } else {

                            $checkValidity = $this->rawQueries("SELECT * FROM jackpot_match WHERE
                             jackpot_event_id='$jackpotId' AND start_time < NOW()");

                            if (!empty($checkValidity)) {

                                $responsePayload = [
                                    "Status" => 4,
                                    "Message" => "Invalid jackpot. Some games have already played!",
                                ];

                                return json_encode($responsePayload);
                            }

                            $jEvent = $jEvent[0];

                            if ($balance < $jEvent['bet_amount']) {

                                $responsePayload = [
                                    "Status" => 5,
                                    "Message" => "Insufficient Balance. Deposit via MPESA Paybill 7290099 Account BETSAFE to place Jackpot",
                                ];
                            } else {

                                if ($jEvent['total_games'] == 9 || strpos($jEvent['jackpot_name'], 'Daily') !== false) {

                                    $numEntriesRS = $this->rawQueries("SELECT COUNT(*) AS num FROM jackpot_bet WHERE
                                        jackpot_event_id = '" . $jEvent['jackpot_event_id'] . "' AND
                                        isolutions_userid = '$userId'");

                                    $numEntries = $numEntriesRS[0]['num'];

                                    if ($numEntries >= 16) {

                                        $responsePayload = [
                                            "Status" => 6,
                                            "Message" => "Maximum number of entries reached for the Daily Jackpot.",
                                        ];

                                        return json_encode($responsePayload);
                                    }
                                }

                                $betQuery = "INSERT INTO `jackpot_bet` (`isolutions_userid`, `jackpot_event_id`,
                                            `status`, `created`, `modified`, `msisdn`, `channel`)
                                            VALUES('$userId','" . $jEvent['jackpot_event_id'] . "', '0', NOW(), NOW(),
                                            '$mobile', 'SMS')";

                                $jackpotBetId = $this->rawInsert($betQuery);

                                if ($jackpotBetId < 1) {

                                    $responsePayload = [
                                        "Status" => 7,
                                        "Message" => "Unable to place your jackpot at this point. Please try again later.",
                                    ];
                                } else {

                                    $userSelections = $decoded->UserSelections;
                                    $totalGames = $jEvent['total_games'];
                                    $totalSelected = count($userSelections);

                                    if ($totalGames != $totalSelected) {

                                        $responsePayload = [
                                            "Status" => 11,
                                            "Message" => "Invalid number of selections.  $totalGames selections expected. Only $totalSelected were found.",
                                        ];

                                        return json_encode($responsePayload);
                                    }

                                    $jackpotGameIds = array();
                                    $gameIDsDetails = $this->rawQueries("SELECT game_id FROM jackpot_match
                                                 WHERE jackpot_event_id='$jackpotId'");

                                    foreach ($gameIDsDetails as $idDetail) {

                                        array_push($jackpotGameIds, $idDetail['game_id']);
                                    }

                                    $userGameIds = array();

                                    $betSlipSQL = "INSERT INTO jackpot_betslip (`parent_match_id`, `jackpot_bet_id`,
                                                `bet_pick`, `special_bet_value`, `win`, `created`, `modified`, `sub_type_id`,
                                                `status`) VALUES ";

                                    $userSelectionsInNumbersForm = "";
                                    foreach ($userSelections as $selection) {

                                        $gameId = $selection->GameID;

                                        $matchDetails = $this->rawQueries("SELECT * FROM jackpot_match
                                                 WHERE game_id='$gameId' AND jackpot_event_id='$jackpotId'");

                                        if (is_null($matchDetails) || empty($matchDetails)) {

                                            $responsePayload = [
                                                "Status" => 8,
                                                "Message" => "Game ID $gameId is not valid for that Jackpot. Please verify and try again.",
                                            ];

                                            return json_encode($responsePayload);
                                        }

                                        array_push($userGameIds, $gameId);
                                        $matchDetails = $matchDetails[0];

                                        $parent_match_id = $matchDetails['parent_match_id'];
                                        $bet_pick = $selection->Selection;
                                        $sub_type_id = 1;
                                        $special_bet_value = "";

                                        $userSelectionsInNumbersForm = $userSelectionsInNumbersForm . "" . $bet_pick;

                                        $betSlipSQL = $betSlipSQL . " ('$parent_match_id','$jackpotBetId','$bet_pick',
                                                '$special_bet_value','0', NOW(),NOW(),'$sub_type_id', '1'),";
                                    }

                                    if (count($jackpotGameIds) !== count(array_unique($userGameIds))) {

                                        $responsePayload = [
                                            "Status" => 12,
                                            "Message" => "Duplicate game IDs found. Please verify your selections and try again",
                                        ];

                                        return json_encode($responsePayload);
                                    }

                                    $missingGameIds = array_diff($jackpotGameIds, array_unique($userGameIds));

                                    if (!empty($missingGameIds)) {

                                        $responsePayload = [
                                            "Status" => 13,
                                            "Message" => "You have not made selections for all the Jackpot games. Please make selections for Game IDS " . implode(",", $missingGameIds),
                                        ];

                                        return json_encode($responsePayload);
                                    }

                                    $betSlipSQL = rtrim($betSlipSQL, ",");
                                    $slipId = $this->rawInsert($betSlipSQL);

                                    if ($slipId < 1) {

                                        $responsePayload = [
                                            "Status" => 7,
                                            "Message" => "Unable to place your jackpot at this point. Please try again later.",
                                        ];

                                    } else {

                                        $internalTranId = 0;
                                        $internalISQ = "SELECT * FROM `transaction` ORDER BY 1 DESC LIMIT 1";
                                        $tranRS = $this->rawQueries($internalISQ);
                                        $internalTranId = $tranRS[0]['id'];

                                        $runningBalance = $balance - $jEvent['bet_amount'];
                                        $transactionsQuery = "INSERT INTO `transaction` VALUES (NULL, '$userId', 'Jackpot',
                                                    '0', '-$internalTranId', '" . $jEvent['bet_amount'] . "', '$runningBalance',
                                                    NOW(), NULL, NULL, 0, 'LITE-SITE', NOW(), NOW())";

                                        $transactionId = $this->rawInsert($transactionsQuery);

                                        $moneyPayload = [
                                            "Notes" => "Jackpot Bet",
                                            "ThirdPartyReasonCode" => "",
                                            "ThirdPartyTaxesAmount" => 0,
                                            "ProviderID" => 368,
                                            "UserID" => $userId,
                                            "ThirdPartyCurrencyCode" => "KES",
                                            "ThirdPartyAmount" => $jEvent['bet_amount'],
                                            "TransactionID" => $transactionId,
                                            "APIAccount" => $this->jpAccount,
                                            "APIPassword" => $this->qaJpPassword,
                                            "IDBookmaker" => $this->bookmarkerID,
                                        ];

                                        $moneyUrl = $this->qaBaseURL . "/api/thirparties/games/TransferMoneyToProviderNoSession";
                                        $moneyResponse = $this->postToUrl($moneyPayload, $moneyUrl);

                                        if ($moneyResponse->ResultCode == 1) {

                                            $isolTranId = $moneyResponse->ISBetsTransactionId;
                                            $updateQuery = "UPDATE `transaction` SET coupon_id = '" . $jackpotBetId . "',
                                                        `status` = 1, `isolutions_transactionid`='$isolTranId'
                                                        WHERE id = '" . $transactionId . "'";

                                            $this->rawInsert($updateQuery);

                                            $updateBetQuery = "UPDATE `jackpot_bet` SET `status` = 1
                                                        WHERE jackpot_bet_id = '" . $jackpotBetId . "'";

                                            $this->rawInsert($updateBetQuery);

                                            $message = "You successfully placed a " . $jEvent['jackpot_name'] . "! Predictions $userSelectionsInNumbersForm KSH " . $jEvent['bet_amount'] . " has been deducted from your account.";

                                            $responsePayload = [
                                                "Status" => 1,
                                                "Message" => $message,
                                            ];

                                        } else {

                                            $responsePayload = [
                                                "Status" => 7,
                                                "Message" => "Unable to place your jackpot at this point. Please try again later.",
                                            ];
                                        }
                                    }

                                }
                            }
                        }
                    } else {

                        $responsePayload = [
                            "Status" => 3,
                            "Message" => "We could not verify your BETSAFE balance. Please contact customer support",
                        ];
                    }
                } else if ($userInfoResponse->ResultCode == -6 ||
                    $userInfoResponse->ResultCode == -22) {

                    $generatedPwd = substr($mobile, 3, 6);
                    $registrationPayload = [
                        "Username" => $mobile,
                        "Password" => $generatedPwd,
                        "ProfileCode" => "User_Web_(StartupSystems)",
                        "PasswordReport" => "",
                        "FatherID" => -1,
                        "MessageLanguageID" => 2,
                        "TestUser" => 0,
                        "UserDocumentTypeID" => 0,
                        "UserDocumentTypeNumber" => "",
                        "UserInfo" => [
                            "UserID" => 0, "FatherID" => 0, "StateUser" => 0,
                            "Birthdate" => "1900-01-01T00:00:00",
                            "Address" => "", "City" => "", "Country" => 114,
                            "Sex" => "", "Email" => "", "Phone" => "254" . $mobile,
                            "Zip" => "", "Currency" => 31, "UserType" => 0,
                            "TestUser" => 0, "NCell" => "254" . $mobile, "Province" => "",
                            "SecQuestion" => -1, "SecAnswer" => "",
                        ],
                        "CommissionTemplate" => "",
                        "VerifyReceivedFields" => true,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID,
                    ];

                    $regURL = $this->qaBaseURL . "/api/users/RegisterUserWithProfileCode";
                    $regResponse = $this->postToUrl($registrationPayload, $regURL);

                    if ($regResponse->ResultCode == 1) {

                        $responsePayload = [
                            "Status" => 2,
                            "Message" => "Newly Registered Customer. Your web password is $generatedPwd. Deposit via MPESA Paybill 7290099 Account BETSAFE to place Jackpot",
                        ];
                    } else {
                        $responsePayload = [
                            "Status" => -1,
                            "Message" => "Unregistered Customer. Please register first or contact our customer support on 0111040000",
                        ];
                    }
                }

            } else {

                $responsePayload = [
                    "Status" => 403,
                    "Message" => "You are not authorized to use this resource",
                ];
            }
        } else {

            $responsePayload = [
                "Status" => 400,
                "Message" => "Bad Request",
            ];
        }

        return json_encode($responsePayload);
    }
}