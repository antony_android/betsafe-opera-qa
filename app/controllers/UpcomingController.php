<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class UpcomingController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('About us');
    }

    public function indexAction()
    {

        $selected = 'soon';
        $eventsTitle = "Football / Starting Soon";
        $skip = 0;
        $limit = 50;

        $keyword = $this->request->getPost('keyword');

        if ($keyword) {
            $selected = "search";
            $eventsTitle = "Search Results";
        }

        list($today, $total) = $this->getGames(
            $keyword,
            $skip,
            $limit,
            ' AND s.sport_id = 7 ',
            'start_time ASC, home_team ASC'
        );

        $theBetslip = $this->session->get("betslip");
        
        if($this->session->has('selectedSportId')){
            $selectedSportId =  $this->session->get('selectedSportId');
        } else {
            $selectedSportId = 7;
        }
        $navigation = $this->getNavigation($selectedSportId);

        $tab = 'upcoming';

        $this->view->setVars([
            'matches'    => $today,
            'tab'        => $tab,
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip)?count($theBetslip):0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'selected' => $selected,
            'eventsTitle' => $eventsTitle
        ]);

        $this->tag->setTitle('BetSafe - The leading sports Betting Website In Kenya');

        $this->view->pick('index/index');
    }
}
