<?php

use Phalcon\Tag;


/**
 * Class IndexController
 */
class LiveController extends ControllerBase
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {
        $eventsTitle = "Live Football";
        $this->tag->setDoctype(Tag::HTML5);

        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }

        $keyword = $this->request->getPost('keyword');

        list($today, $total) = $this->getLiveGames(
            $keyword,'', 'm_priority desc, priority DESC'
        );

        $total = $total['0']['total'];

        $theBetslip = $this->session->get("betslip");

        $tab = 'highlights';
        $men = 'live';
        $selected = 'live';

        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);

        $this->view->setVars([
            'matches'    => $today,
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'eventsTitle' => $eventsTitle,
            'selected' => $selected,
            'total'      => $total,
            'tab'        => $tab,
            'men'        => $men,
        ]);

        $this->tag->setTitle('BetSafe - Leading sports Betting site In Kenya');
    }
}
