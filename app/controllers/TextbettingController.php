<?php

class TextbettingController extends ControllerBase
{
    public function indexAction()
    {

        $theBetslip = $this->session->get("betslip");
        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', NULL);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => count($theBetslip),
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'sms'
        ]);
    }
}
