<?php

class MatchController extends ControllerBase
{
    public function openAction($id)
    {

        $style = [
            '208' => 'one-row',
            '55' => 'one-row',
            '201' => 'one-row',
            '235' => 'one-row',
            '46' => 'three-rows',
            '270' => 'three-rows',
            '323' => 'three-rows',
            '246' => 'one-row',
            '10' => 'one-row',
            '390' => 'one-row',
            '41' => 'one-row',
            '42' => 'one-row',
        ];

        $matchInfo = $this->rawSelect("SELECT m.home_team, m.game_id, m.away_team,m.start_time,
           c.competition_name,c.category,m.parent_match_id, c.sport_id FROM `match` m INNER JOIN competition c
           ON m.competition_id=c.competition_id WHERE m.match_id=? AND m.start_time > NOW() LIMIT 1", [$id]);

        $eventsTitle = "";
        $title = "";

        if (!is_null($matchInfo) && !empty($matchInfo)) {

            $sportId = $matchInfo[0]['sport_id'];
            $sportDetails = $this->rawQueries("SELECT * FROM sport WHERE
               sport_id='$sportId' LIMIT 1");

            $eventsTitle = $sportDetails[0]['sport_name'];

            $matchInfo = array_shift($matchInfo);

            $title = $matchInfo['home_team'] . " vs " . $matchInfo['away_team'];
        }

        $subTypes = $this->rawSelect("SELECT o.priority, m.match_id, st.is_child,
               REPLACE(REPLACE(e.odd_key, ' ', ' ('), '.5', '.5)') AS display, o.name,
               e.betradar_odd_id, e.odd_key, e.odd_value, e.sub_type_id,
               e.special_bet_value FROM event_odd e INNER JOIN  odd_type o
               ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
               INNER JOIN `match` m  ON m.parent_match_id = e.parent_match_id
               INNER JOIN `sub_type` st  ON st.sub_type_id = e.sub_type_id
               LEFT JOIN odd_key_priority op ON e.odd_key = op.odd_key
               WHERE m.start_time > NOW() AND match_id = '$id'
               AND o.name NOT LIKE '%Match HCP%'
               AND o.live_bet = 0 AND o.active = 1 AND e.odd_key <> '-1'
               ORDER BY st.priority, op.priority ASC");

        $theBetslip = $this->session->get("betslip");

        $this->tag->setTitle($title);

        $navigation = $this->getNavigation();
        $selected = 'home';

        $this->view->setVars([
            'subTypes' => $subTypes,
            'eventsTitle' => $eventsTitle,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'selected' => $selected,
            'matchInfo' => $matchInfo,
            'betslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? sizeof($theBetslip) : 0,
            'style' => $style,
        ]);
    }

    public function indexOldAction()
    {
        $id = $this->request->get('id', 'int');

        $style = [
            '208' => 'one-row',
            '55' => 'one-row',
            '201' => 'one-row',
            '235' => 'one-row',
            '46' => 'three-rows',
            '270' => 'three-rows',
            '323' => 'three-rows',
            '246' => 'one-row',
            '10' => 'one-row',
            '390' => 'one-row',
            '41' => 'one-row',
            '42' => 'one-row',
        ];

        $matchInfo = $this->rawSelect("SELECT m.home_team, m.game_id, m.away_team,m.start_time,
        c.competition_name,c.category,m.parent_match_id, c.sport_id FROM `match` m LEFT JOIN competition c
         ON m.competition_id=c.competition_id WHERE match_id='$id' AND m.start_time > NOW() LIMIT 1");

        $sportId = $matchInfo[0]['sport_id'];
        $sportDetails = $this->rawQueries("SELECT * FROM sport WHERE
            sport_id='$sportId' LIMIT 1");

        $eventsTitle = $sportDetails[0]['sport_name'];

        $matchInfo = array_shift($matchInfo);

        $onex2TempSubTypes = $this->rawSelect("SELECT o.priority, m.match_id, e.betradar_odd_id,
          REPLACE(REPLACE(REPLACE(e.odd_key, 'Home', '1'), 'Draw', 'X'), 'Away', '2') AS display, o.name,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id IN (1,140,34648) AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $onex2SubTypes[0] = $onex2TempSubTypes[2];
        $onex2SubTypes[1] = $onex2TempSubTypes[0];
        $onex2SubTypes[2] = $onex2TempSubTypes[1];

        if (count($onex2SubTypes) >= 6) {

            $onex2SubTypes[3] = $onex2TempSubTypes[5];
            $onex2SubTypes[4] = $onex2TempSubTypes[3];
            $onex2SubTypes[5] = $onex2TempSubTypes[4];
        }

        if (count($onex2SubTypes) == 9) {
            $onex2SubTypes[6] = $onex2TempSubTypes[8];
            $onex2SubTypes[7] = $onex2TempSubTypes[6];
            $onex2SubTypes[8] = $onex2TempSubTypes[7];
        }

        $overUnderSubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
          REPLACE(REPLACE(e.odd_key, ' ', ' ('), '.5', '.5)') AS display, o.name, e.betradar_odd_id,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id IN (563, 145, 143, 167) AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $overUnderHomeSubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
          REPLACE(REPLACE(e.odd_key, ' ', ' ('), '.5', '.5)') AS display, o.name, e.betradar_odd_id,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id IN (1000000, 1000000) AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $overUnderAwaySubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
          REPLACE(REPLACE(e.odd_key, ' ', ' ('), '.5', '.5)') AS display, o.name,e.betradar_odd_id,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id IN (1000000, 1000000) AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $bothTeamsToScoreSubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
          e.odd_key AS display, o.name,e.betradar_odd_id,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 34524 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $bothTeamsToScoreFHSubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
          e.odd_key AS display, o.name, e.betradar_odd_id,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 156 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $bothTeamsToScoreSHSubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
          e.odd_key AS display, o.name, e.betradar_odd_id,
          e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e INNER JOIN
          odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 154 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $doubleChanceSubTypes = $this->rawSelect("SELECT o.priority, m.match_id, e.betradar_odd_id,
           REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(e.odd_key, 'Home', '1'), 'Draw', 'X'), 'Away', '2'),'/', ''), 'GROUPED', '')
           AS display, o.name, e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e
           INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 62 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $doubleChanceFHSubTypes = $this->rawSelect("SELECT o.priority, m.match_id, e.betradar_odd_id,
           REPLACE(REPLACE(REPLACE(REPLACE(e.odd_key, 'Home', '1'), 'Draw', 'X'), 'Away', '2'),'/', '')
           AS display, o.name, e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e
           INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 28 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $doubleChanceSHSubTypes = $this->rawSelect("SELECT o.priority, m.match_id, e.betradar_odd_id,
           REPLACE(REPLACE(REPLACE(REPLACE(e.odd_key, 'Home', '1'), 'Draw', 'X'), 'Away', '2'),'/', '')
           AS display, o.name, e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e
           INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 24 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $exactGoalsNumberSubTypes = $this->rawSelect("SELECT o.priority, m.match_id,
           e.odd_key AS display, e.betradar_odd_id,
           o.name, e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value FROM event_odd e
           INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND e.parent_match_id = o.parent_match_id)
           INNER JOIN `match` m ON m.parent_match_id = e.parent_match_id WHERE m.start_time > NOW()
           AND o.sub_type_id = 110 AND match_id = '$id' AND o.live_bet = 0 AND
           o.active = 1 AND e.odd_key <> '-1' ORDER BY sub_type_id, e.odd_key DESC");

        $theBetslip = $this->session->get("betslip");
        $title = $matchInfo['home_team'] . " vs " . $matchInfo['away_team'];

        $this->tag->setTitle($title);

        $navigation = $this->getNavigation();
        $selected = 'home';

        $this->view->setVars([
            'onex2SubTypes' => $onex2SubTypes,
            'overUnderSubTypes' => $overUnderSubTypes,
            'overUnderHomeSubTypes' => $overUnderHomeSubTypes,
            'overUnderAwaySubTypes' => $overUnderAwaySubTypes,
            'bothTeamsToScoreSubTypes' => $bothTeamsToScoreSubTypes,
            'bothTeamsToScoreFHSubTypes' => $bothTeamsToScoreFHSubTypes,
            'bothTeamsToScoreSHSubTypes' => $bothTeamsToScoreSHSubTypes,
            'doubleChanceSubTypes' => $doubleChanceSubTypes,
            'doubleChanceFHSubTypes' => $doubleChanceFHSubTypes,
            'doubleChanceSHSubTypes' => $doubleChanceSHSubTypes,
            'exactGoalsNumberSubTypes' => $exactGoalsNumberSubTypes,
            'eventsTitle' => $eventsTitle,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'selected' => $selected,
            'matchInfo' => $matchInfo,
            'betslip' => $theBetslip,
            'slipCount' => sizeof($theBetslip),
            'style' => $style,
        ]);
    }
}