<?php

use Phalcon\Tag;

/**
 * Class IndexController
 */
class IndexController extends ControllerBase
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {

        $loggedIn = 'FALSE';

        $this->tag->setDoctype(Tag::HTML5);

        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }
        $limit = $this->request->get('limit', 'int') ?: 40;
        $skip = $page * $limit;

        $sport_id = $this->request->get('sport', 'int');
        $keyword = $this->request->getPost('keyword', 'string');

        if (empty($sport_id)) {
            $sport_id = 7;
            $selected = 'home';
        } else {
            $selected = 'other-sport';
        }

        $this->session->set('selectedSportId', $sport_id);
        $sportDetails = $this->rawQueries("SELECT * FROM sport WHERE
            sport_id='$sport_id' LIMIT 1");

        $eventsTitle = $sportDetails[0]['sport_name'];

        $where = "";
        if ($sport_id) {
            $where = " AND s.sport_id = '$sport_id'";
        }

        if ($keyword) {
            $selected = "search";
            $eventsTitle = "Search Results";
        }

        list($today, $total) = $this->getGames(
            $keyword,
            $skip,
            $limit,
            $where,
            'm_priority ASC, priority DESC, start_time ASC, home_team ASC',
            $sport_id
        );

        $theBetslip = $this->session->get("betslip");

        $men = 'home';

        if ($this->session->has('auth') || $this->session->get('auth') != null) {
            $loggedIn = "TRUE";

            $profileId = $this->session->get('auth')['id'];
            $timeout = $this->rawQueries("SELECT * FROM `gaming_timeout`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            $setTimeout = [];
            $timeRemaining = null;

            if (count($timeout) > 0) {
                $setTimeout = $timeout[0]['timeout_period'];
                $createdAt = $timeout[0]['created'];
                $inSeconds = strtotime($createdAt);
                $timeoutSeconds = 0;

                if ($setTimeout == "24 Hours") {
                    $timeoutSeconds = 24 * 3600;
                } else if ($setTimeout == "48 Hours") {
                    $timeoutSeconds = 24 * 3600 * 2;
                } else if ($setTimeout == "1 Week") {
                    $timeoutSeconds = 24 * 3600 * 7;
                }

                $expectedEndDateInSeconds = $inSeconds + $timeoutSeconds;
                $remainingTimeInSeconds = $expectedEndDateInSeconds - strtotime(date('Y-m-d H:i:s'));
                $timeRemaining = ResponsibleController::fromSeconds($remainingTimeInSeconds);
            }
        }

        $referrer = $this->request->getHTTPReferer();
        $navigation = $this->getNavigation($sport_id);

        if ($this->session->get('auth')) {

            $mobile = $this->session->get('auth')['mobile'];
            $userId = $this->session->get('auth')['id'];

            $registeredUser = $this->session->get("RegisteredUser_" . $mobile);
            $justLoggedInUser = $this->session->get("LoggedInUser_" . $mobile);
            $successfulBetPlacement = $this->session->get("SuccessfulBet_" . $userId);
            $initiatedDeposit = $this->session->get("InitiatedDeposit_" . $userId);
            $initiatedWithdrawal = $this->session->get("InitiatedWithdrawal_" . $userId);

            $this->view->setVars([
                'matches' => $today,
                'theBetslip' => $theBetslip,
                'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'sports' => $navigation['sports'],
                'eventsTitle' => $eventsTitle,
                'registeredNumber' => $registeredUser,
                'justLoggedInUserNumber' => $justLoggedInUser,
                'successfulBetPlacement' => $successfulBetPlacement,
                'initiatedDeposit' => $initiatedDeposit,
                'initiatedWithdrawal' => $initiatedWithdrawal,
                'timeOutPeriodRemaining' => isset($timeRemaining) ? $timeRemaining : null,
                'setTimeout' => isset($setTimeout) ? $setTimeout : null,
                'withdrawalAmount' => $this->session->get('withdrawalAmount'),
                'referrer' => $referrer,
                'loggedIn' => $loggedIn,
                'selected' => $selected,
                'total' => $total,
                'men' => $men,
            ]);

            $this->session->set("RegisteredUser_" . $mobile, null);
            $this->session->set("LoggedInUser_" . $mobile, null);
            $this->session->set("SuccessfulBet_" . $userId, null);
            $this->session->set("InitiatedDeposit_" . $userId, null);
            $this->session->set("InitiatedWithdrawal_" . $userId, null);

            $this->tag->setTitle('BetSafe - Leading sports Betting site In Kenya');
        } else {

            $this->view->setVars([
                'matches' => $today,
                'theBetslip' => $theBetslip,
                'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'sports' => $navigation['sports'],
                'eventsTitle' => $eventsTitle,
                'timeOutPeriodRemaining' => isset($timeRemaining) ? $timeRemaining : null,
                'setTimeout' => isset($setTimeout) ? $setTimeout : null,
                'withdrawalAmount' => $this->session->get('withdrawalAmount'),
                'referrer' => $referrer,
                'loggedIn' => $loggedIn,
                'selected' => $selected,
                'total' => $total,
                'men' => $men,
            ]);

            $this->tag->setTitle('BetSafe - Leading sports Betting site In Kenya');

        }
    }
}