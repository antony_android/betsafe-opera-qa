<?php

class MessagesController extends ControllerBase
{

    public function indexAction()
    {

        if ($this->session->get('auth')) {

            $userId = $this->session->get('auth')['id'];
            $theBetslip = $this->session->get("betslip");
            $selectedSportId =  $this->session->get('selectedSportId');
            $navigation = $this->getNavigation($selectedSportId);
            $this->session->set('withdrawalAmount', NULL);

            $today = date('Y-m-d H:i:s');
            $monthAgo = date_create(date('Y-m-d H:i:s'));
            date_sub($monthAgo, date_interval_create_from_date_string("30 days"));
            $monthAgo = date_format($monthAgo, "Y-m-d H:i:s");

            $payload = [
                "UserID" => $userId,
                "StartDate" => $monthAgo,
                "EndDate" => $today,
                "UnreadOnly" => true,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID
            ];

            $url = $this->qaBaseURL . "/api/users/GetMessagesForUser";
            $response = $this->postToUrl($payload, $url);

            $resultCode = $response->ResultCode;
            $messages = [];

            if ($resultCode == 1) {

                $messages = $response->Messages;

                foreach ($messages as $message) {

                    $bindings = [
                        'messageId' => $message->MessageID,
                        'userId' => $userId,
                        'title' => $message->Title,
                        'messageText' => $message->Message,
                        'messageTime' => $message->Date,
                    ];

                    $query = "INSERT IGNORE  INTO `isolutions_inbox` VALUES 
                     (NULL, :messageId, :userId, :title, :messageText, :messageTime,
                      0, 0, NOW(), NOW(), 'ISOLUTIONS')";

                    $this->rawQueries($query, $bindings);
                }
            }

            $allMessages = $this->rawQueries("SELECT * FROM `isolutions_inbox` 
                WHERE `deleted`= 0 AND isolutions_userid = '$userId' ORDER BY 1 DESC LIMIT 50");

            $formattedMessages = [];

            foreach($allMessages as $message){

                $message['message'] = $this->strip($message['message'], true);
                array_push($formattedMessages, $message);
            }

            $this->view->setVars([
                'theBetslip' => $theBetslip,
                'slipCount'  => !is_null($theBetslip)?count($theBetslip):0,
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'sports' => $navigation['sports'],
                'referrer' => $this->request->getHTTPReferer(),
                'selected' => 'messages',
                'messages' => $formattedMessages
            ]);
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function messageAction($messageId)
    {

        if ($this->session->get('auth')) {

            $theBetslip = $this->session->get("betslip");
            $navigation = $this->getNavigation();
            $this->session->set('withdrawalAmount', NULL);

            $message = $this->rawQueries("SELECT * FROM `isolutions_inbox` 
                WHERE `deleted`= 0 AND `isolutions_messageid` = '$messageId' 
                ORDER BY 1 DESC LIMIT 50");

            if (!is_null($message)) {

                $query = "UPDATE `isolutions_inbox` SET `read` = 1 WHERE 
                `isolutions_messageid` = '$messageId'";

                $this->rawInsert($query);

                $payload = [
                    "UserID" => $this->session->get('auth')['id'],
                    "MessageID" => $messageId,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID
                ];

                $url = $this->qaBaseURL . "/api/users/SetMessageAsReadForUser";
                $this->postToUrl($payload, $url);
            }

            $this->view->setVars([
                'theBetslip' => $theBetslip,
                'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'sports' => $navigation['sports'],
                'referrer' => $this->request->getHTTPReferer(),
                'selected' => 'messages',
                'message' => $message
            ]);
        } else {
            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function delAction()
    {
        $messageId = $this->request->getPost('messageId', 'int');
        $query = "UPDATE `isolutions_inbox` SET `deleted` = 1 WHERE 
                `isolutions_messageid` = '$messageId'";

        $this->rawInsert($query);

        $payload = [
            "UserID" => $this->session->get('auth')['id'],
            "MessageID" => $messageId,
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID
        ];

        $url = $this->qaBaseURL . "/api/users/SetMessageAsReadForUser";
        $this->postToUrl($payload, $url);

        $this->flashSession->success($this->flashMessages('Message deleted successfully'));
        $this->response->redirect('/messages');
    }

    public function parametersAction()
    {

        $payload = [
            "Parameters" => [
                "UserBookmakerID"
            ],
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID
        ];

        $url = $this->qaBaseURL . "/api/common/GetBookmakerParameters";
        $encodedData = json_encode($payload);
        $httpRequest = curl_init($url);

        curl_setopt($httpRequest, CURLOPT_POST, TRUE);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, FALSE);
        $results = curl_exec($httpRequest);
        $decodedResults = json_decode($results);
        curl_close($httpRequest);
        print_r($decodedResults);
        exit;
    }

    private function strip($text,$keepLines=true)
{
        if($keepLines) {
            $text=str_replace(array('</p>','<br/>','<br>'),array("</p> \n","<br/> \n","<br> \n"),$text);
        }
        $text = strip_tags($text,"<style>");

        if(strpos($text,"<style")!==false && strpos($text,"</style>")!==false)
        {
            $substring = substr($text,strpos($text,"<style"),strpos($text,"</style>")+8);
            $text = str_replace($substring,'',$text);
        }

        if(!$keepLines)
        {
            $text = str_replace(array("\t","\r","\n"),"",$text);
            $text = preg_replace('/\s+/',' ',$text);
        }
        else
        {
            $text = str_replace('  ',' ',$text);
        }   
        return trim($text);
    }
}
