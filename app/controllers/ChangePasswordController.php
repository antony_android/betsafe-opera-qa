<?php

class ChangePasswordController extends ControllerBase
{
    public function indexAction()
    {

        $theBetslip = $this->session->get("betslip");
        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', NULL);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip)?count($theBetslip):0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'change-password'
        ]);
    }

    public function codeAction()
    {
        if ($this->session->get('auth')) {
            $mobile = $this->request->getPost('mobile', 'int');

            if (!$mobile) {
                $mobile = $this->session->get("resetPwdMobile");
            }

            if (!$mobile) {

                $this->flashSession->error($this->flashMessages('Please enter your mobile number'));
                $this->response->redirect('change-password');

                $this->view->disable();
            } else {
                $mobile = $this->formatMobileNumber($mobile);
                $this->session->set("resetPwdMobile", $mobile);

                if (!$mobile) {
                    $this->flashSession->error($this->flashMessages('Invalid mobile number'));
                    $this->response->redirect('change-password');
                    $this->view->disable();
                } else {

                    $selected = 'change-password-code';
                    $payload = [
                        "Username" => $mobile,
                        "Email" => "",
                        "PhoneNumber" => "254".$mobile,
                        "VerifyReceivedFields" => true,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID
                    ];

                    $url = $this->qaBaseURL . "/api/users/TokenPasswordRecovery";
                    $response = $this->postToUrl($payload, $url);

                    $resultCode = $response->ResultCode;

                    if ($resultCode == 1) {
                        $verification_code = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 4);

                        $token = $response->Token;
                        $bindings = [
                            'userId' => $this->session->get('auth')['id'],
                            'code' => $verification_code,
                            'token' => $token
                        ];

                        $query = "INSERT  INTO `password_reset` VALUES (NULL, :userId, NULL, :code, 
                        :token, NOW()) ON DUPLICATE KEY UPDATE code='$verification_code', token='$token'";
                        $this->rawQueries($query, $bindings);

                        $sms = "Your BetSafe password reset code is $verification_code. Use this to reset your password.";
                        $smsURL = $this->qaBaseURL . "/api/users/SendSMS";

                        $smsPayload = [
                            "SMSType" => 9,
                            "Text" => $sms,
                            "UserID" => $this->session->get('auth')['id'],
                            "APIAccount" => $this->apiAccount,
                            "APIPassword" => $this->qaApiPassword,
                            "IDBookmaker" => $this->bookmarkerID
                        ];

                        $smsResponse = $this->postToUrl($smsPayload, $smsURL);
                        if ($smsResponse->ResultCode == 1) {
                            $message = "An SMS verification PIN code was sent to " . $mobile . ". Use the PIN to change your password.";

                            $this->view->setVars([
                                'selected' => $selected,
                                'topLeagues' => $this->navigation['topLeagues'],
                                'countries' => $this->navigation['countries'],
                                'sports'    => $this->navigation['sports'],
                                'message' => $message
                            ]);

                            $this->response->redirect('/change-password/inputcode');

                        } else {
                            $this->flashSession->error($this->flashMessages('An error occured and we could not send you a reset code! Please contact support.'));
                            $this->response->redirect('/change-password');
                        }
                    } else {

                        $this->flashSession->error($this->flashMessages('An error occured and we could not send you a reset code! Please contact support.'));
                        $this->response->redirect('/change-password');
                    }
                }
            }
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function inputcodeAction()
    {

        $mobile = $this->session->get("resetPwdMobile");

        if (!$mobile) {

            $this->flashSession->error($this->flashMessages('Please enter your mobile number'));
            $this->response->redirect('change-password');

            $this->view->disable();
        } else {
            $message = "An SMS verification PIN code was sent to " . $mobile . ". Use the PIN to change your password.";
            $selected = 'inputcode';
            $this->view->setVars([
                'selected' => $selected,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports'],
                'message' => $message
            ]);
        }
    }


    public function newAction()
    {
        $resetCode = $this->request->getPost('reset_code', 'int');

        if (!$resetCode) {
            $this->flashSession->error($this->flashMessages('Please enter the PIN code 
            sent to your mobile phone'));
            $this->response->redirect('change-password/code');

            $this->view->disable();
        } else {

            $userId = $this->session->get('auth')['id'];
            $resetCodeRS = $this->rawQueries("SELECT * FROM `password_reset` 
                WHERE `code`= '$resetCode' AND `user_id` = '$userId'");

            if (count($resetCodeRS) == 0) {

                $this->flashSession->error($this->flashMessages('Invalid reset code. Please verify or request a new code!'));
                $this->response->redirect('/change-password/code');
                return;
            }

            $selected = 'change-new-password';

            $this->view->setVars([
                'selected' => $selected,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports'],
                'token' => $resetCodeRS[0]['token']
            ]);
            $this->view->pick("change-password/new");
        }
    }

    public function passwordAction()
    {
        $password = $this->request->getPost('password');
        $repeatPassword = $this->request->getPost('repeatPassword');
        $token = $this->request->getPost('token');

        if (!$password || !$repeatPassword) {
            $this->flashSession->error($this->flashMessages('Please specify a new password and 
            key it again in confirm password'));
            $this->response->redirect("change-password/new");

            $this->view->disable();
        } else {
            if ($password != $repeatPassword) {
                $this->flashSession->error($this->flashMessages('Passwords do not match'));
                $this->response->redirect("change-password/new");
                $this->view->disable();
            } else {

                $payload = [
                    "Token" => $token,
                    "NewPassword" => $password,
                    "VerifyReceivedFields" => true,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID
                ];

                $url = $this->qaBaseURL . "/api/users/ResetPasswordWithToken";
                $response = $this->postToUrl($payload, $url);

                $resultCode = $response->ResultCode;

                if ($resultCode == 1) {
                    $this->flashSession->success($this->flashMessages("Your new Password was reset successfully. Make a deposit to continue betting with us."));
                    $this->response->redirect('/');
                } else {
                    $this->flashSession->error($this->flashMessages('An error occured and your password was not changed. Please contact the Support Team!'));
                    $this->response->redirect('/change-password');
                }
            }
        }
    }
}
