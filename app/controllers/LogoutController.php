<?php

class LogoutController extends ControllerBase
{

    public function indexAction()
    {

        $payload = [
            "Token" => $this->session->get('auth')['token'],
            "UserID" => $this->session->get('auth')['id'],
            "ClientIP" => $_SERVER['REMOTE_ADDR'],
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID
        ];

        $this->session->set('auth', NULL);
        $this->session->set('referred-by-mpesa', NULL);
        $this->cookies->get('auth')->delete();

        $url = $this->qaBaseURL . "/api/users/LogoutUser";
        $this->postToUrl($payload, $url);
        
        $this->response->redirect('');
        $this->view->disable();
    }
}
