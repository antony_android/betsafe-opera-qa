<?php

class TransactionsController extends ControllerBase
{
    public function indexAction()
    {

        if ($this->session->get('auth')) {

            $userId = $this->session->get('auth')['id'];
            $theBetslip = $this->session->get("betslip");
            $selectedSportId =  $this->session->get('selectedSportId');
            $navigation = $this->getNavigation($selectedSportId);
            $this->session->set('withdrawalAmount', NULL);

            $today = date('Y-m-d H:i:s');
            $monthAgo = date_create(date('Y-m-d H:i:s'));
            date_sub($monthAgo, date_interval_create_from_date_string("14 days"));
            $monthAgo = date_format($monthAgo, "Y-m-d H:i:s");

            $payload = [
                "UserID" => $userId,
                "StartDate" => $monthAgo,
                "EndDate" => $today,
                "TransactionID" => -1,
                "LanguageID" => 2,
                "NRecord" => 30,
                "VerifyReceivedFields" => true,
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID
            ];

            $url = $this->qaBaseURL . "/api/users/v2/GetTransactionListV6";
            $response = $this->postToUrl($payload, $url);

            $resultCode = $response->ResultCode;
            $transactions = [];

            if ($resultCode == 1) {

                $transactions = $response->TransactionList;

                foreach ($transactions as $transaction) {

                    $iscredit = 0;
                    $amount = 0;

                    if ($transaction->PositiveImport > 0) {
                        $iscredit = 1;
                        $amount = $transaction->PositiveImport;
                    } else if ($transaction->NegativeImport < 0) {
                        $iscredit = 0;
                        $amount = abs($transaction->NegativeImport);
                    }

                    $dateInserted = strtotime($transaction->Data);
                    $localDate = date('Y-m-d H:i:s', $dateInserted + (3600*3));

                    $bindings = [
                        'transactionId' => $transaction->IDTransaction,
                        'userId' => $userId,
                        'account' => $transaction->TransactionCausal,
                        'amount' => $amount,
                        'iscredit' => $iscredit,
                        'balance' => $transaction->Balance,
                        'transactionTime' => $localDate,
                        'couponId' => $transaction->IDCoupon,
                        'coupon' => $transaction->Coupon
                    ];

                    $query = "INSERT IGNORE  INTO `transaction` VALUES 
                      (NULL, :userId, :account, :iscredit, :transactionId, :amount,
                      :balance, :transactionTime, :couponId, :coupon, 0,'ISOLUTIONS-API', NOW(),
                       NOW())";

                    $this->rawQueries($query, $bindings);
                }
            }

            $transactions = $this->rawQueries("SELECT * FROM `transaction` 
                WHERE isolutions_transactionid > 0 AND isolutions_userid='$userId' 
                AND DATE(transaction_time) >=  CURDATE() - INTERVAL 14 DAY ORDER BY
                 transaction_time DESC");

            $this->view->setVars([
                'theBetslip' => $theBetslip,
                'slipCount'  => count($theBetslip),
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'transactions' => $transactions,
                'sports' => $navigation['sports'],
                'referrer' => $this->request->getHTTPReferer(),
                'selected' => 'transactions'
            ]);
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function showAction($couponId, $coupon)
    {

        if($this->session->get('auth')){

            $couponDetailsPayload = [
                "CouponCode" => $coupon,
                "LanguageID" => 2,
                "UserID" => $this->session->get('auth')['id'],
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID
            ];

            $detailsURL = $this->qaBaseURL . "/api/coupon/CouponDetailV10";
            $couponResponse = $this->postToUrl($couponDetailsPayload, $detailsURL);

            $couponEvents = [];

            if ($couponResponse->ResultCode == 1) {

                $checkCashOutPayload = [
					"UserID" => $this->session->get('auth')['id'],
					"CouponID" => $couponId,
					"VerifyReceivedFields" => true,
					"UserBookmakerID" => $couponResponse->UserBookmakerID,
					"APIAccount" => $this->apiAccount,
					"APIPassword" => $this->qaApiPassword,
					"IDBookmaker" => $this->bookmarkerID
				];

                $cashoutEnabled = FALSE;

				$checkCashoutURL = $this->qaBaseURL . "/api/cashout/CheckCashOutV3";
				$cashoutResponse = $this->postToUrl($checkCashOutPayload, $checkCashoutURL);

				if ($cashoutResponse->ResultCode == 1) {

                    $cashoutEnabled = $cashoutResponse->Enabled;
                }

                $date = $couponResponse->Date;
                $date = str_replace("T", " ", $date);
                $localDate = date('Y-m-d H:i:s', strtotime($date) + (3600*3));

                $couponEvents = $couponResponse->Events;
                $totalGamesInBet = count($couponEvents);

                $myBet = [
                    'isolutions_couponid' => $couponResponse->CouponID, 
                    'coupon_code' => $couponResponse->CouponCode, 
                    'created' => $localDate,
                    'total_odd' => $couponResponse->TotalOdds,
                    'bet_amount' => round($couponResponse->BetDetails[0]->TotalAmount*1.075,2),
                    'total_matches' => $totalGamesInBet,
                    'possible_win' => number_format($couponResponse->MinPotentialWinnings, 2),
                    'user_winnings' => number_format($couponResponse->BetDetails[0]->Winnings, 2),
                    'status' => $couponResponse->CouponStateID,
                    'win' => $couponResponse->CouponOutcomeID,
                    'cashout' => $couponResponse->IsCashout,
                    'cashout_enabled' => $cashoutEnabled,
                    'cashout_amount' => $cashoutResponse->Amount
                ];

                $betDetails = [];

                foreach ($couponEvents as $event) {

                    $game = $event->SubEvent;
                    $game = str_replace(" ", "", $game);
                    $teams = explode("-", $game);
                    $oddTypeCode = $event->OddTypeCode;
                    $userSelection = $oddTypeCode;

                    if(strpos($oddTypeCode, 'UnoXDue_') !== false){

                        $userSelection = substr($oddTypeCode, -1);
                    }

                    $couponDet = [
                        'start_time' => str_replace("T", " ", $event->StartDate),
                        'home_team' => $teams[0],
                        'away_team' => $teams[1],
                        'sport' => $event->Sport,
                        'competition_name' => $event->Event,
                        'outcome_id' => $event->OutcomeID,
                        'outcome' => $event->Outcome,
                        'ft_score' => $event->FinalOutcomeR1.":".$event->FinalOutcomeR2,
                        'odd_value' => $event->Odd,
                        'isol_oddclassid' => $event->OddID,
                        'bet_pick' => $userSelection,
                        'bet_type' => $couponResponse->BetType
                    ];

                    array_push($betDetails, $couponDet);
                }
            }

            $this->session->set("CASHOUT_FROM", "TRANSACTIONS");

            $this->view->setVars([
                "betDetails" => $betDetails,
                'myBet' => $myBet,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports'],
                'selected' => 'transaction-details'
            ]);

        } else {
			$this->flashSession->error($this->flashMessages('Login to access this page'));
			$this->response->redirect('login');
		}
    }
}
