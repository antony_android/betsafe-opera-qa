<?php

class ResponsibleController extends ControllerBase
{

    private $navigation;

    public function initialize()
    {
        $selectedSportId = $this->session->get('selectedSportId');
        $this->navigation = $this->getNavigation($selectedSportId);
    }

    public function indexAction()
    {

        $theBetslip = $this->session->get("betslip");
        $this->session->set('withdrawalAmount', null);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'resp-gaming',
            'selectedRespControl' => 'general',
        ]);
    }

    public function limitsAction()
    {

        $theBetslip = $this->session->get("betslip");
        $this->session->set('withdrawalAmount', null);

        $profileId = $this->session->get('auth')['id'];

        $limit = $this->rawQueries("SELECT daily_limit FROM `gaming_limit`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

        $setLimit = null;

        if (count($limit) > 0) {
            $setLimit = $limit[0]['daily_limit'];
        }

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => count($theBetslip),
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'setLimit' => $setLimit,
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'resp-gaming',
            'selectedRespControl' => 'limits',
        ]);
    }

    public function timeoutAction()
    {

        $theBetslip = $this->session->get("betslip");
        $this->session->set('withdrawalAmount', null);

        $profileId = $this->session->get('auth')['id'];
        $selectedTimeoutPeriod = 'None';

        $timeout = $this->rawQueries("SELECT * FROM `gaming_timeout`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

        $setTimeout = [];
        $timeRemaining = null;

        if (count($timeout) > 0) {
            $setTimeout = $timeout[0]['timeout_period'];
            $createdAt = $timeout[0]['created'];
            $inSeconds = strtotime($createdAt);
            $timeoutSeconds = 0;

            $selectedTimeoutPeriod = $setTimeout;

            if ($setTimeout == "24 Hours") {
                $timeoutSeconds = 24 * 3600;
            } else if ($setTimeout == "48 Hours") {
                $timeoutSeconds = 24 * 3600 * 2;
            } else if ($setTimeout == "1 Week") {
                $timeoutSeconds = 24 * 3600 * 7;
            }

            $expectedEndDateInSeconds = $inSeconds + $timeoutSeconds;
            $remainingTimeInSeconds = $expectedEndDateInSeconds - strtotime(date('Y-m-d H:i:s'));
            $timeRemaining = self::fromSeconds($remainingTimeInSeconds);
        }

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !empty($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'setTimeout' => $setTimeout,
            'timeRemaining' => $timeRemaining,
            'selected' => 'resp-gaming',
            'selectedRespControl' => 'timeout',
            'selectedTimeoutPeriod' => $selectedTimeoutPeriod,
        ]);
    }

    public function exclusionAction()
    {

        $theBetslip = $this->session->get("betslip");
        $this->session->set('withdrawalAmount', null);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => count($theBetslip),
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'resp-gaming',
            'selectedRespControl' => 'exclusion',
        ]);
    }

    public function addlimitAction()
    {
        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];
            $dailyLimit = $this->request->getPost('dailyLimit', 'float');

            $limit = $this->rawQueries("SELECT * FROM `gaming_limit`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            if (count($limit) > 0) {

                $this->flashSession->error($this->flashMessages('You already have an active daily limit set. Please remove it first!'));
                $this->response->redirect('/responsible/limits');
            } else {

                $bindings = [
                    'profileId' => $profileId,
                    'dailyLimit' => $dailyLimit,
                ];

                $query = "INSERT  INTO `gaming_limit` VALUES
                (NULL, :profileId, :dailyLimit, 1, NOW(), NOW())";

                $this->rawQueries($query, $bindings);
                $this->flashSession->success($this->flashMessages('Daily limit set successfully'));
                $this->response->redirect('/responsible/limits');
            }
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function addtimeoutAction()
    {
        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];
            $timeoutPeriod = $this->request->getPost('timeoutPeriod');

            $limit = $this->rawQueries("SELECT * FROM `gaming_timeout`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            if (count($limit) > 0) {

                $this->flashSession->error($this->flashMessages('You already have an active time-out period set. Please remove it first!'));
                $this->response->redirect('/responsible/timeout');
            } else {

                $bindings = [
                    'profileId' => $profileId,
                    'timeoutPeriod' => $timeoutPeriod,
                ];

                $query = "INSERT  INTO `gaming_timeout` VALUES
                (NULL, :profileId, :timeoutPeriod, 1, NOW(), NOW())";

                $this->rawQueries($query, $bindings);
                $this->flashSession->success($this->flashMessages('Time-out period set successfully'));
                $this->response->redirect('/responsible/timeout');
            }
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function removelimitAction()
    {
        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];

            $limit = $this->rawQueries("SELECT * FROM `gaming_limit`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            if (count($limit) == 0) {

                $this->flashSession->error($this->flashMessages('You do not have an active daily limit set!'));
                $this->response->redirect('/responsible/limits');
            } else {

                $bindings = [
                    'profileId' => $profileId,
                ];

                $query = "UPDATE gaming_limit SET `status` = 0 WHERE profile_id = :profileId AND `status` = 1";

                $this->rawQueries($query, $bindings);
                $this->flashSession->success($this->flashMessages('Daily limit removed successfully'));
                $this->response->redirect('/responsible/limits');
            }
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function filterAction()
    {

        $selection = $this->request->getPost('selection');
        if ($selection == "general") {
            $selection = "";
        }

        $this->session->set('selected-responsible', $selection);
        $this->response->redirect('/responsible/' . $selection);
    }

    public static function fromSeconds($seconds)
    {
        $interval = date_diff(date_create("@0"), date_create("@$seconds"));

        foreach (array('y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second') as $format => $desc) {
            if ($interval->$format >= 1) {
                $thetime[] = $interval->$format . ($interval->$format == 1 ? " $desc" : " {$desc}s");
            }

        }

        return isset($thetime) ? implode(' ', $thetime) . ($interval->invert ? ' ago' : '') : null;
    }
}