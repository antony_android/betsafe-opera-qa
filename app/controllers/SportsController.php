<?php

/**
 * Class SportsController
 */
class SportsController extends ControllerBase
{
    /**
     *
     */
    public function IndexAction()
    {
        $sports = $this->redisCache->get('sports');
        if (empty($sports)) {
            $sports =  $this->rawSelect(
                "SELECT sport.* FROM sport WHERE sport_id IN "
                    . " (SELECT distinct sport_id FROM ux_categories WHERE games_count > 0);"
            );
            $this->redisCache->set('sports', $sports, 7200);
        }

        $this->view->setVars([
            'sports'    => $sports,
            'men'       => 'sports',
        ]);
    }

    /**
     *
     */
    public function threewayAction()
    {
        $id = $this->request->get('id', 'int');

        $matches = $this->rawSelect("SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
         FROM event_odd e INNER JOIN odd_type o ON o.sub_type_id = e.sub_type_id WHERE 
         parent_match_id = m.parent_match_id AND o.active = 1) AS side_bets, o.sub_type_id,
          MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, 
          MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS neutral_odd, 
          MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd, 
          m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, o.betradar_odd_id,
          m.parent_match_id,c.competition_name,c.category FROM `match` m INNER JOIN event_odd o 
          ON m.parent_match_id = o.parent_match_id INNER JOIN competition c ON c.competition_id
           = m.competition_id INNER JOIN sport s ON s.sport_id = c.sport_id WHERE 
           c.competition_id='$id' AND m.start_time > NOW() AND o.sub_type_id = 10 AND 
           m.status <> 3 GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, 
           m.start_time LIMIT 60");

        $theCompetition = $this->rawSelect("SELECT competition_name,competition_id,category,sport_id
         FROM competition WHERE competition_id='$id' LIMIT 1");

        $sport_id = $theCompetition['0']['sport_id'];

        $sport = $this->rawSelect("SELECT sport_name FROM sport WHERE sport_id='$sport_id' LIMIT 1");
        $sport = $sport['0']['sport_name'];

        $theBetslip = $this->session->get("betslip");

        $title = $sport . ' > ' . $theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];

        $theCompetition = $theCompetition['0'];

        $pages = 0;

        $this->tag->setTitle($title);

        $this->view->setVars([
            'matches' => $matches,
            'title'   => $title,
            'pages'   => $pages,
        ]);

        $this->view->pick("sports/threeway");
    }

    /**
     *
     */
    public function twowayAction()
    {
        $id = $this->request->get('id', 'int');
        $matches = $this->rawSelect("SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id) FROM
         event_odd e INNER JOIN odd_type o ON o.sub_type_id = e.sub_type_id WHERE parent_match_id
          = m.parent_match_id AND o.active = 1) AS side_bets, o.sub_type_id, 
          MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, 
          MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS neutral_odd, 
          MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd, 
          m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, o.betradar_odd_id,
          m.parent_match_id,c.competition_name,c.category FROM `match` m INNER JOIN event_odd o 
          ON m.parent_match_id = o.parent_match_id INNER JOIN competition c ON c.competition_id 
          = m.competition_id INNER JOIN sport s ON s.sport_id = c.sport_id WHERE 
          c.competition_id='$id' AND m.start_time > NOW() AND o.sub_type_id = 20 AND m.status <> 3 
          GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC , m.start_time 
          LIMIT 60");

        $theCompetition = $this->rawSelect("SELECT competition_name,competition_id,category,
        sport_id FROM competition WHERE competition_id='$id' LIMIT 1");

        $sport_id = $theCompetition['0']['sport_id'];

        $sport = $this->rawSelect("select sport_name from sport where sport_id='$sport_id' limit 1");
        $sport = $sport['0']['sport_name'];

        $theBetslip = $this->session->get("betslip");

        $title = $sport . ' > ' . $theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];

        $this->tag->setTitle($title);

        $this->view->setVars([
            'matches' => $matches,
            'title'   => $title,
        ]);

        $this->view->pick("sports/twoway");
    }

    /**
     *
     */
    public function upcomingAction()
    {
        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }
        $limit = $this->request->get('limit', 'int') ?: 60;
        $skip = $page * $limit;

        $keyword = $this->request->getPost('keyword');

        list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit);


        $total = $total['0']['total'];

        $pages = ceil($total / $limit);

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            'matches'       => $today,
            'theBetslip'    => $theBetslip,
            'total'         => $total,
            'pages'         => $pages > 14 ? 14 : $pages,
            'page'          => $page,
        ]);

        $this->tag->setTitle('BetSafe - Fast Play, Fast Pay (Rahisi kama Kupuliza Kipenga)');
    }
}
