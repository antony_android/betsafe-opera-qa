<?php

class PromotionsController extends ControllerBase
{
    public function indexAction()
    {

        $theBetslip = $this->session->get("betslip");
        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', NULL);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'promotions'
        ]);
    }

    public function detailAction()
    {

        $theBetslip = $this->session->get("betslip");
        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', NULL);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'promotions'
        ]);
    }
}
