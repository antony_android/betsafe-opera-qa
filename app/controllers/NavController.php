<?php

class NavController extends ControllerBase
{
    public function indexAction()
    {

        $slipCount = 0;
        $theBetslip = $this->session->get("betslip");
        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', NULL);

        if(!is_null($theBetslip)){

            $slipCount = !is_null($theBetslip)?count($theBetslip):0;
        }

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => $slipCount,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'nav'
        ]);
    }

    public function rightAction()
    {

        $theBetslip = $this->session->get("betslip");
        $selectedSportId =  $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', NULL);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'profile-nav'
        ]);
    }

}
