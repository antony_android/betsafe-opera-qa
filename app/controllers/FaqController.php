<?php

class FaqController extends ControllerBase
{
    public function indexAction()
    {

        $theBetslip = $this->session->get("betslip");
        $navigation = $this->getNavigation();
        $this->session->set('withdrawalAmount', NULL);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => count($theBetslip),
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'faq'
        ]);
    }
}
