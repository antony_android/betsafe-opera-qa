<?php

 /** 
  * Class FootballController
  */
class TopLeaguesController extends ControllerBase
{
     /**
      *
      */
     public function IndexAction()
     {
         $id = $this->request->get('id', 'int');
         $c = $this->request->get('c', 'int') ?: 0;
         $id = $id ?: 79;


         $top_competitions = $this->redisCache->get('top-leagues-'.$id.$c);
         if(empty($top_competitions)){
             $cat_filter = $c != 0 ? " and category.category_id=$c": "";
             $limit = $c != 0 ? ""  : " limit 20 " ;
             $query =  "SELECT competition.competition_id, "
                 . " category.category_name as country, competition.competition_name,"
                 . " category.country_code, count(*) AS games_count, s.sport_name FROM competition "
                 . " INNER JOIN category ON category.category_id = competition.category_id "
                 . " INNER JOIN `match` ON `match`.competition_id = competition.competition_id "
                 . " INNER JOIN sport s on s.sport_id = competition.sport_id "
                 . " WHERE competition.sport_id = :id $cat_filter AND `match`.start_time > now() "
                 . " GROUP BY competition.competition_id having games_count > 0 "
                 . " ORDER BY competition.priority  DESC $limit ";

             $top_competitions = $this->rawSelect($query, ['id' => $id]);

             $this->redisCache->set('top-leagues-'.$id, $top_competitions, 7200);

         }

         $this->view->setVars([
             'men'          => 'top-leagues',
             'sport_name' => $top_competitions[0]['sport_name'] ,
             'category' => $c != 0 ? $top_competitions[0]['country'] : "Top",
             'top_competitions' => $top_competitions,
         ]);
     }

}

