<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class AboutController extends ControllerBase
{

    /**
     * Index
     */
    public function indexAction()
    {
        $this->tag->setTitle('About BetSafe');
        $this->view->pick("about/index");

        $this->view->setVars([
            'data' => [
                'pageTitle'      => 'About BetSafe sports betting',
                'pageKeywords'      => 'Football bet, Soccer bet, bet Kenya',
                'pageDescription'      => 'About BetSafe sports betting'
            ]
        ]);
    }
}
