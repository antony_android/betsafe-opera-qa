<?php

/**
 * Created by
 * User: Antonio
 * Date: 03/02/2021
 * Time: 15:32
 */
class CompetitionController extends ControllerBase
{

    /**
     *
     */
    public function openAction($id)
    {
        $selectedSportId =  $this->session->get('selectedSportId');

        $matches = $this->rawQueries("SELECT c.priority, 
        (SELECT 
            count(DISTINCT e.sub_type_id) FROM event_odd e INNER JOIN odd_type o ON 
            (o.sub_type_id = e.sub_type_id AND o.parent_match_id = e.parent_match_id) 
            WHERE e.parent_match_id = m.parent_match_id AND o.active = 1) 
            AS side_bets, o.sub_type_id, 
            MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, 
            MAX(CASE WHEN o.odd_key = 'X' THEN odd_value END) AS neutral_odd, 
            MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd,
            MAX(CASE WHEN o.odd_key = '1' THEN betradar_odd_id END) AS home_odd_id,
            MAX(CASE WHEN o.odd_key = 'X' THEN betradar_odd_id END) AS neutral_odd_id,
            MAX(CASE WHEN o.odd_key = '2' THEN betradar_odd_id END) AS away_odd_id,  
            m.game_id, m.match_id, m.start_time, m.away_team, m.home_team,
            m.parent_match_id,c.competition_name,c.category, o.betradar_odd_id
            FROM `match` m INNER JOIN event_odd o 
            ON m.parent_match_id = o.parent_match_id INNER JOIN competition c 
            ON c.competition_id = m.competition_id INNER JOIN sport s 
            ON s.sport_id = c.sport_id 
            INNER JOIN odd_type ot ON (ot.sub_type_id = o.sub_type_id AND 
            o.parent_match_id = ot.parent_match_id)
            WHERE c.competition_id=? AND m.start_time > NOW() 
            AND o.sub_type_id IN (1, 3916, 3925, 34430) AND o.odd_value IS NOT NULL and ot.active = 1
            AND m.status <> 3 GROUP BY m.parent_match_id ORDER BY m.priority ASC, 
            c.priority ASC , m.start_time, m.home_team ASC", [
            $id,
        ]);

        $theCompetition = $this->rawQueries("SELECT competition_name,competition_id,category, sport_name
         FROM competition INNER JOIN sport USING(sport_id) WHERE competition_id=? LIMIT 1", [$id]);

        $theBetslip = $this->session->get("betslip");
        $competitionName = "";
        $sportName = "";

        $title = "";

        if(!is_null($theCompetition) && !empty($theCompetition)){

            $title = $theCompetition[0]['competition_name'] . ", " . $theCompetition[0]['category'];
            $competitionName = $theCompetition[0]['competition_name'];
            $sportName = $theCompetition[0]['sport_name'];
        }

        $this->tag->setTitle($title);

        $men = 'home';
        $selected = $id;

        $navigation = $this->getNavigation($selectedSportId);

        $this->view->setVars([
            'matches'    => $matches,
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip)?count($theBetslip):0,
            'competitionName' => $competitionName,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'eventsTitle' => $sportName,
            'sports' => $navigation['sports'],
            'selected' => $selected,
            'men'        => $men,
        ]);

        $this->view->pick('index/index');
    }
}
