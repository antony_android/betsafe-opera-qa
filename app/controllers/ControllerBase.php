<?php

use Phalcon\Crypt;
use Phalcon\Mvc\Controller;
use \Firebase\JWT\JWT;

class ControllerBase extends Controller
{

    protected $baseURL = "https://apirest.betsafe.co.ke";
    protected $qaBaseURL = "https://apirest.qa.betsafe.co.ke";
    protected $moneyBaseURL = "https://cashier.betsafe.co.ke";
    protected $qaMoneyBaseURL = "https://cashier.qa.betsafe.co.ke";
    protected $apiAccount = "StartupSystems";
    protected $jpAccount = "LiteJackpot";
    protected $apiPassword = "Q5Gfk?11eE4Y%9Lx";
    protected $qaApiPassword = "xL9%Y4Ee11?kfG5Q";
    protected $qaJpPassword = "Yw1n8?gHx33p0=6p";
    protected $jpPassword = "Aab0&nK3R!FF_b%4";
    protected $bookmarkerID = 172;
    const QA_TOKEN_ACCESS_TOKEN = "iHB+H1Xzn9IzKx2ukLPDzDOioF0rxAwWDmMvx4SAC0Y=";
    const TOKEN_ACCESS_TOKEN = "08f530d48831a8a30242ac120002";

    public function beforeExecuteRoute($dispatcher)
    {
        if ($this->cookies->has('auth')) {
            $token = $this->cookies->get('auth');
            $key = "5eBOGiKXt7dsKwaaJcRX8owIH7BbJ8F9";
            if (!$this->session->has("auth")) {
                try {
                    $user = JWT::decode($token, $key, array('HS256'));
                    $user = $user->user;
                    if ($user->device == '1') {
                        $user = [
                            'id' => $user->id,
                            'sessionId' => $user->sessionId,
                            'balance' => $user->balance,
                            'username' => $user->username,
                            'phone' => $user->phone,
                            'bonus' => $user->bonus,
                            'token' => $user->token,
                            'mobile' => $user->mobile,
                            'device' => $user->device,
                        ];
                        $this->_registerSession($user);
                    }
                } catch (Exception $e) {
                    $decoded = $e->getMessage();
                }
            }
        }

        $sportType = [
            '79' => 'competition',
            '85' => 'twoway',
            '97' => 'competition',
            '84' => 'twoway',
            '82' => 'competition',
            '83' => 'competition',
            '86' => 'competition',
            '96' => 'competition',
            '98' => 'twoway',
            '99' => 'competition',
            '100' => 'competition',
            '102' => 'competition',
            '104' => 'competition',
            '105' => 'competition',
            '106' => 'competition',
            '107' => 'competition',
            '108' => 'competition',
            '109' => 'competition',
            '162' => 'competition',
            '163' => 'competition',
            '164' => 'competition',
            '165' => 'twoway',
            '167' => 'competition',
            '168' => 'competition',
            '80' => 'twoway',
            '239' => 'competition',
        ];

        $slipCount = 0;
        $betslip = $this->session->get('betslip');
        if (!is_null($betslip)) {
            $slipCount = sizeof($betslip);
        }

        $jackpotSlip = 0;

        $sports = $this->sports();

        $this->view->setVars([
            'slipCount' => $slipCount, 'sportType' => $sportType,
            'betslip' => $betslip, 'jackpotSlip' => $jackpotSlip,
            'sports' => $sports, 'notifications' => $this->getNotificationsCount(),
            'freebetscount' => $this->getFreeBetsCount(),
        ]);
    }

    /**
     * @return mixed
     */
    protected function sports()
    {
        return $this->rawQueries("SELECT ux.ux_id, ux.sport_name, IF(LENGTH(ux.category) <17,
           ux.category, concat(SUBSTRING(ux.category, 1,15), '...')) AS category,
           IF(LENGTH(ux.competition_name) <17, ux.competition_name,
           concat(SUBSTRING(ux.competition_name, 1,15), '...')) AS competition_name, ux.sport_id,
           ux.category_id, ux.competition_id, country_code, (SELECT count(*) FROM `match` m
           WHERE competition_id = ux.competition_id AND m.bet_closure > NOW() AND m.status=1) AS games,
           (SELECT count(*) FROM `match` mm WHERE competition_id IN (SELECT competition_id FROM
           competition cc WHERE category_id=ux.category_id AND mm.bet_closure > NOW() AND mm.status=1 ))
            AS cat_games FROM ux_categories ux INNER JOIN category c ON c.category_id = ux.category_id
             HAVING games > 0 ORDER BY ux.sport_id ASC, ux.category ASC");
    }

    protected function getMicrotime()
    {
        list($msec, $sec) = explode(" ", microtime());
        return ((float) $msec + (float) $sec);
    }

    protected function topLeagues()
    {
        return $this->rawQueries('SELECT c.competition_id, c.competition_name, c.category FROM
            competition c INNER JOIN `match` m ON m.competition_id = c.competition_id WHERE
            c.sport_id = 7 AND m.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 DAY)
            GROUP BY m.competition_id ORDER BY c.priority DESC , c.competition_name LIMIT 10');
    }

    protected function allLeagues()
    {
        return $this->rawQueries('SELECT * FROM ux_categories WHERE sport_id = 7');
    }

    protected function rugby()
    {
        return $this->rawQueries('SELECT category,competition_id, competition_name FROM
        competition WHERE sport_id=149 GROUP BY category ORDER BY category ASC');
    }

    /**
     * @param $statement
     *
     * @param array $bindings
     *
     * @return mixed
     */
    protected function rawQueries($statement, $bindings = [])
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement, $bindings);
        $success->setFetchMode(Phalcon\Db\Enum::FETCH_ASSOC);
        $success = $success->fetchAll($success);

        return $success;
    }

    protected function rawInsert($statement)
    {
        $connection = $this->di->getShared("db");
        $connection->query($statement);
        return $connection->lastInsertId();
    }

    protected function flashMessages($message)
    {
        return $message;
    }

    protected function flashSuccess($message)
    {
        return $message;
    }

    protected function reference()
    {

        if (!$this->cookies->has('referenceID')) {

            $crypt = new Crypt();

            $key = 'FbxH8j7SPeeVDE7i';
            $text = $_SERVER['HTTP_USER_AGENT'] . time() . uniqid();

            $key = $crypt->encryptBase64($text, $key);

            $this->cookies->set('referenceID', $key, time() + 15 * 86400);
        }

        $referenceID = $this->cookies->get('referenceID');

        $referenceID = $referenceID->getValue();

        return $referenceID;
    }

    protected function registerAuth($user)
    {
        $exp = time() + (3600 * 24 * 5);
        $token = $this->generateToken($user, $exp);
        $this->cookies->set('auth', $token, $exp);
        $this->_registerSession($user);
    }

    private function _registerSession($user)
    {
        $this->session->set('auth', $user);
    }

    protected function generateToken($data, $exp)
    {
        $key = "5eBOGiKXt7dsKwaaJcRX8owIH7BbJ8F9";
        $token = array(
            "iss" => "https://betsafe.co.ke",
            "iat" => time(),
            "nbf" => time() + 7200,
            "exp" => $exp,
            "user" => $data,
        );

        return JWT::encode($token, $key);
    }

    protected function deleteReference()
    {

        $this->cookies->set('referenceID');
    }

    protected function getDevice()
    {
        $device = '2';
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $device = '1';
        }
        return $device;
    }

    protected function topupThirdParty($providerId, $payload)
    {

        $url = $this->qaMoneyBaseURL . "/api/Deposit/" . $providerId;

        return $this->postToUrl($payload, $url);
    }

    protected function withdrawThirdParty($providerId, $payload)
    {

        $url = $this->qaMoneyBaseURL . "/api/Withdraw/" . $providerId;

        return $this->postToUrl($payload, $url);
    }

    public function getNotificationsCount()
    {

        if (!is_null($this->session->get('auth'))) {

            $payload = [
                "UserID" => $this->session->get('auth')['id'],
                "VerifyReceivedFields" => true,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/users/GetNotificationsCount";
            $response = $this->postToUrl($payload, $url);

            if ($response->ResultCode == 1) {
                return $response->NotificationCount;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getFreeBetsCount()
    {

        if (!is_null($this->session->get('auth'))) {

            $payload = [
                "UserID" => $this->session->get('auth')['id'],
                "ApplicationType" => 1,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/bonuscampaigns/GetFreebetsByUser";
            $response = $this->postToUrl($payload, $url);

            if ($response->ResultCode == 1) {

                $freebets = $response->Freebets;
                return count($freebets);
            }
        } else {
            return 0;
        }
        return 0;
    }

    protected function formatMobileNumber($number)
    {

        $regex = '/^(?:\+?(?:[1-9]{3})|0)?([1,7][0-9]{8})$/';

        if (preg_match_all($regex, $number, $capture)) {
            $msisdn = $capture[1][0];
        } else {
            $msisdn = false;
        }

        return $msisdn;
    }

    protected function sendZimobiSMS($sms, $mobile)
    {

        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, 'http://178.18.247.235:8084/BetSafe/SMS_API?mobile=' . $mobile . '&text=' . urlencode($sms));
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($cURLConnection);
        curl_close($cURLConnection);
    }

    public function postToUrl($data, $url)
    {
        $encodedData = json_encode($data);
        $httpRequest = curl_init($url);

        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);
        $results = curl_exec($httpRequest);
        $decodedResults = json_decode($results);
        curl_close($httpRequest);

        return $decodedResults;
    }

    public function postToUrlEncoded($data, $url)
    {
        $encodedData = json_encode($data);
        $httpRequest = curl_init($url);

        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);
        $results = curl_exec($httpRequest);
        curl_close($httpRequest);
        return $results;
    }

    protected function generateMoneyAccessToken()
    {

        $url = $this->qaMoneyBaseURL . "/api/GenerateToken";
        $payload = [
            "clientToken" => ControllerBase::TOKEN_ACCESS_TOKEN,
        ];

        $response = $this->postToUrl($payload, $url);
        $resultCode = $response->resultCode;
        $accessToken = null;

        if ($resultCode == 1) {
            $accessToken = $response->accessToken;
        }

        return $accessToken;
    }

    /**
     * @param $keyword
     * @param $skip
     * @param $limit
     *
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */

    protected function getGames($keyword, $skip, $limit, $filter = "",
        $orderBy = "start_time ASC", $sportId = 7) {
        $search_where = "WHERE start_time >= NOW() AND DATE(start_time) <= DATE(NOW() + INTERVAL 2 MONTH) " . $filter;
        $where = "WHERE start_time >= NOW() AND DATE(start_time) <= DATE(NOW() + INTERVAL 2 MONTH)
            AND home_odd IS NOT NULL " . $filter;

        $items = [];

        if ($keyword) {

            $bindings = [
                'keyword' => "%$keyword%",
            ];

            $whereClause = "$search_where AND (game_id LIKE :keyword OR home_team LIKE :keyword OR away_team LIKE :keyword OR competition_name LIKE :keyword)";
            $query = "SELECT c.priority, (SELECT
                        COUNT(DISTINCT e.sub_type_id) FROM event_odd e INNER JOIN odd_type o ON
                        (o.sub_type_id = e.sub_type_id AND o.parent_match_id = e.parent_match_id)
                        WHERE e.parent_match_id = m.parent_match_id)
                        AS side_bets, o.sub_type_id,
                        MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd,
                        MAX(CASE WHEN o.odd_key = 'X' THEN odd_value END) AS neutral_odd,
                        MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd,
                        MAX(CASE WHEN o.odd_key = '1' THEN betradar_odd_id END) AS home_odd_id,
                        MAX(CASE WHEN o.odd_key = 'X' THEN betradar_odd_id END) AS neutral_odd_id,
                        MAX(CASE WHEN o.odd_key = '2' THEN betradar_odd_id END) AS away_odd_id,
                         m.game_id, m.match_id, m.start_time, m.away_team, m.home_team,
                         m.parent_match_id,c.competition_name,c.category
                        FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                        INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s
                        ON s.sport_id = c.sport_id $whereClause  AND o.sub_type_id IN (1, 3916, 3925, 34430)
                        AND o.odd_value IS NOT NULL AND m.status <> 3 GROUP BY m.parent_match_id
                         ORDER BY m.priority ASC, c.priority DESC, m.start_time, m.home_team ASC LIMIT $skip,$limit";

            $items = $this->rawQueries("SELECT COUNT(*) AS total FROM `match` m INNER JOIN
                event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c
                ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id
                = c.sport_id $whereClause LIMIT 1", $bindings);

            $today = $this->rawQueries($query, $bindings);

        } else if ($sportId != 7) {

            $today = $this->rawQueries("SELECT c.priority,
                (SELECT COUNT(DISTINCT e.sub_type_id) FROM event_odd e INNER JOIN odd_type o ON
                (o.sub_type_id = e.sub_type_id AND o.parent_match_id = e.parent_match_id)
                WHERE e.parent_match_id = m.parent_match_id AND o.active = 1)
                AS side_bets, o.sub_type_id,
                MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd,
                MAX(CASE WHEN o.odd_key = 'X' THEN odd_value END) AS neutral_odd,
                MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd,
                MAX(CASE WHEN o.odd_key = '1' THEN betradar_odd_id END) AS home_odd_id,
                MAX(CASE WHEN o.odd_key = 'X' THEN betradar_odd_id END) AS neutral_odd_id,
                MAX(CASE WHEN o.odd_key = '2' THEN betradar_odd_id END) AS away_odd_id,
                m.game_id, m.match_id, m.start_time, m.away_team, m.home_team,
                m.parent_match_id,c.competition_name,c.category, o.betradar_odd_id
                FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN
                sport s ON s.sport_id = c.sport_id INNER JOIN odd_type ot ON
                (ot.sub_type_id = o.sub_type_id AND o.parent_match_id = ot.parent_match_id)
                WHERE s.sport_id='$sportId' AND m.start_time > NOW() AND o.sub_type_id
                IN (1, 3916, 3925, 34430) AND o.odd_value IS NOT NULL AND ot.active = 1
                AND m.status <> 3 GROUP BY m.parent_match_id ORDER BY m.priority ASC,
                c.priority DESC, m.start_time, m.home_team ASC");
        } else {
            $whereClause = "$where ";

            $today = $this->rawQueries("SELECT * FROM ux_todays_highlights s $whereClause
                AND s.m_priority >= 0 AND s.home_odd IS NOT NULL ORDER BY $orderBy
                LIMIT 50");

            $items = $this->rawQueries("SELECT count(*) AS total FROM
                ux_todays_highlights s $whereClause LIMIT 1");
        }

        return [
            $today,
            $items,
        ];
    }

    protected function getLiveGames($keyword, $filter = "", $orderBy = "start_time ASC")
    {
        $search_where = "WHERE 1=1 " . $filter;
        $where = "WHERE odd_value IS NOT NULL AND odd_value > 0 " . $filter;

        if ($keyword) {

            $bindings = [
                'keyword' => "%$keyword%",
            ];

            $whereClause = "$search_where AND (home_team LIKE :keyword OR away_team LIKE :keyword OR competition_name LIKE :keyword)";
            $query = "SELECT m.sport_name, c_priority, s_priority, (SELECT
                        COUNT(DISTINCT e.odd_key) FROM live_odds e
                        WHERE e.parent_match_id = m.parent_match_id  AND e.odd_active =1)
                        AS side_bets, o.sub_type_id, o.betradar_odd_id,
                        MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd,
                        MAX(CASE WHEN o.odd_key = 'X' THEN odd_value END) AS neutral_odd,
                        MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd,
                        MAX(CASE WHEN o.odd_key = '1' THEN betradar_odd_id END) AS home_odd_id,
                        MAX(CASE WHEN o.odd_key = 'X' THEN betradar_odd_id END) AS neutral_odd_id,
                        MAX(CASE WHEN o.odd_key = '2' THEN betradar_odd_id END) AS away_odd_id,
                        MAX(CASE WHEN o.odd_key = '1' THEN odd_active END) AS home_odd_active,
                        MAX(CASE WHEN o.odd_key = 'X' THEN odd_active END) AS neutral_odd_active,
                        MAX(CASE WHEN o.odd_key = '2' THEN odd_active END) AS away_odd_active,
                         m.game_id, m.match_id, m.start_time, m.away_team, m.home_team,
                         m.parent_match_id, m.competition_name, m.score FROM `live_match` m
                         INNER JOIN live_odds o ON m.parent_match_id = o.parent_match_id
                         $whereClause  AND o.market_code IN ('3way_UnoXDue', 'UnoDue',
                          'FightOutright_UnoXDue', 'Whichteamwillwinthematch_UnoDue')
                         AND m.status = 1  AND o.market_active=1 GROUP BY m.parent_match_id
                         ORDER BY s_priority ASC, c_priority ASC, m.start_time LIMIT 100";

            $items = $this->rawQueries("SELECT COUNT(*) AS total FROM `live_match` m INNER JOIN
                live_odds o ON m.parent_match_id = o.parent_match_id $whereClause LIMIT 1",
                $bindings);

            $today = $this->rawQueries($query, $bindings);

        } else {
            $whereClause = "$where ";

            $today = $this->rawQueries("SELECT m.sport_name, c_priority, s_priority, (SELECT
                    COUNT(DISTINCT e.odd_key) FROM live_odds e
                    WHERE e.parent_match_id = m.parent_match_id AND e.odd_active =1)
                    AS side_bets, o.sub_type_id, o.betradar_odd_id,
                    MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd,
                    MAX(CASE WHEN o.odd_key = 'X' THEN odd_value END) AS neutral_odd,
                    MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd,
                    MAX(CASE WHEN o.odd_key = '1' THEN betradar_odd_id END) AS home_odd_id,
                    MAX(CASE WHEN o.odd_key = 'X' THEN betradar_odd_id END) AS neutral_odd_id,
                    MAX(CASE WHEN o.odd_key = '2' THEN betradar_odd_id END) AS away_odd_id,
                    MAX(CASE WHEN o.odd_key = '1' THEN o.odd_active END) AS home_odd_active,
                    MAX(CASE WHEN o.odd_key = 'X' THEN o.odd_active END) AS neutral_odd_active,
                    MAX(CASE WHEN o.odd_key = '2' THEN o.odd_active END) AS away_odd_active,
                    m.game_id, m.match_id, m.start_time, m.away_team, m.home_team,
                    m.parent_match_id, m.competition_name, m.score FROM `live_match` m
                    INNER JOIN live_odds o ON m.parent_match_id = o.parent_match_id
                     $whereClause AND o.market_code IN ('3way_UnoXDue', 'UnoDue',
                      'FightOutright_UnoXDue', 'Whichteamwillwinthematch_UnoDue')
                      AND m.status = 1 AND o.market_active=1 GROUP BY m.parent_match_id
                      ORDER BY s_priority ASC, c_priority ASC,
                        m.start_time LIMIT 100");

            $items = $this->rawQueries("SELECT COUNT(*) AS total FROM `live_match` m INNER JOIN
                live_odds o ON m.parent_match_id = o.parent_match_id $whereClause LIMIT 1");
        }

        return [
            $today,
            $items,
        ];
    }

    /**
     * @param $statement
     *
     * @param array $bindings
     *
     * @return mixed
     */
    protected function rawSelect($statement, $bindings = [])
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement, $bindings);
        $success->setFetchMode(Phalcon\Db\Enum::FETCH_ASSOC);
        $success = $success->fetchAll($success);

        return $success;
    }

    protected function getNavigation($sportId = null)
    {

        if (is_null($sportId)) {

            $sportId = 7;
        }

        $topLeagues = $this->rawSelect(
            "SELECT competition.competition_id, competition.competition_name as competition_name,
            count(*) AS games_count, s.sport_name FROM competition
            INNER JOIN category ON category.category_id = competition.category_id
            INNER JOIN `match` ON `match`.competition_id = competition.competition_id
            INNER JOIN sport s on s.sport_id = competition.sport_id
            WHERE `match`.start_time > NOW() AND s.sport_id = '$sportId'
            GROUP BY competition.competition_id HAVING
            games_count > 0 ORDER BY competition.priority DESC LIMIT 10"
        );

        $countries = $this->rawSelect(
            "SELECT category.category_id, category.category_name as country, category.priority,
            count(*) AS games_count FROM competition INNER JOIN category
            ON category.category_id = competition.category_id
            INNER JOIN sport s on s.sport_id = competition.sport_id INNER JOIN
            `match` ON `match`.competition_id = competition.competition_id
            WHERE `match`.start_time > NOW() AND s.sport_id = '$sportId'
            GROUP BY category.category_id HAVING games_count > 0
            ORDER BY category.priority DESC, category.category_name ASC;"
        );

        $countryLeagues = array();
        foreach ($countries as $category) {

            $cateName = $category['country'];
            $leagues = $this->rawSelect(
                "SELECT competition.competition_id, competition.competition_name, competition.priority
                 FROM competition INNER JOIN sport s on s.sport_id = competition.sport_id
                 WHERE `competition`.category_id = '" . $category['category_id'] . "'
                 AND s.sport_id = '$sportId' ORDER BY competition.priority DESC,
                 competition.competition_name ASC;"
            );

            array_push($countryLeagues, [$cateName => $leagues]);
        }

        $sports = $this->rawSelect(
            "SELECT s.sport_id, s.sport_name, count(*) AS games_count, s.priority FROM sport s
             INNER JOIN competition c on s.sport_id = c.sport_id INNER JOIN
            `match` m ON m.competition_id = c.competition_id WHERE m.start_time > NOW()
             GROUP BY s.sport_id HAVING games_count > 0 ORDER BY s.priority ASC;"
        );
        return [
            "topLeagues" => $topLeagues,
            "countries" => $countryLeagues,
            "sports" => $sports,
        ];
    }

    /**
     * @return array
     */
    protected function getCompetitions()
    {
        return [
            'UEFA Youth League, Group A' => 'UEFA Youth',
            'UEFA Youth League, Group B' => 'UEFA Youth',
            'UEFA Youth League, Group C' => 'UEFA Youth',
            'UEFA Youth League, Group D' => 'UEFA Youth',
            'UEFA Youth League, Group E' => 'UEFA Youth',
            'UEFA Youth League, Group F' => 'UEFA Youth',
            'UEFA Youth League, Group H' => 'UEFA Youth',
            'UEFA Youth League, Group G' => 'UEFA Youth',
            'Int. Friendly Games, Women' => 'Women',
            'U17 European Championship, Group B' => 'U17',
            'U17 European Championship, Qualification Gr. 9' => 'U17',
            'U21' => 'U21',
            'Premier League 2, Division 1' => 'Amateur',
            'Premier League 2, Division 2' => 'Amateur',
            'Youth League' => 'Youth',
            'Development League' => 'Youth',
            'U19 European Championship Qualif. - Gr. 1' => 'U19',
            'U19 European Championship Qualif. - Gr. 2' => 'U19',
            'U19 European Championship Qualif. - Gr. 3' => 'U19',
            'U19 European Championship Qualif. - Gr. 4' => 'U19',
            'U19 European Championship Qualif. - Gr. 5' => 'U19',
            'U19 European Championship Qualif. - Gr. 6' => 'U19',
            'U19 European Championship Qualif. - Gr. 7' => 'U19',
            'U19 European Championship Qualif. - Gr. 8' => 'U19',
            'U19 European Championship Qualif. - Gr. 9' => 'U19',
            'U19 European Championship Qualif. - Gr. 10' => 'U19',
            'U19 European Championship Qualif. - Gr. 11' => 'U19',
            'U19 European Championship Qualif. - Gr. 12' => 'U19',
            'U19 European Championship Qualif. - Gr. 13' => 'U19',
            'U19 Int. Friendly Games' => 'U19',
            'U20 Friendly Games' => 'U20',
            'U21 Friendly Games' => 'U21',
            'U21 EURO, Qualification, Group 1' => 'U21',
            'U21 EURO, Qualification, Group 2' => 'U21',
            'U21 EURO, Qualification, Group 3' => 'U21',
            'U21 EURO, Qualification, Group 4' => 'U21',
            'U21 EURO, Qualification, Group 5' => 'U21',
            'U21 EURO, Qualification, Group 6' => 'U21',
            'U21 EURO, Qualification, Group 7' => 'U21',
            'U21 EURO, Qualification, Group 8' => 'U21',
            'UEFA Youth League, Knockout stage' => 'UEFA Youth',
            'U21 EURO, Qualification, Group 9' => 'U21',
            'U21 EURO, Qualification, Playoffs' => 'U21',
            'UEFA Champions League, Women' => 'W',
            'Africa Cup Of Nations, Women, Group A' => 'W',
            'Primera Division Femenina' => 'W',
            'W-League' => 'W',
            'A-Jun-BL West' => 'U19',
            'Bundesliga, Women' => 'W',
            'Campionato Primavera, Girone A' => 'Youth',
            'Campionato Primavera, Girone B' => 'Youth',
            'Campionato Primavera, Girone C' => 'Youth',
        ];
    }

    /**
     * @return array
     */
    protected function getPaginationParams()
    {
        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }
        $limit = $this->request->get('limit', 'int') ?: 50;
        $skip = $page * $limit;

        return [
            $page,
            $limit,
            $skip,
        ];
    }

    /**
     * @param $total
     * @param $limit
     *
     * @return float|int
     */
    protected function getResultPages($total, $limit)
    {
        $pages = ceil($total / $limit);

        if ($pages > 14) {
            $pages = 14;
        }

        return $pages;
    }

    /**
     * @return string
     */
    protected function getDefaultDateFormat()
    {
        return 'l jS, F Y';
    }

    protected function topSports()
    {
        return array(
            '79' => 'Soccer',
            '85' => 'Basketball',
            '97' => 'Rugby',
            '98' => 'Cricket',
            '80' => 'Tennis',
            '84' => 'Volleyball',
            '82' => 'Ice Hockey',
            '107' => 'Baseball',
            '163' => 'Field Hockey',
            '165' => 'Football',
        );
    }
}