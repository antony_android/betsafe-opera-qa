<?php

/**
 * Copyright (c) Murwa 2018.
 *
 * All rights reserved.
 */
class AndroidController extends ControllerBase
{

    /**
     * Index
     */
    public function indexAction()
    {
        $this->tag->setTitle('Download App');
        $this->view->pick("android/index");

        $this->view->setVars([
            'data' => []
        ]);
    }
}

