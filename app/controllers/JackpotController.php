<?php

/**
 * Copyright (c) Antony 2022.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;
use Phalcon\Mvc\View;

/**
 * Class JackpotController
 */
class JackpotController extends ControllerBase
{

    private $navigation;

    public function initialize()
    {
        $selectedSportId = $this->session->get('selectedSportId');
        $this->navigation = $this->getNavigation($selectedSportId);
    }

    /**
     *
     */
    public function indexAction()
    {
        $jackpots = $this->rawQueries("SELECT je.jackpot_event_id, je.jackpot_name,
            je.jackpot_amount,je.status, je.total_games, je.requisite_wins,
             je.bet_amount FROM jackpot_event je WHERE je.status = 'ACTIVE' AND
             je.jackpot_event_id NOT IN (SELECT mj.jackpot_event_id FROM jackpot_match mj
              WHERE mj.start_time < NOW())  ORDER BY je.jackpot_name DESC;");

        $activeJackpots = [];
        foreach ($jackpots as $jackpot) {

            $totalMatches = $jackpot['total_games'];
            $allJPMatches = $this->rawQueries("SELECT COUNT(*) AS total_added FROM jackpot_match
             WHERE jackpot_event_id = '" . $jackpot['jackpot_event_id'] . "'");

            if ($totalMatches != $allJPMatches[0]['total_added']) {

                continue;
            }

            $jackpotEventId = $jackpot['jackpot_event_id'];
            $earliestKickOffRS = $this->rawQueries("SELECT jm.start_time FROM
              jackpot_match jm WHERE jm.jackpot_event_id = '$jackpotEventId'
              ORDER BY jm.start_time ASC LIMIT 1");

            $earliestKickOff = $earliestKickOffRS[0]['start_time'];

            $remainingTimeInSeconds = strtotime($earliestKickOff) - strtotime(date('Y-m-d H:i:s'));
            $timeRemaining = self::fromSeconds($remainingTimeInSeconds);

            array_push($activeJackpots, [
                'jackpot_event_id' => $jackpotEventId,
                'jackpot_name' => $jackpot['jackpot_name'],
                'status' => $jackpot['status'],
                'bet_amount' => $jackpot['bet_amount'],
                'jackpot_amount' => $jackpot['jackpot_amount'],
                'total_games' => $jackpot['total_games'],
                'requisite_wins' => $jackpot['requisite_wins'],
                'kick_off' => $timeRemaining,
            ]);
        }

        $this->tag->setTitle($this->view->t->_('Jackpots'));

        $this->view->setVars([
            'selected' => 'jackpots',
            'jackpots' => $activeJackpots,
            'timeRemaining' => $timeRemaining,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'page_description' => $this->view->t->_('Jackpots'),
        ]);
    }

    public function openAction($jackpotID)
    {

        $jEvent = $this->rawQueries("SELECT je.jackpot_event_id, je.jackpot_name, je.jackpot_amount,
              je.total_games, je.requisite_wins, jm.start_time AS kick_off, je.bet_amount FROM
              jackpot_event je INNER JOIN jackpot_match jm ON je.jackpot_event_id=jm.jackpot_event_id
               WHERE je.jackpot_event_id='$jackpotID' ORDER BY jm.start_time ASC LIMIT 1");

        $jEvent = $jEvent[0];

        $matches = $this->rawQueries("SELECT j.game_order AS pos, j.jackpot_match_id,
            e.sub_type_id,j.start_time, e.betradar_odd_id,
            MAX(CASE WHEN e.odd_key = '1' THEN odd_value END) AS home_odd,
            MAX(CASE WHEN e.odd_key = 'X' THEN odd_value END) AS neutral_odd,
            MAX(CASE WHEN e.odd_key = '2' THEN odd_value END) AS away_odd,
            j.parent_match_id, j.away_team, j.home_team, c.competition_name, c.category FROM
            jackpot_match j INNER JOIN competition c ON j.competition_id = c.competition_id
            INNER JOIN event_odd e ON e.parent_match_id = j.parent_match_id WHERE
            j.jackpot_event_id='$jackpotID' AND j.status='ACTIVE' AND
            e.sub_type_id=1 GROUP BY j.parent_match_id ORDER BY j.game_order ASC");

        $theBetslip[] = '';
        $jPCount = 0;

        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];

            if ($this->session->has("JPBetslip_" . $jackpotID)) {
                $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);

                foreach ($jpBetslip as $slip) {

                    $homeTeam = str_replace("'", "''", $slip['home_team']);
                    $awayTeam = str_replace("'", "''", $slip['away_team']);

                    $this->rawInsert("INSERT INTO jackpot_session (`jackpot_session_id`, `jackpot_event_id`,
                        `isol_userid`, `parent_match_id`,`bet_pick`, `sub_type_id`, `special_bet_value`,
                        `bet_type`, `home_team`, `away_team`,`odd_value` ,`odd_type`, `pos`, `betradar_odd_id`)
                        VALUES (NULL, '$jackpotID', '$profileId', '" . $slip['parent_match_id'] . "',
                        '" . $slip['bet_pick'] . "', '" . $slip['sub_type_id'] . "', '0',
                        'jackpot', '" . $homeTeam . "', '" . $awayTeam . "', '1', '1X2', '1',
                         '" . $slip['betradar_odd_id'] . "') ON DUPLICATE KEY UPDATE
                         bet_pick='" . $slip['bet_pick'] . "',
                         betradar_odd_id='" . $slip['betradar_odd_id'] . "'");
                }

                $this->session->remove("JPBetslip_" . $jackpotID);
            }

            $jpBetslip = $this->rawQueries("SELECT * FROM jackpot_session WHERE
                isol_userid = '$profileId' AND jackpot_event_id = '$jackpotID'");
        } else {

            if ($this->session->has("JPBetslip_" . $jackpotID)) {
                $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);
            }
        }

        $earliestKickOff = $jEvent['kick_off'];
        $remainingTimeInSeconds = strtotime($earliestKickOff) - strtotime(date('Y-m-d H:i:s'));
        $timeRemaining = self::fromSeconds($remainingTimeInSeconds);

        $jEvent = [
            'jackpot_event_id' => $jEvent['jackpot_event_id'],
            'jackpot_name' => $jEvent['jackpot_name'],
            'jackpot_amount' => $jEvent['jackpot_amount'],
            'total_games' => $jEvent['total_games'],
            'bet_amount' => $jEvent['bet_amount'],
            'requisite_wins' => $jEvent['requisite_wins'],
            'kick_off' => $timeRemaining,
        ];

        if (isset($jpBetslip) && !is_null($jpBetslip)) {

            $jPCount = count($jpBetslip);
        }

        $title = "Jackpot";
        if (!is_null($jEvent)) {

            $title = $jEvent['jackpot_name'];
        }

        $formattedSlip = [];

        if (!is_null($jpBetslip) && !empty($jpBetslip)) {

            foreach ($jpBetslip as $slip) {

                $oneSelection = [
                    'bet_pick' => $slip['bet_pick'],
                    'sub_type_id' => $slip['sub_type_id'],
                    'special_bet_value' => $slip['special_bet_value'],
                    'bet_type' => $slip['bet_type'],
                    'home_team' => $slip['home_team'],
                    'away_team' => $slip['away_team'],
                    'odd_value' => $slip['odd_value'],
                    'odd_type' => $slip['odd_type'],
                    'parent_match_id' => $slip['parent_match_id'],
                    'pos' => $slip['pos'],
                    'betradar_odd_id' => $slip['betradar_odd_id'],
                ];

                $formattedSlip[$slip['parent_match_id']] = $oneSelection;
            }
        }

        $this->session->set("referred_by", null);
        $this->tag->setTitle($this->view->t->_($title));

        $this->view->setVars([
            "matches" => $matches,
            'theBetslip' => $formattedSlip,
            'selected' => 'jackpot',
            'jackpotID' => $jackpotID,
            'jEvent' => $jEvent,
            'jPCount' => $jPCount,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'page_description' => $this->view->t->_($title),
        ]);
    }

    public function placebetAction()
    {

        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');
        if ($this->session->get('auth')) {

            $loggedInUser = $this->session->get('auth');
            $jEvent = $this->rawQueries("SELECT * FROM jackpot_event
               WHERE jackpot_event_id='$jackpotID'");

            $jEvent = $jEvent[0];

            $userId = $loggedInUser['id'];
            $balance = $loggedInUser['balance'];
            $msisdn = $loggedInUser['phone'];

            $checkValidity = $this->rawQueries("SELECT * FROM jackpot_match WHERE
                jackpot_event_id='$jackpotID' AND start_time < NOW()");

            if (!empty($checkValidity)) {

                $this->flashSession->error($this->flashMessages("Invalid jackpot. Some games
                 have already played!"));

                return $this->response->redirect("/");
            }

            $jpBetslip = $this->rawQueries("SELECT * FROM jackpot_session WHERE
                isol_userid = '$userId' AND jackpot_event_id = '$jackpotID'");

            if (!isset($jpBetslip) || is_null($jpBetslip) || count($jpBetslip) < $jEvent['total_games']) {

                $this->flashSession->error($this->flashMessages("You have not made
                selections for all the jackpot matches. Please add all the selections and
                retry!"));
                return $this->response->redirect("/jackpot/open/" . $jackpotID);
            }
	    
	    $balancePayload = [
                "UserID" => $loggedInUser['id'],
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/users/GetBalanceFull";
            $response = $this->postToUrl($balancePayload, $url);

            if ($response->ResultCode == 1) {
                $balance = $response->Balance->Amount;
            }

            $jPSelections = [];

            foreach ($jpBetslip as $match) {

                $parentMatchId = $match['parent_match_id'];
                $betPick = $match['bet_pick'];
                $jPSelections[$parentMatchId] = $betPick;
            }

            $formattedSelections = $this->betoptions($jPSelections);
            $noOfFormattedSelections = count($formattedSelections);
            $totalPotentialBetAmount = $jEvent['bet_amount'] * $noOfFormattedSelections;

            if ($balance < $totalPotentialBetAmount) {

                $this->flashSession->error($this->flashMessages("You have insufficient
                 balance to place this jackpot. Please load your account and try again!"));

                return $this->response->redirect('/deposit');

            } else {

                if ($jEvent['total_games'] == 9 || strpos($jEvent['jackpot_name'], 'Daily') !== false) {

                    $numEntriesRS = $this->rawQueries("SELECT COUNT(*) AS num FROM jackpot_bet WHERE
                     jackpot_event_id = '" . $jEvent->jackpot_event_id . "' AND
                     isolutions_userid = '$userId'");

                    $numEntries = $numEntriesRS[0]['num'];

                    if ($numEntries >= 16) {
                        $this->flashSession->error($this->flashMessages("You have reached the
                         maximum number of entries allowed for this jackpot. Please place
                         your jackpot bets on a different jackpot!"));

                        return $this->response->redirect('/');

                    } elseif ($numEntries + $noOfFormattedSelections >= 16) {

                        $this->flashSession->error($this->flashMessages("Maximum number of
                         Daily jackpots allowed are 16"));

                        return $this->response->redirect($_SERVER['HTTP_REFERER']);
                    }
                }

                $newBalance = $response->Balance->Amount;
                $auth = $this->session->get('auth');
                $auth['balance'] = $newBalance;
                $this->session->set('auth', $auth);

                $jPBetsPlaced = [];

                for ($control = 0; $control < $formattedSelections; $control++) {

                    $betQuery = "INSERT INTO `jackpot_bet` (`isolutions_userid`, `jackpot_event_id`,
                    `status`, `created`, `modified`, `msisdn`, `channel`)
                    VALUES('$userId','" . $jEvent['jackpot_event_id'] . "', '0', NOW(), NOW(),
                     '$msisdn', 'LITE WEB')";

                    $jackpotBetId = $this->rawInsert($betQuery);

                    if ($jackpotBetId < 1) {

                        $this->rawInsert("DELETE FROM jackpot_bet WHERE jackpot_event_id = '$jackpotID'
                            AND isol_userid='$userId' AND `status` = '0'");

                        $this->flashSession->error($this->flashMessages("We are unable to place
                         your jackpot at this point. Please try again later!"));

                        return $this->response->redirect('/');
                    }

                    array_push($jPBetsPlaced, $jackpotBetId);
                }

                $userSelectionsInNumbersForm = "";

                $betSlipSQL = "INSERT INTO jackpot_betslip (`parent_match_id`, `jackpot_bet_id`,
                `bet_pick`, `special_bet_value`, `win`, `created`, `modified`, `sub_type_id`,
                `status`) VALUES ";

                for ($control = 0; $control < $jPBetsPlaced; $control++) {

                    $selectedJPId = $jPBetsPlaced[$control];
                    $selectionsInTheCombination = $formattedSelections[$control];

                    foreach ($selectionsInTheCombination as $key => $selectedJPMatch) {

                        $parentMatchId = $key;
                        $betPick = $selectedJPMatch;
                        $subTypeId = "1";
                        $specialBetValue = "0.0";

                        $userSelectionsInNumbersForm = $userSelectionsInNumbersForm . "" . $betPick;

                        $betSlipSQL = $betSlipSQL . " ('$parentMatchId','$selectedJPId',
                        '$betPick', '$specialBetValue','0', NOW(),NOW(),'$subTypeId',
                         '1'),";
                    }
                }

                $betSlipSQL = rtrim($betSlipSQL, ",");
                $slipId = $this->rawInsert($betSlipSQL);

                if ($slipId < 1) {

                    for ($control = 0; $control < $jPBetsPlaced; $control++) {

                        $selectedJPId = $jPBetsPlaced[$control];
                        $this->rawInsert("DELETE FROM jackpot_bet WHERE jackpot_event_id = '$jackpotID'
                            AND isol_userid='$userId' AND `jackpot_bet` = '$selectedJPId'");
                    }

                    $this->flashSession->error($this->flashMessages("We are unable to place
                    your jackpot at this point. Please try again later!"));

                    return $this->response->redirect('/');

                } else {

                    $internalTranId = 0;
                    $internalISQ = "SELECT * FROM `transaction` ORDER BY 1 DESC LIMIT 1";
                    $tranRS = $this->rawQueries($internalISQ);
                    $internalTranId = $tranRS[0]['id'];

                    $runningBalance = $balance - $totalPotentialBetAmount;
                    $transactionsQuery = "INSERT INTO `transaction`
                        VALUES(NULL, '$userId', 'Jackpot',
                        '0', '-$internalTranId', '" . $totalPotentialBetAmount . "', '$runningBalance',
                        NOW(), NULL, NULL, 0, 'LITE-SITE', NOW(), NOW())";

                    $transactionId = $this->rawInsert($transactionsQuery);

                    $payload = [
                        "Notes" => "Jackpot Bet",
                        "ThirdPartyCausalCode" => "",
                        "CurrentSessionID" => -1,
                        "_ThirdPartyTaxesAmount" => 0,
                        "VerifyReceivedFields" => true,
                        "ProviderID" => 368,
                        "UserID" => $this->session->get('auth')['id'],
                        "ThirdPartyCurrencyCode" => "KES",
                        "ThirdPartyAmount" => $totalPotentialBetAmount,
                        "TransactionID" => $transactionId,
                        "APIAccount" => $this->jpAccount,
                        "APIPassword" => $this->qaJpPassword,
                        "IDBookmaker" => $this->bookmarkerID,
                    ];
		    
		    $url = $this->qaBaseURL . "/api/thirparties/v2/games/TransferMoneyToProviderV6";
                    $response = $this->postToUrl($payload, $url);
		    
		    if ($response->ResultCode == 1) {

                        $isolTranId = $response->ISBetsTransactionID;
                        $updateQuery = "UPDATE `transaction` SET coupon_id = '" . $jackpotBetId . "',
                            `status` = 1, `isolutions_transactionid`='$isolTranId'
                            WHERE id = '" . $transactionId . "'";

                        $this->rawInsert($updateQuery);

                        for ($control = 0; $control < $jPBetsPlaced; $control++) {

                            $updateBetQuery = "UPDATE `jackpot_bet` SET `status` = 1
                            WHERE jackpot_bet_id = '" . $jackpotBetId . "'";

                            $this->rawInsert($updateBetQuery);
                        }

                        $this->rawInsert("DELETE FROM jackpot_session WHERE jackpot_event_id = '$jackpotID'
                            AND isol_userid='$userId'");

                        $message = "You successfully placed a " . $jEvent['jackpot_name'] . "!
                        Predictions $userSelectionsInNumbersForm  KSH " . $jEvent['bet_amount'] . " has been
                         deducted from your account. Contact Us: 0111 040000";
                        $this->sendZimobiSMS($message, $msisdn);

                        $this->flashSession->success($this->flashMessages($message));
                        return $this->response->redirect('/jackpot');
                    } else {

                        $file = fopen("/var/log/jackpot/jackpot_web_errors.log", "a");

                        if (fwrite($file, $response) === false) {
                            fwrite($file, "Error: Response data not written");
                        }

                        if (fwrite($file, $loggedInUser) === false) {
                            fwrite($file, "Error: User data not written");
                        }

                        fwrite($file, "\r\n");
                        fclose($file);

                        $bytes = openssl_random_pseudo_bytes(32);
                        $hash = bin2hex($bytes);
                        $this->flashSession->error($this->flashMessages("Unable to place your
                         jackpot bet. Please try again later!"));

                        return $this->response->redirect($_SERVER['HTTP_REFERER'] . "?v=$hash");
                    }
                }
            }
        } else {

            $this->session->set("referred_by", "/jackpot/open/" . $jackpotID);
            $this->view->disable();
            return $this->response->redirect('/login');
        }
    }

    public function autoAction()
    {
        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');

        $matches = $this->rawQueries("SELECT j.game_order AS pos, jackpot_match_id,
            j.start_time, e.betradar_odd_id, e.sub_type_id,
            j.parent_match_id, j.away_team, j.home_team, c.competition_name, c.category FROM
            jackpot_match j INNER JOIN competition c ON j.competition_id = c.competition_id
             INNER JOIN event_odd e ON e.parent_match_id = j.parent_match_id WHERE
            j.jackpot_event_id='$jackpotID' AND j.status='ACTIVE' AND
            e.sub_type_id=1 GROUP BY j.parent_match_id ORDER BY game_order");

        $picks = ['1', 'X', '2'];

        foreach ($matches as $match) {

            $index = rand(0, 2);
            $selection = $picks[$index];
            $match_id = $match['parent_match_id'];

            $jpBetslip["$match_id"] = [
                'bet_pick' => $selection,
                'sub_type_id' => $match['sub_type_id'],
                'special_bet_value' => 0,
                'bet_type' => 'jackpot',
                'home_team' => $match['home_team'],
                'away_team' => $match['away_team'],
                'odd_value' => 1,
                'odd_type' => "1X2",
                'parent_match_id' => $match['parent_match_id'],
                'pos' => 1,
                'betradar_odd_id' => $match['betradar_odd_id'],
            ];

            if ($this->session->get('auth')) {

                $profileId = $this->session->get('auth')['id'];

                $this->rawInsert("INSERT INTO jackpot_session (`jackpot_session_id`, `jackpot_event_id`,
                `isol_userid`, `parent_match_id`,`bet_pick`, `sub_type_id`, `special_bet_value`,
                `bet_type`, `home_team`, `away_team`,`odd_value` ,`odd_type`, `pos`, `betradar_odd_id`)
                VALUES (NULL, '$jackpotID', '$profileId', '" . $match['parent_match_id'] . "',
                '$selection', '" . $match['sub_type_id'] . "', '0', 'jackpot', '" . $match['home_team'] . "',
                '" . $match['away_team'] . "', '1', '1X2', '1', '" . $match['betradar_odd_id'] . "') ON
                DUPLICATE KEY UPDATE bet_pick='$selection',
                betradar_odd_id='" . $match['betradar_odd_id'] . "'");
            } else {

                $this->session->set("JPBetslip_" . $jackpotID, $jpBetslip);
            }

        }

        return $this->response->redirect('/jackpot/open/' . $jackpotID);
    }

    public function autoXAction()
    {
        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');

        if ($this->session->has("JPBetslip_" . $jackpotID)) {
            $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);
            unset($jpBetslip);
        } else {
            $jpBetslip = [];
        }

        $matches = $this->rawQueries("SELECT j.game_order AS pos, jackpot_match_id,
            j.start_time, e.betradar_odd_id, e.sub_type_id,
            j.parent_match_id, j.away_team, j.home_team, c.competition_name, c.category FROM
            jackpot_match j INNER JOIN competition c ON j.competition_id = c.competition_id
             INNER JOIN event_odd e ON e.parent_match_id = j.parent_match_id WHERE
            j.jackpot_event_id='$jackpotID' AND j.status='ACTIVE' AND
            e.sub_type_id=1 GROUP BY j.parent_match_id ORDER BY game_order");

        $picks = ['1', 'X', '2'];

        foreach ($matches as $match) {

            $index = rand(0, 2);
            $selection = $picks[$index];
            $match_id = $match['parent_match_id'];

            $jpBetslip["$match_id"] = [
                'bet_pick' => $selection,
                'sub_type_id' => $match['sub_type_id'],
                'special_bet_value' => 0,
                'bet_type' => 'jackpot',
                'home_team' => $match['home_team'],
                'away_team' => $match['away_team'],
                'odd_value' => 1,
                'odd_type' => "1X2",
                'parent_match_id' => $match['parent_match_id'],
                'pos' => 1,
                'betradar_odd_id' => $match['betradar_odd_id'],
            ];
        }

        $this->session->set("JPBetslip_" . $jackpotID, $jpBetslip);
        return $this->response->redirect('/jackpot/open/' . $jackpotID);
    }

    public function historyAction()
    {

        if ($this->session->get('auth')) {

            $id = $this->session->get('auth')['id'];

            $jackpotBets = $this->rawQueries("SELECT jb.status, je.jackpot_event_id, jb.jackpot_bet_id,
                je.jackpot_name,je.status AS event_status,je.bet_amount FROM jackpot_bet jb
                INNER JOIN jackpot_event je USING(jackpot_event_id) INNER JOIN jackpot_match jm
                ON je.jackpot_event_id = jm.jackpot_event_id WHERE
                jb.isolutions_userid='$id' AND jb.status = 1 AND jm.home_team <> ''
                GROUP BY jb.jackpot_bet_id ORDER BY jb.created DESC LIMIT 30;");

            $title = "Jackpot History";

            $this->tag->setTitle($title);

            $this->view->setVars([
                "jackpotBets" => $jackpotBets,
                'selected' => 'jackpot-history',
                "type_of_bets" => "open",
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'selectedBets' => 'open',
            ]);
        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function closedbetsAction()
    {

        if ($this->session->get('auth')) {

            $id = $this->session->get('auth')['id'];

            $jackpotBets = $this->rawQueries("SELECT jb.status, je.jackpot_event_id,
                jb.jackpot_bet_id, je.jackpot_name,je.status AS event_status,je.bet_amount
                FROM jackpot_bet jb INNER JOIN jackpot_event je USING(jackpot_event_id)
                 INNER JOIN jackpot_match jm ON je.jackpot_event_id = jm.jackpot_event_id
                 WHERE jb.isolutions_userid='$id' AND jb.status <> 0 AND jb.status <> 1
                 AND jm.home_team <> '' GROUP BY jb.jackpot_bet_id ORDER BY
                 jb.created DESC LIMIT 30;");

            $title = "Jackpot History";
            $this->session->set('selected-jp-history', "closedbets");
            $this->tag->setTitle($title);

            $this->view->setVars([
                "jackpotBets" => $jackpotBets,
                "type_of_bets" => "closed",
                'selected' => 'jackpot-history',
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'selectedBets' => 'closed',
            ]);

            $this->view->pick("jackpot/history");
        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function showAction($betID, $referrer)
    {

        $myBet = $this->rawSelect("SELECT jb.status, je.jackpot_event_id, jb.jackpot_bet_id,
            je.jackpot_name,je.status AS event_status,je.bet_amount, jb.created FROM jackpot_bet jb
            INNER JOIN jackpot_event je USING(jackpot_event_id) WHERE
			jb.jackpot_bet_id='$betID' GROUP BY jb.jackpot_bet_id ORDER BY
			jb.created DESC LIMIT 1");

        $myBet = $myBet['0'];

        $betDetails = $this->rawSelect("SELECT jb.jackpot_bet_id, jb.created,
			js.created, js.win, j.away_team, c.competition_name, ct.category_name,
		    j.home_team, js.bet_pick, js.status FROM jackpot_bet jb
			INNER JOIN jackpot_betslip js ON jb.jackpot_bet_id = js.jackpot_bet_id
            INNER JOIN `jackpot_match` j ON j.parent_match_id = js.parent_match_id
			INNER JOIN `competition` c ON j.competition_id = c.competition_id
			INNER JOIN `category` ct ON c.category_id = ct.category_id
			WHERE jb.jackpot_bet_id = '$betID' ORDER BY js.jackpot_betslip_id ASC");

        $this->view->setVars([
            "betDetails" => $betDetails,
            'myBet' => $myBet,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'selected' => 'jackpot-details',
            'referrer' => $referrer,
        ]);
    }

    public function filterAction()
    {

        $selection = $this->request->getPost('selection');
        if ($selection == "open") {
            $selection = "";
            $this->session->set('selected-jp-history', $selection);
            return $this->response->redirect('/jackpot/history');
        }

        $this->session->set('selected-jp-history', $selection);
        return $this->response->redirect('/jackpot/' . $selection);
    }

    public static function fromSeconds($seconds)
    {
        $interval = date_diff(date_create("@0"), date_create("@$seconds"));

        foreach (array('y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour',
            'i' => 'minute', 's' => 'second') as $format => $desc) {
            if ($interval->$format >= 1) {
                $thetime[] = $interval->$format . ($interval->$format == 1 ? " $desc" : " {$desc}s");
            }

        }

        return isset($thetime) ? implode(' ', $thetime) . ($interval->invert ? ' ago' : '') : null;
    }

    private function betoptions($selections)
    {

        $keys = array_keys($selections);

        foreach ($selections as $key => $val) {

            $selections[$key] = explode(",", $val);
        }

        $combinations = [[]];
        $length = count($selections);

        for ($count = 0; $count < $length; $count++) {
            $tmp = [];
            foreach ($combinations as $v1) {
                foreach ($selections[$keys[$count]] as $v2) {
                    $tmp[] = array_merge($v1, [$v2]);
                }
                array_push($combinations, $tmp);
            }
            $combinations = $tmp;
        }

        $withKeysArr = [];
        foreach ($combinations as $combined) {

            $tempInner = [];
            for ($counter = 0; $counter < count($combined); $counter++) {

                $tempInner[$keys[$counter]] = $combined[$counter];
            }
            array_push($withKeysArr, $tempInner);
        }

        return $withKeysArr;
    }
}
