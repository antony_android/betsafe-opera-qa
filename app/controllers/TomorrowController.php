<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class TomorrowController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('About us');
    }

    public function indexAction()
    {

        $sport_id = $this->request->get('id', 'int') ?: 7;
        $keyword = $this->request->getPost('keyword');

        $current_sport = [];
        foreach ($this->sports as $key => $sp) {
            if ($sp['sport_id'] == $sport_id) {
                $current_sport = $sp;
                break;
            }
        }
        $default_sub_type_id = $current_sport['default_market'];

        list($today, $total) = $this->getGames(
            $keyword,
            $skip,
            $limit,
            $sport_id,
            $default_sub_type_id,
            ' AND DATE(start_time) = CURDATE() + INTERVAL 1 DAY',
            'm_priority DESC, priority DESC, start_time ASC'
        );

        $tab = 'tomorrow';
        $this->view->setVars(
            [
                'matches' => $today,
                'total' => $total,
                'tab' => $tab,
                'current_sport' => $current_sport,
            ]
        );

        $this->tag->setTitle('BetSafe - Fast Play, Fast Pay');

        $this->view->pick('index/index');
    }
}
