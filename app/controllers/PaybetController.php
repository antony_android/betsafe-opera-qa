<?php

//use Phalcon\Logger\Adapter\File as FileLogger;

class PaybetController extends ControllerBase
{
    public function indexAction()
    {

        $betID = $this->request->get('ref', 'int');
        $invoice_amount = (int) $this->request->get('amount');
        $phone_number = $this->request->get('msisdn');

        $payment_method = $this->ssj_get_number_network($phone_number);

        $spid = 101010;
        if (is_numeric($betID)) {
            $redirectURL = urlencode('https://betsafe.co.ke/mybets?a=1&id=' . $betID);
            $payload = [
                'spId' => $spid,
                'msisdn' => $phone_number,
                'amount' => $invoice_amount,
                'reference' => $betID,
                'betId' => $betID,
                'redirectUri' => $redirectURL,
            ];
            $response = $this->payBet($payload);

            $data = json_decode($response['message'], 1);

            if ($payment_method == 'TIGO') {
                if (is_null($data['Error']) && !is_null($data['tigoUrl'])) {
                    $tigoURL = $data['tigoUrl'];
                    header("location:$tigoURL");
                    exit();
                } else {
                    header("location:/mybets?a=1&id=$betID&error=1&msg=" . $data['Error']);
                    exit();
                }
            } else {

                echo "<p> Please you phone to complete your deposit, then click here to proceed <a href='https://betsafe.co.ke/mybets?a=1&id=$betID'> Click to Proceed </a></p>";
            }
        }
        //    $this->view->disable();
    }
}