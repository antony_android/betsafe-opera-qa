<?php

class MyProfileController extends ControllerBase
{
    public function indexAction()
    {

        if ($this->session->get('auth')) {
            $theBetslip = $this->session->get("betslip");
            $selectedSportId =  $this->session->get('selectedSportId');
            $navigation = $this->getNavigation($selectedSportId);
            $this->session->set('withdrawalAmount', NULL);

            $userId = $this->session->get('auth')['id'];
            $cities = $this->rawQueries("SELECT * FROM city WHERE `status` = 1 ORDER BY city_name");
            $profile = $this->rawQueries("SELECT * FROM `profile` WHERE `isolutions_userid` = '$userId'");

            $this->view->setVars([
                'theBetslip' => $theBetslip,
                'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'sports' => $navigation['sports'],
                'cities' => $cities,
                'profile' => $profile,
                'referrer' => $this->request->getHTTPReferer(),
                'selected' => 'my-profile'
            ]);
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function balanceAction()
    {

        if(!$this->session->get("auth")){
            return;
        }
        
        $payload = [
            "UserID" => $this->session->get("auth")['id'],
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID
        ];

        $url = $this->qaBaseURL . "/api/users/GetBalanceFull";
        $encodedResponse = $this->postToUrlEncoded($payload, $url);

        $decodedResults = json_decode($encodedResponse);

        if ($decodedResults->ResultCode == 1) {

            $balance = $decodedResults->Balance->Amount;
            $auth = $this->session->get('auth');
            $auth['balance'] = $balance;
            $this->session->set('auth', $auth);
        }

        return $encodedResponse;
    }

    public function useragentAction()
    {
       
        if(is_null($this->session->get('user-agent'))){
            $this->session->set('user-agent', 'OperaMiniExtreme');
        }
        return true;
    }

    public function unsetagentAction()
    {
       
        if(!is_null($this->session->get('user-agent'))){
            $this->session->set('user-agent', NULL);
        }
        return true;
    }

    public function showpassAction()
    {
       
        if(is_null($this->session->get('showpass'))){
            $this->session->set('showpass', 'TRUE');
        }else {
            $showPass = $this->session->get('showpass');

            if($showPass == 'TRUE'){

                $showPass = 'FALSE';
                $this->session->set('showpass', $showPass);
            }else if($showPass == 'FALSE'){

                $showPass = 'TRUE';
                $this->session->set('showpass', $showPass);
            }
        }
        return json_encode(['showpass' => $showPass]);
    }

    public function testAction(){

        $testPayload = [
            "Version" => NULL,
            "LanguageID" => 2,
            "EventID" => 30013363,
            "APIAccount" => $this->jpAccount,
            "APIPassword" => $this->jpApiPassword,
            "IDBookmaker" => $this->bookmarkerID
        ];

        $fullURL = $this->qaBaseURL . "/api/eventprogramlive/GetEventDetailsFull";
        $response = $this->postToUrl($testPayload, $fullURL);

        print_r($response); exit;
    }

    public function updateAction()
    {

        if ($this->session->get('auth')) {
            $mobile = str_replace("'", "", $this->request->getPost('mobile'));
            $firstName = str_replace("'", "\'", $this->request->getPost('first_name'));
            $surname = str_replace("'", "\'", $this->request->getPost('surname'));
            $lastName = str_replace("'", "\'", $this->request->getPost('last_name'));
            $email = str_replace("'", "\'", $this->request->getPost('email'));
            $hseStreet = str_replace("'", "\'", $this->request->getPost('hse_street'));
            $city = str_replace("'", "\'", $this->request->getPost('city'));
            $address = str_replace("'", "\'", $this->request->getPost('address'));
            $country = str_replace("'", "\'", $this->request->getPost('country'));
            $dontReceiveProm = $this->request->getPost('dont_receive_communication', 'int');
            $receiveProm = $this->request->getPost('receive_communication', 'int');

            $status = 0;
            if ($receiveProm == 1) {
                $status = 1;
            } else if ($dontReceiveProm == 1) {
                $status = 0;
            }

            $userId = $this->session->get('auth')['id'];

            $query = "INSERT  INTO `profile` VALUES (NULL, '$userId', '$firstName', '$surname',
             '$lastName', '$email', '$hseStreet', '$city', '$address', '$country', '$mobile',
              NOW(), 1, '$status', NULL, NOW(), 'SYSTEM', NULL, NULL) ON DUPLICATE KEY 
              UPDATE first_name='$firstName', surname='$surname', last_name='$lastName',
               email='$email', hse_street='$hseStreet', city='$city', `address`='$address',
                country='$country', msisdn='$mobile', receive_promo_comm='$status'";

            $this->rawInsert($query);

            $userInfoPayload = [
                    "Name" => $firstName." ".$lastName,
                    "Surname" => $surname,
                    "UserID" => $userId,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID
                ];

            $userInfoURL = $this->qaBaseURL . "/api/users/SetUserInfo";
            $userInfoResponse = $this->postToUrl($userInfoPayload, $userInfoURL);

            $payload = [
                "UserDetails" => [
                    "Address" => $address,
                    "City" => $city,
                    "Zip" => "",
                    "Province" => "",
                    "NCell" => "254".$mobile,
                    "Phone" => "254".$mobile,
                    "Email" => $email,
                    "SecQuestion" => "",
                    "SecAnswer" => ""
                ],
                "UserID" => $userId,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID
            ];

            $profileURL = $this->qaBaseURL . "/api/users/SetUserDetails";
            $response = $this->postToUrl($payload, $profileURL);

            $promotionsPayload = [
                "UserID" => $userId,
                "AdditionalDataID" => 132,
                "Value" => "$status",
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID
            ];

            $promotionsURL = $this->qaBaseURL . "/api/users/SetAdditionalDataValue";
            $promotionsResponse = $this->postToUrl($promotionsPayload, $promotionsURL);

            if($response->ResultCode == 1 && $promotionsResponse->ResultCode == 1 && $userInfoResponse->ResultCode == 1){
                $this->flashSession->success($this->flashMessages('Your profile has been updated successfully'));
                $this->response->redirect('/');
            }else {
                $this->flashSession->error($this->flashMessages('Profile details not fully set. Please try again later'));
                $this->response->redirect('/my-profile');
            }
        } else {

            $this->view->disable();
            $this->response->redirect('/login');
        }
    }
}
