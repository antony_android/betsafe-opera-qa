<?php

/**
 * Copyright (c) Antony 2021.
 *
 * All rights reserved.
 */

class BetHistoryController extends ControllerBase
{

    private $navigation;

    public function initialize()
    {
        $selectedSportId = $this->session->get('selectedSportId');
        $this->navigation = $this->getNavigation($selectedSportId);
    }

    public function indexAction()
    {

        if ($this->session->get('auth')) {

            $today = date('Y-m-d H:i:s', strtotime(' -3 hours'));
            $monthAgo = date_create(date('Y-m-d H:i:s', strtotime(' -3 hours')));
            date_sub($monthAgo, date_interval_create_from_date_string("30 days"));
            $monthAgo = date_format($monthAgo, "Y-m-d H:i:s");
            $userId = $this->session->get('auth')['id'];

            $payload = [
                "UserID" => $userId,
                "StartDate" => $monthAgo,
                "EndDate" => $today,
                "IDCouponResult" => 0,
                "LanguageID" => 2,
                "VerifyReceivedFields" => true,
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/users/v2/GetBetsListV6";
            $response = $this->postToUrl($payload, $url);

            $resultCode = $response->ResultCode;
            $coupons = [];
            $myBets = [];

            if ($resultCode == 1) {

                $coupons = $response->BetsList;

                foreach ($coupons as $coupon) {

                    $dateInserted = strtotime($coupon->Data);
                    $localDate = date('Y-m-d H:i:s', $dateInserted + (3600 * 3));

                    $couponDet = [
                        'isolutions_couponid' => $coupon->IDCoupon,
                        'coupon_code' => $coupon->Coupon,
                        'created' => $localDate,
                        'total_odd' => number_format($coupon->TotalOdds, 2),
                        'bet_amount' => round($coupon->Amount * 1.075),
                        'possible_win' => number_format($coupon->PotentialWinning, 2),
                        'user_winnings' => ($coupon->UserWinnings > 0) ? number_format($coupon->UserWinnings, 2) : number_format($coupon->PotentialWinning, 2),
                        'status' => $coupon->IDCouponState,
                        'win' => $coupon->IDCouponResult,
                        'cashout' => $coupon->IsCashout,
                        'coupon_result' => $coupon->CouponResult,
                    ];

                    array_push($myBets, $couponDet);
                }
            }

            $title = "Bet History";

            $this->tag->setTitle($title);

            $this->view->setVars([
                "myBets" => $myBets,
                'selected' => 'bet-history',
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'selectedBets' => 'all',
            ]);

        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function openAction()
    {
        if ($this->session->get('auth')) {

            $userId = $this->session->get('auth')['id'];
            $today = date('Y-m-d H:i:s', strtotime(' -3 hours'));
            $monthAgo = date_create(date('Y-m-d H:i:s', strtotime(' -3 hours')));
            date_sub($monthAgo, date_interval_create_from_date_string("30 days"));
            $monthAgo = date_format($monthAgo, "Y-m-d H:i:s");

            $payload = [
                "UserID" => $userId,
                "StartDate" => $monthAgo,
                "EndDate" => $today,
                "IDCouponResult" => 3,
                "LanguageID" => 2,
                "VerifyReceivedFields" => true,
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/users/v2/GetBetsListV6";
            $response = $this->postToUrl($payload, $url);

            $resultCode = $response->ResultCode;
            $coupons = [];
            $myBets = [];

            if ($resultCode == 1) {

                $coupons = $response->BetsList;

                foreach ($coupons as $coupon) {

                    $dateInserted = strtotime($coupon->Data);
                    $localDate = date('Y-m-d H:i:s', $dateInserted + (3600 * 3));

                    $couponDet = [
                        'isolutions_couponid' => $coupon->IDCoupon,
                        'coupon_code' => $coupon->Coupon,
                        'created' => $localDate,
                        'total_odd' => number_format($coupon->TotalOdds, 2),
                        'bet_amount' => round($coupon->Amount * 1.075),
                        'possible_win' => number_format($coupon->PotentialWinning, 2),
                        'user_winnings' => ($coupon->UserWinnings > 0) ? number_format($coupon->UserWinnings, 2) : number_format($coupon->PotentialWinning, 2),
                        'status' => $coupon->IDCouponState,
                        'win' => $coupon->IDCouponResult,
                        'cashout' => $coupon->IsCashout,
                        'coupon_result' => $coupon->CouponResult,
                    ];

                    array_push($myBets, $couponDet);

                }
            }

            $title = "Bet History";

            $this->tag->setTitle($title);

            $this->view->setVars([
                "myBets" => $myBets,
                'selected' => 'bet-history',
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'selectedBets' => 'open',
            ]);

            $this->view->pick("bet-history/index");

        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function settledAction()
    {
        if ($this->session->get('auth')) {

            $userId = $this->session->get('auth')['id'];
            $today = date('Y-m-d H:i:s', strtotime(' -3 hours'));
            $monthAgo = date_create(date('Y-m-d H:i:s', strtotime(' -3 hours')));
            date_sub($monthAgo, date_interval_create_from_date_string("30 days"));
            $monthAgo = date_format($monthAgo, "Y-m-d H:i:s");

            $payload = [
                "UserID" => $this->session->get('auth')['id'],
                "StartDate" => $monthAgo,
                "EndDate" => $today,
                "IDCouponResult" => 0,
                "LanguageID" => 2,
                "VerifyReceivedFields" => true,
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/users/v2/GetBetsListV6";
            $response = $this->postToUrl($payload, $url);

            $resultCode = $response->ResultCode;
            $coupons = [];
            $myBets = [];

            if ($resultCode == 1) {

                $coupons = $response->BetsList;

                foreach ($coupons as $coupon) {

                    if ($coupon->IDCouponResult == 3) {
                        continue;
                    }

                    $dateInserted = strtotime($coupon->Data);
                    $localDate = date('Y-m-d H:i:s', $dateInserted + (3600 * 3));

                    $couponDet = [
                        'isolutions_couponid' => $coupon->IDCoupon,
                        'coupon_code' => $coupon->Coupon,
                        'created' => $localDate,
                        'total_odd' => number_format($coupon->TotalOdds, 2),
                        'bet_amount' => round($coupon->Amount * 1.075),
                        'possible_win' => number_format($coupon->PotentialWinning, 2),
                        'user_winnings' => ($coupon->UserWinnings > 0) ? number_format($coupon->UserWinnings, 2) : number_format($coupon->PotentialWinning, 2),
                        'status' => $coupon->IDCouponState,
                        'win' => $coupon->IDCouponResult,
                        'cashout' => $coupon->IsCashout,
                        'coupon_result' => $coupon->CouponResult,
                    ];

                    array_push($myBets, $couponDet);
                }
            }

            $title = "Bet History";

            $this->tag->setTitle($title);

            $this->view->setVars([
                "myBets" => $myBets,
                'selected' => 'bet-history',
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'selectedBets' => 'settled',
            ]);

            $this->view->pick("bet-history/index");

        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function showAction($couponId, $coupon)
    {

        if ($this->session->get('auth')) {

            $couponDetailsPayload = [
                "CouponCode" => $coupon,
                "LanguageID" => 2,
                "UserID" => $this->session->get('auth')['id'],
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $detailsURL = $this->qaBaseURL . "/api/coupon/CouponDetailV10";
            $couponResponse = $this->postToUrl($couponDetailsPayload, $detailsURL);

            $couponEvents = [];

            if ($couponResponse->ResultCode == 1) {

                $checkCashOutPayload = [
                    "UserID" => $this->session->get('auth')['id'],
                    "CouponID" => $couponId,
                    "VerifyReceivedFields" => true,
                    "UserBookmakerID" => $couponResponse->UserBookmakerID,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID,
                ];

                $cashoutEnabled = false;

                $checkCashoutURL = $this->qaBaseURL . "/api/cashout/CheckCashOutV3";
                $cashoutResponse = $this->postToUrl($checkCashOutPayload, $checkCashoutURL);

                if ($cashoutResponse->ResultCode == 1) {

                    $cashoutEnabled = $cashoutResponse->Enabled;
                }

                $date = $couponResponse->Date;
                $date = str_replace("T", " ", $date);
                $localDate = date('Y-m-d H:i:s', strtotime($date) + (3600 * 3));

                $couponEvents = $couponResponse->Events;
                $totalGamesInBet = count($couponEvents);

                $myBet = [
                    'isolutions_couponid' => $couponResponse->CouponID,
                    'coupon_code' => $couponResponse->CouponCode,
                    'created' => $localDate,
                    'total_odd' => $couponResponse->TotalOdds,
                    'bet_amount' => round($couponResponse->BetDetails[0]->TotalAmount * 1.075, 2),
                    'total_matches' => $totalGamesInBet,
                    'possible_win' => number_format($couponResponse->MinPotentialWinnings, 2),
                    'user_winnings' => number_format($couponResponse->BetDetails[0]->Winnings, 2),
                    'status' => $couponResponse->CouponStateID,
                    'win' => $couponResponse->CouponOutcomeID,
                    'cashout' => $couponResponse->IsCashout,
                    'cashout_enabled' => $cashoutEnabled,
                    'cashout_amount' => $cashoutResponse->Amount,
                ];

                $betDetails = [];

                foreach ($couponEvents as $event) {

                    $game = $event->SubEvent;

                    if ($event->SportTypeIDLive == 1) {
                        $game = $event->Event;
                    }
                    $game = str_replace(" ", "", $game);
                    $teams = explode("-", $game);
                    $oddTypeCode = $event->OddTypeCode;
                    $userSelection = $oddTypeCode;

                    if (strpos($oddTypeCode, 'UnoXDue_') !== false ||
                        strpos($oddTypeCode, 'UnoDue_') !== false) {

                        $userSelection = substr($oddTypeCode, -1);
                    }

                    $couponDet = [
                        'start_time' => date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $event->StartDate)) + (3600 * 3)),
                        'home_team' => $teams[0],
                        'away_team' => isset($teams[1]) ? $teams[1] : "",
                        'sport' => $event->Sport,
                        'competition_name' => $event->Event,
                        'outcome_id' => $event->OutcomeID,
                        'outcome' => $event->Outcome,
                        'ft_score' => $event->FinalOutcomeR1 . ":" . $event->FinalOutcomeR2,
                        'odd_value' => $event->Odd,
                        'isol_oddclassid' => $event->OddID,
                        'bet_pick' => $userSelection,
                        'bet_type' => $couponResponse->BetType,
                    ];

                    array_push($betDetails, $couponDet);
                }
            }

            $this->session->set("CASHOUT_FROM", "BET_HISTORY");
            $pageToLoad = "MAIN";

            if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == "/bet-history/cashout") {
                $pageToLoad = "CASHOUT";
            }

            $this->view->setVars([
                "betDetails" => $betDetails,
                'myBet' => $myBet,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'pageToLoad' => $pageToLoad,
                'selected' => 'bet-details',
            ]);

        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function cashoutAction($couponId, $couponCode)
    {

        if ($this->session->get('auth')) {

            $couponDetailsPayload = [
                "CouponCode" => $couponCode,
                "LanguageID" => 2,
                "UserID" => $this->session->get('auth')['id'],
                "UserBookmakerID" => $this->bookmarkerID,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $detailsURL = $this->qaBaseURL . "/api/coupon/CouponDetailV10";
            $couponResponse = $this->postToUrl($couponDetailsPayload, $detailsURL);

            $couponEvents = [];

            if ($couponResponse->ResultCode == 1) {

                $checkCashOutPayload = [
                    "UserID" => $this->session->get('auth')['id'],
                    "CouponID" => $couponId,
                    "VerifyReceivedFields" => true,
                    "UserBookmakerID" => $couponResponse->UserBookmakerID,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID,
                ];

                $cashoutEnabled = false;

                $checkCashoutURL = $this->qaBaseURL . "/api/cashout/CheckCashOutV3";
                $cashoutResponse = $this->postToUrl($checkCashOutPayload, $checkCashoutURL);

                if ($cashoutResponse->ResultCode == 1) {

                    $cashoutEnabled = $cashoutResponse->Enabled;
                }

                $date = $couponResponse->Date;
                $date = str_replace("T", " ", $date);
                $localDate = date('Y-m-d H:i:s', strtotime($date) + (3600 * 3));

                $couponEvents = $couponResponse->Events;
                $totalGamesInBet = count($couponEvents);

                $myBet = [
                    'isolutions_couponid' => $couponResponse->CouponID,
                    'coupon_code' => $couponResponse->CouponCode,
                    'created' => $localDate,
                    'total_odd' => $couponResponse->TotalOdds,
                    'bet_amount' => round($couponResponse->BetDetails[0]->TotalAmount * 1.075, 2),
                    'total_matches' => $totalGamesInBet,
                    'possible_win' => number_format($couponResponse->MinPotentialWinnings, 2),
                    'user_winnings' => number_format($couponResponse->BetDetails[0]->Winnings, 2),
                    'status' => $couponResponse->CouponStateID,
                    'win' => $couponResponse->CouponOutcomeID,
                    'cashout' => $couponResponse->IsCashout,
                    'cashout_enabled' => $cashoutEnabled,
                    'cashout_amount' => $cashoutResponse->Amount,
                ];

                $betDetails = [];

                foreach ($couponEvents as $event) {

                    $game = $event->SubEvent;
                    $game = str_replace(" ", "", $game);
                    $teams = explode("-", $game);
                    $betPick = "";

                    $couponDetailsRS = $this->rawQueries("SELECT * FROM event_odd WHERE
                        betradar_odd_id = '" . $event->OddID . "'");

                    if (!is_null($couponDetailsRS) && !empty($couponDetailsRS)) {

                        $betPick = $couponDetailsRS[0]['odd_key'];
                    }

                    $couponDet = [
                        'start_time' => str_replace("T", " ", $event->StartDate),
                        'home_team' => $teams[0],
                        'away_team' => isset($teams[1]) ? $teams[1] : "",
                        'sport' => $event->Sport,
                        'competition_name' => $event->Event,
                        'outcome_id' => $event->OutcomeID,
                        'outcome' => $event->Outcome,
                        'ft_score' => $event->FinalOutcomeR1 . ":" . $event->FinalOutcomeR2,
                        'odd_value' => $event->Odd,
                        'isol_oddclassid' => $event->OddID,
                        'bet_pick' => $betPick,
                        'bet_type' => $couponResponse->BetType,
                    ];

                    array_push($betDetails, $couponDet);
                }
            }

            $cashoutFrom = $this->session->get("CASHOUT_FROM");

            $this->view->setVars([
                "betDetails" => $betDetails,
                'myBet' => $myBet,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports' => $this->navigation['sports'],
                'pageToLoad' => "CASHOUT",
                'selected' => 'cashout',
            ]);

            if ($cashoutFrom == "BET_HISTORY") {
                $this->view->pick('bet-history/show');
            } else if ($cashoutFrom == "TRANSACTIONS") {
                $this->view->pick('transactions/show');
            }

        } else {
            $this->flashSession->error($this->flashMessages('Login to access this page'));
            $this->response->redirect('login');
        }
    }

    public function confirmcashoutAction()
    {

        if (!$this->session->get('auth')) {

            $this->view->disable();
            $this->response->redirect('login');
            return;
        }

        $couponID = $this->request->getPost('coupon_id');
        $cashoutAmount = $this->request->getPost('cashout_amount');

        $payCashOutPayload = [
            "UserID" => $this->session->get('auth')['id'],
            "CouponID" => $couponID,
            "CashOutValue" => $cashoutAmount,
            "VerifyReceivedFields" => true,
            "UserBookmakerID" => $this->bookmarkerID,
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID,
        ];

        $payCashoutURL = $this->qaBaseURL . "/api/cashout/PayCouponCashOutV5";
        $cashoutResponse = $this->postToUrl($payCashOutPayload, $payCashoutURL);

        if ($cashoutResponse->ResultCode == 1) {

            $updateCouponStatusQuery = "UPDATE coupon SET `status`=50, `win`=100
				WHERE isolutions_couponid = '$couponID'";
            $updateCouponDetailsStatusQuery = "UPDATE coupon c INNER JOIN coupon_details s
				ON c.coupon_id=s.coupon_id SET s.status=50, s.win=100 WHERE
				c.isolutions_couponid = '$couponID'";

            $this->rawInsert($updateCouponStatusQuery);
            $this->rawInsert($updateCouponDetailsStatusQuery);
            $this->flashSession->success($this->flashMessages("Congratulations! You have successfully cashed out your bet for " . $cashoutAmount));
            return $this->response->redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/bet-history");
        } else {
            $this->flashSession->error($this->flashMessages("Cashout failed! Please try again later. If the problem persists, please get in touch with our support team."));
            return $this->response->redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/bet-history");
        }
    }

    public function filterAction()
    {

        $selection = $this->request->getPost('selection');
        if ($selection == "all") {
            $selection = "";
        }

        $this->session->set('selected-bet-history', $selection);
        return $this->response->redirect('/bet-history/' . $selection);
    }
}
