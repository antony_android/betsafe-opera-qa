<?php

class ResetpasswordController extends ControllerBase
{

    private $navigation;

    public function initialize()
    {
        $selectedSportId =  $this->session->get('selectedSportId');
        $this->navigation = $this->getNavigation($selectedSportId);
    }

    public function indexAction()
    {
        $id = $this->request->get('id', 'int');
        $selected = 'resetpassword';

        if ($id) {
            $user = $this->rawSelect("SELECT * FROM `profile` WHERE profile_id='$id' LIMIT 1");
            $user = $user['0'];

            $this->view->setVars([
                "msisdn" => $user['msisdn'],
                "profile_id" => $user['profile_id'],
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports'],
                'selected' => $selected
            ]);
        } else {
            $this->view->setVars([
                'selected' => $selected,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports']
            ]);
        }
    }

    public function codeAction()
    {
        $mobile = $this->request->getPost('mobile', 'int');

        if (!$mobile) {
            $mobile = $this->session->get("resetPwdMobile");
        }

        if (!$mobile) {

            $this->flashSession->error($this->flashMessages('Please enter your mobile number'));
            $this->response->redirect('change-password');

            $this->view->disable();
        } else {
            $mobile = $this->formatMobileNumber($mobile);
            $this->session->set("resetPwdMobile", $mobile);

            if (!$mobile) {
                $this->flashSession->error($this->flashMessages('Invalid mobile number'));
                $this->response->redirect('change-password');
                $this->view->disable();
            } else {

                $selected = 'reset-password-code';
                $payload = [
                    "Username" => $mobile,
                    "Email" => "",
                    "PhoneNumber" => "254".$mobile,
                    "VerifyReceivedFields" => true,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID
                ];

                $url = $this->qaBaseURL . "/api/users/TokenPasswordRecovery";
                $response = $this->postToUrl($payload, $url);

                $resultCode = $response->ResultCode;

                if ($resultCode == 1) {

                    $payload = [
                        "MobilePhone" => "254".$mobile,
                        "VerifyReceivedFields" => true,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID
                    ];

                    $url = $this->qaBaseURL . "/api/users/v2/GetUserInfoFromMobilePhoneV3";
                    $userInfoResponse = $this->postToUrl($payload, $url);

                    if ($userInfoResponse->ResultCode == 1) {

                        $verification_code = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 4);
                        $token = $response->Token;

                        $bindings = [
                            'msisdn' => $this->session->get('resetPwdMobile'),
                            'code' => $verification_code,
                            'token' => $token
                        ];

                        $query = "INSERT  INTO `password_reset` VALUES (NULL, NULL, :msisdn, :code, :token, NOW()) 
                        ON DUPLICATE KEY UPDATE code='$verification_code', token='$token'";
                        $this->rawQueries($query, $bindings);

                        $sms = "Your BetSafe password reset code is $verification_code. Use this to reset your password.";
                        $smsURL = $this->qaBaseURL . "/api/users/SendSMS";

                        $smsPayload = [
                            "SMSType" => 9,
                            "Text" => $sms,
                            "UserID" => $userInfoResponse->UserInfo->UserID,
                            "APIAccount" => $this->apiAccount,
                            "APIPassword" => $this->qaApiPassword,
                            "IDBookmaker" => $this->bookmarkerID
                        ];

                        $smsResponse = $this->postToUrl($smsPayload, $smsURL);

                        if ($smsResponse->ResultCode == 1) {
                            $message = "An SMS verification PIN code was sent to " . $mobile . ". Use the PIN to change your password.";

                            $this->view->setVars([
                                'selected' => $selected,
                                'topLeagues' => $this->navigation['topLeagues'],
                                'countries' => $this->navigation['countries'],
                                'sports'    => $this->navigation['sports'],
                                'message' => $message
                            ]);
                            $this->response->redirect('/resetpassword/inputcode');
                        } else {
                            $this->flashSession->error($this->flashMessages('An error occured and we could not send you a reset code! Please contact support.'));
                            $this->response->redirect('/resetpassword');
                        }
                    } else {

                        $this->flashSession->error($this->flashMessages('We could not retrieve your information. Please confirm the entered mobile number and try again.'));
                        $this->response->redirect('/resetpassword');
                    }
                } else {

                    $this->flashSession->error($this->flashMessages('An error occured and we could not send you a reset code! Please contact support.'));
                    $this->response->redirect('/resetpassword');
                }
            }
        }
    }

    public function inputcodeAction()
    {

        $mobile = $this->session->get("resetPwdMobile");

        if (!$mobile) {

            $this->flashSession->error($this->flashMessages('Please enter your mobile number'));
            $this->response->redirect('resetpassword');

            $this->view->disable();
        } else {
            $message = "An SMS verification PIN code was sent to " . $mobile . ". Use the PIN to change your password.";
            $selected = 'inputcode';
            $this->view->setVars([
                'selected' => $selected,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports'],
                'message' => $message
            ]);
        }
    }


    public function newAction()
    {
        $resetCode = $this->request->getPost('reset_code', 'int');

        if (!$resetCode) {
            $this->flashSession->error($this->flashMessages('Please enter the PIN code 
            sent to your mobile phone'));
            $this->response->redirect('resetpassword/code');

            $this->view->disable();
        } else {

            $msisdn = $this->session->get('resetPwdMobile');
            $resetCodeRS = $this->rawQueries("SELECT * FROM `password_reset` 
                WHERE `code`= '$resetCode' AND `msisdn` = '$msisdn'");

            if (count($resetCodeRS) == 0) {

                $this->flashSession->error($this->flashMessages('Invalid reset code. Please verify or request a new code!'));
                $this->response->redirect('/resetpassword');
                return;
            }

            $selected = 'create-new-password';

            $this->view->setVars([
                'selected' => $selected,
                'topLeagues' => $this->navigation['topLeagues'],
                'countries' => $this->navigation['countries'],
                'sports'    => $this->navigation['sports'],
                'token' => $resetCodeRS[0]['token'],
                'mobile' => $msisdn
            ]);
            $this->view->pick("resetpassword/new");
        }
    }

    public function passwordAction()
    {
        $password = $this->request->getPost('password');
        $repeatPassword = $this->request->getPost('repeatPassword');
        $token = $this->request->getPost('token');
        $mobile = $this->request->getPost('msisdn');

        if (!$password || !$repeatPassword) {
            $this->flashSession->error($this->flashMessages('Please specify a new password and 
            key it again in confirm password'));
            $this->response->redirect("resetpassword/new");

            $this->view->disable();
        } else {
            if ($password != $repeatPassword) {
                $this->flashSession->error($this->flashMessages('Passwords do not match'));
                $this->response->redirect("/resetpassword/new");
                $this->view->disable();
            } else {

                $payload = [
                    "Token" => $token,
                    "NewPassword" => $password,
                    "VerifyReceivedFields" => true,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID
                ];

                $url = $this->qaBaseURL . "/api/users/ResetPasswordWithToken";
                $response = $this->postToUrl($payload, $url);

                $resultCode = $response->ResultCode;
                if ($resultCode == 1) {

                    $payload = [
                        "Username" => $mobile,
                        "Password" => $password,
                        "ClientIP" => $_SERVER['REMOTE_ADDR'],
                        "ProviderID" => 0,
                        "VerifyReceivedFields" => true,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID
                    ];

                    $url = $this->qaBaseURL . "/api/users/LogOnUserV3";
                    $response = $this->postToUrl($payload, $url);

                    $resultCode = $response->ResultCode;

                    if ($resultCode == 1) {

                        $device = $this->getDevice();
                        $sessionData = [
                            'id'       => $response->UserInfo->UserID,
                            'sessionId' => $response->SessionID,
                            'balance' => number_format($response->Balance->Amount, 2),
                            'username' => $response->UserInfo->Username,
                            'bonus' => $response->Balance->BonusAmount,
                            'mobile'   => $mobile,
                            'device'   => $device
                        ];

                        $this->registerAuth($sessionData);
                        $this->flashSession->success($this->flashMessages('Your new Password was reset successfully. Make a deposit to start betting.'));
                        return $this->response->redirect('/');
                    } else if ($resultCode == -4 || $resultCode == -6) {
                        $this->flashSession->error("Invalid username or password. Please try again.");
                        $this->response->redirect("/login");
                        $this->view->disable();
                    } else if ($resultCode == -21) {
                        $this->flashSession->error("Your user account is disabled. Please contact the Support Team.");
                        $this->response->redirect("/");
                        $this->view->disable();
                    } else {
                        $this->flashSession->error("We could not log you in due to some unspecified reason. Please contact Support");
                        $this->response->redirect("/");
                        $this->view->disable();
                    }
                } else {
                    $this->flashSession->error($this->flashMessages('An error occured and your password was not changed. Please contact the Support Team!'));
                    $this->response->redirect('/resetpassword');
                }
            }
        }
    }
}
