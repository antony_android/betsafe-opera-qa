<?php

/** 
 * Class NavController
 */
class MenuController extends ControllerBase
{
    /**
     * 
     */
    public function IndexAction()
    {
        $id = $this->request->get('id', 'int');
        $id = $id ?: 79;

        $topLeagues = $this->rawSelect(
            "SELECT competition.competition_id, competition.competition_name as competition_name, 
            count(*) AS games_count, s.sport_name FROM competition
            INNER JOIN category ON category.category_id = competition.category_id
            INNER JOIN `match` ON `match`.competition_id = competition.competition_id 
            INNER JOIN sport s on s.sport_id = competition.sport_id
            WHERE `match`.start_time > now() GROUP BY competition.competition_id HAVING
            games_count > 0 ORDER BY competition.priority desc  limit 10"
        );

        $countries = $this->rawSelect(
            "SELECT category.category_id, category.category_name as country, 
            count(*) AS games_count FROM competition INNER JOIN category 
            ON category.category_id = competition.category_id INNER JOIN
            `match` ON `match`.competition_id = competition.competition_id
            WHERE `match`.start_time > now() GROUP BY category.category_id
            HAVING games_count > 0 ORDER BY category_name  ASC;"
        );

        $sports = $this->rawSelect(
            "SELECT sport.sport_id, sport.sport_name FROM sport ORDER BY sport_name  ASC;"
        );


        $this->view->setVars([
            'topLeagues' => $topLeagues,
            'sports' => $sports,
            'countries' => $countries,
        ]);
    }
}
