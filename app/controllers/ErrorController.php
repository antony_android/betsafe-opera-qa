<?php

/**
 * ErrorController
 */
class ErrorController extends \Phalcon\Mvc\Controller
{
    public function show404Action()
    {
        $this->response->setStatusCode(404, 'Not Found');
        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'error',
        ]);

        $this->view->pick('error/index');
    }
}