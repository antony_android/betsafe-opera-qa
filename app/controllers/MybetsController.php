<?php

class MybetsController extends ControllerBase
{
  public function IndexAction()
  {
    $betID = $this->request->get('id', 'int');

    if (!is_numeric($betID))
      $betID = 0;

    $id = $this->session->get('auth')['id'];

    if ($id) {

      $myBets = $this->rawSelect("SELECT b.bet_id,b.created, b.total_odd, jb.jackpot_bet_id, 
         total_games AS total_matches, b.bet_message,b.bet_amount,b.possible_win,b.raw_possible_win,
         b.status, IF(FIND_IN_SET(b.status,(SELECT group_concat(`status`) FROM bet_slip WHERE
         bet_id = b.bet_id)), b.status, 1) xstatus FROM bet b INNER JOIN bet_slip s ON b.bet_id
         =s.bet_id LEFT JOIN jackpot_bet jb ON jb.bet_id=b.bet_id WHERE b.profile_id=$id GROUP BY
         s.bet_id ORDER BY b.created DESC LIMIT 30;");

      $title = "Bet History";

      $this->tag->setTitle($title);

      $this->view->setVars(["myBets" => $myBets]);
    } else {
      $this->flashSession->error($this->flashMessages('You do not have permission to view this page'));
      $this->response->redirect('login?ref=mybets');
      // Disable the view to avoid rendering
      $this->view->disable();
    }
  }

  public function showAction()
  {

    $betID = $this->request->get('id', 'int');
    $myBet = $this->rawSelect("SELECT b.bet_id,b.created, total_games AS total_matches, 
        b.bet_message,b.bet_amount,b.possible_win,b.raw_possible_win, b.status, b.profile_id,
         IF(FIND_IN_SET(b.status,(SELECT group_concat(`status`) FROM bet_slip WHERE 
         bet_id = b.bet_id)), b.status, 1) xstatus FROM bet b INNER JOIN bet_slip s ON b.bet_id=
         s.bet_id WHERE b.bet_id='$betID' GROUP BY s.bet_id ORDER BY b.created DESC LIMIT 1");

    $myBet = $myBet['0'];
    $betDetails = $this->rawSelect("SELECT b.bet_id, b.created, m.start_time, b.bet_amount,
      possible_win, b.win, b.status, m.game_id, m.away_team, (SELECT winning_outcome FROM outcome
      WHERE sub_type_id=45 AND parent_match_id=s.parent_match_id AND is_winning_outcome =1 AND 
      s.live_bet = outcome.live_bet) AS ft_score, m.home_team, s.odd_value, s.sub_type_id, 
      s.bet_pick, group_concat(o.winning_outcome) AS winning_outcome, concat(t.name,' ',
      s.special_bet_value) AS bet_type FROM bet b INNER JOIN bet_slip s ON s.bet_id = b.bet_id 
      INNER JOIN `match` m ON m.parent_match_id = s.parent_match_id INNER JOIN odd_type t ON 
      (t.parent_match_id = s.parent_match_id AND s.sub_type_id=t.sub_type_id) AND s.live_bet = 
      t.live_bet LEFT JOIN outcome o ON o.parent_match_id = s.parent_match_id AND s.sub_type_id = 
      o.sub_type_id AND s.special_bet_value = o.special_bet_value AND s.live_bet = o.live_bet AND 
      o.is_winning_outcome = 1 WHERE s.bet_id = '$betID' GROUP BY s.bet_slip_id, s.special_bet_value 
      ORDER BY b.bet_id DESC");

    $this->view->setVars(["betDetails" => $betDetails, 'myBet' => $myBet]);
  }
}
