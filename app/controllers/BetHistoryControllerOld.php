<?php

/**
 * Copyright (c) Antony 2021.
 *
 * All rights reserved.
 */

class BetHistoryController extends ControllerBase
{

	private $navigation;

	public function initialize()
	{
		$selectedSportId =  $this->session->get('selectedSportId');
		$this->navigation = $this->getNavigation($selectedSportId);
	}

	public function indexAction()
	{

		$today = date('Y-m-d H:i:s', strtotime(' -3 hours'));
		$monthAgo = date_create(date('Y-m-d H:i:s', strtotime(' -3 hours')));
		date_sub($monthAgo, date_interval_create_from_date_string("30 days"));
		$monthAgo = date_format($monthAgo, "Y-m-d H:i:s");

		$payload = [
			"UserID" => $this->session->get('auth')['id'],
			"StartDate" => $monthAgo,
			"EndDate" => $today,
			"IDCouponResult" => 0,
			"LanguageID" => 2,
			"VerifyReceivedFields" => true,
			"UserBookmakerID" => $this->bookmarkerID,
			"APIAccount" => $this->apiAccount,
			"APIPassword" => $this->qaApiPassword,
			"IDBookmaker" => $this->bookmarkerID
		];

		$url = $this->qaBaseURL . "/api/users/v2/GetBetsListV4";
		$response = $this->postToUrl($payload, $url);

		$resultCode = $response->ResultCode;
		$coupons = [];

		if ($resultCode == 1) {

			$coupons = $response->BetsList;

			foreach ($coupons as $coupon) {

				$dateInserted = strtotime($coupon->Data);
				$localDate = date('Y-m-d H:i:s', $dateInserted + (3600*3));

				$couponQuery = "INSERT INTO `coupon` VALUES(NULL, '" . $response->UserID . "',
					'" . $coupon->IDCoupon . "', NULL, '" . $coupon->IDBetType . "', NULL,
					NULL, '" . $coupon->Amount . "', NULL, '" . $coupon->PotentialWinning . "',
					'" . $coupon->PotentialWinning . "', '" . $coupon->PotentialWinning . "',
					'" . $coupon->IDCouponState . "', '" . $coupon->IDCouponResult . "',
					 '" . $coupon->UserWinnings . "', '" . $coupon->IDCouponResult . "',
					 '" . $coupon->CouponResult . "', NULL, NULL, 
					'" . $coupon->Coupon . "', 'ISOLUTIONS-API', '".$localDate ."',
					 NOW(),0,0.00) ON DUPLICATE KEY UPDATE `status`='".$coupon->IDCouponState."',
					 win='".$coupon->IDCouponResult."', user_winnings='".$coupon->UserWinnings."',
					 id_coupon_result='" . $coupon->IDCouponResult . "', 
					 coupon_result='".$coupon->CouponResult."'";

				$couponLocalID = $this->rawInsert($couponQuery);

				if($couponLocalID < 1 || is_null($couponLocalID)){

					$couponResult = $this->rawQueries("SELECT * FROM coupon WHERE 
                                    `isolutions_couponid` = '" . $coupon->IDCoupon . "'");
						if(!is_null($couponResult) && !empty($couponResult)){

							$couponLocalID = $couponResult[0]['coupon_id'];
						}
				}

				$checkCashOutPayload = [
					"UserID" => $response->UserID,
					"CouponID" => $coupon->IDCoupon,
					"VerifyReceivedFields" => true,
					"UserBookmakerID" => $response->UserBookmakerID,
					"APIAccount" => $this->apiAccount,
					"APIPassword" => $this->qaApiPassword,
					"IDBookmaker" => $this->bookmarkerID
				];

				$checkCashoutURL = $this->qaBaseURL . "/api/cashout/CheckCashOutV3";
				$cashoutResponse = $this->postToUrl($checkCashOutPayload, $checkCashoutURL);

				if ($cashoutResponse->ResultCode == 1) {

					$cashoutEnabled = 0;

					if($cashoutResponse->Enabled){
						$cashoutEnabled = 1;
					}
					$cashoutQuery = "UPDATE coupon SET cashout_enabled='".$cashoutEnabled."' WHERE 
						isolutions_couponid = '" . $coupon->IDCoupon . "'";
					$this->rawInsert($cashoutQuery);
				}

				$couponDetailsPayload = [
					"CouponCode" => $coupon->Coupon,
					"LanguageID" => 2,
					"UserID" => $response->UserID,
					"UserBookmakerID" => $response->UserBookmakerID,
					"APIAccount" => $this->apiAccount,
					"APIPassword" => $this->qaApiPassword,
					"IDBookmaker" => $this->bookmarkerID
				];

				$detailsURL = $this->qaBaseURL . "/api/coupon/CouponDetailV10";
				$couponResponse = $this->postToUrl($couponDetailsPayload, $detailsURL);

				$couponEvents = [];
				if ($couponResponse->ResultCode == 1) {

					$couponEvents = $couponResponse->Events;
					$totalGamesInBet = count($couponEvents);

					foreach ($couponEvents as $event) {

						$pendingBet = 0;
						$rebetAvailable = 0;
						$isCashout = 0;

						if (
							!is_null($couponResponse->PendingBet) &&
							strlen(trim($couponResponse->PendingBet)) > 0
						) {
							$pendingBet = $couponResponse->PendingBet;
						}
						if (
							!is_null($couponResponse->RebetAvailable) &&
							strlen(trim($couponResponse->RebetAvailable)) > 0
						) {
							$rebetAvailable = $couponResponse->RebetAvailable;
						}
						if (
							!is_null($couponResponse->IsCashout) && $couponResponse->IsCashout) {
							$isCashout = 1;
						}

						if(is_null($isCashout) || strlen(trim($isCashout)) == 0 || !$couponResponse->IsCashout){

							$isCashout = 0;
						}

						$subTypeId = 0;
						$subTypeResult = $this->rawQueries("SELECT * FROM event_odd WHERE 
                                    betradar_odd_id = '" . $event->OddID . "'");
						if(!is_null($subTypeResult) && !empty($subTypeResult)){

							$subTypeId = $subTypeResult[0]['sub_type_id'];
						}

						$isCashout = intval($isCashout);

						$couponDetailsQuery = "INSERT INTO coupon_details VALUES (NULL, 
							'" . $event->SubEventID . "', '$couponLocalID',
							 '" . $couponResponse->BetType . "',
							'1', '" . $couponResponse->PendingBetState . "', '".$isCashout."',
							 '".$rebetAvailable."', NULL, '" . $event->HND . "',
							'$totalGamesInBet', '" . $event->Odd . "', '".$event->OutcomeID."',
							 '".$event->Outcome."', 0, 0, NOW(), NOW(),
							'" . $event->ResultStateID . "', '" . $event->FinalOutcomeR1 . "',
							 '" . $event->FinalOutcomeR2 . "', 
							 '" . $subTypeId . "',
							'".$event->OddClassID."') ON DUPLICATE KEY UPDATE 
							pending_bet=" . $pendingBet . ",
							pending_bet_state='" . $couponResponse->PendingBetState . "',
							is_cashout=" . $isCashout . ",
							rebet_available=" . $rebetAvailable . ",
							bet_pick='" . $event->OddType . "',
							win='" . $event->OutcomeID . "',
							outcome='" . $event->Outcome . "',
							home_result='" . $event->FinalOutcomeR1 . "',
							away_result='" . $event->FinalOutcomeR2 . "'";

						$this->rawInsert($couponDetailsQuery);
					}
				}
			}
		}

		$betID = $this->request->get('id', 'int');

		if (!is_numeric($betID)) {
			$betID = 0;
		}

		$id = $this->session->get('auth')['id'];

		if ($id) {

			$myBets = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid,b.created,
			  b.total_odd,s.total_games AS total_matches, (SELECT COUNT(*) FROM coupon_details cd
			  INNER JOIN coupon c ON cd.coupon_id = c.coupon_id WHERE cd.`win`=3 AND 
			  c.isolutions_userid = '$id' AND cd.coupon_id=s.coupon_id) AS settled, 
			  b.bet_amount, b.possible_win,b.user_winnings, b.status, b.win, b.coupon_result 
			  FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id 
			  WHERE b.isolutions_userid='$id' GROUP BY s.coupon_id ORDER BY 
			  b.created DESC LIMIT 30;");

			$title = "Bet History";

			$this->tag->setTitle($title);

			$this->view->setVars([
				"myBets" => $myBets,
				'selected' => 'bet-history',
				'topLeagues' => $this->navigation['topLeagues'],
				'countries' => $this->navigation['countries'],
				'sports'    => $this->navigation['sports'],
				'selectedBets' => 'all'
			]);

			if ($betID != 0) {
				$myBet = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid,b.created, 
					 s.total_games AS total_matches, b.cashout_enabled, b.total_odd, b.bet_amount,
					 b.possible_win,b.user_winnings, b.status, b.isolutions_userid, b.win, b.is_void
					  FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id WHERE 
					 b.isolutions_couponid='$betID' GROUP BY s.coupon_id ORDER BY b.created 
					 DESC LIMIT 40");

				$myBet = $myBet[0];

				$betDetails = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid, b.created,
					 m.start_time, b.bet_amount,possible_win, b.user_winnings, s.win, b.status, 
					 m.away_team, c.competition_name, ct.category_name, 
					 CONCAT(CONCAT(s.home_result, ':'), s.away_result) AS ft_score, m.home_team,
					 s.odd_value, s.isol_oddclassid, s.bet_pick, s.void_factor, s.bet_type FROM coupon b 
					 INNER JOIN coupon_details s ON s.coupon_id = b.coupon_id 
					 INNER JOIN `match` m ON m.parent_match_id = s.parent_match_id 
					 INNER JOIN `competition` c ON m.competition_id = c.competition_id 
					 INNER JOIN `category` ct ON c.category_id = ct.category_id 
					 WHERE b.isolutions_couponid = '$betID' GROUP BY s.coupon_details_id, 
					 s.special_bet_value ORDER BY b.coupon_id DESC");

				$this->view->setVars([
					"betDetails" => $betDetails,
					'myBet' => $myBet,
					'topLeagues' => $this->navigation['topLeagues'],
					'countries' => $this->navigation['countries'],
					'sports'    => $this->navigation['sports'],
					'selectedBets' => 'all'
				]);
				$this->view->pick("bet-history/details");
			}
		} else {
			$this->flashSession->error($this->flashMessages('Login to access this page'));
			$this->response->redirect('login');
		}
	}


	public function openAction()
	{
		$betID = $this->request->get('id', 'int');

		if (!is_numeric($betID)) {
			$betID = 0;
		}

		$id = $this->session->get('auth')['id'];

		if ($id) {

			$where = " AND b.status=10 ";

			$myBets = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid,b.created,
			 b.total_odd,s.total_games AS total_matches, 
			  b.bet_amount, b.possible_win,b.user_winnings, b.status, b.win, b.coupon_result 
			  FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id 
			  WHERE b.isolutions_userid='$id' " . $where . " GROUP BY s.coupon_id ORDER BY 
			  b.created DESC LIMIT 30;");

			$title = "Bet History";

			$this->tag->setTitle($title);

			$this->view->setVars([
				"myBets" => $myBets, 'selected' => 'bet-history',
				'topLeagues' => $this->navigation['topLeagues'],
				'countries' => $this->navigation['countries'],
				'sports'    => $this->navigation['sports'],
				'selectedBets' => 'open'
			]);
			$this->view->pick("bet-history/index");

			if ($betID != 0) {

				$myBet = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid,b.created, 
					s.total_games AS total_matches, b.total_odd, b.cashout_enabled b.bet_amount,
					b.possible_win, b.user_winnings, b.status, b.isolutions_userid, b.win, b.is_void
					 FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id WHERE 
					b.isolutions_couponid='$betID' GROUP BY s.coupon_id ORDER BY b.created 
					DESC LIMIT 40;");

				$myBet = $myBet['0'];

				$betDetails = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid, b.created,
					 m.start_time, b.bet_amount,possible_win, b.user_winnings, s.win, b.status, 
					 m.away_team, c.competition_name, ct.category_name, 
					 CONCAT(CONCAT(s.home_result, ':'), s.away_result) AS ft_score, m.home_team,
					 s.odd_value, s.isol_oddclassid, s.bet_pick, s.void_factor, s.bet_type FROM coupon b 
					 INNER JOIN coupon_details s ON s.coupon_id = b.coupon_id 
					 INNER JOIN `match` m ON m.parent_match_id = s.parent_match_id 
					 INNER JOIN `competition` c ON m.competition_id = c.competition_id 
					 INNER JOIN `category` ct ON c.category_id = ct.category_id 
					 WHERE b.isolutions_couponid = '$betID' GROUP BY s.coupon_details_id, 
					 s.special_bet_value ORDER BY b.coupon_id DESC");

				$this->view->setVars([
					"betDetails" => $betDetails,
					'topLeagues' => $this->navigation['topLeagues'],
					'countries' => $this->navigation['countries'],
					'sports'    => $this->navigation['sports'],
					'myBet' => $myBet, 'selectedBets' => 'open'
				]);
				$this->view->pick("bet-history/details");
			}
		}
	}

	public function settledAction()
	{
		$betID = $this->request->get('id', 'int');

		if (!is_numeric($betID)) {
			$betID = 0;
		}

		$id = $this->session->get('auth')['id'];

		if ($id) {

			$where = " AND b.status=50 ";

			$myBets = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid,b.created,
			 b.total_odd,s.total_games AS total_matches,  
			  b.bet_amount, b.possible_win,b.user_winnings, b.status, b.win, b.coupon_result 
			  FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id 
			  WHERE b.isolutions_userid='$id' " . $where . " GROUP BY s.coupon_id ORDER BY 
			  b.created DESC LIMIT 30;");

			$title = "Bet History";

			$this->tag->setTitle($title);

			$this->view->setVars([
				"myBets" => $myBets,
				'selected' => 'bet-history',
				'topLeagues' => $this->navigation['topLeagues'],
				'countries' => $this->navigation['countries'],
				'sports'    => $this->navigation['sports'],
				'selectedBets' => 'settled'
			]);
			$this->view->pick("bet-history/index");

			if ($betID != 0) {

				$myBet = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid,b.created, 
					s.total_games AS total_matches, b.total_odd, b.cashout_enabled, b.bet_amount,
					b.possible_win,b.user_winnings, b.status, b.isolutions_userid, b.win, b.is_void
					 FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id WHERE 
					b.isolutions_couponid='$betID' GROUP BY s.coupon_id ORDER BY b.created 
					DESC LIMIT 40;");

				$myBet = $myBet['0'];

				$betDetails = $this->rawQueries("SELECT b.coupon_id, b.isolutions_couponid, b.created,
					 m.start_time, b.bet_amount,possible_win, b.user_winnings, s.win, b.status, 
					 m.away_team, c.competition_name, ct.category_name, 
					 CONCAT(CONCAT(s.home_result, ':'), s.away_result) AS ft_score, m.home_team,
					 s.odd_value, s.isol_oddclassid, s.bet_pick, s.void_factor, s.bet_type FROM coupon b 
					 INNER JOIN coupon_details s ON s.coupon_id = b.coupon_id 
					 INNER JOIN `match` m ON m.parent_match_id = s.parent_match_id 
					 INNER JOIN `competition` c ON m.competition_id = c.competition_id 
					 INNER JOIN `category` ct ON c.category_id = ct.category_id 
					 WHERE b.isolutions_couponid = '$betID' GROUP BY s.coupon_details_id, 
					 s.special_bet_value ORDER BY b.coupon_id DESC");

				$this->view->setVars([
					"betDetails" => $betDetails,
					'myBet' => $myBet,
					'topLeagues' => $this->navigation['topLeagues'],
					'countries' => $this->navigation['countries'],
					'sports'    => $this->navigation['sports'],
					'selectedBets' => 'settled'
				]);
				$this->view->pick("bet-history/details");
			}
		}
	}


	public function showAction()
	{
		$betID = $this->request->get('id', 'int');

		$myBet = $this->rawSelect("SELECT b.coupon_id, b.isolutions_couponid,b.created, 
			s.total_games AS total_matches, b.total_odd, b.cashout_enabled, b.bet_amount,
			b.possible_win,b.user_winnings, b.status, b.isolutions_userid, b.win, b.is_void
			 FROM coupon b INNER JOIN coupon_details s ON b.coupon_id=s.coupon_id WHERE 
			b.isolutions_couponid='$betID' GROUP BY s.coupon_id ORDER BY 
			b.created DESC LIMIT 1");

		$myBet = $myBet['0'];

		$betDetails = $this->rawSelect("SELECT b.coupon_id, b.isolutions_couponid, b.created,
			m.start_time, b.bet_amount,possible_win, b.user_winnings, s.win, b.status, 
			m.away_team, c.competition_name, ct.category_name, 
			CONCAT(CONCAT(s.home_result, ':'), s.away_result) AS ft_score, m.home_team,
			s.odd_value, s.isol_oddclassid, s.bet_pick, s.void_factor, s.bet_type FROM coupon b 
			INNER JOIN coupon_details s ON s.coupon_id = b.coupon_id 
			INNER JOIN `match` m ON m.parent_match_id = s.parent_match_id 
			INNER JOIN `competition` c ON m.competition_id = c.competition_id 
			INNER JOIN `category` ct ON c.category_id = ct.category_id 
			WHERE b.isolutions_couponid = '$betID' GROUP BY s.coupon_details_id, 
			s.special_bet_value ORDER BY b.coupon_id DESC");

		$this->view->setVars([
			"betDetails" => $betDetails,
			'myBet' => $myBet,
			'topLeagues' => $this->navigation['topLeagues'],
			'countries' => $this->navigation['countries'],
			'sports'    => $this->navigation['sports'],
			'selected' => 'bet-details'
		]);
	}

	public function cashoutAction()
	{
		$betID = $this->request->get('id', 'int');

		$myBet = $this->rawSelect("SELECT * FROM coupon WHERE isolutions_couponid='$betID'");
		$myBet = $myBet['0'];

		$this->view->setVars([
			'myBet' => $myBet,
			'topLeagues' => $this->navigation['topLeagues'],
			'countries' => $this->navigation['countries'],
			'sports'    => $this->navigation['sports'],
			'selected' => 'bet-cashout'
		]);
	}

	public function confirmcashoutAction()
	{

		if (!$this->session->get('auth')) {

			$this->view->disable();
			$this->response->redirect('login');
			return;
		}

		$couponID = $this->request->get('coupon_id', 'int');
		$cashoutAmount = $this->request->get('cashout_amount', 'float');

		$payCashOutPayload = [
			"UserID" => $this->session->get('auth')['id'],
			"CouponID" => $couponID,
			"CashOutValue" => $cashoutAmount,
			"VerifyReceivedFields" => true,
			"UserBookmakerID" => $this->bookmarkerID,
			"APIAccount" => $this->apiAccount,
			"APIPassword" => $this->qaApiPassword,
			"IDBookmaker" => $this->bookmarkerID
		];

		$payCashoutURL = $this->qaBaseURL . "/api/cashout/PayCouponCashOutV5";
		$cashoutResponse = $this->postToUrl($payCashOutPayload, $payCashoutURL);

		if ($cashoutResponse->ResultCode == 1) {

			$updateCouponStatusQuery = "UPDATE coupon SET `status`=50, `win`=100 
				WHERE isolutions_couponid = '$couponID'";
			$updateCouponDetailsStatusQuery = "UPDATE coupon c INNER JOIN coupon_details s 
				ON c.coupon_id=s.coupon_id SET s.status=50, s.win=100 WHERE 
				c.isolutions_couponid = '$couponID'";

			$this->rawInsert($updateCouponStatusQuery);
			$this->rawInsert($updateCouponDetailsStatusQuery);
			$this->flashSession->success($this->flashMessages("Congratulations! You have successfully cashed out your bet for " . $cashoutAmount));
			return $this->response->redirect('/bet-history');
		} else {
			$this->flashSession->error($this->flashMessages("Cashout failed! Please try again later. If the problem persists, please get in touch with our support team."));
			return $this->response->redirect('/bet-history');
		}
	}

	public function filterAction(){

        $selection = $this->request->getPost('selection');
        if($selection == "all"){
            $selection = "";
        }
		
		$this->session->set('selected-bet-history', $selection);
        $this->response->redirect('/bet-history/'.$selection);
    }
}
