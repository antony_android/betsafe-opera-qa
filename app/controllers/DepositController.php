<?php

class DepositController extends ControllerBase
{

    const MPESA_PROVIDERID = 340;

    public function indexAction()
    {

        if ($this->session->get('auth')) {

            $theBetslip = $this->session->get("betslip");
            $selectedSportId = $this->session->get('selectedSportId');
            $navigation = $this->getNavigation($selectedSportId);
            $this->session->set("referred_by", null);

            $this->view->setVars([
                'theBetslip' => $theBetslip,
                'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
                'topLeagues' => $navigation['topLeagues'],
                'countries' => $navigation['countries'],
                'sports' => $navigation['sports'],
                'referrer' => $this->request->getHTTPReferer(),
                'selected' => 'deposit',
            ]);
        } else {

            $this->session->set("referred_by", "/deposit");
            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function topupAction()
    {
        if ($this->session->get('auth')) {
            $amount = $this->request->getPost('amount', 'int');

            if ($amount < 10) {
                $this->flashSession->error($this->flashMessages('Sorry, minimum top up amount is KSH. 10'));
                $this->view->disable();
                return $this->response->redirect('deposit');
            } elseif ($amount > 10000) {
                $this->flashSession->error($this->flashMessages('Sorry, maximum top up amount is KSH. 10000'));
                $this->view->disable();
                return $this->response->redirect('deposit');
            } else {
                $id = $this->session->get('auth')['id'];

                $payLoad = [
                    "userId" => $id,
                    "amount" => $amount,
                    "accessToken" => $this->generateMoneyAccessToken(),
                ];

                $stkInProgress = $this->session->get("STKInProgress" . $id);

                if (!isset($stkInProgress) || is_null($stkInProgress)) {

                    $response = $this->topupThirdParty(DepositController::MPESA_PROVIDERID, $payLoad);
                    $this->session->set("STKInProgress" . $id, $id);
                }

                if (!is_null($response)) {
                    if ($response->resultCode == 1) {

                        $userId = $this->session->get('auth')['id'];
                        $mobile = $this->session->get('auth')['mobile'];

                        $this->flashSession->success($this->flashSuccess("Please authorize the request sent to your phone"));
                        $this->view->disable();
                        $this->session->set("InitiatedDeposit_" . $userId, $mobile);

                        $this->response->redirect('/');
                    } else if ($response->resultCode == 5) {
                        $this->flashSession->error($this->flashSuccess($response->resultDescription));
                        $this->view->disable();
                        $this->response->redirect('/deposit');
                    } else {
                        $this->flashSession->error($this->flashSuccess("Deposit failed. Please try again later"));
                        $this->view->disable();
                        $this->response->redirect('/deposit');
                    }
                } else {
                    $this->flashSession->error($this->flashSuccess("Deposit failed. Please try again later"));
                    $this->view->disable();
                    $this->response->redirect('/deposit');
                }
                $this->session->set("STKInProgress" . $id, null);

            }
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function fortynineAction()
    {

        $this->session->set('depositAmt', 49);
        return $this->response->redirect('/deposit');
    }

    public function ninetynineAction()
    {

        $this->session->set('depositAmt', 99);
        return $this->response->redirect('/deposit');
    }

    public function oneninetynineAction()
    {

        $this->session->set('depositAmt', 199);
        return $this->response->redirect('/deposit');
    }

    public function fourninetynineAction()
    {

        $this->session->set('depositAmt', 499);
        return $this->response->redirect('/deposit');
    }
}