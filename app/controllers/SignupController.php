<?php

class SignupController extends ControllerBase
{

    private $navigation;

    public function initialize()
    {
        $this->tag->setTitle('Sign Up');
        $selectedSportId =  $this->session->get('selectedSportId');
        $this->navigation = $this->getNavigation($selectedSportId);
    }

    public function indexAction()
    {

        $selected = 'signup';
        $this->view->setVars([
            'selected' => $selected,
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports'    => $this->navigation['sports']
        ]);
    }

    public function joinAction()
    {

        if ($this->request->isPost()) {

            $mobile = $this->request->getPost('mobile');
            $password = $this->request->getPost('password');
            $repeatPassword = $this->request->getPost('confpassword');

            if (!$mobile || !$password || !$repeatPassword) {
                $this->flashSession->error($this->flashMessages('All fields are required'));
                $this->response->redirect('signup');

                $this->view->disable();
            } else {

                if ($password != $repeatPassword) {
                    $this->flashSession->error($this->flashMessages('Passwords do not match'));
                    $this->response->redirect('signup');

                    $this->view->disable();
                }

                $mobile = $this->formatMobileNumber($mobile);

                if (!$mobile) {

                    $this->flashSession->error($this->flashMessages('Invalid Phone Number'));
                    $this->response->redirect('signup');
                    $this->view->disable();
                }

                $payload = [
                    "Username" => $mobile,
                    "Password" => $password,
                    "ProfileCode" => "User_Web_(StartupSystems)",
                    "PasswordReport" => "",
                    "FatherID" => -1,
                    "MessageLanguageID" => 2,
                    "TestUser" => 0,
                    "UserDocumentTypeID" => 0,
                    "UserDocumentTypeNumber" => "",
                    "UserInfo" => [
                        "UserID" => 0, "FatherID" => 0, "StateUser" => 0,
                        "Birthdate" => "1900-01-01T00:00:00",
                        "Address" => "", "City" => "", "Country" => 114,
                        "Sex" => "", "Email" => "", "Phone" => "254".$mobile,
                        "Zip" => "", "Currency" => 31, "UserType" => 0,
                        "TestUser" => 0, "NCell" => "254".$mobile, "Province" => "",
                        "SecQuestion" => -1, "SecAnswer" => ""
                    ],
                    "CommissionTemplate" => "",
                    "VerifyReceivedFields" => true,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID
                ];

                $payloadJSON = json_encode($payload);

                $url = $this->qaBaseURL . "/api/users/RegisterUserWithProfileCode";
                $httpRequest = curl_init($url);

                curl_setopt($httpRequest, CURLOPT_POST, TRUE);
                curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $payloadJSON);
                curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, FALSE);
                $results = curl_exec($httpRequest);
                $decodedResults = json_decode($results);
                $resultCode = $decodedResults->ResultCode;
                curl_close($httpRequest);

                if ($resultCode == 1) {

                    $sessionData = [
                        'id' => $decodedResults->UserID,
                        'mobile'   => $mobile,
                        'balance' => 0.00
                    ];
                    $exp = time() + (3600 * 24 * 5);
                    $this->registerAuth($sessionData, $exp);

                    $sms = "Welcome to Betsafe, home of great Sportsbook & Live Betting. Make a deposit to start betting.";

                    $smsPayload = [
                        "SMSType" => 1,
                        "Text" => $sms,
                        "UserID" => $decodedResults->UserID,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID
                    ];

                    $smsURL = $this->qaBaseURL . "/api/users/SendSMS";
                    $this->postToUrl($smsPayload, $smsURL);

                    $this->session->set("RegisteredUser_".$mobile, $mobile);

                    $this->flashSession->success($this->flashMessages("Welcome to Betsafe, home of great Sportsbook & Live Betting. Make a deposit to start betting."));
                    $this->view->disable();
                    $this->response->redirect('/');
                }else if(strpos($decodedResults->ResultDescription, 'already exist') !== false){
                    $this->flashSession->error($this->flashMessages("Mobile number already in use."));
                    $this->view->disable();
                    $this->response->redirect('signup');
                }else {
                    $this->flashSession->error($this->flashMessages("Unable to register your account. Please try again later."));
                    $this->view->disable();
                    $this->response->redirect('signup');
                }
            }
        }
    }
}
