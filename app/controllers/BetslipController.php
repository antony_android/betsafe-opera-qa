<?php

use Phalcon\Http\Response;

class BetslipController extends ControllerBase
{
    public function indexAction()
    {
        $stake = $this->session->get('stake');
        $betslip = $this->session->get("betslip") ?: [];
        $newslip = [];
        $totalOdd = 1;
        foreach ($betslip as $match_id => $slip) {
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_type = $slip['bet_type'];
            if ($bet_type == 'live') {
                $query = "SELECT lo.odd_value, l.status AS m_active, lo.odd_active FROM "
                    . " live_odds lo INNER JOIN live_match l USING(parent_match_id) WHERE "
                    . " l.parent_match_id='$parent_match_id' AND lo.sub_type_id='$sub_type_id' "
                    . " AND lo.odd_active = '1' AND l.status = '1'
                     AND lo.special_bet_value='$special_bet_value' AND lo.odd_key='$bet_pick' "
                    . " ORDER BY lo.created DESC LIMIT 1 ";
            } else {
                $query = "SELECT odd_value, 1 as odd_active FROM event_odd WHERE "
                    . "parent_match_id='$parent_match_id' AND sub_type_id='$sub_type_id' "
                    . " AND special_bet_value='$special_bet_value' AND odd_key='$bet_pick'";
            }

            $data = $this->rawQueries($query);
            if (
                !empty($data) &&
                $data[0]['odd_active'] == 1
            ) {
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            } else {
                $slip['odd_value'] = 1;
                $slip['market_status'] = 'Market Disabled';
                $newslip[$match_id] = $slip;
            }
            $totalOdd *= $slip['odd_value'];
        }

        $totalOdd = round($totalOdd, 2);
        $betslip = $newslip;
        $this->session->set('betslip', $betslip);

        $userFreeBets = [];
        $balance = 0;

        if ($this->session->get("auth")) {

            $payload = [
                "UserID" => $this->session->get("auth")['id'],
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $url = $this->qaBaseURL . "/api/users/GetBalanceFull";
            $response = $this->postToUrl($payload, $url);
            $balance = number_format($response->Balance->Amount, 2);

            $freebetsPayload = [

                "UserID" => $this->session->get("auth")['id'],
                "ApplicationType" => 1,
                "APIAccount" => $this->apiAccount,
                "APIPassword" => $this->qaApiPassword,
                "IDBookmaker" => $this->bookmarkerID,
            ];

            $freebetsPayloadUrl = $this->qaBaseURL . "/api/bonuscampaigns/GetFreebetsByUser";
            $freebetsResponse = $this->postToUrl($freebetsPayload, $freebetsPayloadUrl);

            if ($freebetsResponse->ResultCode == 1) {

                $freebets = $freebetsResponse->Freebets;

                if (count($freebets) > 0) {
                    foreach ($freebets as $bet) {

                        $placementRules = $bet->PlacementRules;
                        $maxPayout = 0;
                        $minSelectionOdds = 1;
                        $minNumSelections = 1;
                        $minTotalOdds = 1;

                        foreach ($placementRules as $rule) {

                            if ($rule->Code == "@MaxPayout") {
                                $amountArr = explode(",", $rule->Value);
                                $maxPayout = $amountArr[1];
                            } else if ($rule->Code == "@MinOddsPerSelection") {
                                $minSelectionOdds = $rule->Value;
                            } else if ($rule->Code == "@MinSelections") {
                                $minNumSelections = $rule->Value;
                            } else if ($rule->Code == "@MinTotalOdds") {
                                $minTotalOdds = $rule->Value;
                            }
                        }

                        $totalOdds = 1;
                        $minSelections = 0;

                        foreach ($betslip as $slip) {

                            if ($minSelectionOdds < $slip['odd_value']) {
                                $minSelections++;
                                $totalOdds *= $slip['odd_value'];
                            }

                        }

                        if ($minTotalOdds <= $totalOdds && $minNumSelections <= $minSelections) {

                            array_push($userFreeBets,
                                [
                                    "freebetId" => $bet->FreebetID,
                                    "name" => $bet->Description,
                                    "amount" => $bet->Amount,
                                    "maxPayout" => $maxPayout,
                                    "minSelectionOdds" => $minSelectionOdds,
                                    "minNumSelections" => $minNumSelections,
                                    "minTotalOdds" => $minTotalOdds,
                                ]);
                        }
                    }
                }
            }

        }

        $count = sizeof($betslip);

        if (!$this->session->has('oddschange') || is_null($this->session->get('oddschange'))) {

            $this->session->set('oddschange', "Yes");
        }
        $this->view->setVars([
            'stake' => $stake,
            'betslip' => $betslip,
            'selected' => 'betslip',
            'freebets' => $userFreeBets,
            'totalOdd' => $totalOdd,
            'balance' => $balance,
            'slipCount' => $count,
            'boost' => $this->calculateMultibetBoost(),
        ]);
    }

    public function stakeAction()
    {
        $stake = $this->request->get('stake') ?: 50;

        if ($stake < 50) {
            $stake = 50;
        }

        $this->session->set('stake', $stake);

        $this->flashSession->error($this->flashSuccess('Possible win updated'));

        $this->response->redirect('betslip');
        // Disable the view to avoid rendering
        $this->view->disable();
    }

    private function calculateMultibetBoost()
    {

        $percentBoost = 0;
        $slipCount = 0;

        if ($this->session->has("betslip")) {
            $betslip = $this->session->get("betslip");
            foreach ($betslip as $slip) {

                if ($slip["odd_value"] >= 1.2) {
                    $slipCount++;
                }
            }
            if ($slipCount >= 4) {

                $boostSet = $this->rawQueries("SELECT * FROM multibet_boost WHERE selections = '$slipCount'");
                if (!is_null($boostSet) && !empty($boostSet)) {

                    $percentBoost = $boostSet[0]['percent_boost'];
                }
            }

        }

        return intVal($percentBoost);
    }

    public function addAction()
    {

        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];

            $timeout = $this->rawQueries("SELECT * FROM `gaming_timeout`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            $setTimeout = [];
            $timeRemaining = null;

            if (!empty($timeout)) {
                $setTimeout = $timeout[0]['timeout_period'];
                $createdAt = $timeout[0]['created'];
                $inSeconds = strtotime($createdAt);
                $timeoutSeconds = 0;

                if ($setTimeout == "24 Hours") {
                    $timeoutSeconds = 24 * 3600;
                } elseif ($setTimeout == "48 Hours") {
                    $timeoutSeconds = 24 * 3600 * 2;
                } elseif ($setTimeout == "1 Week") {
                    $timeoutSeconds = 24 * 3600 * 7;
                }

                $expectedEndDateInSeconds = $inSeconds + $timeoutSeconds;
                $remainingTimeInSeconds = $expectedEndDateInSeconds - strtotime(date('Y-m-d H:i:s'));
                $timeRemaining = ResponsibleController::fromSeconds($remainingTimeInSeconds);

                $data = [
                    'status' => 410,
                    'message' => "You have chosen to Time-Out for a
                    period " . $setTimeout . " Time remaining " . $timeRemaining,
                ];

                $response = new Response();
                $response->setStatusCode(201, "OK");
                $response->setHeader("Content-Type", "application/json");

                $response->setContent(json_encode($data));
                return $response;
            }
        }

        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid', 'int');
        $pos = $this->request->getPost('pos');
        $oddsid = $this->request->getPost('oddsid', 'int');

        $status = 1;

        if ($this->session->has("betslip")) {
            $betslip = $this->session->get("betslip");
            unset($betslip["$match_id"]);
        } else {
            $betslip = array();
        }

        $betslip["$match_id"] = [
            'match_id' => $match_id,
            'bet_pick' => $bet_pick,
            'sub_type_id' => $sub_type_id,
            'special_bet_value' => $special_bet_value,
            'bet_type' => $bet_type,
            'home_team' => $home_team,
            'away_team' => $away_team,
            'odd_value' => $odd_value,
            'odd_type' => $odd_type,
            'parent_match_id' => $parent_match_id,
            'pos' => $pos,
            'oddsid' => $oddsid,
        ];

        $this->session->set("betslip", $betslip);

        $bets = $this->session->get('betslip');

        $count = sizeof($bets);

        $data = [
            'status' => $status,
            'total' => $count,
            'betslip' => $betslip,
            'boost' => $this->calculateMultibetBoost(),
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    public function oddsAction()
    {

        $selection = $this->request->getPost('selected');

        $this->session->set('oddschange', $selection);

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $data = [
            'selected' => $selection,
        ];

        $response->setContent(json_encode($data));
        return $response;
    }

    public function removeAction()
    {
        $match_id = $this->request->getPost('match_id', 'int');
        $stake = $this->request->getPost('selected_stake', 'int');
        $betslip = $this->session->get("betslip");

        unset($betslip["$match_id"]);

        $this->session->set("stake", $stake);
        $this->session->set("betslip", $betslip);

        $this->response->redirect('betslip');

        $this->view->disable();
    }

    public function clearslipAction()
    {

        $this->session->remove("betslip");

        $this->response->redirect('betslip');
        $this->view->disable();
    }

    public function refreshBetslip()
    {

        $betslip = $this->session->get('betslip');
        $invalid = array();

        foreach ($betslip as $key => $slip) {

            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_type = $slip['bet_type'];
            $home = $slip['home_team'];
            $away = $slip['away_team'];

            if ($bet_type == 'live') {
                $query = "SELECT lo.odd_value, l.status AS m_active, lo.odd_active,
                        lo.betradar_odd_id FROM  live_odds lo INNER JOIN live_match l
                        USING(parent_match_id) WHERE  l.parent_match_id='$parent_match_id'
                        AND lo.sub_type_id='$sub_type_id' AND lo.odd_active = '1' AND
                        lo.special_bet_value='$special_bet_value' AND lo.odd_key='$bet_pick'
                         ORDER BY lo.created DESC LIMIT 1 ";
            } else {
                $query = "SELECT odd_value, 1 as active, 1 as m_active, betradar_odd_id FROM
                     event_odd WHERE "
                    . "parent_match_id='$parent_match_id' AND sub_type_id='$sub_type_id' "
                    . " AND special_bet_value='$special_bet_value' AND odd_key='$bet_pick'";
            }

            $newOdds = $this->rawQueries($query);
            if (!is_null($newOdds)) {

                $newOdds = $newOdds[0];
                if ($newOdds['m_active'] == 0 && $bet_type == 'live') {

                    array_push($invalid, $home . " vs " . $away);
                } else {

                    unset($betslip["$$key"]);

                    $betslip["$key"] = [
                        'match_id' => $key,
                        'bet_pick' => $bet_pick,
                        'sub_type_id' => $sub_type_id,
                        'special_bet_value' => $special_bet_value,
                        'bet_type' => $bet_type,
                        'home_team' => $home,
                        'away_team' => $away,
                        'odd_value' => $newOdds['odd_value'],
                        'odd_type' => $slip['odd_type'],
                        'parent_match_id' => $parent_match_id,
                        'pos' => $slip['pos'],
                        'oddsid' => $newOdds['betradar_odd_id'],
                    ];

                    $this->session->set("betslip", $betslip);
                }
            } else {

                array_push($invalid, $home . " vs " . $away);
            }
        }

        return $invalid;
    }

    private function searchForLive($betslip)
    {

        foreach ($betslip as $slip) {

            if (array_search("live", $slip)) {
                return true;
            }
        }

        return false;
    }

    public function placebetAction()
    {

        if (!$this->session->get('auth')) {

            $this->view->disable();
            $this->response->redirect('login');
            return;
        }

        $userId = $this->session->get('auth')['id'];
        $mobile = $this->session->get('auth')['mobile'];

        $bet_amount = $this->request->getPost('stakeAmt', 'float');
        $src = $this->request->getPost('src', 'string') ?: 'internet';

        $oddsChangeStatus = $this->session->get('oddschange');

        if ($oddsChangeStatus != "Yes") {
            $oddsChangeStatus = $this->request->getPost('oddschangeaccept');
        }

        $invalid = $this->refreshBetslip();

        if (count($invalid) > 0) {

            $invalidMatches = implode(",", $invalid);
            $this->flashSession->error($this->flashMessages("The match(es) between $invalidMatches is/are disabled"));
            $this->response->redirect('/betslip');
            $this->view->disable();
        }

        $betslip = $this->session->get('betslip');

        // if($this->searchForLive($betslip) && $oddsChangeStatus != "Yes"){

        //     $this->flashSession->error($this->flashMessages('You must accept odds change when live event is included!'));
        //     $this->view->disable();
        //     return $this->response->redirect('betslip');
        // }

        $freebet = $this->request->getPost('freebet', 'int');

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        if (!$bet_amount) {
            $this->flashSession->error($this->flashMessages('Please specify your stake!'));
            $this->view->disable();
            $this->response->redirect('betslip');
        } else {

            if ($bet_amount < 1) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('Bet amount should be atleast KSH. 1'));
                    $this->response->redirect('betmobile');
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code" => 421,
                        "message" => "Bet amount should be atleast KSH. 1",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                }
            } else {

                $totalMatches = sizeof($betslip);
                $placementOptions = 4;

                $slip = [];

                foreach ($betslip as $match) {

                    if ($totalMatches == 1) {

                        $slip['OddID'] = $match['oddsid'];
                        $slip['Fix'] = false;
                    } else {
                        $temporaryArr = [];
                        $temporaryArr['OddID'] = $match['oddsid'];
                        $temporaryArr['Fix'] = false;
                        array_push($slip, $temporaryArr);
                    }

                    if ($match['bet_type'] == 'live' && $placementOptions == 0) {

                        $placementOptions = 4;
                    }
                }

                if ($oddsChangeStatus != "Yes" && !$this->searchForLive($betslip)) {
                    $placementOptions = 0;
                }

                if ($freebet == 0) {

                    $url = $this->qaBaseURL . "/api/coupon/NewSingleBetV5";

                    $payload = [
                        "UserID" => $userId,
                        "AgentID" => -1,
                        "AutomaticMoneyTransfer" => false,
                        "LanguageID" => 2,
                        "PlacementOptions" => $placementOptions,
                        "ApplicationCode" => "litesite",
                        "Odd" => $slip,
                        "Amount" => $bet_amount,
                        "VerifyReceivedFields" => true,
                        "UserBookmakerID" => $this->bookmarkerID,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID,
                    ];

                    if ($totalMatches > 1) {
                        $url = $this->qaBaseURL . "/api/coupon/NewMultipleBetV5";

                        $payload = [
                            "UserID" => $userId,
                            "AgentID" => -1,
                            "AutomaticMoneyTransfer" => false,
                            "LanguageID" => 2,
                            "PlacementOptions" => $placementOptions,
                            "ApplicationCode" => "litesite",
                            "Odds" => $slip,
                            "Amount" => $bet_amount,
                            "VerifyReceivedFields" => true,
                            "UserBookmakerID" => $this->bookmarkerID,
                            "APIAccount" => $this->apiAccount,
                            "APIPassword" => $this->qaApiPassword,
                            "IDBookmaker" => $this->bookmarkerID,
                        ];
                    }

                } else {

                    $url = $this->qaBaseURL . "/api/coupon/NewBetWithFreeBet";

                    $payload = [
                        "FreeBetID" => $freebet,
                        "isBonus" => false,
                        "UserID" => $userId,
                        "AgentID" => -1,
                        "AutomaticMoneyTransfer" => false,
                        "LanguageID" => 2,
                        "PlacementOptions" => $placementOptions,
                        "ApplicationCode" => "litesite",
                        "Odds" => $slip,
                        "Amount" => $bet_amount,
                        "VerifyReceivedFields" => true,
                        "UserBookmakerID" => $this->bookmarkerID,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID,
                    ];
                }

                $response = $this->postToUrl($payload, $url);

                if ($response->ResultCode == 1) {

                    if ($response->Coupon->CouponID > 0) {

                        $this->session->remove("betslip");
                        $coupon = $response->Coupon;

                        $couponDetailsPayload = [
                            "CouponCode" => $coupon->CouponCode,
                            "LanguageID" => 2,
                            "UserID" => $coupon->UserID,
                            "UserBookmakerID" => $response->UserBookmakerID,
                            "APIAccount" => $this->apiAccount,
                            "APIPassword" => $this->qaApiPassword,
                            "IDBookmaker" => $this->bookmarkerID,
                        ];

                        $totalOdds = 1;
                        $detailsURL = $this->qaBaseURL . "/api/coupon/CouponDetailV10";
                        $couponResponse = $this->postToUrl($couponDetailsPayload, $detailsURL);

                        $couponEvents = [];
                        if ($couponResponse->ResultCode == 1) {

                            if (isset($couponResponse->Balance)) {

                                $newBalance = $couponResponse->Balance->Amount;
                                $loggedInUser = $this->session->get('auth');
                                $loggedInUser['balance'] = $newBalance;
                                $this->session->set('auth', $loggedInUser);
                            }

                            $totalOdds = $couponResponse->TotalOdds;
                            $couponEvents = $couponResponse->Events;

                            $taxAmount = $bet_amount - $coupon->Amount;

                            $couponQuery = "INSERT INTO `coupon` VALUES(NULL, '" . $coupon->UserID . "',
                                '" . $coupon->CouponID . "', NULL, '" . $couponResponse->IDBetType . "',
                                '" . $totalOdds . "', '" . $bet_amount . "', '" . $coupon->Amount . "',
                                '" . $taxAmount . "', '" . $coupon->MaxPotentialWinnings . "',
                                '" . $coupon->MinPotentialWinnings . "', '" . $coupon->PotentialWinnings . "',
                                '" . $coupon->CouponStateID . "', 0, 0, 0, NULL, NULL, NULL,
                                '" . $coupon->CouponCode . "', 'ISOLUTIONS-API', NOW(), NOW(),0,0.00)";

                            $couponLocalID = $this->rawInsert($couponQuery);
                            $totalGamesInBet = count($couponEvents);

                            foreach ($couponEvents as $event) {

                                $couponDetailsQuery = "INSERT INTO coupon_details VALUES (NULL,
                                    '" . $event->SubEventID . "', '$couponLocalID',
                                     '" . $couponResponse->BetType . "', '1',
                                      '" . $couponResponse->PendingBetState . "', '0', '0',
                                     NULL, '" . $event->HND . "', '$totalGamesInBet',
                                      '" . $event->Odd . "', 0, 0, 0, 0, NOW(), NOW(),
                                       '" . $event->ResultStateID . "', NULL, NULL,
                                    '" . $event->OddID . "',
                                     '" . $event->OddClassID . "')";

                                $this->rawInsert($couponDetailsQuery);
                            }
                        }

                        $this->session->set("SuccessfulBet_" . $userId, $mobile);

                        $message = "Good Luck! Bet Id: " . $coupon->CouponCode . " was successfully placed. Potential payout KES " . round($coupon->MinPotentialWinnings, 2) . " New balance KES " . round($response->Balance->Amount, 2) . ". Customer Care: 0111 040000";
                        $this->sendZimobiSMS($message, $mobile);

                        $this->flashSession->success($this->flashMessages("You successfully placed your bet. Go to Bet History in the Account Menu to track your bets. Good luck!"));
                        return $this->response->redirect('/');
                    } else if ($response->Coupon->CouponID == -1 && count($response->NewOdds) > 0) {

                        $newSlip = [];
                        $newOdds = $response->NewOdds;
                        $totalGames = count($newOdds);

                        foreach ($newOdds as $oddRecord) {

                            if ($oddRecord->OldOddID != $oddRecord->OddID) {

                                $query = "UPDATE `event_odd` SET `odd_value` = '" . $oddRecord->Odd . "',
                                 betradar_odd_id='" . $oddRecord->OddID . "' WHERE
                                 `betradar_odd_id` = '" . $oddRecord->OldOddID . "'";

                                $this->rawInsert($query);
                            }

                            if ($totalGames > 1) {

                                $temporaryArr = [];
                                $temporaryArr['OddID'] = $oddRecord->OddID;
                                $temporaryArr['Fix'] = false;
                                array_push($newSlip, $temporaryArr);
                            }
                        }

                        if ($freebet == 0) {

                            $reSubmitURL = $this->qaBaseURL . "/api/coupon/NewMultipleBetV5";

                            $newPayload = [
                                "UserID" => $userId,
                                "AgentID" => -1,
                                "AutomaticMoneyTransfer" => false,
                                "LanguageID" => 2,
                                "PlacementOptions" => $placementOptions,
                                "ApplicationCode" => "litesite",
                                "Odds" => $newSlip,
                                "Amount" => $bet_amount,
                                "VerifyReceivedFields" => true,
                                "UserBookmakerID" => $this->bookmarkerID,
                                "APIAccount" => $this->apiAccount,
                                "APIPassword" => $this->qaApiPassword,
                                "IDBookmaker" => $this->bookmarkerID,
                            ];

                            if ($totalGames == 1) {

                                $newSlip = [];
                                $newSlip['OddID'] = $newOdds[0]->OddID;
                                $newSlip['Fix'] = false;
                                $reSubmitURL = $this->qaBaseURL . "/api/coupon/NewSingleBetV5";

                                $newPayload = [
                                    "UserID" => $userId,
                                    "AgentID" => -1,
                                    "AutomaticMoneyTransfer" => false,
                                    "LanguageID" => 2,
                                    "PlacementOptions" => $placementOptions,
                                    "ApplicationCode" => "litesite",
                                    "Odd" => $newSlip,
                                    "Amount" => $bet_amount,
                                    "VerifyReceivedFields" => true,
                                    "UserBookmakerID" => $this->bookmarkerID,
                                    "APIAccount" => $this->apiAccount,
                                    "APIPassword" => $this->qaApiPassword,
                                    "IDBookmaker" => $this->bookmarkerID,
                                ];
                            }

                        } else {

                            $reSubmitURL = $this->qaBaseURL . "/api/coupon/NewBetWithFreeBet";

                            $newPayload = [
                                "FreeBetID" => $freebet,
                                "isBonus" => false,
                                "UserID" => $userId,
                                "AgentID" => -1,
                                "AutomaticMoneyTransfer" => false,
                                "LanguageID" => 2,
                                "PlacementOptions" => $placementOptions,
                                "ApplicationCode" => "litesite",
                                "Odds" => $slip,
                                "Amount" => $bet_amount,
                                "VerifyReceivedFields" => true,
                                "UserBookmakerID" => $this->bookmarkerID,
                                "APIAccount" => $this->apiAccount,
                                "APIPassword" => $this->qaApiPassword,
                                "IDBookmaker" => $this->bookmarkerID,
                            ];
                        }

                        $newResponse = $this->postToUrl($newPayload, $reSubmitURL);
                        if ($newResponse->ResultCode == 1) {

                            if ($newResponse->Coupon->CouponID > 0) {

                                $this->session->remove("betslip");
                                $coupon = $newResponse->Coupon;

                                $couponDetailsPayload = [
                                    "CouponCode" => $coupon->CouponCode,
                                    "LanguageID" => 2,
                                    "UserID" => $coupon->UserID,
                                    "UserBookmakerID" => $newResponse->UserBookmakerID,
                                    "APIAccount" => $this->apiAccount,
                                    "APIPassword" => $this->qaApiPassword,
                                    "IDBookmaker" => $this->bookmarkerID,
                                ];

                                $totalOdds = 1;
                                $detailsURL = $this->qaBaseURL . "/api/coupon/CouponDetailV10";
                                $couponResponse = $this->postToUrl($couponDetailsPayload, $detailsURL);

                                $couponEvents = [];
                                if ($couponResponse->ResultCode == 1) {

                                    $newBalance = $couponResponse->Balance->Amount;
                                    $this->session->set(('auth')['balance'], $newBalance);

                                    $totalOdds = $couponResponse->TotalOdds;
                                    $couponEvents = $couponResponse->Events;

                                    $taxAmount = $bet_amount - $coupon->Amount;

                                    $couponQuery = "INSERT INTO `coupon` VALUES(NULL, '" . $coupon->UserID . "',
                                        '" . $coupon->CouponID . "', NULL, '" . $coupon->CouponTypeID . "',
                                        '" . $totalOdds . "', '" . $bet_amount . "', '" . $coupon->Amount . "',
                                        '" . $taxAmount . "', '" . $coupon->MaxPotentialWinnings . "',
                                        '" . $coupon->MinPotentialWinnings . "', '" . $coupon->PotentialWinnings . "',
                                        '" . $coupon->CouponStateID . "', 0, 0, 0, NULL, NULL, NULL,
                                        '" . $coupon->CouponCode . "', 'ISOLUTIONS-API', NOW(), NOW(),0,0.00)";

                                    $couponLocalID = $this->rawInsert($couponQuery);
                                    $totalGamesInBet = count($couponEvents);

                                    foreach ($couponEvents as $event) {

                                        $couponDetailsQuery = "INSERT INTO coupon_details VALUES (NULL,
                                            '" . $event->SubEventID . "', '$couponLocalID', '" . $couponResponse->BetType . "',
                                            '1', '" . $couponResponse->PendingBetState . "', '0', '0', NULL, '" . $event->HND . "',
                                            '$totalGamesInBet', '" . $event->Odd . "', 0, NULL, 0, 0, NOW(), NOW(),
                                            '" . $event->ResultStateID . "', NULL, NULL, '" . $event->OddID . "',
                                             '" . $event->OddClassID . "')";

                                        $this->rawInsert($couponDetailsQuery);
                                    }
                                }

                                $this->session->set("SuccessfulBet_" . $userId, $mobile);

                                $message = "Good Luck! Bet Id: " . $coupon->CouponCode . " was successfully placed. Potential payout KES " . round($coupon->MinPotentialWinnings, 2) . " New balance KES " . round($response->Balance->Amount, 2) . ". Customer Care: 0111 040000";
                                $this->sendZimobiSMS($message, $mobile);

                                $this->flashSession->success($this->flashMessages("You successfully placed your bet. Go to Bet History in the Account Menu to track your bets. Good luck!"));
                                return $this->response->redirect('/');
                            } else {
                                $this->flashSession->error($this->flashMessages("Unable to place your bet. Please contact support!"));
                                return $this->response->redirect('/');
                            }
                        } else {
                            $this->flashSession->error($this->flashMessages("Unable to place your bet. Please contact support! "));
                            return $this->response->redirect('/');
                        }
                    } else {
                        $this->flashSession->error($this->flashMessages("Unable to place your bet. Please contact support! "));
                        return $this->response->redirect('/');
                    }
                } else if ($response->ResultCode == -17) {
                    $this->flashSession->error($this->flashMessages("Insufficient funds. Please make a deposit to place bet"));
                    return $this->response->redirect('/deposit');
                } else if ($response->ResultCode == -14) {
                    $this->flashSession->error($this->flashMessages($response->ResultDescription));
                    return $this->response->redirect('/');
                } else {
                    $this->flashSession->error($this->flashMessages("Unable to place your bet. Please contact support"));
                    $this->view->disable();
                    $this->response->redirect('betslip');
                }
            }
        }
    }

    public function addjpActionX()
    {

        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];

            $timeout = $this->rawQueries("SELECT * FROM `gaming_timeout`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            $setTimeout = [];
            $timeRemaining = null;

            if (count($timeout) > 0) {
                $setTimeout = $timeout[0]['timeout_period'];
                $createdAt = $timeout[0]['created'];
                $inSeconds = strtotime($createdAt);
                $timeoutSeconds = 0;

                if ($setTimeout == "24 Hours") {
                    $timeoutSeconds = 24 * 3600;
                } else if ($setTimeout == "48 Hours") {
                    $timeoutSeconds = 24 * 3600 * 2;
                } else if ($setTimeout == "1 Week") {
                    $timeoutSeconds = 24 * 3600 * 7;
                }

                $expectedEndDateInSeconds = $inSeconds + $timeoutSeconds;
                $remainingTimeInSeconds = $expectedEndDateInSeconds - strtotime(date('Y-m-d H:i:s'));
                $timeRemaining = ResponsibleController::fromSeconds($remainingTimeInSeconds);

                $data = [
                    'status' => 410,
                    'message' => "You have chosen to Time-Out for a period " . $setTimeout . " Time remaining " . $timeRemaining,
                ];

                $response = new Response();
                $response->setStatusCode(201, "OK");
                $response->setHeader("Content-Type", "application/json");

                $response->setContent(json_encode($data));
                return $response;
            }
        }

        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'jackpot';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid', 'int');
        $pos = $this->request->getPost('pos');
        $oddsid = $this->request->getPost('oddsid', 'int');
        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');
        $isOperaMini = $this->request->getPost('isoperamini', 'int');

        if ($this->session->has("JPBetslip_" . $jackpotID)) {
            $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);
        }

        unset($jpBetslip["$parent_match_id"]);

        $jpBetslip["$parent_match_id"] = [
            'bet_pick' => $bet_pick,
            'sub_type_id' => $sub_type_id,
            'special_bet_value' => $special_bet_value,
            'bet_type' => $bet_type,
            'home_team' => $home_team,
            'away_team' => $away_team,
            'odd_value' => $odd_value,
            'odd_type' => $odd_type,
            'parent_match_id' => $parent_match_id,
            'pos' => $pos,
            'oddsid' => $oddsid,
        ];

        $jEvent = $this->rawQueries("SELECT * FROM jackpot_event
               WHERE jackpot_event_id='$jackpotID'");

        $totalJPGames = $jEvent[0]['total_games'];

        $this->session->set("JPBetslip_" . $jackpotID, $jpBetslip);
        $bets = $this->session->get("JPBetslip_" . $jackpotID);

        $count = sizeof($bets);

        $data = [
            'status' => 200,
            'total' => $count,
            'odd_value' => $odd_value,
            'jpBetslip' => $jpBetslip,
            'total_jp_games' => $totalJPGames,
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    public function addjpAction()
    {

        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];

            $timeout = $this->rawQueries("SELECT * FROM `gaming_timeout`
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            $setTimeout = [];
            $timeRemaining = null;

            if (!empty($timeout)) {
                $setTimeout = $timeout[0]['timeout_period'];
                $createdAt = $timeout[0]['created'];
                $inSeconds = strtotime($createdAt);
                $timeoutSeconds = 0;

                if ($setTimeout == "24 Hours") {
                    $timeoutSeconds = 24 * 3600;
                } elseif ($setTimeout == "48 Hours") {
                    $timeoutSeconds = 24 * 3600 * 2;
                } elseif ($setTimeout == "1 Week") {
                    $timeoutSeconds = 24 * 3600 * 7;
                }

                $expectedEndDateInSeconds = $inSeconds + $timeoutSeconds;
                $remainingTimeInSeconds = $expectedEndDateInSeconds - strtotime(date('Y-m-d H:i:s'));
                $timeRemaining = ResponsibleController::fromSeconds($remainingTimeInSeconds);

                $data = [
                    'status' => 410,
                    'message' => "You have chosen to Time-Out for a period
                    " . $setTimeout . " Time remaining " . $timeRemaining,
                ];

                $response = new Response();
                $response->setStatusCode(201, "OK");
                $response->setHeader("Content-Type", "application/json");

                $response->setContent(json_encode($data));
                return $response;
            }
        }

        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'jackpot';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid', 'int');
        $oddsid = $this->request->getPost('oddsid', 'int');
        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');

        $jEvent = $this->rawQueries("SELECT * FROM jackpot_event
               WHERE jackpot_event_id='$jackpotID'");

        $totalJPGames = $jEvent[0]['total_games'];
        $pos = 1;
        $selectedCustoms = "";
        $jpBetAmount = $jEvent[0]['bet_amount'];
        $totalDoubleSelections = 0;
        $errors = 0;
        $errorMessage = "";
        $selectionRemoved = 0;

        if ($this->session->get('auth')) {

            $doubleSelectionsCounter = $this->rawQueries("SELECT COUNT(*) AS total FROM
              jackpot_session WHERE isol_userid = '$profileId' AND jackpot_event_id
              = '$jackpotID' AND LENGTH(bet_pick)=3");

            $totalDoubleSelections = !empty($doubleSelectionsCounter) ? $doubleSelectionsCounter[0]['total'] : 0;

            $checkSelection = $this->rawQueries("SELECT * FROM jackpot_session WHERE
                isol_userid = '$profileId' AND jackpot_event_id = '$jackpotID' AND
                parent_match_id='$parent_match_id'");

            if (empty($checkSelection)) {

                $this->rawInsert("INSERT INTO jackpot_session (`jackpot_session_id`, `jackpot_event_id`,
                    `isol_userid`, `parent_match_id`,`bet_pick`, `sub_type_id`, `special_bet_value`,
                    `bet_type`, `home_team`, `away_team`,`odd_value` ,`odd_type`, `pos`, `betradar_odd_id`)
                    VALUES (NULL, '$jackpotID', '$profileId', '$parent_match_id', '$bet_pick',
                    '$sub_type_id', '$special_bet_value', '$bet_type', '$home_team', '$away_team', '$odd_value',
                    '$odd_type', '$pos', '$oddsid')");

                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $bet_pick, $home_team, $away_team);

            } else {

                $existingBetPick = $checkSelection[0]['bet_pick'];
                if ($existingBetPick == $bet_pick && strlen($existingBetPick) == 1) {

                    $this->rawInsert("DELETE FROM jackpot_session WHERE jackpot_event_id = '$jackpotID'
                        AND isol_userid='$profileId' AND parent_match_id = '$parent_match_id'");
                    $selectionRemoved = 1;

                } else {

                    if (strlen($existingBetPick) > 1) {

                        if (strpos($existingBetPick, $bet_pick) !== false) {
                            $updatedPick = str_replace(",", "", str_replace($bet_pick,
                                "", $existingBetPick));

                            $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                $updatedPick, $home_team, $away_team);

                        } else {

                            $updatedPick = $existingBetPick;
                            $existingArr = explode(",", $updatedPick);
                            $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                $existingArr[0], $home_team, $away_team) . "," .
                            $this->getSelectedCustoms($parent_match_id, $existingArr[1],
                                $home_team, $away_team);
                            $errors = 1;
                            $errorMessage = "Maximum of 2 selections per fixture";

                        }
                    } else {

                        if ($bet_pick == "1") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {
                                $updatedPick = $bet_pick . "," . $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $bet_pick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $existingBetPick,
                                    $home_team, $away_team);
                            }

                        } elseif ($existingBetPick == "1") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {
                                $updatedPick = $existingBetPick . "," . $bet_pick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $existingBetPick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $bet_pick,
                                    $home_team, $away_team);
                            }

                        }

                        if ($bet_pick == "X" && $existingBetPick == "2") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {
                                $updatedPick = $bet_pick . "," . $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $bet_pick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $existingBetPick,
                                    $home_team, $away_team);
                            }

                        }

                        if ($bet_pick == "2" && $existingBetPick == "X") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } else if ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {
                                $updatedPick = $existingBetPick . "," . $bet_pick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $existingBetPick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $bet_pick,
                                    $home_team, $away_team);
                            }

                        }

                    }

                    if (strlen($updatedPick) > 0) {

                        $this->rawInsert("UPDATE jackpot_session SET bet_pick='$updatedPick'
                        WHERE jackpot_event_id = '$jackpotID' AND isol_userid='$profileId'
                        AND parent_match_id = '$parent_match_id'");
                    }
                }
            }

            $slips = $this->rawQueries("SELECT * FROM jackpot_session WHERE
                isol_userid = '$profileId' AND jackpot_event_id = '$jackpotID'");

            $formattedSlip = [];
            foreach ($slips as $slip) {

                $oneSelection = [
                    'bet_pick' => $slip['bet_pick'],
                    'sub_type_id' => $slip['sub_type_id'],
                    'special_bet_value' => $slip['special_bet_value'],
                    'bet_type' => $slip['bet_type'],
                    'home_team' => $slip['home_team'],
                    'away_team' => $slip['away_team'],
                    'odd_value' => $slip['odd_value'],
                    'odd_type' => $slip['odd_type'],
                    'parent_match_id' => $slip['parent_match_id'],
                    'pos' => $slip['pos'],
                    'betradar_odd_id' => $slip['betradar_odd_id'],
                ];

                $formattedSlip[$slip['parent_match_id']] = $oneSelection;

                if (count(explode(",", $slip['bet_pick'])) == 2) {
                    $jpBetAmount = $jpBetAmount * 2;
                }
            }

            $count = count($formattedSlip);

            $data = [
                'status' => 200,
                'total' => $count,
                'odd_value' => $odd_value,
                'jpBetslip' => $formattedSlip,
                'selectedCustoms' => $selectedCustoms,
                'jpBetAmount' => number_format($jpBetAmount, 2),
                'errors' => $errors,
                'errorMessage' => $errorMessage,
                'total_jp_games' => $totalJPGames,
                'selectionRemoved' => $selectionRemoved,
            ];

        } else {

            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            if ($this->session->has("JPBetslip_" . $jackpotID)) {
                $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);
            }

            $totalDoubleSelections = 0;

            foreach ($jpBetslip as $oneSlip) {
                if (strlen($oneSlip['bet_pick']) > 1) {
                    $totalDoubleSelections++;
                }
            }

            $existingSelection = $jpBetslip["$parent_match_id"];

            if (is_null($existingSelection) || empty($existingSelection)) {

                $jpBetslip["$parent_match_id"] = [
                    'bet_pick' => $bet_pick,
                    'sub_type_id' => $sub_type_id,
                    'special_bet_value' => $special_bet_value,
                    'bet_type' => $bet_type,
                    'home_team' => $home_team,
                    'away_team' => $away_team,
                    'odd_value' => $odd_value,
                    'odd_type' => $odd_type,
                    'parent_match_id' => $parent_match_id,
                    'pos' => $pos,
                    'betradar_odd_id' => $oddsid,
                ];

                $selectedCustoms = $this->getSelectedCustoms($parent_match_id, $bet_pick,
                    $home_team, $away_team);

            } else {

                $existingBetPick = $existingSelection['bet_pick'];

                if (strlen($existingBetPick) > 1) {

                    if (strpos($existingBetPick, $bet_pick) !== false) {
                        $updatedPick = str_replace(",", "", str_replace($bet_pick,
                            "", $existingBetPick));

                        $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                            $updatedPick, $home_team, $away_team);

                    } else {

                        $updatedPick = $existingBetPick;
                        $existingArr = explode(",", $updatedPick);
                        $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                            $existingArr[0], $home_team, $away_team) . "," .
                        $this->getSelectedCustoms($parent_match_id, $existingArr[1],
                            $home_team, $away_team);
                        $errors = 1;
                        $errorMessage = "Maximum of 2 selections per fixture";

                    }
                } else {

                    if ($bet_pick == $existingBetPick) {

                        unset($jpBetslip["$parent_match_id"]);
                        $selectionRemoved = 1;

                    } else {
                        if ($bet_pick == "1") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {

                                $updatedPick = $bet_pick . "," . $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $bet_pick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $existingBetPick,
                                    $home_team, $away_team);
                            }

                        } elseif ($existingBetPick == "1") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {

                                $updatedPick = $existingBetPick . "," . $bet_pick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $existingBetPick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $bet_pick,
                                    $home_team, $away_team);
                            }
                        }

                        if ($bet_pick == "X" && $existingBetPick == "2") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {

                                $updatedPick = $bet_pick . "," . $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $bet_pick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $existingBetPick,
                                    $home_team, $away_team);
                            }
                        }

                        if ($bet_pick == "2" && $existingBetPick == "X") {

                            if ($totalJPGames < 13 && ($totalDoubleSelections + 1 > 2)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 4 entries";
                            } elseif ($totalJPGames >= 13 && ($totalDoubleSelections + 1 > 5)) {

                                $updatedPick = $existingBetPick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $updatedPick, $home_team, $away_team);
                                $errors = 1;
                                $errorMessage = "You have reached a maximum of 32 entries";
                            } else {

                                $updatedPick = $existingBetPick . "," . $bet_pick;
                                $selectedCustoms = $this->getSelectedCustoms($parent_match_id,
                                    $existingBetPick, $home_team, $away_team) . "," .
                                $this->getSelectedCustoms($parent_match_id, $bet_pick,
                                    $home_team, $away_team);
                            }
                        }

                    }
                }

                $jpBetslip["$parent_match_id"] = [
                    'bet_pick' => $updatedPick,
                    'sub_type_id' => $sub_type_id,
                    'special_bet_value' => $special_bet_value,
                    'bet_type' => $bet_type,
                    'home_team' => $home_team,
                    'away_team' => $away_team,
                    'odd_value' => $odd_value,
                    'odd_type' => $odd_type,
                    'parent_match_id' => $parent_match_id,
                    'pos' => $pos,
                    'betradar_odd_id' => $oddsid,
                ];

            }

            foreach ($jpBetslip as $slip) {

                if (count(explode(",", $slip['bet_pick'])) == 2) {
                    $jpBetAmount = $jpBetAmount * 2;
                }
            }

            $jEvent = $this->rawQueries("SELECT * FROM jackpot_event WHERE
             jackpot_event_id='$jackpotID'");

            $totalJPGames = $jEvent[0]['total_games'];

            $this->session->set("JPBetslip_" . $jackpotID, $jpBetslip);
            $bets = $this->session->get("JPBetslip_" . $jackpotID);

            $count = sizeof($bets);

            $data = [
                'status' => 200,
                'total' => $count,
                'odd_value' => $odd_value,
                'selectedCustoms' => $selectedCustoms,
                'jpBetAmount' => number_format($jpBetAmount, 2),
                'jpBetslip' => $jpBetslip,
                'total_jp_games' => $totalJPGames,
                'errors' => $errors,
                'errorMessage' => $errorMessage,
                'selectionRemoved' => $selectionRemoved,
            ];

        }

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    private function getSelectedCustoms($parentMatchId, $oddKey, $home_team, $away_team)
    {

        if ($oddKey == "1") {
            return $parentMatchId . "1" . str_replace(" ", "-", $home_team);
        } elseif ($oddKey == "X") {
            return $parentMatchId . "1Draw";
        } elseif ($oddKey == "2") {
            return $parentMatchId . "1" . str_replace(" ", "-", $away_team);
        }
    }

    public function clearjpslipAction()
    {

        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');

        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];
            $this->rawInsert("DELETE FROM jackpot_session WHERE jackpot_event_id = '$jackpotID'
            AND isol_userid='$profileId'");
        } else {

            $this->session->remove("JPBetslip_" . $jackpotID);
        }
        $this->view->disable();
        return $this->response->redirect("/jackpot/open/" . $jackpotID);
    }

    public function clearjpslipActionX()
    {

        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');
        $this->session->remove("JPBetslip_" . $jackpotID);

        $this->response->redirect($_SERVER['HTTP_REFERER']);
        $this->view->disable();
    }

    public function removejpAction()
    {
        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');
        $match_id = $this->request->getPost('parent_match_id', 'int');

        if ($this->session->get('auth')) {

            $profileId = $this->session->get('auth')['id'];

            $this->rawInsert("DELETE FROM jackpot_session WHERE parent_match_id = '$match_id'
                AND jackpot_event_id='$jackpotID' AND isol_userid='$profileId'");

            $jpBetslip = $this->rawQueries("SELECT * FROM jackpot_session WHERE
                isol_userid = '$profileId' AND jackpot_event_id = '$jackpotID'");

            $formattedSlip = [];
            foreach ($jpBetslip as $slip) {

                $oneSelection = [
                    'bet_pick' => $slip['bet_pick'],
                    'sub_type_id' => $slip['sub_type_id'],
                    'special_bet_value' => $slip['special_bet_value'],
                    'bet_type' => $slip['bet_type'],
                    'home_team' => $slip['home_team'],
                    'away_team' => $slip['away_team'],
                    'odd_value' => $slip['odd_value'],
                    'odd_type' => $slip['odd_type'],
                    'parent_match_id' => $slip['parent_match_id'],
                    'pos' => $slip['pos'],
                    'betradar_odd_id' => $slip['betradar_odd_id'],
                ];

                $formattedSlip[$slip['parent_match_id']] = $oneSelection;
            }

            $count = count($formattedSlip);

            $jEvent = $this->rawQueries("SELECT * FROM jackpot_event
                WHERE jackpot_event_id='$jackpotID'");

            $totalJPGames = $jEvent[0]['total_games'];

            $data = [
                'total' => $count,
                'jpBetslip' => $formattedSlip,
                'total_jp_games' => $totalJPGames,
            ];

        } else {

            if ($this->session->has("JPBetslip_" . $jackpotID)) {

                $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);

                unset($jpBetslip["$match_id"]);

                $this->session->set("JPBetslip_" . $jackpotID, $jpBetslip);
                $count = sizeof($jpBetslip);

                $jEvent = $this->rawQueries("SELECT * FROM jackpot_event
                WHERE jackpot_event_id='$jackpotID'");

                $totalJPGames = $jEvent[0]['total_games'];

                $data = [
                    'total' => $count,
                    'jpBetslip' => $jpBetslip,
                    'total_jp_games' => $totalJPGames,
                ];
            }
        }

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    public function removejpActionX()
    {
        $jackpotID = $this->request->getPost('jackpot_event_id', 'int');
        $match_id = $this->request->getPost('parent_match_id', 'int');

        if ($this->session->has("JPBetslip_" . $jackpotID)) {
            $jpBetslip = $this->session->get("JPBetslip_" . $jackpotID);

            unset($jpBetslip["$match_id"]);

            $this->session->set("JPBetslip_" . $jackpotID, $jpBetslip);
            $count = sizeof($jpBetslip);

            $jEvent = $this->rawQueries("SELECT * FROM jackpot_event
               WHERE jackpot_event_id='$jackpotID'");

            $totalJPGames = $jEvent[0]['total_games'];

            $data = [
                'total' => $count,
                'jpBetslip' => $jpBetslip,
                'total_jp_games' => $totalJPGames,
            ];

            $isOperaMini = $this->request->getPost('isoperamini', 'int');
            if ($isOperaMini) {

                $bytes = openssl_random_pseudo_bytes(32);
                $hash = bin2hex($bytes);
                return $this->response->redirect("/jackpot/open/$jackpotID?v=$hash");
            } else {
                $response = new Response();
                $response->setStatusCode(201, "OK");
                $response->setHeader("Content-Type", "application/json");

                $response->setContent(json_encode($data));

                return $response;
            }
        }
    }

    public function betJackpotAction()
    {

        $user_id = $this->request->getPost('user_id', 'int');
        $src = $this->request->getPost('src', 'string');
        $jackpot_type = $this->request->getPost('jackpot_type', 'int');
        $jackpot_id = $this->request->getPost('jackpot_id', 'int');
        $account = $this->request->getPost('account', 'int');
        $msisdn = $this->request->getPost('msisdn', 'int');
        $msisdn = $msisdn ?: 255600000000;

        $account = $user_id > 0 ? 1 : $account;

        if ($account !== 1) {
            $account = 0;
        }

        $jackpots = [
            '5' => 'correct',
            '12' => 'jackpot',
        ];

        $bet_type = 'jackpot';
        $matches = $this->betslip('jackpot');
        if ($jackpot_type == 5) {
            $bet_type = 'bingwafour';
            $matches = $this->betslip('bingwafour');
        }

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        if (!$jackpot_id || !$jackpot_type || !($user_id || $msisdn)) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('all fields are required'));
                $this->view->disable();
                return $this->response->redirect($jackpots[$jackpot_type]);
            } else {
                $data = [
                    "status_code" => "421",
                    "message" => "All fields are required",
                ];
                $response->setContent(json_encode($data));

                return $response;
            }
        } else {

            $matches = $this->array_msort($matches, ['pos' => SORT_ASC]);

            $totalMatch = sizeof($matches);
            if ($totalMatch < $jackpot_type) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('You must select an outcome for all
                     Jackpot Matches'));
                    $this->response->redirect($jackpots[$jackpot_type]);
                    $this->view->disable();
                } else {
                    $data = [
                        "status_code" => "421",
                        "message" => "You must select an outcome for all Jackpot Matches",
                    ];
                    $response->setContent(json_encode($data));

                    return $response;
                }
            } else {
                $mobile = $this->session->get('auth')['mobile'];

                $message = '';

                foreach ($matches as $match) {
                    if ($jackpot_type == 3) {
                        $message = $message . "#" . $match['bet_pick'];
                        $message = str_replace(":", "-", $message);
                    } else {
                        $message = $message . "|" . $match['bet_pick'];
                    }
                }

                if ($jackpot_type == 3) {
                    $message = substr($message, 1);
                }

                $bet = [
                    "app_name" => "LITE",
                    "profile_id" => $user_id,
                    'jackpot_id' => $jackpot_id,
                    'message' => $message,
                    'account' => $account,
                    'msisdn' => "" . $mobile ?: $msisdn . "",
                ];

                $placeB = $this->betJackpot($bet);
                if ($placeB['status_code'] == 201) {
                    $message = $placeB['message'];
                    $sms = [
                        "msisdn" => $mobile,
                        'message' => $message,
                        'short_code' => 101010,
                        'correlator' => '',
                        'message_type' => 'BULK',
                        'link_id' => '',
                    ];
                    // $this->sendSMS($sms);
                }

                if ($src == 'mobile') {
                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $feedback = $placeB['message'];
                        $this->betslipUnset($bet_type);
                        $this->flashSession->error($this->flashSuccess($feedback));

                        if (!empty($matches)) {
                            $id = $matches[1] ?: $matches[2];
                            $this->view->disable();
                            $redirect_url = "https://betsafe.co.ke/mybets?a=1&id=" . $id;
                            header('Location: ' . $redirect_url);
                            exit;
                        }
                    } else {
                        $this->flashSession->error($this->flashMessages($feedback));
                    }

                    $this->response->redirect($jackpots[$jackpot_type]);
                    $this->view->disable();
                } else {
                    $response->setContent(json_encode($placeB));

                    return $response;
                }
            }
        }
    }

    public function fiftystakeAction()
    {

        $stake = $this->session->get('stake');

        if (is_null($stake)) {
            $this->session->set('stake', 50);
        } else {
            $this->session->set('stake', $stake + 50);
        }
        return $this->response->redirect('/betslip');
    }

    public function freebetAction()
    {

        $stake = $this->request->getPost('freebet_amount', 'float');
        $freebetId = $this->request->getPost('freebets', 'int');

        if (!is_null($stake)) {

            $this->session->set('stake', $stake);
            return $this->response->redirect('/betslip?freebet=' . $freebetId);
        }
    }

    public function hundredstakeAction()
    {

        $stake = $this->session->get('stake');

        if (is_null($stake)) {
            $this->session->set('stake', 100);
        } else {
            $this->session->set('stake', $stake + 100);
        }
        return $this->response->redirect('/betslip');
    }

    public function twohundredstakeAction()
    {

        $stake = $this->session->get('stake');

        if (is_null($stake)) {
            $this->session->set('stake', 200);
        } else {
            $this->session->set('stake', $stake + 200);
        }
        return $this->response->redirect('/betslip');
    }

    public function fivehundredstakeAction()
    {

        $stake = $this->session->get('stake');

        if (is_null($stake)) {
            $this->session->set('stake', 500);
        } else {
            $this->session->set('stake', $stake + 500);
        }
        return $this->response->redirect('/betslip');
    }

    public function updatebuttAction()
    {

        $stake = $this->request->getPost('stake', 'int');
        $this->session->set('stake', $stake);
        return $this->response->redirect('/betslip');
    }

    private function array_msort($array, $cols)
    {
        $colarr = [];
        foreach ($cols as $col => $order) {
            $colarr[$col] = [];
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = [];
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k])) {
                    $ret[$k] = $array[$k];
                }

                $ret[$k][$col] = $array[$k][$col];
            }
        }

        return $ret;
    }
}