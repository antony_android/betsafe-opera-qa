<?php

use Phalcon\Tag;


/**
 * Class IndexController
 */
class SportController extends ControllerBase
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function openAction($sport_id)
    {

        $loggedIn = 'FALSE';

        $this->tag->setDoctype(Tag::HTML5);

        $keyword = $this->request->getPost('keyword', 'string');

        if (!isset($sport_id) || empty($sport_id) || strlen($sport_id) == 0) {
            $sport_id = 7;
            $selected = 'home';
        } else {
            $selected = 'other-sport';
        }

        $this->session->set('selectedSportId', $sport_id);
        
        $sportDetails = $this->rawQueries("SELECT * FROM sport WHERE 
            sport_id=? LIMIT 1", [$sport_id]);

        $eventsTitle = $sportDetails[0]['sport_name'];

        $where = "";
        if ($sport_id) {
            $where = " AND s.sport_id = '$sport_id'";
        }

        if ($keyword) {
            $selected = "search";
            $eventsTitle = "Search Results";
        }

        $skip = 0;
        $limit = 50;

        list($today, $total) = $this->getGames(
            $keyword,
            $skip,
            $limit,
            $where,
            'm_priority ASC, priority ASC, start_time ASC, home_team ASC',
            $sport_id
        );

        $theBetslip = $this->session->get("betslip");

        $men = 'home';

        if ($this->session->has('auth') || $this->session->get('auth') != null) {
            $loggedIn = "TRUE";

            $profileId = $this->session->get('auth')['id'];
            $timeout = $this->rawQueries("SELECT * FROM `gaming_timeout` 
                WHERE `status`= 1 AND `profile_id` = '$profileId'");

            $setTimeout = [];
            $timeRemaining = null;

            if (!empty($timeout)) {
                $setTimeout = $timeout[0]['timeout_period'];
                $createdAt = $timeout[0]['created'];
                $inSeconds = strtotime($createdAt);
                $timeoutSeconds = 0;

                if ($setTimeout == "24 Hours") {
                    $timeoutSeconds = 24 * 3600;
                } else if ($setTimeout == "48 Hours") {
                    $timeoutSeconds = 24 * 3600 * 2;
                } else if ($setTimeout == "1 Week") {
                    $timeoutSeconds = 24 * 3600 * 7;
                }

                $expectedEndDateInSeconds = $inSeconds + $timeoutSeconds;
                $remainingTimeInSeconds = $expectedEndDateInSeconds - strtotime(date('Y-m-d H:i:s'));
                $timeRemaining = ResponsibleController::fromSeconds($remainingTimeInSeconds);
            }
        }

        $referrer = $this->request->getHTTPReferer();
        $navigation = $this->getNavigation($sport_id);

        $this->view->setVars([
            'matches'    => $today,
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports'    => $navigation['sports'],
            'eventsTitle' => $eventsTitle,
            'timeOutPeriodRemaining' => isset($timeRemaining)?$timeRemaining:NULL,
            'setTimeout' => isset($setTimeout)?$setTimeout:NULL,
            'withdrawalAmount' => $this->session->get('withdrawalAmount'),
            'referrer' => $referrer,
            'loggedIn' => $loggedIn,
            'selected' => $selected,
            'total'    => $total,
            'men'      => $men,
        ]);

        $this->tag->setTitle('BetSafe - Leading sports Betting site In Kenya');
    }
}
