<?php

class BonusesController extends ControllerBase
{
    public function indexAction()
    {

        $userId = $this->session->get('auth')['id'];

        $theBetslip = $this->session->get("betslip");
        $selectedSportId = $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', null);

        $payload = [
            "UserID" => $userId,
            "ApplicationType" => 1,
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID,
        ];

        $url = $this->qaBaseURL . "/api/bonuscampaigns/GetFreebetsByUser";
        $response = $this->postToUrl($payload, $url);

        if ($response->ResultCode == 1) {

            $freebets = $response->Freebets;
            $userFreeBets = [];

            if (count($freebets) > 0) {
                foreach ($freebets as $bet) {

                    $placementRules = $bet->PlacementRules;
                    $maxPayout = 0;
                    $minSelectionOdds = 1;
                    $minNumSelections = 1;
                    $minTotalOdds = 1;

                    foreach ($placementRules as $rule) {

                        if ($rule->Code == "@MaxPayout") {
                            $amountArr = explode(",", $rule->Value);
                            $maxPayout = $amountArr[1];
                        } else if ($rule->Code == "@MinOddsPerSelection") {
                            $minSelectionOdds = $rule->Value;
                        } else if ($rule->Code == "@MinSelections") {
                            $minNumSelections = $rule->Value;
                        } else if ($rule->Code == "@MinTotalOdds") {
                            $minTotalOdds = $rule->Value;
                        }
                    }

                    array_push($userFreeBets,
                        [
                            "freebetId" => $bet->FreebetID,
                            "name" => $bet->Description,
                            "amount" => $bet->Amount,
                            "maxPayout" => $maxPayout,
                            "minSelectionOdds" => $minSelectionOdds,
                            "minNumSelections" => $minNumSelections,
                            "minTotalOdds" => $minTotalOdds,
                        ]);
                }
            }
        }

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'freebets' => $userFreeBets,
            'freebetsCount' => count($userFreeBets),
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'bonuses',
            'selectedBonusesControl' => 'all',
        ]);
    }

    public function activeAction()
    {

        $userId = $this->session->get('auth')['id'];

        $theBetslip = $this->session->get("betslip");
        $selectedSportId = $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', null);

        $payload = [
            "UserID" => $userId,
            "ApplicationType" => 1,
            "APIAccount" => $this->apiAccount,
            "APIPassword" => $this->qaApiPassword,
            "IDBookmaker" => $this->bookmarkerID,
        ];

        $url = $this->qaBaseURL . "/api/bonuscampaigns/GetFreebetsByUser";
        $response = $this->postToUrl($payload, $url);

        if ($response->ResultCode == 1) {

            $freebets = $response->Freebets;
            $userFreeBets = [];

            if (count($freebets) > 0) {
                foreach ($freebets as $bet) {

                    $placementRules = $bet->PlacementRules;
                    $maxPayout = 0;
                    $minSelectionOdds = 1;
                    $minNumSelections = 1;
                    $minTotalOdds = 1;

                    foreach ($placementRules as $rule) {

                        if ($rule->Code == "@MaxPayout") {
                            $amountArr = explode(",", $rule->Value);
                            $maxPayout = $amountArr[1];
                        } else if ($rule->Code == "@MinOddsPerSelection") {
                            $minSelectionOdds = $rule->Value;
                        } else if ($rule->Code == "@MinSelections") {
                            $minNumSelections = $rule->Value;
                        } else if ($rule->Code == "@MinTotalOdds") {
                            $minTotalOdds = $rule->Value;
                        }
                    }

                    array_push($userFreeBets,
                        [
                            "freebetId" => $bet->FreebetID,
                            "name" => $bet->Description,
                            "amount" => $bet->Amount,
                            "maxPayout" => $maxPayout,
                            "minSelectionOdds" => $minSelectionOdds,
                            "minNumSelections" => $minNumSelections,
                            "minTotalOdds" => $minTotalOdds,
                        ]);
                }
            }
        }

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'freebets' => $userFreeBets,
            'freebetsCount' => count($userFreeBets),
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'bonuses',
            'selectedBonusesControl' => 'active',
        ]);

        $this->view->pick("bonuses/index");
    }

    public function expiredAction()
    {

        $navigation = $this->getNavigation();
        $this->view->setVars([
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'expired-bonuses',
            'selectedBonusesControl' => 'expired',
        ]);
    }

    public function freebetAction()
    {

        $navigation = $this->getNavigation();
        $this->view->setVars([
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'freebetId' => $this->request->getPost('freebetId', 'int'),
            'name' => $this->request->getPost('name'),
            'amount' => $this->request->getPost('amount', 'float'),
            'maxPayout' => $this->request->getPost('maxPayout', 'float'),
            'minSelectionOdds' => $this->request->getPost('minSelectionOdds', 'float'),
            'minNumSelections' => $this->request->getPost('minNumSelections', 'int'),
            'minTotalOdds' => $this->request->getPost('minTotalOdds', 'float'),
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'freebet',
        ]);
    }

    public function welcomeAction()
    {

        $theBetslip = $this->session->get("betslip");
        $navigation = $this->getNavigation();
        $this->session->set('withdrawalAmount', null);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'bonuses',
        ]);
    }

    public function boostAction()
    {

        $theBetslip = $this->session->get("betslip");
        $navigation = $this->getNavigation();
        $this->session->set('withdrawalAmount', null);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'bonuses',
        ]);
    }

    public function fortyAction()
    {

        $theBetslip = $this->session->get("betslip");
        $navigation = $this->getNavigation();
        $this->session->set('withdrawalAmount', null);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'forty-bonuses',
        ]);
    }

    public function filterAction()
    {

        $selection = $this->request->getPost('selection');
        if ($selection == "all") {
            $selection = "";
        }

        $this->session->set('selected-bonuses', $selection);
        $this->response->redirect('/bonuses/' . $selection);
    }
}