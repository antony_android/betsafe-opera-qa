<?php

class WithdrawController extends ControllerBase
{

    const MPESA_PROVIDERID = 340;

    public function indexAction()
    {

        $theBetslip = $this->session->get("betslip");
        $selectedSportId = $this->session->get('selectedSportId');
        $navigation = $this->getNavigation($selectedSportId);
        $this->session->set('withdrawalAmount', null);

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount' => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'referrer' => $this->request->getHTTPReferer(),
            'selected' => 'withdraw',
        ]);
    }

    public function withdrawalAction()
    {
        if ($this->session->get('auth')) {
            $amount = $this->request->getPost('amount', 'int');

            if ($amount < 20) {
                $this->flashSession->error($this->flashMessages('Sorry, minimum withdraw amount is KSH. 20.'));
                return $this->response->redirect('withdraw');
            } elseif ($amount > 70000) {
                $this->flashSession->error($this->flashMessages('Sorry, maximum withdraw amount is KSH. 70,000.'));
                return $this->response->redirect('deposit');
            } else {

                $id = $this->session->get('auth')['id'];

                $balancePayload = [
                    "UserID" => $id,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID,
                ];

                $balance = $this->session->get('auth')['balance'];

                $balanceURL = $this->qaBaseURL . "/api/users/GetBalanceFull";
                $balanceResponse = $this->postToUrl($balancePayload, $balanceURL);

                if ($balanceResponse->ResultCode == 1) {
                    $balance = $balanceResponse->Balance->Amount;
                }

                if ($balance < $amount) {

                    $balance = number_format($balance, 2);
                    $this->flashSession->error($this->flashMessages("Insufficient balance! Your balance is KSH $balance."));
                    return $this->response->redirect('/');
                }

                $payLoad = [
                    "userId" => $id,
                    "amount" => $amount,
                    "accessToken" => $this->generateMoneyAccessToken(),
                ];

                $response = $this->withdrawThirdParty(WithdrawController::MPESA_PROVIDERID, $payLoad);
                if (!is_null($response)) {
                    if ($response->resultCode == 1) {

                        $sessiondata = $this->session->get('auth');
                        $sessiondata['balance'] = $sessiondata['balance'] - $amount;
                        $this->session->set('auth', $sessiondata);

                        $this->session->set('withdrawalAmount', $amount);

                        $userId = $sessiondata['id'];
                        $mobile = $sessiondata['mobile'];

                        $this->session->set("InitiatedWithdrawal_" . $userId, $mobile);

                        $this->view->disable();

                        $this->flashSession->success($this->flashSuccess("Withdrawal request initiated successfully"));
                        return $this->response->redirect('/');
                    } else {
                        $this->flashSession->error($this->flashSuccess("Withdrawal failed. Please try again later"));
                        $this->view->disable();
                        return $this->response->redirect('/withdraw');
                    }
                } else {
                    $this->flashSession->error($this->flashSuccess("Withdrawal failed. Please try again later"));
                    $this->view->disable();
                    return $this->response->redirect('/withdraw');
                }
            }
        } else {

            $this->view->disable();
            $this->response->redirect('login');
        }
    }

    public function fortynineAction()
    {

        $this->session->set('withdrawalAmt', 49);
        return $this->response->redirect('/withdraw');
    }

    public function ninetynineAction()
    {

        $this->session->set('withdrawalAmt', 99);
        return $this->response->redirect('/withdraw');
    }

    public function oneninetynineAction()
    {

        $this->session->set('withdrawalAmt', 199);
        return $this->response->redirect('/withdraw');
    }

    public function fourninetynineAction()
    {

        $this->session->set('withdrawalAmt', 499);
        return $this->response->redirect('/withdraw');
    }
}