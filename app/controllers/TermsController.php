<?php

class TermsController extends ControllerBase
{
    public function indexAction()
    {

        $selected = 'terms';
        $theBetslip = $this->session->get("betslip");
        $navigation = $this->getNavigation();

        $this->view->setVars([
            'theBetslip' => $theBetslip,
            'slipCount'  => !is_null($theBetslip) ? count($theBetslip) : 0,
            'topLeagues' => $navigation['topLeagues'],
            'countries' => $navigation['countries'],
            'sports' => $navigation['sports'],
            'selected' => $selected
        ]);

        $this->tag->setTitle('BetSafe - The leading sports Betting Website In Kenya');
    }
}
