<?php

/**
 * Class LoginController
 */
class LoginController extends ControllerBase
{

    private $navigation;

    public function initialize()
    {
        $this->tag->setTitle('Sign Up');
        $selectedSportId = $this->session->get('selectedSportId');
        $this->navigation = $this->getNavigation($selectedSportId);
    }

    /**
     *
     */
    public function indexAction()
    {
        $selected = 'login';
        $refURL = $this->request->getPost('ref') ?: '';
        $this->view->setVars([
            'topLeagues' => $this->navigation['topLeagues'],
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'ref' => $refURL,
            'selected' => $selected,
        ]);
    }

    public function refAction($ref)
    {
        $selected = 'login';
        $this->view->setVars([
            'topLeagues' => $this->navigation['topLeagues'],
            'ref' => str_replace("_", "/", $ref),
            'countries' => $this->navigation['countries'],
            'sports' => $this->navigation['sports'],
            'selected' => $selected,
        ]);

        $this->view->pick('login/index');
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function authenticateAction()
    {
        if ($this->request->isPost()) {
            $mobile = $this->request->getPost('mobile', 'int');
            $password = $this->request->getPost('password');
            $refURL = $this->request->getPost('ref') ?: '';

            if (!$mobile || !$password) {
                $this->flashSession->error($this->flashMessages('Please specify both your
                 mobile number and your password'));
                return $this->response->redirect($refU);
            }

            $mobile = $this->formatMobileNumber($mobile);

            if (!$mobile) {

                $this->flashSession->error($this->flashMessages('Invalid Mobile number! Please confirm and try again'));
                return $this->response->redirect($refU);
            }

            if ($mobile) {

                $payload = [
                    "Username" => $mobile,
                    "Password" => $password,
                    "ClientIP" => $_SERVER['REMOTE_ADDR'],
                    "ProviderID" => 0,
                    "VerifyReceivedFields" => true,
                    "APIAccount" => $this->apiAccount,
                    "APIPassword" => $this->qaApiPassword,
                    "IDBookmaker" => $this->bookmarkerID,
                ];

                $url = $this->qaBaseURL . "/api/users/LogOnUserV3";
                $response = $this->postToUrl($payload, $url);

                $resultCode = $response->ResultCode;

                if ($resultCode == 1) {

                    $token = "";
                    $tokenURL = $this->qaBaseURL . "/api/autologin/GetToken";

                    $tokenPayload = [
                        "UserID" => $response->UserInfo->UserID,
                        "APIAccount" => $this->apiAccount,
                        "APIPassword" => $this->qaApiPassword,
                        "IDBookmaker" => $this->bookmarkerID,
                    ];

                    $tokenResponse = $this->postToUrl($tokenPayload, $tokenURL);
                    if ($tokenResponse->ResultCode == 1) {
                        $token = $tokenResponse->Token;
                    }

                    $device = $this->getDevice();
                    $userBalance = $response->Balance->Amount;

                    if (is_null($userBalance)) {
                        $userBalance = 0.00;
                    }

                    $phone = $response->UserInfo->Phone;

                    if (substr($phone, 0, 3) == "254") {
                        $phone = substr($phone, 3, strlen($phone) - 3);
                    }

                    $sessionData = [
                        'id' => $response->UserInfo->UserID,
                        'sessionId' => $response->SessionID,
                        'balance' => number_format($userBalance, 2),
                        'username' => $response->UserInfo->Username,
                        'phone' => $phone,
                        'bonus' => $response->Balance->BonusAmount,
                        'token' => $token,
                        'mobile' => $mobile,
                        'device' => $device,
                    ];

                    $this->session->set("LoggedInUser_" . $mobile, $mobile);

                    if (!$refURL && $this->session->get("referred_by")) {

                        $refURL = $this->session->get("referred_by");
                    }
                    $this->registerAuth($sessionData);
                    return $this->response->redirect($refURL);
                } elseif ($resultCode == -4 || $resultCode == -6 || $resultCode == -22) {
                    $this->flashSession->error("Invalid username or password. Please try again.");
                    return $this->response->redirect('/login');
                } elseif ($resultCode == -21) {
                    $this->flashSession->error("Your user account is disabled. Please contact the Support Team.");
                    return $this->response->redirect('/login');
                } else {
                    $this->flashSession->error("Unable to log you in.");
                    return $this->response->redirect('/login');
                }
            }
        }
    }

    public function autologinAction()
    {

        $sessionData = [
            'id' => 846128,
            'sessionId' => "001",
            'balance' => number_format(500, 2),
            'username' => "254726498973",
            'phone' => "726498973",
            'bonus' => 100.0,
            'mobile' => "254726498973",
            'device' => "",
        ];

        $this->registerAuth($sessionData);
        $refURL = "/";

        if ($this->session->get("referred_by")) {
            $refURL = $this->session->get("referred_by");

        }

        return $this->response->redirect($refURL);
    }
}
